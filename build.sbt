name := "az-prime"

fork in run := true

// set the initial commands when entering 'console' or 'console-quick', but not 'console-project'
initialCommands in console := "import uk.ac.cam.cl._"

javacOptions += "-g"

javaOptions in run += "-Xms1024m"

javaOptions in run += "-Xmx4g"

javaOptions in run += "-ea"

version := "0.9-SNAPSHOT"

organization := "uk.ac.cam.cl"

scalaVersion := "2.10.4"

resolvers ++= Seq(
  //"Sonatype Nexus releases" at "https://oss.sonatype.org/content/repositories/releases",
	//"Sonatype Nexus snapshots" at "https://oss.sonatype.org/content/repositories/snapshots",
	"T repository" at "http://maven.twttr.com/",
	"AKKA repository" at "http://repo.akka.io/releases/",
  //"Typesafe repository" at "repo.typesafe.com/typesafe/releases/",
	"Central repository" at "http://repo1.maven.org/maven2")

libraryDependencies ++= Seq(
	"org.scalatest" %% "scalatest" % "1.9.2" % "test",
	"com.novocode" % "junit-interface" % "0.7" % "test->default")
	
libraryDependencies += "com.github.scala-incubator.io" %% "scala-io-file" % "0.4.3"

//libraryDependencies += "org.streum" %% "configrity-core" % "0.10.2"

//libraryDependencies += "com.nicta" %% "scoobi" % "0.4.0-SNAPSHOT"

libraryDependencies += "org.tempura" %% "common-utils" % "0.1-SNAPSHOT" changing()

libraryDependencies += "cc.mallet" % "mallet" % "2.0.7"

libraryDependencies += "de.bwaldvogel" % "liblinear" % "1.8"

libraryDependencies += "net.sf.trove4j" % "trove4j" % "3.0.3"

//libraryDependencies += "edu.stanford.nlp" % "stanford-corenlp" % "1.3.3" withSources() classifier "models"
libraryDependencies += "edu.stanford.nlp" % "stanford-corenlp" % "1.3.3"

libraryDependencies += "com.twitter" % "util-eval_2.10" % "6.3.4"

// disable using the Scala version in output paths and artifacts
crossPaths := false
