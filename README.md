# AZ Prime #

Rewrite of the original AZ perl code by Simone Teufel. 

## Setup ##

Download the [simple-build-tool](https://github.com/harrah/xsbt/wiki/), install it, and then `compile`/`test` from the SBT console, launched from the project root (run `sbt` from the commandline). You can also run `sbt test` and `sbt compile` from the commandline just to run that one task.

### Dependencies ###

The project requires one dependency to be installed locally. You will need to also checkout the [tempura-utils project](http://bitbucket.org/dainkaplan/tempura-utils), and run `sbt publish-local` to push it to the local ivy2 repository on your machine. After doing this, you can run any of the sbt commands for this project and they will complete successfully. 

### Eclipse ###

Don't. Use IntelliJ IDEA instead.

### Intellij IDEA ###

Importing the project works as expected; it will scan the build.sbt file accordingly.

## Unit Tests ##

The project has a battery of unit tests that should all pass. Run `sbt test` to run them from the commandline. This is also a good place to start to see how various components work.

## Project Structure ##

The main entry point for the project is currently `AZGenerateTrainingData`, located in `uk.ac.cam.cl.azprime.AZPrime`. A series of data files (patterns, verbs and their classes, etc.) are located in `src/main/resources`. These are in turn loaded by the classes in `uk.ac.cam.cl.azprime.resources`, specifically by instantiating a new `Resources` (`new Resources(config)`, where `config` is an instance of `Configuration`, which can be loaded dynamically at runtime). the class `AZPrime` instantiates all feature extractors and their dependencies (see TODO), and then runs the `FeatureEngine` over the loaded data to produce a vector of features for each sentence. The data is currently loaded from `data/` but this is specified in the configuration file (`cfg/config.scala`) which is loaded at runtime.

Note that most classes are designed to be immutable once created. The feature extraction is also done in parallel using Scala's parallel collections.

### "Resources" ###

The various classes for resources have backing data files (see above). Patterns is akin (but different) to regular expressions, with the goal of matching an input sequence. Agents use a subset of the patterns data (any pattern class that ends with "_A"). Actions also use the pattern syntax, but have their own data file. All of these rely on the `Expression` class, that does the matching of a a series of pattern tokens (the pattern) against input (a string such as a sentence). Headers are for classifying sections of an article (paper), and lexicon provides classes for certain words (with very crude lemmatisation). 

### Semantic Verbs ###

A large portion of the project is the semantic verb extraction. See the `uk.ac.uk.cam.azprime.verbs` package for details (warning: it's rather complex).

## Adding new feature extractors ##

`uk.ac.cam.cl.azprime.ml/features` contains all the feature extractors for the project. To add a new feature, simply copy one of these classes and make the necessary modifications (`SentenceContent.scala` is a good example of a simple feature, which just outputs the string value of the sentence as a feature). Each class has basically an extractor class, and a feature value class that the extractor returns when it runs. There are two types of feature extractors, a `SingleFeatureExtractor` which returns a single feature value instance, and a `MultipleFeatureExtractor`, which returns a `List` of many (see `SemanticVerbs.scala` for an example). Feature extractors can also declare dependencies to other extractors if they require the output of one extractor in order to continue processing. To do this you add a `dependsOn[FeatureExtractorClass]` into the body of the feature extractor (see `SemanticVerbs.scala`). Declaring the dependency does not reorder the feature extractors to satisfy the constraints, but it will cause an error to be thrown if they are declared in an order that does not allow extractors to run in the appropriate order (the list of feature extractor instances is passed in to the `FeatureEngine`).

### Outputting training data ###

Refer to the `ArffFileWriter` class in `uk.ac.cam.cl.weka` for an example. It does basic nominalization of values.

### Predicting AZ Labels on new documents ###

There are two classes of interest for this:

* `uk.ac.cam.cl.azprime.system.AZClassifier`
* `uk.ac.cam.cl.azprime.system.AZPredict`

If you are doing prediction in code, use `AZClassifier`. It can be used like this:

    val c = new uk.ac.cam.cl.azprime.system.AZClassifier()
    val labels = c.classify(document)

Where `document` is a loaded SciXml document.

If you want to process a directory of documents from the command line, use `AZPredict`. Call like it like this:

    azprime$ sbt 'run-main uk.ac.cam.cl.azprime.system.AZPredict --in data/cl --out tmp --in-suffix pos.xml --out-suffix pred.txt'

There are several useful arguments:

* `--output-scixml` true|false default false: If set, predictions will be embedded within each sci-xml document.
* `--output-pos` true|false default true: If set, part of speech tags will be included in sci-xml document.
* `--overwrite` true|false default false: If not set, any existing prediction files for sci-xml documents will not be overwritten.
* `--out` default to "in": Location to output predictions to.
* `--out-suffix` default "predictions.txt"
* `--in`: Location for input documents
* `--in-suffix` default "pos.xml"

Sample output:

    [info] Running uk.ac.cam.cl.azprime.system.AZPredict --in data/cl --out tmp --in-suffix pos.xml --out-suffix pred.txt
    [info] in-suffix	pos.xml
    [info] out-suffix	pred.txt
    [info] in	data/cl
    [info] out	tmp
    [info] Reading model from /Users/dain/Projects/azprime/target/classes/models
    [info] Read 520834 features
    [info] Reading data from data/cl
    [info] Reading from data/cl/9405001.pos.xml, predicting and writing to tmp/9405001.pred.txt
    [info] Reading from data/cl/9405002.pos.xml, predicting and writing to tmp/9405002.pred.txt
    [info] Reading from data/cl/9405004.pos.xml, predicting and writing to tmp/9405004.pred.txt
    [info] Reading from data/cl/9405010.pos.xml, predicting and writing to tmp/9405010.pred.txt
    [info] Reading from data/cl/9405013.pos.xml, predicting and writing to tmp/9405013.pred.txt
    [info] Reading from data/cl/9405022.pos.xml, predicting and writing to tmp/9405022.pred.txt

## TODO ##

* Load feature extractors for the extractor engine via what is specified in the configuration file.
* Currently system expects POS-tagged sci-xml files (with individual words tagged with <W> and the part-of-speech). Instead, expect untagged files, tag them, and generate cached files with the tags that will be used in subsequent runs if found. The POS labels are the PENN ones. This cannot be changed easily, so use 
* Play with 2-step classification? (BAS+CTR+OTH vs TXT+AIM+OWN vs. BKG) and then within each.
* Look into WAPITI for CRf/MEMM classification.