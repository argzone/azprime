<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE PAPER SYSTEM "paper-structure-annotation.dtd">
<PAPER><METADATA><FILENO>b311492a</FILENO>10.1039/b311492aarticle<JOURNAL>CC<YEAR>2004</YEAR><ISSUE>2</ISSUE>218-219</JOURNAL></METADATA><CURRENT_TITLE>The first ligand-modulated oxidative Heck vinylation. Efficient catalysis with molecular oxygen as palladium(0) oxidant</CURRENT_TITLE><CURRENT_AUTHORLIST><CURRENT_AUTHOR ID="1">Murugaiah M. S.<CURRENT_SURNAME>Andappan</CURRENT_SURNAME>   Box 574, Organic Pharmaceutical Chemistry  Department of Medicinal Chemistry  BMC    Uppsala SE-75123 Sweden mats@orgfarm.uu.se 46 18 4714474  46 18 4714667 </CURRENT_AUTHOR><CURRENT_AUTHOR ID="2">Peter<CURRENT_SURNAME>Nilsson</CURRENT_SURNAME>   Box 574, Organic Pharmaceutical Chemistry  Department of Medicinal Chemistry  BMC    Uppsala SE-75123 Sweden mats@orgfarm.uu.se 46 18 4714474  46 18 4714667 </CURRENT_AUTHOR><CURRENT_AUTHOR ID="3">Mats<CURRENT_SURNAME>Larhed</CURRENT_SURNAME>   Box 574, Organic Pharmaceutical Chemistry  Department of Medicinal Chemistry  BMC    Uppsala SE-75123 Sweden mats@orgfarm.uu.se 46 18 4714474  46 18 4714667 </CURRENT_AUTHOR></CURRENT_AUTHORLIST><ABSTRACT><A-S AZ="AIM" ID="s1">The discovery of the first ligand-supported Pd(ii) catalysed oxidative Heck reaction with molecular oxygen as reoxidant and the scope with diverse arylboronic acids and olefins using only 1–2percent of catalyst are reported.</A-S></ABSTRACT><BODY>
		<DIV DEPTH="1"><HEADER/>
			<P><S AZ="OTH_GOOD" ID="s2">The transition metal (Pd, Rh, Ru and Ir)<REF ID="R-0" REFID="cit1"/> catalysed cross-coupling of organoboronic acids and olefins is rapidly growing as a versatile synthetic methodology. </S><S AZ="OTH_GOOD" ID="s3">The multifarious advantages of arylboronic acid over other arylpalladium precursors (triflates, halides, etc.) include 1) the availability of vast numbers of arylboronic acids, 2) the stability to air and moisture, 3) the low toxicity and 4) the easy removal of boron-derived byproducts unlike that of other organometalloids.<REF ID="R-1" REFID="cit2"/> </S><S AZ="OTHR" ID="s4">The Pd(ii)-mediated vinylic substitution of organoboronic acids, known as the oxidative Heck reaction, was first reported by Heck<REF ID="R-2" REFID="cit1"/> with stoichiometric amounts of Pd(ii) more than two decades ago. </S><S AZ="OTHR" Comment="not really a weakness" ID="s5">However, this reaction has been developed very little, barring a few recent reports. </S><S AZ="OTHR" ID="s6">The first catalytic protocol was reported by Cho et al. using AcOH as solvent<REF ID="R-3" REFID="cit3a"/> followed by a mild method by Du et al. using DMF as solvent with Cu(OAc)2 as reoxidant.<REF ID="R-4" REFID="cit3b"/> </S><S AZ="PREV_OWN" ID="s7">We recently developed a microwave-accelerated procedure for the fast generation of oxidative Heck products,<REF ID="R-5" REFID="cit3c"/> and Jung et al. reported the utility of molecular oxygen as an efficient reoxidant of Pd(0), though with relatively high amount of Pd(ii)
				(10 molpercent).<REF ID="R-6" REFID="cit3d"/></S></P>
			<P><S AZ="GAP_WEAK" ID="s8">The complete absence of ligand-supported oxidative Heck protocols limits the development of selective and efficient applications in organic synthesis. </S><S AZ="OWN_MTHD" Comment="speculation over possibly advantageous method" ID="s9">The presence of ligands might not only invoke the stability of the catalyst by preventing the aggregation of Pd(0), prompting smaller amounts of catalyst, but also aid in control over the regio- and stereoselectivity with electron-rich and prochiral olefins respectively. </S><S AZ="OWN_MTHD" Comment="or cogro?" ID="s10">In particular, the nitrogen ligands are known to facilitate the efficient reoxidation of Pd(0) by molecular oxygen.<REF ID="R-7" REFID="cit4"/> </S><S AZ="OWN_MTHD" Comment="or cogro?" ID="s11">In general, nitrogen-based ligands are very cheap, air and moisture stable compared to their phosphine counterparts. </S><S AZ="OWN_MTHD" ID="s12">To start with, we undertook studies on the viability of ligands in the oxidative Heck reaction with more-common electron-poor olefins, which are reported herein.</S></P>
			<P><S AZ="OWN_MTHD" ID="s13">A set of 19 optimisation experiments were performed to discover proper conditions in terms of Pd(ii) source, solvent, ligand, base, temperature and time with PhB(OH)2
				(1a) and n-butyl acrylate (2a) as model reactants (Table 1). </S><S AZ="OWN_MTHD" ID="s14">Molecular oxygen was used throughout as reoxidant for catalyst regeneration because of its eco-friendly advantages. </S><S AZ="OWN_MTHD" ID="s15">The oxidatively stable phenanthroline-class ligand, 2,9-dimethyl-1,10-phenanthroline (dmphen), known to provide high yields in internal arylation with aryl triflates,<REF ID="R-8" REFID="cit5"/> performed generally well. </S><S AZ="OWN_FAIL" Comment="just a control rather than a failure?" ID="s16">In contrast, the exclusion of the ligand did not afford any product (entry 18). </S><S AZ="OWN_FAIL" ID="s17">Bipyridyl and monodentate pyridine<REF ID="R-9" REFID="cit6"/> were ineffective (entries 14, 15). </S><S AZ="OWN_FAIL" ID="s18">The phosphines, Ph3P and BINAP, which are susceptible to oxidation,<REF ID="R-10" REFID="cit7"/> gave moderate yields (entries 12, 13). </S><S AZ="OWN_FAIL" Comment="reason for failure - O_Conc?" ID="s19">The inferior yields with phosphine ligands were due to the poor percentage of conversion and also the concurrent formation of biphenyl through the homocoupling of PhB(OH)2, which was negligible with dmphen ligand. </S><S AZ="OWN_RES" ID="s20">The nitrile solvents, MeCN and EtCN, gave the highest yields (entries 8–11). </S><S AZ="OWN_FAIL" ID="s21">The polar, aprotic solvent DMSO afforded only poor to moderate yields, even after prolonged time (entries 4–6). </S><S AZ="OWN_FAIL" ID="s22">Toluene gave a slightly depressed yield (entry 7). </S><S AZ="OWN_RES" ID="s23">N-Methylmorpholine (NMM) was identified as an effective base and 2 equiv. proved to be optimal. </S><S AZ="OWN_FAIL" Comment="fail overrides res?" ID="s24">Et3N was also effective (entry 16), but the inorganic base LiOAc gave a poor result. </S><S AZ="OWN_RES" ID="s25">While the acid 1a readily coupled to the olefin, the corresponding boronic ester was surprisingly inert under the reaction conditions.</S></P>
			
			<P><S AZ="OWN_MTHD" ID="s26">The immediate impact of the dmphen ligand-support was that the reaction could be performed with 1 molpercent of Pd(ii), down from 10 molpercent in the absence of the ligand.<REF ID="R-11" REFID="cit3d"/> </S><S AZ="OWN_MTHD" ID="s27">Having identified an efficient catalytic system, we then investigated the scope of arylboronic and olefinic substitution. </S><S AZ="OWN_RES" ID="s28">The results from preparative coupling of olefins (2a–d) with a diverse set of boronic acids, bearing sensitive nitro, keto, bromo, and iodo functionalities (1a–m), are summarised in Table 2. </S><S AZ="OWN_MTHD" ID="s29">The standard conditions identified from Table 1
				(entry 11) were employed, even though individual cases were optimised for high yield, with regard to Pd(OAc)2 amount (1 or 2 molpercent), solvent and temperature. </S><S AZ="OWN_RES" Comment="expert knowledge &quot;regioselective&quot;?" ID="s30">The reactions were diastereo- and regioselective, except that of the styrene derivative (last entry).</S></P>
			
			<P><S AZ="OWN_RES" ID="s31">The general trend was that the electron-rich arylboronic acids were the superior substrates affording high yields without any byproduct. </S><S AZ="OWN_FAIL" Comment="or res, dependent on override" ID="s32">The m-substituted electron-poor arylboronic acids (1e–f, 1h–j) afforded moderate to good yields, while those with p-substitution were inert towards vinylation and only furnished homocoupling products in small amounts (e.g.
				1g). </S><S AZ="OWN_RES" ID="s33">The fluoride salt additives like CsF and TBAF, which are known to generate highly-nucleophilic “ate” complexes with ArB(OH)2, made no difference in the latter case.<REF ID="R-12" REFID="cit9"/> </S><S AZ="OWN_CONC" ID="s34">This large disparity in the reactivity might indicate that the insertion of electron-poor aryl groups in a chelation-controlled cationic Pd(ii)
				π-intermediate is a sluggish process. </S><S AZ="CODI" ID="s35">This is in contrast to ligand-free oxidative Heck conditions, where the π-complex might be neutral.<REF ID="R-13" REFID="cit11"/> </S><S AZ="OWN_RES" Comment="don't want to judge if this is good or bad" ID="s36">The addition of LiBr decelerated the reaction rate with non-functionalised 1a, which supports the role of the cationic pathway (entry 19, Table 1). </S><S AZ="OWN_CONC" ID="s37">The formation of the branched isomer (17percent) in the last entry (Table 2) may also be interpreted based on the cationic mechanism. </S><S AZ="OWN_RES" ID="s38">The sterically-hindered boronic acids, 1k–l, gave moderate to good yields. </S><S AZ="OWN_RES" ID="s39">The reactions with 1h-j are highly chemoselective as no competitive Pd(0)-catalysed Heck vinylation and Suzuki coupling were noticed.</S></P>
			<P><S AZ="AIM" ID="s40">In conclusion, ligand modulated oxidative Heck chemistry was developed with 1) arylboronic acids as latent aryl halides, 2) low catalyst loading with an inexpensive ligand and 3) molecular oxygen as a “green reoxidant”. </S><S AZ="NOV_ADV" ID="s41">This protocol should serve as the foundation for the future development of regio- and stereocontrolled chemistry. </S><S AZ="FUT" ID="s42">Our further efforts are directed towards the above goal, and overcoming limitations pertaining to the eletron-poor arylboronic acids.</S></P>
			<P><S ID="s43">The authors thank Prof. Anders Hallberg for valuable suggestions and the Swedish Research Council for financial support.</S></P>
		</DIV>
	</BODY>
		<REFERENCELIST><REFERENCE ID="cit1"><AUTHORLIST><AUTHOR>H. A.<SURNAME>Dieck</SURNAME></AUTHOR><AUTHOR>R. F.<SURNAME>Heck</SURNAME></AUTHOR></AUTHORLIST><TITLE/><JOURNAL>J. Org. Chem.<YEAR>1975</YEAR>401083</JOURNAL></REFERENCE><REFERENCE ID="cit1"><AUTHORLIST><AUTHOR>K.<SURNAME>Fagnou</SURNAME></AUTHOR><AUTHOR>M.<SURNAME>Lautens</SURNAME></AUTHOR></AUTHORLIST><TITLE/><JOURNAL>Chem. Rev.<YEAR>2003</YEAR>103169</JOURNAL></REFERENCE><REFERENCE ID="cit1"><AUTHORLIST><AUTHOR>E. J.<SURNAME>Farrington</SURNAME></AUTHOR><AUTHOR>J. M.<SURNAME>Brown</SURNAME></AUTHOR><AUTHOR>C. F. J.<SURNAME>Barnard</SURNAME></AUTHOR><AUTHOR>E.<SURNAME>Roswell</SURNAME></AUTHOR></AUTHORLIST><TITLE/><JOURNAL>Angew. Chem., Int. Ed.<YEAR>2002</YEAR>41169</JOURNAL></REFERENCE><REFERENCE ID="cit1"><AUTHORLIST><AUTHOR>T.<SURNAME>Koike</SURNAME></AUTHOR><AUTHOR>X.<SURNAME>Du</SURNAME></AUTHOR><AUTHOR>T.<SURNAME>Sanada</SURNAME></AUTHOR><AUTHOR>Y.<SURNAME>Danda</SURNAME></AUTHOR><AUTHOR>A.<SURNAME>Mori</SURNAME></AUTHOR></AUTHORLIST><TITLE/><JOURNAL>Angew. Chem., Int. Ed.<YEAR>2003</YEAR>4289</JOURNAL></REFERENCE><REFERENCE ID="cit2"><AUTHORLIST><AUTHOR>S.<SURNAME>Kotha</SURNAME></AUTHOR><AUTHOR>K.<SURNAME>Lahiri</SURNAME></AUTHOR><AUTHOR>D.<SURNAME>Kashinath</SURNAME></AUTHOR></AUTHORLIST><TITLE/><JOURNAL>Tetrahedron<YEAR>2002</YEAR>589633</JOURNAL></REFERENCE><REFERENCE ID="cit3a"><AUTHORLIST><AUTHOR>C. S.<SURNAME>Cho</SURNAME></AUTHOR><AUTHOR>S.<SURNAME>Uemura</SURNAME></AUTHOR></AUTHORLIST><TITLE/><JOURNAL>J. Organomet. Chem.<YEAR>1994</YEAR>46585</JOURNAL></REFERENCE><REFERENCE ID="cit3b"><AUTHORLIST><AUTHOR>X.<SURNAME>Du</SURNAME></AUTHOR><AUTHOR>M.<SURNAME>Suguro</SURNAME></AUTHOR><AUTHOR>K.<SURNAME>Hirabayashi</SURNAME></AUTHOR><AUTHOR>A.<SURNAME>Mori</SURNAME></AUTHOR><AUTHOR>T.<SURNAME>Nishikata</SURNAME></AUTHOR><AUTHOR>N.<SURNAME>Hagiwara</SURNAME></AUTHOR><AUTHOR>K.<SURNAME>Kawata</SURNAME></AUTHOR><AUTHOR>T.<SURNAME>Okeda</SURNAME></AUTHOR><AUTHOR>H. F.<SURNAME>Wang</SURNAME></AUTHOR><AUTHOR>K.<SURNAME>Fugami</SURNAME></AUTHOR><AUTHOR>M.<SURNAME>Kosugi</SURNAME></AUTHOR></AUTHORLIST><TITLE/><JOURNAL>Org. Lett.<YEAR>2001</YEAR>33313</JOURNAL></REFERENCE><REFERENCE ID="cit3c"><AUTHORLIST><AUTHOR>A. M. S.<SURNAME>Murugaiah</SURNAME></AUTHOR><AUTHOR>P.<SURNAME>Nilsson</SURNAME></AUTHOR><AUTHOR>M.<SURNAME>Larhed</SURNAME></AUTHOR></AUTHORLIST><TITLE/><JOURNAL>Mol. Div.<YEAR>2003</YEAR>in press</JOURNAL></REFERENCE><REFERENCE ID="cit3d"><AUTHORLIST><AUTHOR>Y. C.<SURNAME>Jung</SURNAME></AUTHOR><AUTHOR>R. K.<SURNAME>Mishra</SURNAME></AUTHOR><AUTHOR>C. H.<SURNAME>Yoon</SURNAME></AUTHOR><AUTHOR>K. W.<SURNAME>Jung</SURNAME></AUTHOR></AUTHORLIST><TITLE/><JOURNAL>Org. Lett.<YEAR>2003</YEAR>52231</JOURNAL></REFERENCE><REFERENCE ID="cit4"><AUTHORLIST><AUTHOR>R. A.<SURNAME>Sheldon</SURNAME></AUTHOR><AUTHOR>I.<SURNAME>Arends</SURNAME></AUTHOR><AUTHOR>G. J.<SURNAME>Ten Brink</SURNAME></AUTHOR><AUTHOR>A.<SURNAME>Dijksman</SURNAME></AUTHOR></AUTHORLIST><TITLE/><JOURNAL>Acc. Chem. Res.<YEAR>2002</YEAR>35774</JOURNAL></REFERENCE><REFERENCE ID="cit4"><AUTHORLIST><AUTHOR>B. A.<SURNAME>Steinhoff</SURNAME></AUTHOR><AUTHOR>S. R.<SURNAME>Fix</SURNAME></AUTHOR><AUTHOR>S. S.<SURNAME>Stahl</SURNAME></AUTHOR></AUTHORLIST><TITLE/><JOURNAL>J. Am. Chem. Soc.<YEAR>2002</YEAR>124766</JOURNAL></REFERENCE><REFERENCE ID="cit5"><AUTHORLIST><AUTHOR>W.<SURNAME>Cabri</SURNAME></AUTHOR><AUTHOR>I.<SURNAME>Candiani</SURNAME></AUTHOR><AUTHOR>A.<SURNAME>Bedeschi</SURNAME></AUTHOR><AUTHOR>R.<SURNAME>Santi</SURNAME></AUTHOR></AUTHORLIST><TITLE/><JOURNAL>J. Org. Chem.<YEAR>1993</YEAR>587421</JOURNAL></REFERENCE><REFERENCE ID="cit6"><AUTHORLIST><AUTHOR>B. A.<SURNAME>Steinhoff</SURNAME></AUTHOR><AUTHOR>S. S.<SURNAME>Stahl</SURNAME></AUTHOR></AUTHORLIST><TITLE/><JOURNAL>Org. Lett.<YEAR>2002</YEAR>234179</JOURNAL></REFERENCE><REFERENCE ID="cit7"><AUTHORLIST><AUTHOR>C. W.<SURNAME>Lee</SURNAME></AUTHOR><AUTHOR>J. S.<SURNAME>Lee</SURNAME></AUTHOR><AUTHOR>N. S.<SURNAME>Cho</SURNAME></AUTHOR><AUTHOR>K. D.<SURNAME>Kim</SURNAME></AUTHOR><AUTHOR>S. M.<SURNAME>Lee</SURNAME></AUTHOR><AUTHOR>J. S.<SURNAME>Oh</SURNAME></AUTHOR></AUTHORLIST><TITLE/><JOURNAL>J. Mol. Catal.<YEAR>1993</YEAR>8031</JOURNAL></REFERENCE><REFERENCE ID="cit9"><AUTHORLIST><AUTHOR>N.<SURNAME>Miyaura</SURNAME></AUTHOR></AUTHORLIST><TITLE/><JOURNAL>J. Organomet. Chem.<YEAR>2002</YEAR>65354</JOURNAL></REFERENCE><REFERENCE ID="cit11"><AUTHORLIST><AUTHOR>M.<SURNAME>Larhed</SURNAME></AUTHOR><AUTHOR>A.<SURNAME>Hallberg</SURNAME></AUTHOR></AUTHORLIST>E. Negishi<TITLE>Handbook of Organopalladium Chemistry for Organic Synthesis</TITLE><DATE>2002</DATE></REFERENCE></REFERENCELIST>
		<FOOTNOTELIST><FOOTNOTE ID="fn1" MARKER="1">Electronic Supplementary Information (ESI) available: Experimental and analytical data. See http://www.rsc.org/suppdata/cc/b3/b311492a/</FOOTNOTE><FOOTNOTE ID="tab1fna" MARKER="2">1a
										(2.0 mmol), 2a
										(1.0 mmol), Pd(OAc)2
										(1 molpercent), ligand (L) and base (2.0 mmol) in MeCN (2.5 mL) were stirred under O2 atm.</FOOTNOTE><FOOTNOTE ID="tab1fnb" MARKER="3">Isolated yield with purity ≥95percent
										(GC-MS).</FOOTNOTE><FOOTNOTE ID="tab1fnc" MARKER="4">Pd(ii): Pd(phen)Cl2.</FOOTNOTE><FOOTNOTE ID="tab1fnd" MARKER="5">Pd(ii): Pd(MeCN)2Cl2.</FOOTNOTE><FOOTNOTE ID="tab1fne" MARKER="6">Additive: LiBr (5 equiv.).</FOOTNOTE><FOOTNOTE ID="tab2fna" MARKER="7">Condition A: ArB(OH)2
										(2.0 mmol), alkene (1.0 mmol), Pd(OAc)2
										(2.3 mg, 1 molpercent), dmphen (2.6 mg, 1.2 molpercent), and NMM (0.22 mL, 2.0 mmol) in MeCN (2.5 mL) were stirred under O2 atm at 50 °C, except as noted; B: L/Pd(ii): 2.4/2 molpercent.</FOOTNOTE><FOOTNOTE ID="tab2fnb" MARKER="8">2a: n-Butyl acrylate; 2b: styrene; 2c: 1-octen-3-one; 2d: 4-ethoxystyrene.</FOOTNOTE><FOOTNOTE ID="tab2fnc" MARKER="9">New compounds gave satisfactory 1H and 13C NMR, IR, and combustion analysis.</FOOTNOTE><FOOTNOTE ID="tab2fnd" MARKER="10">Isolated yield with purity ≥95percent
										(GC-MS or 1H NMR).</FOOTNOTE><FOOTNOTE ID="tab2fne" MARKER="11">L/Pd(ii): 6.0/5.0 molpercent.</FOOTNOTE><FOOTNOTE ID="tab2fnf" MARKER="12">Solvent: DMF, 100 °C.</FOOTNOTE><FOOTNOTE ID="tab2fng" MARKER="13">Solvent: EtCN, 80 °C.</FOOTNOTE><FOOTNOTE ID="tab2fnh" MARKER="14">β : α
										= 83 : 17.</FOOTNOTE><FOOTNOTE ID="cit1" MARKER="1">Pd: . Rh: . Ru: . Ir: </FOOTNOTE><FOOTNOTE ID="cit5" MARKER="2">. Arylation of electron-poor olefins with Pd(0)/dmphen was reported unsuccessful in this article</FOOTNOTE><FOOTNOTE ID="cit6" MARKER="3">Pyridine has been shown to accelerate a related oxidation by the “in situ” formation of Py2Pd(OAc)2: </FOOTNOTE><FOOTNOTE ID="cit8" MARKER="4">The following arylboronic acids with electronically deactivating groups at the p-position were investigated: CF3, Br, CHO, COCH3</FOOTNOTE><FOOTNOTE ID="cit10" MARKER="5">For example, under ligand free conditions [Pd(OAc)2/LiOAc/DMSO/O2], the product from the reaction of 1g was isolated in 80percent yield (unpublished result)</FOOTNOTE><FOOTNOTE ID="cit12" MARKER="6">This is in agreement with the results obtained in internal vinylation of aryl triflates with dmphen as ligand. See ref. <REF ID="R-14" REFID="cit5"/></FOOTNOTE><FOOTNOTE ID="cit13" MARKER="7">Preliminary investigation has established excellent internal regiocontrol in the arylation of electron-rich olefins</FOOTNOTE></FOOTNOTELIST><TABLELIST><TABLE ID="tab1"><TITLE>Ligand-based optimisation of oxidative Heck protocol</TITLE>
					
						
						
						
						
						
						
						
						
							
								Entry
								Solvent
								Ligand [L/Pd]
								Base
								T/°C
								t/h
								Yield
									(percent)
							
						
						
							
								
							
						
						
							
								1
								DMSO
								—
								LiOAc
								50
								24
								36
							
							
								2
								DMSO
								—
								LiOAc
								70
								24
								0
							
							
								3
								1,4-Dioxane
								dmphen [1.0]
								2,6-Lutidine
								70
								24
								0
							
							
								4
								DMSO
								dmphen [1.0]
								LiOAc
								70
								24
								27
							
							
								5
								DMSO
								dmphen [4.0]
								LiOAc
								70
								30
								25
							
							
								6
								DMSO
								dmphen [1.0]
								NMM
								50
								24
								69
							
							
								7
								Toluene
								dmphen [1.0]
								NMM
								80
								12
								78
							
							
								8
								EtCN
								dmphen [1.2]
								NMM
								70
								12
								95
							
							
								9
								EtCN
								dmphen [1.0]
								NMM
								50
								12
								97
							
							
								10
								MeCN
								dmphen [1.2]
								NMM
								50
								12
								97
							
							
								11
								MeCN
								dmphen [1.2]
								NMM
								50
								6
								96
							
							
								12
								MeCN
								Ph3P [2.5]
								NMM
								50
								24
								44
							
							
								13
								MeCN
								BINAP [1.2]
								NMM
								50
								24
								47
							
							
								14
								MeCN
								bipy [1.2]
								NMM
								50
								24
								0
							
							
								15
								MeCN
								pyridine [4.0]
								—
								50
								6
								0
							
							
								16
								MeCN
								dmphen [1.2]
								Et3N
								50
								12
								90
							
							
								17
								MeCN
								dmphen [1.2]
								LiOAc
								50
								12
								20
							
							
								18
								MeCN
								—
								NMM
								50
								12
								0
							
							
								19
								MeCN
								dmphen [1.2]
								NMM
								50
								12
								22
							
						
					
				</TABLE><TABLE ID="tab2"><TITLE>Vinylation of diverse arylboronic acids with olefins</TITLE>
					
						
						
						
						
						
						
						
							
								R–PhB(OH)2
								Olefin
								Product
								Cond.
								t/h
								Yield
									(percent)
							
						
						
							
								
							
						
						
							
								3,4-dioxy methylene 1b
								2a
								
								A
								6
								95
							
							
								4-methyl 1c
								2a
								
								A
								3
								73
							
							
								B
								6
								92
							
							
								4-methoxy 1d
								2a
								
								A
								6
								70
							
							
								3-nitro 1e
								2a
								
								B
								12
								40
							
							
								3-acetyl 1f
								2b
								
								A
								24
								77
							
							
								4-acetyl 1g
								2a
								
								A
								12
								0
							
							
								3-iodo 1h
								2a
								
								A
								12
								43
							
							
								12
								45
							
							
								3,5-dibromo 1i
								2a
								
								B
								12
								67
							
							
								3-bromo 1j
								2c
								
								B
								12
								73
							
							
								2,4,6-trimethyl 1k
								2a
								
								B
								12
								43
							
							
								2-methoxy 1l
								2a
								
								B
								6
								77
							
							
								1-naphthyl 1m
								2c
								
								B
								6
								77
							
							
								1a
								2d
								
								B
								12
								83
							
						
					
				</TABLE></TABLELIST></PAPER>