<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE PAPER SYSTEM "paper-structure-annotation.dtd">
<PAPER><METADATA><FILENO>b311804e</FILENO>10.1039/b311804earticle<JOURNAL>CC<YEAR>2004</YEAR><ISSUE>1</ISSUE>122-123</JOURNAL></METADATA><CURRENT_TITLE>A new synthesis of 1,3-dihydrobenzo[1,2]oxasiloles by a novel rearrangement of a pentavalent silicon intermediate</CURRENT_TITLE><CURRENT_AUTHORLIST><CURRENT_AUTHOR ID="1">George<CURRENT_SURNAME>Bashiardes</CURRENT_SURNAME>   Departement de Chimie  Université de Poitiers     SFA-UMR 6514  40 avenue du Recteur Pineau  86022 Poitiers cedex France george.bashiardes@univ-poitiers.fr</CURRENT_AUTHOR><CURRENT_AUTHOR ID="2">Vanessa<CURRENT_SURNAME>Chaussebourg</CURRENT_SURNAME>   Departement de Chimie  Université de Poitiers     SFA-UMR 6514  40 avenue du Recteur Pineau  86022 Poitiers cedex France george.bashiardes@univ-poitiers.fr</CURRENT_AUTHOR><CURRENT_AUTHOR ID="3">Géraldine<CURRENT_SURNAME>Laverdan</CURRENT_SURNAME>   Departement de Chimie  Université de Poitiers     SFA-UMR 6514  40 avenue du Recteur Pineau  86022 Poitiers cedex France george.bashiardes@univ-poitiers.fr</CURRENT_AUTHOR><CURRENT_AUTHOR ID="4">Jacques<CURRENT_SURNAME>Pornet</CURRENT_SURNAME>   Departement de Chimie  Université de Poitiers     SFA-UMR 6514  40 avenue du Recteur Pineau  86022 Poitiers cedex France george.bashiardes@univ-poitiers.fr</CURRENT_AUTHOR></CURRENT_AUTHORLIST><ABSTRACT><A-S AZ="AIM" ID="s1">A new synthesis of benzo[1,2]oxasiloles is described, wherein an unprecedented intramolecular allylic transposition takes place probably involving a pentavalent silicon intermediate.</A-S></ABSTRACT><BODY>
		<DIV DEPTH="1"><HEADER/>
			<P><S AZ="COGRO" ID="s2">Benzoxasiloles are interesting in their structure in that they contain the chemically labile O–Si and C–Si bonds, allowing for much derivatizing. </S><S AZ="OTHR" ID="s3">Few methods for the synthesis of substituted benzoxasiloles have been reported, and in each case the preparations concerned mostly specific examples.<REF ID="R-0" REFID="cit2"/><REF ID="R-1" REFID="cit3"/><REF ID="R-2" REFID="cit4"/><REF ID="R-3" REFID="cit5"/><REF ID="R-4" REFID="cit6"/><REF ID="R-5" REFID="cit7"/><REF ID="R-6" REFID="cit8"/></S></P>
			<P><S AZ="AIM" ID="s4">We report herein a new, general synthesis of benzo[1,2]oxasiloles 1 (Fig. 1) from easily accessible ortho-silylated benzaldehydes 7a–c, for which we also describe new preparations.</S></P>
			
			<P><S AZ="OWN_MTHD" ID="s5">The reaction sequence we employed is quite general, allowing possible substitution on the allylic group and the silicon atom. </S><S AZ="OWN_MTHD" ID="s6">Other benzaldehydes could also be employed to prepare new functionalized analogues. </S><S AZ="OWN_MTHD" ID="s7">In Scheme 1 is described the preparation of ortho-allylsilylbenzaldehydes by two different approaches. </S><S AZ="USE" ID="s8">In the first case benzaldehyde 2 is silylated with chlorosilanes 3a–c following orthometallation using N,N,N′-trimethylethylenediamine and n-butyllithium<REF ID="R-7" REFID="cit9"/>
				(method A). </S><S AZ="OWN_MTHD" ID="s9">In the second case, ortho-bromobenzaldehyde diethylacetal 4 undergoes a halogen–metal exchange using n-butyllithium, followed by silylation to give acetals 6. </S><S AZ="OWN_MTHD" ID="s10">Acid hydrolysis then furnishes the required ortho-substituted benzaldehydes 7
				(method B). </S><S AZ="OWN_RES" ID="s11">In both cases, the overall yields are good (Table 1), while the latter, two-step approach is generally higher yielding overall. </S><S AZ="OWN_MTHD" ID="s12">In addition, a ortho-vinylsilyl benzaldehyde 7d was also prepared by the above procedures.</S></P>
			
			
			<P><S AZ="OWN_MTHD" ID="s13">Silane derivatives 7a and 7b are then heated in toluene or xylene to effect the observed rearrangement into benzo[1,2]oxasiloles (Scheme 2). </S><S AZ="OWN_MTHD" ID="s14">Interestingly, in one case, during the preparation of required arylallylsilane 7c
				(entry 3), spontaneous rearrangement under the experimental conditions (THF, 20°C), provided 1c directly (see later). </S><S AZ="OWN_MTHD" ID="s15">As a test experiment for mechanistic elucidation, we also treated the vinylic analogue 7d in the same manner. </S><S AZ="OWN_RES" ID="s16">Indeed no reaction was observed and starting material was totally recovered, which concurs with the proposed mechanism.</S></P>
			
			<P><S AZ="OWN_RES" ID="s17">Following the thermal reaction, in the cases of 7a and 7b the required 3-allyl benzo[1,2]oxasiloles 1a
				(R1
				= H; R2
				= Me) and 1b
				(R1
				= R2
				= Me) were obtained in 96percent and 70percent yields, respectively (Table 2). </S><S AZ="OWN_RES" ID="s18">In the latter example, the remainder of the yield corresponds to recovered starting benzaldehyde 7b. </S><S AZ="OWN_RES" ID="s19">1c was obtained in 38percent overall yield, directly from benzaldehyde 2, without isolation of the corresponding ortho-allylsilyl benzaldehyde 7c.</S></P>
			
			<P><S AZ="AIM" Comment="I am confused. Were'nt schemes 1 and 2 theirs too?" ID="s20">We propose a mechanism (Scheme 3) for the observed transformation by which an intermediate pentavalent silicon is formed,<REF ID="R-8" REFID="cit1"/><REF ID="R-9" REFID="cit7"/> followed by a “nucleophilic” intramolecular allylic transposition.</S></P>
			
			<P><S AZ="OWN_CONC" ID="s21">From the observed results, it appears that the groups present on the silicon atom can influence the yield and the ease of reaction. </S><S AZ="OWN_RES" ID="s22">2b containing a Z crotyl group gave a single product albeit in lower yields certainly due to the added steric bulk of the methyl substituent on the attacking allyl moiety. </S><S AZ="OWN_CONC" ID="s23">It is proposed that pentavalent silicon adopts a trigonal bipyramidal configuration<REF ID="R-10" REFID="cit1"/><REF ID="R-11" REFID="cit7"/>
				(Fig. 2) in which the oxygen atom occupies an axial position and that the allylic group must have an equatorial arrangement in order to transpose to the cationic benzylic methylene. </S><S AZ="OWN_CONC" ID="s24">A pseudo-equilibrium (rearrangement) would assure that such a relative configuration can occur. </S><S AZ="OWN_CONC" ID="s25">The ease of reaction observed for the triallyl analogue would suggest that such a sequence is plausible, since at all times one allyl group is at an equatorial position. </S><S AZ="OWN_CONC" ID="s26">Furthermore, formation of 1b as the only product confirms the concerted intramolecular character of the allylic transposition.</S></P>
			
			<P><S AZ="CODI" Comment="could also be gap, or NOVADV" ID="s27">No precedent for this type of allylic rearrangement has been described to our knowledge, while it has been reported<REF ID="R-12" REFID="cit2"/> that pentavalent silicon derivatives obtained from benzyl alcoholates do give rise to allyl, benzyl or trimethysilyl anions which can be captured by electrophiles such as benzaldehyde or benzophenone.</S></P>
			<P><S AZ="OWN_MTHD" ID="s28">In order to explore the reactivity and usefulness of the compounds prepared above, benzo[1,2]oxasiloles 1a–c were transformed to 8a,b, 9a,b and 10a–c
				(Scheme 4). </S><S AZ="OWN_RES" ID="s29">Indeed homoallylic alcohols 8a, where desilylation was achieved, were obtained by treatment with tetrabutylammonium fluoride (TBAF). </S><S AZ="OWN_RES" ID="s30">Fluoride ion (KF) and hydrogen peroxide (H2O2) conditions<REF ID="R-13" REFID="cit12"/> provided desilylated ortho-phenolic benzyl alcohols 9a,b. </S><S AZ="OWN_RES" ID="s31">Treatment of 1a–c with methyllithium<REF ID="R-14" REFID="cit13"/>
				(MeLi) resulted in nucleophilic methylation on silicon and opening of the Si–O bond to provide the silicon-containing homoallylic benzylic alcohols 10a–c.</S></P>
			
			<P><S AZ="OWN_RES" ID="s32">In all cases, the yields of the reactions were satisfactory and the compounds were obtained pure by flash column chromatography.</S></P>
			<P><S AZ="AIM" ID="s33">In conclusion, we describe a novel rearrangement of a pentavalent silicon intermediate generated from ortho-allylsilyl benzaldehydes to provide 1,2-benzo[1,2]oxasiloles. </S><S AZ="NOV_ADV" ID="s34">The method accommodates substitution on the allylic moiety and proceeds in synthetically useful to excellent yields. </S><S AZ="OWN_MTHD" Comment="fut?" ID="s35">The benzo[1,2]oxasiloles thus obtained can be further derivatized by various methods. </S><S AZ="AIM" ID="s36">A general method is also described for the preparation of the ortho-silyl benzaldehydes.</S></P>
		</DIV>
	</BODY>
		<REFERENCELIST><REFERENCE ID="cit1"><AUTHORLIST><AUTHOR>Y. M.<SURNAME>Hijji</SURNAME></AUTHOR><AUTHOR>P. F.<SURNAME>Hudrlik</SURNAME></AUTHOR><AUTHOR>A. M.<SURNAME>Hudrlik</SURNAME></AUTHOR></AUTHORLIST><TITLE/><JOURNAL>Chem. Commun.<YEAR>1998</YEAR>1213-1214</JOURNAL></REFERENCE><REFERENCE ID="cit2"><AUTHORLIST><AUTHOR>P. F.<SURNAME>Hudrlik</SURNAME></AUTHOR><AUTHOR>J. O.<SURNAME>Arango</SURNAME></AUTHOR><AUTHOR>Y. M.<SURNAME>Hijji</SURNAME></AUTHOR><AUTHOR>C. O.<SURNAME>Okoro</SURNAME></AUTHOR><AUTHOR>A. M.<SURNAME>Hudrlik</SURNAME></AUTHOR></AUTHORLIST><TITLE/><JOURNAL>Can. J. Chem.<YEAR>2000</YEAR>781421-1427</JOURNAL></REFERENCE><REFERENCE ID="cit3"><AUTHORLIST><AUTHOR>S. McN.<SURNAME>Sieburth</SURNAME></AUTHOR><AUTHOR>L.<SURNAME>Fensterbank</SURNAME></AUTHOR></AUTHORLIST><TITLE/><JOURNAL>J. Org. Chem.<YEAR>1992</YEAR>575279-5281</JOURNAL></REFERENCE><REFERENCE ID="cit4"><AUTHORLIST><AUTHOR>W.<SURNAME>Ando</SURNAME></AUTHOR><AUTHOR>A.<SURNAME>Sekiguchi</SURNAME></AUTHOR></AUTHORLIST><TITLE/><JOURNAL>J. Organomet. Chem.<YEAR>1977</YEAR>133219-230</JOURNAL></REFERENCE><REFERENCE ID="cit5"><AUTHORLIST><AUTHOR>W.<SURNAME>Ando</SURNAME></AUTHOR><AUTHOR>M.<SURNAME>Ikeno</SURNAME></AUTHOR><AUTHOR>A.<SURNAME>Sekiguchi</SURNAME></AUTHOR></AUTHORLIST><TITLE/><JOURNAL>J. Am. Chem. Soc.<YEAR>1977</YEAR>996447-6449</JOURNAL></REFERENCE><REFERENCE ID="cit6"><AUTHORLIST><AUTHOR>J.<SURNAME>Belzner</SURNAME></AUTHOR><AUTHOR>H.<SURNAME>Ihmels</SURNAME></AUTHOR><AUTHOR>L.<SURNAME>Pauletto</SURNAME></AUTHOR><AUTHOR>M.<SURNAME>Noltemeyer</SURNAME></AUTHOR></AUTHORLIST><TITLE/><JOURNAL>J. Org. Chem.<YEAR>1996</YEAR>613315-3319</JOURNAL></REFERENCE><REFERENCE ID="cit7"><AUTHORLIST><AUTHOR>Y.<SURNAME>Yamamoto</SURNAME></AUTHOR><AUTHOR>Y.<SURNAME>Takeda</SURNAME></AUTHOR><AUTHOR>K.<SURNAME>Akiba</SURNAME></AUTHOR></AUTHORLIST><TITLE/><JOURNAL>Tetrahedron Lett.<YEAR>1989</YEAR>30725-728</JOURNAL></REFERENCE><REFERENCE ID="cit8"><AUTHORLIST><AUTHOR>J. W.<SURNAME>Fitch, III</SURNAME></AUTHOR><AUTHOR>P. E.<SURNAME>Cassidy</SURNAME></AUTHOR><AUTHOR>M. J.<SURNAME>Ahmed</SURNAME></AUTHOR></AUTHORLIST><TITLE/><JOURNAL>J. Organomet. Chem.<YEAR>1996</YEAR>52255-57</JOURNAL></REFERENCE><REFERENCE ID="cit9"><AUTHORLIST><AUTHOR>D. L.<SURNAME>Comins</SURNAME></AUTHOR><AUTHOR>J. D.<SURNAME>Brown</SURNAME></AUTHOR></AUTHORLIST><TITLE/><JOURNAL>Tetrahedron Lett.<YEAR>1983</YEAR>245465-5468</JOURNAL></REFERENCE><REFERENCE ID="cit10"><AUTHORLIST><AUTHOR>A.<SURNAME>Furstner</SURNAME></AUTHOR><AUTHOR>D.<SURNAME>Voigtländer</SURNAME></AUTHOR></AUTHORLIST><TITLE/><JOURNAL>Synthesis<YEAR>2000</YEAR>959-969</JOURNAL></REFERENCE><REFERENCE ID="cit11"><AUTHORLIST><AUTHOR>R. A.<SURNAME>Gossage</SURNAME></AUTHOR><AUTHOR>E.<SURNAME>Munoz-Martinez</SURNAME></AUTHOR><AUTHOR>H.<SURNAME>Frey</SURNAME></AUTHOR><AUTHOR>A.<SURNAME>Burgath</SURNAME></AUTHOR><AUTHOR>M.<SURNAME>Lutz</SURNAME></AUTHOR><AUTHOR>A. L.<SURNAME>Spek</SURNAME></AUTHOR><AUTHOR>G.<SURNAME>van Koten</SURNAME></AUTHOR></AUTHORLIST><TITLE/><JOURNAL>Chem. Eur. J.<YEAR>1999</YEAR>52191-2197</JOURNAL></REFERENCE><REFERENCE ID="cit12"><AUTHORLIST><AUTHOR>K.<SURNAME>Tamao</SURNAME></AUTHOR><AUTHOR>T.<SURNAME>Kakui</SURNAME></AUTHOR><AUTHOR>M.<SURNAME>Akita</SURNAME></AUTHOR><AUTHOR>T.<SURNAME>Iwahara</SURNAME></AUTHOR><AUTHOR>R.<SURNAME>Kanatani</SURNAME></AUTHOR><AUTHOR>J.<SURNAME>Yoshida</SURNAME></AUTHOR><AUTHOR>M.<SURNAME>Kumada</SURNAME></AUTHOR></AUTHORLIST><TITLE/><JOURNAL>Tetrahedron<YEAR>1983</YEAR>39983-990</JOURNAL></REFERENCE><REFERENCE ID="cit13"><AUTHORLIST><AUTHOR>R. J. P.<SURNAME>Corriu</SURNAME></AUTHOR><AUTHOR>A.<SURNAME>Kpoton</SURNAME></AUTHOR><AUTHOR>J.<SURNAME>Barrau</SURNAME></AUTHOR><AUTHOR>J.<SURNAME>Satgé</SURNAME></AUTHOR></AUTHORLIST><TITLE/><JOURNAL>J. Organomet. Chem.<YEAR>1976</YEAR>11421-33</JOURNAL></REFERENCE></REFERENCELIST>
		<FOOTNOTELIST><FOOTNOTE ID="tab2fna" MARKER="1">Obtained by spontaneous transposition during the previous step.</FOOTNOTE><FOOTNOTE ID="cit10" MARKER="1">Synthesis of 3b: </FOOTNOTE><FOOTNOTE ID="cit11" MARKER="2">Synthesis of 3c: </FOOTNOTE><FOOTNOTE ID="cit14" MARKER="3">All compounds were verified pure by various spectral techniques including 1H and 13C NMR and gas chromatography</FOOTNOTE></FOOTNOTELIST><FIGURELIST><FIGURE ID="fig1" SEQ="figure" SRC="fig1"><TITLE>3-Allylbenzo[1,2]oxasiloles.</TITLE></FIGURE><FIGURE ID="fig2" SEQ="figure" SRC="fig2"><TITLE>Trigonal bipyramid pentavalent silicon.</TITLE></FIGURE><FIGURE ID="sch1" SEQ="scheme" SRC="sch1"><TITLE>Synthetic scheme for the preparation of 2-silyl benzaldehydes. i) LiNMeCH2CH2NMe2, THF, –78 °C, ii) nBuLi then iii) chlorosilane 3a–3c; iv) iPrOH, H2O, THF, TsOH.</TITLE></FIGURE><FIGURE ID="sch2" SEQ="scheme" SRC="sch2"><TITLE/></FIGURE><FIGURE ID="sch3" SEQ="scheme" SRC="sch3"><TITLE>Proposed pentavalent silicon mechanism.</TITLE></FIGURE><FIGURE ID="sch4" SEQ="scheme" SRC="sch4"><TITLE>Transformations of benzo[1,2]oxasiloles. i) TBAF, THF, ii) KF, H2O2
					(Tamao conditions), iii) MeLi, Et2O</TITLE></FIGURE></FIGURELIST><TABLELIST><TABLE ID="tab1"><TITLE>ortho-Allyl- and vinylsilyl benzaldehydes 7a–d</TITLE>
					
						
						
						
						
						
						
							
								Entry
								Chlorosilane<REF ID="R-15" REFID="cit10"/><REF ID="R-16" REFID="cit11"/>
								Compound
								Yield (method A) (percent)
								Yield (method B) (percent)
							
						
						
							
								1
								
								
								
								
								51
								87
							
							
								2
								
								
								
								
								71
								n/a
							
							
								3
								
								
								
								
								other product (1c)
								n/a
							
							
								4
								
								
								
								
								67
								87
							
						
					
				</TABLE><TABLE ID="tab2"><TITLE>Benzo[1,2]oxasiloles</TITLE>
					
						
						
						
						
						
						
							
								Entry
								Product
								Solvent, Temp./°C
								Time/h
								Yield (percent)
							
						
						
							
								
							
						
						
							
								1
								
								
								Toluene, 110
								14
								96
							
							
								2
								
								
								Xylene, 140
								24
								70
							
							
								3
								
								
								THF, 20
								4
								38
							
						
					
				</TABLE></TABLELIST></PAPER>