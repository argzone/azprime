<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE PAPER SYSTEM "paper-structure-annotation.dtd">
<PAPER><METADATA><FILENO>b314140c</FILENO>10.1039/b314140carticle<JOURNAL>CE<YEAR>2004</YEAR>6<ISSUE>2</ISSUE>1-4</JOURNAL></METADATA><CURRENT_TITLE>Guest-dependent photochromism of 3,3′-bis-(4-fluoro-phenyl)-3H,3′H- [2,2′]biindenylidene-1,1′-dione in its inclusion crystals</CURRENT_TITLE><CURRENT_AUTHORLIST><CURRENT_AUTHOR ID="1">Koichi<CURRENT_SURNAME>Tanaka</CURRENT_SURNAME>   Department of Applied Chemistry  Faculty of Engineering  Ehime University    Matsuyama Ehime 790-8577 Japan ktanaka@eng.ehime-u.ac.jp</CURRENT_AUTHOR><CURRENT_AUTHOR ID="2">Yohei<CURRENT_SURNAME>Yamamoto</CURRENT_SURNAME>   Department of Applied Chemistry  Faculty of Engineering  Ehime University    Matsuyama Ehime 790-8577 Japan ktanaka@eng.ehime-u.ac.jp</CURRENT_AUTHOR><CURRENT_AUTHOR ID="3">Mino R.<CURRENT_SURNAME>Caira</CURRENT_SURNAME>   Department of Chemistry  University of Cape Town    Rondebosch 7701South Africa </CURRENT_AUTHOR></CURRENT_AUTHORLIST><ABSTRACT><A-S AZ="AIM" ID="s1">Inclusion crystals of 3,3′-bis-(4-fluoro-phenyl)-3H,3′H- [2,2′]biindenylidene-1,1′-dione (1b) showed a reversible color change from yellow to red upon photoirradiation, although 1b itself did not show photochromic property in the solid state.</A-S></ABSTRACT><BODY>
		<DIV DEPTH="1"><HEADER/>
			<P><S AZ="COGRO" ID="s2a">Organic photochromic compounds have received much attention in recent years because of their potential applications such as information storage, electronic display systems, optical switching devices, ophthalmic glasses, etc.<REF ID="R-0" REFID="cit1"/> </S><S AZ="COGRO" ID="s2b">Several types of organic photochromic compounds such as naphthopyrans, spiropyrans, fulgides, N-salicylideneanilines and diarylethenes have been discovered and their properties investigated.<REF ID="R-1" REFID="cit2a"/><REF ID="R-2" REFID="cit2b"/><REF ID="R-3" REFID="cit2c"/><REF ID="R-4" REFID="cit2d"/><REF ID="R-5" REFID="cit2e"/><REF ID="R-6" REFID="cit2f"/><REF ID="R-7" REFID="cit2g"/><REF ID="R-8" REFID="cit2h"/><REF ID="R-9" REFID="cit2i"/><REF ID="R-10" REFID="cit2j"/><REF ID="R-11" REFID="cit2k"/><REF ID="R-12" REFID="cit2l"/><REF ID="R-13" REFID="cit2m"/><REF ID="R-14" REFID="cit2n"/><REF ID="R-15" REFID="cit2o"/></S></P>
			<P><S AZ="PREV_OWN" ID="s3">Recently, we have reported a new type of photochromic compound, trans-3,3′-diaryl-3H,3′H-[2,2′]biindenylidene-1,1′-dione 1 which shows photochromism in the solid state.<REF ID="R-16" REFID="cit3a"/><REF ID="R-17" REFID="cit3b"/><REF ID="R-18" REFID="cit3c"/> </S><S AZ="PREV_OWN" ID="s4">For example, the yellow crystals of 1a turn reddish-purple when exposed to sunlight for a few minutes, and the reddish-purple crystals return slowly to yellow in the dark. </S><S AZ="PREV_OWN" ID="s5">However, irradiation of 1a in a CH2Cl2 solution gives its cis-isomer 2a, irreversibly. </S><S AZ="PREV_OWN" ID="s6">The reddish-purple crystals show EPR signals of triplet biradicals such as 3a formed by twisting the central CC bond of 1a upon photoirradiation in the solid state.<REF ID="R-19" REFID="cit3a"/><REF ID="R-20" REFID="cit3b"/><REF ID="R-21" REFID="cit3c"/></S></P>
			
			<P><S AZ="AIM" ID="s7">We have now found that the title compound 1b forms inclusion crystals with various kinds of organic guest molecules, and these inclusion crystals show reversible color change from yellow to red upon photoirradiation in the solid state, although 1b itself does not show any photocoloration. </S><S AZ="OWN_CONC" ID="s8">It was also found that the stability of the photochromism of inclusion crystals of 1b depends on the type of guest molecule included.</S></P>
			<P><S AZ="USE" ID="s9">trans-Biindenylidenedione 1b was prepared according to the previously reported method.<REF ID="R-22" REFID="cit3a"/><REF ID="R-23" REFID="cit3b"/><REF ID="R-24" REFID="cit3c"/> </S><S AZ="OWN_RES" ID="s10">The biindenylidenedione 1b was found to form inclusion complexes with various kinds of guest compounds. </S><S AZ="OWN_RES" ID="s11">(Table 1) For example, when 1b was recrystallized from the CH2Cl2 solution, 1 ∶ 1 inclusion complex crystals of 1b with CH2Cl2 were obtained as yellow prisms. </S><S AZ="OWN_RES" ID="s12">Similarly, the inclusion crystals with carbon tetrachloride (2 ∶ 1), tetrachloroethylene (4 ∶ 1), cyclopentanone (1 ∶ 1), γ-butyrolactone (1 ∶ 1), THF (1 ∶ 1), 1,4-dioxane (1 ∶ 1), DMSO (1 ∶ 1), benzene (4 ∶ 1), and toluene (4 ∶ 1) were obtained as yellow crystals. </S><S AZ="OWN_RES" ID="s13">(Table 1) Interestingly, these inclusion crystals show reversible color change from yellow to red upon photoirradiation in the solid state, whereas 1b itself does not show any visible coloration (Fig. 1). </S><S AZ="OWN_RES" ID="s14">For example, yellow crystals of the 1 ∶ 1 inclusion complex of 1b with CH2Cl2 turned into red crystals immediately upon photoirradiation. </S><S AZ="OWN_RES" ID="s15">The red color faded in the dark on storage for 1 h at room temperature. </S><S AZ="OWN_RES" ID="s16">The UV spectral changes of 1b and its 1 ∶ 1 CH2Cl2 inclusion complex are shown in Fig. 2 and Fig. 3, respectively.</S></P>
			
			
			
			
			<P><S AZ="OWN_CONC" ID="s17">It is also remarkable that the stability of the photochromism depends on the type of guest molecule. </S><S AZ="OWN_RES" ID="s18">For example, the red color developed upon photoirradiation of the 1 ∶ 1 inclusion crystals of 1b with cyclopentanone was more stable, and its half-life was 24 h in the dark (Fig. 4).</S></P>
			
			<P><S AZ="OWN_RES" ID="s19">Similarly, the half-lives of the red color of the inclusion crystals were 3 h (tetrachloroethylene), 5 h (benzene), 6 h (CCl4), 12 h (γ-butyrolactone, 1,4-dioxane and toluene), 24 h (THF), 36 h (DMSO), respectively. </S><S AZ="OWN_CONC" ID="s20">The stable photochromism of 1b in inclusion crystals may be due to the molecular arrangement suitable for twisting the central CC bond of 1b upon photoirradiation in the crystal.</S></P>
			<P><S AZ="OWN_CONC" ID="s21">The high inclusion ability of 1b may be attributed to the substituent effect of the 4-fluoro-phenyl groups, since biindenylidene-1,1′-dione 1a includes only limited guest molecules (Table 1). </S><S AZ="OWN_RES" ID="s22">For example, 1a formed inclusion complexes with CH2Cl2
				(1 ∶ 1), cyclopentanone (1 ∶ 1), and γ-butyrolactone (1 ∶ 1) as yellow prisms. </S><S AZ="OWN_RES" ID="s23">The photochromism of the inclusion crystals of 1a is more stable than that of 1a itself. </S><S AZ="OWN_RES" ID="s24">The half-lives of the red color of the inclusion crystals of 1a were 6 h (CH2Cl2), 12 h (cyclopentanone) and 12 h (γ-butyrolactone), respectively.</S></P>
			<P><S AZ="OWN_MTHD" ID="s25">In order to clarify the reason for the drastic changes in photochromicity of 1b by complexation with guest molecules, the X-ray crystal structures of the inclusion complex were studied.
				</S><S AZ="OWN_RES" ID="s26">Figs. 5–7 show the asymmetric units in the crystals of 1b and its inclusion complexes with CH2Cl2 and cyclopentanone, respectively. </S><S AZ="OWN_RES" ID="s27">Thermal ellipsoids are drawn at the 50percent probability level. </S><S AZ="OWN_RES" ID="s28">In the crystal of the pure host, the asymmetric unit comprises three independent molecules while in the solvates the asymmetric unit is a 1 ∶ 1 host–guest unit.</S></P>
			
			
			
			<P><S AZ="OWN_MTHD" ID="s29">The molecular structure of the host in the three compounds was carefully analysed in order to identify structural features which might correlate with the observed stabilities of photochromism. </S><S AZ="OWN_RES" ID="s30">Fig. 8 shows all of the host molecules viewed from a common direction, namely normal to the central CC double bond and with the respective biindenylidene dione units seen on edge.</S></P>
			
			<P><S AZ="OWN_RES" ID="s31">A distinctly bowed conformation is adopted by the biindenylidene dione unit in each of the three independent host molecules in 1b. </S><S AZ="OWN_RES" ID="s32">In contrast, this effect is hardly evident in the host molecules of 1b·CH2Cl2 and 1b·cyclopentanone. </S><S AZ="OWN_MTHD" ID="s33">To quantify the molecular distortions, separate parameters were selected to isolate the twist around the central double bond and the bowing effect, namely the pseudo-torsion angle C7–O10⋯C24–O27 (τ) and the centroid–centroid distance P1⋯P2, respectively (Fig. 7). </S><S AZ="OWN_RES" ID="s34">The former parameter is zero when there is no twist and the latter has its maximum value of 8.278(5)
				Å in the case of the least bowed host molecule in 1b·cyclopentanone. </S><S AZ="OWN_RES" ID="s35">These parameters are listed in Table 2.</S></P>
			
			<P><S AZ="OWN_RES" ID="s36">The data show that on proceeding from the pure host to the CH2Cl2 and cyclopentanone inclusion complexes, there is a definite increase in the extent of twist around the central CC double bond and an accompanying decrease in the extent of bowing. </S><S AZ="OWN_RES" ID="s37">The former parameter shows the more significant changes (from an average of ca. 1.5° for molecules in 1b to −18.6° in the cyclopentanone complex) and the degree of twist increases with the stability of photochromism (0.5 h for 1b·CH2Cl2, 24 h for 1b·cyclopentanone, relative to the host in 1b which is not photochromic). </S><S AZ="OWN_CONC" ID="s38">Thus, the X-ray structures reveal unequivocal differences in the host geometries in the solid state that can be correlated with the photochromic behaviour.</S></P>
			<P><S AZ="OWN_RES" ID="s39">Crystal stabilisation in 1b
				(Fig. 9) is based on π–π stacking (nine unique interactions with ring centroid···centroid distances &lt; 4.00 Å), ten C–H⋯O hydrogen bonds (both intra- and intermolecular), as well as one C–H⋯F hydrogen bond.</S></P>
			
			<P><S AZ="OWN_RES" ID="s40">Enclathration of the guests CH2Cl2 and cyclopentanone by host 1b results in a significant reduction in π–π stacking in crystals of the inclusion complexes. </S><S AZ="OWN_RES" ID="s41">In the complex with CH2Cl2
				(Fig. 10), pairs of guest molecules are trapped in cages formed by the fluorophenyl rings of four host molecules, host–guest interaction consisting of one (solvent)C–H⋯O10(host) hydrogen bond.</S></P>
			
			<P><S AZ="OWN_RES" ID="s42">Further stabilisation is afforded by two intermolecular host–host C–H⋯O hydrogen bonds and two π–π interactions with distances 3.847 and 3.875 Å.</S></P>
			<P><S AZ="OWN_RES" ID="s43">In the cyclopentanone inclusion complex (Fig. 11), the guest molecules occupy isolated sites, each guest oxygen atom linking two adjacent host molecules via C–H⋯O hydrogen bonds.</S></P>
			
			<P><S AZ="OWN_RES" ID="s44">Further crystal stabilisation is provided by one intermolecular host–host C–H⋯F hydrogen bond and four π–π interactions with ring centroid···centroid distances in the range 3.827–3.998 Å.</S></P>
			<P><S ID="s45">CCDC reference numbers 221994–221996. </S><S ID="s46">See http://www.rsc.org/suppdata/ce/b3/b314140c/ for crystallographic data in CIF format.</S></P>
			<P><S ID="s47">K.T. acknowledges financial support from The Suzuki Foundation. </S><S ID="s48">M.R.C. thanks the University of Cape Town and the National Research Foundation (Pretoria) for financial support.</S></P>
		</DIV>
	</BODY>
		<REFERENCELIST><REFERENCE ID="cit1"><AUTHORLIST><AUTHOR>B. V.<SURNAME>Gemert</SURNAME></AUTHOR></AUTHORLIST>J. C. CranoR. J. Guglielmetti<TITLE>Organic Photochromic and Thermochromic Compounds</TITLE><DATE>1999</DATE></REFERENCE><REFERENCE ID="cit2a"><AUTHORLIST><AUTHOR>K.<SURNAME>Tanaka</SURNAME></AUTHOR><AUTHOR>A.<SURNAME>Tomomori</SURNAME></AUTHOR><AUTHOR>J. L.<SURNAME>Scott</SURNAME></AUTHOR></AUTHORLIST><TITLE/><JOURNAL>CrystEngComm.<YEAR>2003</YEAR>5147</JOURNAL></REFERENCE><REFERENCE ID="cit2b"><AUTHORLIST><AUTHOR>S.<SURNAME>Iyengar</SURNAME></AUTHOR><AUTHOR>M. C.<SURNAME>Biewer</SURNAME></AUTHOR></AUTHORLIST><TITLE/><JOURNAL>Chem. Commun.<YEAR>2002</YEAR>1398</JOURNAL></REFERENCE><REFERENCE ID="cit2c"><AUTHORLIST><AUTHOR>H.<SURNAME>Nishikiori</SURNAME></AUTHOR><AUTHOR>N.<SURNAME>Matsushita</SURNAME></AUTHOR></AUTHORLIST><TITLE/><JOURNAL>Chem. Lett<YEAR>2002</YEAR>930</JOURNAL></REFERENCE><REFERENCE ID="cit2d"><AUTHORLIST><AUTHOR>S.<SURNAME>Kobatake</SURNAME></AUTHOR><AUTHOR>K.<SURNAME>Uchida</SURNAME></AUTHOR><AUTHOR>E.<SURNAME>Tsuchida</SURNAME></AUTHOR><AUTHOR>M.<SURNAME>Irie</SURNAME></AUTHOR></AUTHORLIST><TITLE/><JOURNAL>Chem. Commun.<YEAR>2002</YEAR>2804</JOURNAL></REFERENCE><REFERENCE ID="cit2e"><AUTHORLIST><AUTHOR>J. N.<SURNAME>Moothy</SURNAME></AUTHOR><AUTHOR>P.<SURNAME>Mal</SURNAME></AUTHOR><AUTHOR>R.<SURNAME>Natarajan</SURNAME></AUTHOR><AUTHOR>P.<SURNAME>Venugoplan</SURNAME></AUTHOR></AUTHORLIST><TITLE/><JOURNAL>Org. Lett.<YEAR>2001</YEAR>31579</JOURNAL></REFERENCE><REFERENCE ID="cit2f"><AUTHORLIST><AUTHOR>K.<SURNAME>Tanaka</SURNAME></AUTHOR><AUTHOR>T.<SURNAME>Watanabe</SURNAME></AUTHOR><AUTHOR>F.<SURNAME>Toda</SURNAME></AUTHOR></AUTHORLIST><TITLE/><JOURNAL>Chem. Commun.<YEAR>2000</YEAR>1361</JOURNAL></REFERENCE><REFERENCE ID="cit2g"><AUTHORLIST><AUTHOR>H.<SURNAME>Yoshikawa</SURNAME></AUTHOR><AUTHOR>S.<SURNAME>Nishikiori</SURNAME></AUTHOR></AUTHORLIST><TITLE/><JOURNAL>Chem. Lett<YEAR>2000</YEAR>142</JOURNAL></REFERENCE><REFERENCE ID="cit2h"><AUTHORLIST><AUTHOR>M.<SURNAME>Nanasawa</SURNAME></AUTHOR><AUTHOR>M.<SURNAME>Miwa</SURNAME></AUTHOR><AUTHOR>M.<SURNAME>Hirai</SURNAME></AUTHOR><AUTHOR>T.<SURNAME>Kuwabara</SURNAME></AUTHOR></AUTHORLIST><TITLE/><JOURNAL>J. Org. Chem.<YEAR>2000</YEAR>65593</JOURNAL></REFERENCE><REFERENCE ID="cit2i"><AUTHORLIST><AUTHOR>L.<SURNAME>Dinescu</SURNAME></AUTHOR><AUTHOR>Z. Y.<SURNAME>Wang</SURNAME></AUTHOR></AUTHORLIST><TITLE/><JOURNAL>Chem. Commun.<YEAR>1999</YEAR>2497</JOURNAL></REFERENCE><REFERENCE ID="cit2j"><AUTHORLIST><AUTHOR>S.<SURNAME>Kobatake</SURNAME></AUTHOR><AUTHOR>M.<SURNAME>Yamada</SURNAME></AUTHOR><AUTHOR>T.<SURNAME>Yamada</SURNAME></AUTHOR><AUTHOR>M.<SURNAME>Irie</SURNAME></AUTHOR></AUTHORLIST><TITLE/><JOURNAL>J. Am. Chem. Soc.<YEAR>1999</YEAR>1212380 and 8450</JOURNAL></REFERENCE><REFERENCE ID="cit2k"><AUTHORLIST><AUTHOR>T.<SURNAME>Arai</SURNAME></AUTHOR><AUTHOR>M.<SURNAME>Ikegami</SURNAME></AUTHOR></AUTHORLIST><TITLE/><JOURNAL>Chem. Lett.<YEAR>1999</YEAR>965</JOURNAL></REFERENCE><REFERENCE ID="cit2l"><AUTHORLIST><AUTHOR>J.<SURNAME>Harada</SURNAME></AUTHOR><AUTHOR>H.<SURNAME>Uekusa</SURNAME></AUTHOR><AUTHOR>Y.<SURNAME>Ohashi</SURNAME></AUTHOR></AUTHORLIST><TITLE/><JOURNAL>J. Am. Chem. Soc.<YEAR>1999</YEAR>1215809</JOURNAL></REFERENCE><REFERENCE ID="cit2m"><AUTHORLIST><AUTHOR>S.<SURNAME>Delbaere</SURNAME></AUTHOR><AUTHOR>B.<SURNAME>L-Houze</SURNAME></AUTHOR><AUTHOR>C.<SURNAME>Bochu</SURNAME></AUTHOR><AUTHOR>Y.<SURNAME>Teral</SURNAME></AUTHOR><AUTHOR>M.<SURNAME>Campredon</SURNAME></AUTHOR><AUTHOR>G.<SURNAME>Vermeersch</SURNAME></AUTHOR></AUTHORLIST><TITLE/><JOURNAL>J. Chem. Soc., Perkin Trans. 2<YEAR>1998</YEAR>1153</JOURNAL></REFERENCE><REFERENCE ID="cit2n"><AUTHORLIST><AUTHOR>Y.<SURNAME>Eichen</SURNAME></AUTHOR><AUTHOR>J.-M.<SURNAME>Lehn</SURNAME></AUTHOR><AUTHOR>M.<SURNAME>Scherl</SURNAME></AUTHOR><AUTHOR>D.<SURNAME>Haarer</SURNAME></AUTHOR><AUTHOR>J.<SURNAME>Fischer</SURNAME></AUTHOR><AUTHOR>A.<SURNAME>DeCian</SURNAME></AUTHOR><AUTHOR>A.<SURNAME>Corval</SURNAME></AUTHOR><AUTHOR>P.<SURNAME>Trommsdorff</SURNAME></AUTHOR></AUTHORLIST><TITLE/><JOURNAL>Angew. Chem., Int. Ed. Engl.<YEAR>1995</YEAR>342530</JOURNAL></REFERENCE><REFERENCE ID="cit2o"><AUTHORLIST><AUTHOR>H.<SURNAME>Koyama</SURNAME></AUTHOR><AUTHOR>T.<SURNAME>Kawato</SURNAME></AUTHOR><AUTHOR>H.<SURNAME>Kanatomi</SURNAME></AUTHOR><AUTHOR>H.<SURNAME>Hatsushita</SURNAME></AUTHOR><AUTHOR>K.<SURNAME>Yonetani</SURNAME></AUTHOR></AUTHORLIST><TITLE/><JOURNAL>Chem. Commun.<YEAR>1994</YEAR>579</JOURNAL></REFERENCE><REFERENCE ID="cit3a"><AUTHORLIST><AUTHOR>K.<SURNAME>Tanaka</SURNAME></AUTHOR><AUTHOR>Y.<SURNAME>Yamamoto</SURNAME></AUTHOR><AUTHOR>H.<SURNAME>Takano</SURNAME></AUTHOR><AUTHOR>M. R.<SURNAME>Caira</SURNAME></AUTHOR></AUTHORLIST><TITLE/><JOURNAL>Chem. Lett.<YEAR>2003</YEAR>680</JOURNAL></REFERENCE><REFERENCE ID="cit3b"><AUTHORLIST><AUTHOR>K.<SURNAME>Tanaka</SURNAME></AUTHOR><AUTHOR>Y.<SURNAME>Yamamoto</SURNAME></AUTHOR><AUTHOR>S.<SURNAME>Ohba</SURNAME></AUTHOR></AUTHORLIST><TITLE/><JOURNAL>Chem. Commun.<YEAR>2003</YEAR>1866</JOURNAL></REFERENCE><REFERENCE ID="cit3c"><AUTHORLIST><AUTHOR>K.<SURNAME>Tanaka</SURNAME></AUTHOR><AUTHOR>F.<SURNAME>Toda</SURNAME></AUTHOR></AUTHORLIST><TITLE/><JOURNAL>J. Chem. Soc., Perkin Trans 1<YEAR>2000</YEAR>873</JOURNAL></REFERENCE><REFERENCE ID="cit4"><DATE/></REFERENCE><REFERENCE ID="cit5a"><DATE/></REFERENCE><REFERENCE ID="cit5b"><DATE/></REFERENCE><REFERENCE ID="cit5c"><DATE/></REFERENCE></REFERENCELIST>
		<FOOTNOTELIST><FOOTNOTE ID="tab1fna" MARKER="1">The host–guest ratio was determined by TG.</FOOTNOTE><FOOTNOTE ID="tab1fnb" MARKER="2">The half-life was determined by solid-state UV spectra and the average error in t1/2 is about 10 min.</FOOTNOTE><FOOTNOTE ID="tab1fnc" MARKER="3">No inclusion complexation occurred.</FOOTNOTE><FOOTNOTE ID="tab2fna" MARKER="4">average e.s.d.'s are 0.3° and 0.005Å</FOOTNOTE></FOOTNOTELIST><FIGURELIST><FIGURE ID="fig1" SEQ="figure" SRC="fig1"><TITLE>Photographs of (a)
					1b
					(crystal size: 3.5 mm × 1.5 mm × 0.2 mm) and (b) 1 ∶ 1 CH2Cl2 complex of 1b
					(crystal size: 5.0 mm × 0.5 mm × 0.3 mm) before (top) and after photoirradiation (bottom).</TITLE></FIGURE><FIGURE ID="fig2" SEQ="figure" SRC="fig2"><TITLE>UV spectral changes of 1b in the solid state. Measurement was successively made every 30 min from the top (after irradiation) to the bottom.</TITLE></FIGURE><FIGURE ID="fig3" SEQ="figure" SRC="fig3"><TITLE>UV spectral changes of 1 ∶ 1 inclusion complex of 1b with CH2Cl2 in the solid state. Measurement was successively made every 30 min from the top (after irradiation) to the bottom.</TITLE></FIGURE><FIGURE ID="fig4" SEQ="figure" SRC="fig4"><TITLE>UV spectral changes of 1 ∶ 1 inclusion complex of 1b with cyclopentanone in the solid state. Measurement was successively taken every 30 min from the top (after irradiation) to the bottom.</TITLE></FIGURE><FIGURE ID="fig5" SEQ="figure" SRC="fig5"><TITLE>The three independent molecules in the asymmetric unit of compound 1b.</TITLE></FIGURE><FIGURE ID="fig6" SEQ="figure" SRC="fig6"><TITLE>Molecular structure of the 1 ∶ 1 inclusion complex of 1b with CH2Cl2.</TITLE></FIGURE><FIGURE ID="fig7" SEQ="figure" SRC="fig7"><TITLE>Molecular structure of the 1 ∶ 1 inclusion complex of 1b with cyclopentanone. Labelled items are described in the text.</TITLE></FIGURE><FIGURE ID="fig8" SEQ="figure" SRC="fig8"><TITLE>The three independent host molecules in 1b
					(A, B, C) and the host molecules in 1b·CH2Cl2
					(lower left) and 1b·cyclopentanone (lower right).</TITLE></FIGURE><FIGURE ID="fig9" SEQ="figure" SRC="fig9"><TITLE>Stereoview of the crystal packing in 1b. Click here to access a 3D view of Fig. 9.</TITLE></FIGURE><FIGURE ID="fig10" SEQ="figure" SRC="fig10"><TITLE>Stereoview of the crystal packing in 1b·CH2Cl2. Four unit cells are drawn. Click here to access a 3D view of Fig. 10.</TITLE></FIGURE><FIGURE ID="fig11" SEQ="figure" SRC="fig11"><TITLE>Stereoview of the crystal packing in 1b·cyclopentanone. Four unit cells are drawn. Click here to access a 3D view of Fig. 11.</TITLE></FIGURE></FIGURELIST><TABLELIST><TABLE ID="tab1"><TITLE>Photochromism of the inclusion complex of 1a and 1b in the solid state</TITLE>
					
						
						
						
						
						
						
						
						
							
								Guest
								Inclusion complex of 1b
								Inclusion complex of 1a
							
							
								h ∶ g ratio
								Photochromism
								Half-life/h
								h ∶ g ratio
								Photochromism
								Half-life/h
							
						
						
							
								
							
						
						
							
								none
								—
								no
								—
								—
								yes
								1
							
							
								CH2Cl2
								1 ∶ 1
								yes
								0.5
								1 ∶ 1
								yes
								6
							
							
								CCl4
								2 ∶ 1
								yes
								6
								—
								—
								—
							
							
								Tetrachloroethylene
								4 ∶ 1
								yes
								3
								—
								—
								—
							
							
								Cyclopentanone
								1 ∶ 1
								yes
								24
								1 ∶ 1
								yes
								12
							
							
								γ-Butyrolactone
								1 ∶ 1
								yes
								12
								1 ∶ 1
								yes
								12
							
							
								THF
								1 ∶ 1
								yes
								24
								—
								—
								—
							
							
								1,4-Dioxane
								1 ∶ 1
								yes
								12
								—
								—
								—
							
							
								DMSO
								1 ∶ 1
								yes
								36
								—
								—
								—
							
							
								Benzene
								4 ∶ 1
								yes
								5
								—
								—
								—
							
							
								Toluene
								4 ∶ 1
								yes
								12
								—
								—
								—
							
						
					
				</TABLE><TABLE ID="tab2"><TITLE>Molecular parameters defining host conformations</TITLE>
					
						
						
						
						
						
						
						
							
								Parameter
								1b
									(A)
								1b
									(B)
								1b
									(C)
								1b·CH2Cl2
								1b·C5H8O
							
						
						
							
								
							
						
						
							
								τ/°
								5.7
								−0.2
								−0.8
								−15.2
								−18.6
							
							
								P1⋯P2/Å
								8.159
								8.161
								8.204
								8.270
								8.278
							
						
					
				</TABLE></TABLELIST></PAPER>