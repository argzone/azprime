package uk.ac.cam.cl.azprime.ml.features

import uk.ac.cam.cl.azprime.resources.Headers
import uk.ac.cam.cl.azprime.xml.ParsedSciXml
import uk.ac.cam.cl.azprime.ml.ExtractedFeature
import uk.ac.cam.cl.azprime.ml.FeatureExtractor
import uk.ac.cam.cl.azprime.ml.ExtractedFeatures
import uk.ac.cam.cl.azprime.ml.SingleFeatureExtractor

object DocumentInitialSectionValues extends Enumeration {
  type Values = Value

  val InitialSection = Value("1a")
  val InitialParagraph = Value("1c")
  val NotInitial = Value("0")
}

case class DocumentInitialSection(val value: DocumentInitialSectionValues.Values) extends ExtractedFeature {
  type ValueType = DocumentInitialSectionValues.Values
  val label = "ISEC"
}

class DocumentInitialSectionExtractor(headers: Headers, initialParagraphs: Int) extends SingleFeatureExtractor {
  import DocumentInitialSectionValues._
  type Extracted = DocumentInitialSection

  val Threshold = initialParagraphs

  def extract(sentence: ParsedSciXml.Sentence, document: ParsedSciXml.Document, features: ExtractedFeatures) = {
    val header = document.header(sentence)
    val res = if (headers.hasStarting(document.allHeaders)) {
      if (headers.starting(header)) InitialSection
      else NotInitial
    } else {
      if (document.paragraphLocation(sentence) < Threshold) InitialParagraph
      else NotInitial
    }
    new DocumentInitialSection(res)
  }
}