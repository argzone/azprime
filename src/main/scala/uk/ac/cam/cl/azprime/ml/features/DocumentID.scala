package uk.ac.cam.cl.azprime.ml.features

import uk.ac.cam.cl.azprime.ml.ExtractedFeature
import uk.ac.cam.cl.azprime.ml.FeatureExtractor
import uk.ac.cam.cl.azprime.ml.ExtractedFeatures
import uk.ac.cam.cl.azprime.ml.SingleFeatureExtractor
import uk.ac.cam.cl.azprime.xml.ParsedSciXml

case class DocumentID(val value: String) extends ExtractedFeature {
  type ValueType = String
  val label = "ID"
}

class DocumentIDExtractor extends SingleFeatureExtractor {
  type Extracted = DocumentID
   def extract(sentence: ParsedSciXml.Sentence, document: ParsedSciXml.Document, features: ExtractedFeatures) = {
    new DocumentID(document.fileno)
  }
}