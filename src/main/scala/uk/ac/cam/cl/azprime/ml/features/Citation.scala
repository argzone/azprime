package uk.ac.cam.cl.azprime.ml.features

import uk.ac.cam.cl.azprime.resources.Headers
import uk.ac.cam.cl.azprime.xml.ParsedSciXml
import uk.ac.cam.cl.azprime.ml.ExtractedFeature
import uk.ac.cam.cl.azprime.ml.FeatureExtractor
import uk.ac.cam.cl.azprime.ml.ExtractedFeatures
import uk.ac.cam.cl.azprime.ml.MultipleFeatureExtractor
import uk.ac.cam.cl.azprime.ml.SingleFeatureExtractor

object CitationTypeValues extends Enumeration {
  type Values = Value
  val None = Value("0")
  val Citation = Value("REF")
  val Author = Value("REFA")
}

object SelfCitationValues extends Enumeration {
  type Values = Value
  val NA = Value("0")
  val Self = Value("SELF")
  val Other = Value("OTHER")
  val Author = Value("REFA")
}

object CitationLocationValues extends Enumeration {
  type Values = Value
  val NA = Value("0")
  val SentenceStart = Value("CITE-BEG")
  val SentenceMiddle = Value("CITE-MID")
  val SentenceEnd = Value("CITE-END")
  val Author = Value("REFA")
}

object CitationCountValues extends Enumeration {
  type Values = Value
  val None = Value("0")
  val One = Value("CITE-ONCE")
  val Many = Value("CITE-SEVERAL")
  val Author = Value("REFA")
}

object CitationUtil {
  def isRef(pos: String) = (pos startsWith "REF") && !(pos startsWith "REFA")
}

case class CitationType(val value: CitationTypeValues.Values) extends ExtractedFeature {
  type ValueType = CitationTypeValues.Values
  val label = "CIT"
}

case class SelfCitation(val value: SelfCitationValues.Values) extends ExtractedFeature {
  type ValueType = SelfCitationValues.Values
  val label = "SEL"
}

case class CitationLocation(val value: CitationLocationValues.Values) extends ExtractedFeature {
  type ValueType = CitationLocationValues.Values
  val label = "CLO"
}

case class CitationCount(val value: CitationCountValues.Values) extends ExtractedFeature {
  type ValueType = CitationCountValues.Values
  val label = "CNO"
}

class CitationTypeExtractor extends SingleFeatureExtractor {
  type Extracted = CitationType

  def extract(sentence: ParsedSciXml.Sentence, document: ParsedSciXml.Document, features: ExtractedFeatures) = {
    import CitationTypeValues._
    val isCitationRef = sentence.pos.exists(x => (x startsWith "REF") && !(x startsWith "REFA"))
    val isAuthorRef = sentence.pos.exists(_ startsWith "REFAUTHOR")
    new CitationType(if (isCitationRef) {
      Citation
    } else if (isAuthorRef) {
      Author
    } else {
      None
    })
  }
}

class SelfCitationExtractor extends SingleFeatureExtractor {
  type Extracted = SelfCitation

  dependsOn[CitationTypeExtractor]

  def extract(sentence: ParsedSciXml.Sentence, document: ParsedSciXml.Document, features: ExtractedFeatures) = {
    import SelfCitationValues._
    val isSelf = sentence.pos.exists(_ endsWith "-SELF")
    val ctype = features[CitationType]()
    new SelfCitation(ctype.value match {
      case CitationTypeValues.Author => Author
      case CitationTypeValues.Citation =>
        if (isSelf) Self
        else Other
      case _ => NA
    })
  }
}

class CitationLocationExtractor extends SingleFeatureExtractor {
  type Extracted = CitationLocation

  dependsOn[CitationTypeExtractor]

  def extract(sentence: ParsedSciXml.Sentence, document: ParsedSciXml.Document, features: ExtractedFeatures) = {
    import CitationLocationValues._
    new CitationLocation(features[CitationType]().value match {
      case CitationTypeValues.Author => Author
      case CitationTypeValues.Citation =>
        val length = sentence.pos.length
        val locOpt = sentence.pos.zipWithIndex.reverse.dropWhile(x => !CitationUtil.isRef(x._1)).headOption
        val loc = locOpt match {
          case Some((_, l)) => l
          case None => 0
        }
        if (loc < length / 5d) SentenceStart
        else if (loc > length * 4d / 5d) SentenceEnd
        else SentenceMiddle
      case _ => NA
    })
  }
}

class CitationCountExtractor extends SingleFeatureExtractor {
  type Extracted = CitationCount

  dependsOn[CitationTypeExtractor]

  def extract(sentence: ParsedSciXml.Sentence, document: ParsedSciXml.Document, features: ExtractedFeatures) = {
    import CitationCountValues._
    val refs = sentence.pos.zipWithIndex.filter(x => CitationUtil.isRef(x._1))
    new CitationCount(features[CitationType]().value match {
      case CitationTypeValues.Author => Author
      case CitationTypeValues.Citation =>
        if (refs.length == 1) One
        else if (refs.length > 1) Many
        else None
      case _ => None
    })
  }
}