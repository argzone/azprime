package uk.ac.cam.cl.azprime.resources

import java.io.InputStream

case class Indicator(
  trigger: String,
  phrase: String,
  category: String,
  category2: String,
  score: Int)

object Indicators {
  import org.tempura.utils.StringOps._
  def matchingIndicators(input: String, byTrigger: Map[String, Set[Indicator]]) = {
    def rx(trigger: String) = {
      ("""\b""" + trigger + """\b""").r
    }

    (for {
      (trigger, indicators) <- byTrigger;
      if input =~ rx(trigger)
    } yield {
      indicators filter (i => input =~ rx(i.phrase))
    }).flatten.toList
  }
  def parseIndicator(input: String) = {
    val Array(score, trigger, cat, cat2, indicatorWords @ _*) = input.split("""\s+""", 5)
    val indicatorStr = indicatorWords mkString " "
    val indicator = Indicator(trigger, indicatorStr, cat, cat2, score.toInt)
    indicator
  }
}

class Indicators(instream: InputStream) {
  lazy val (indicators, byIndicator, byCategory, byCategory2, byTrigger) = {
    import collection.mutable.Map
    val lines = io.Source.fromInputStream(instream).getLines
    val indicators = collection.mutable.Set[Indicator]()
    val byIndicator = Map[String, Indicator]()
    val byCategory = Map[String, Set[Indicator]]() withDefaultValue (Set())
    val byCategory2 = Map[String, Set[Indicator]]() withDefaultValue (Set())
    val byTrigger = Map[String, Set[Indicator]]() withDefaultValue (Set())

    for (line_ <- lines; line = line_.trim; if (!DataFile.isComment(line))) {
      val indicator = Indicators.parseIndicator(line)
      indicators += indicator
      byIndicator(indicator.phrase) = indicator
      byCategory(indicator.category) += indicator
      byCategory2(indicator.category2) += indicator
      byTrigger(indicator.trigger) += indicator
    }
    // Return immutable versions of the mutable ones above.
    (indicators.toSet,
      byIndicator.toMap,
      byCategory.toMap,
      byCategory2.toMap,
      byTrigger.toMap)
  }

  def find(input: String) = {
    val matchedIndicators = Indicators.matchingIndicators(input, byTrigger)
    val bestScoredIndicator = matchedIndicators reduceRight { (a, b) =>
      if (a.score > b.score) a else b
    }
    val longestIndicator = matchedIndicators reduceRight { (a, b) =>
      if (a.phrase.length > b.phrase.length) a else b
    }
    if (matchedIndicators.nonEmpty)
      Some(bestScoredIndicator, longestIndicator)
    else
      None
  }
}
