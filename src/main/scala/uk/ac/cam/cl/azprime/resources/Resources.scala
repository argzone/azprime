package uk.ac.cam.cl.azprime.resources

import uk.ac.cam.cl.azprime.config.Configuration

import java.io.File

class Resources(config: Configuration) {

  def asStream(filepath: String) = {
    this.getClass.getResourceAsStream(filepath)
  }

  lazy val indicators = new Indicators(asStream(config.files.indicators))
  lazy val lexicon = new Lexicon(asStream(config.files.lexicon))
  lazy val actions = new Actions(asStream(config.files.actions))
  lazy val patterns = new Patterns(asStream(config.files.patterns))
  lazy val stoplist = new StopWords(asStream(config.files.stoplist))
  lazy val headers = new Headers(asStream(config.files.headers))
  lazy val trainingFiles = getDataFiles(config.files.data, config.files.datatype)
  
  def getDataFiles(path: String, filetype: String): List[File] = {
	  val files = getContents(new File(path),filetype)
      if (files.length < 1)
        throw new RuntimeException("No files found at %s!".format(path))
      files.toList
  }
  
  def getContents(dir: File, filetype: String): Array[File] = {
    assert(dir.isDirectory)
    val contents = dir.listFiles.filter(_.getAbsolutePath.endsWith("." + filetype))
    contents ++ dir.listFiles.filter(_.isDirectory).flatMap(getContents(_,filetype))
  }
  
  /*
  def getDataFiles(path: String, filetype: String) {
    val rootpath = new File(path)
    assert(rootpath.isDirectory)
    
    val filenames = rootpath.listFiles.filter(_.getAbsolutePath.endsWith("." + filetype))
    if (filenames.length < 1) 
      throw new RuntimeException("No files found at %s!".format(rootpath.getAbsolutePath))
    filenames.toList
  }
  */
}