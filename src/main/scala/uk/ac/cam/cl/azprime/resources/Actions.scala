package uk.ac.cam.cl.azprime.resources

import java.io.InputStream
import org.tempura.utils.CollectionOps._
import org.tempura.utils.StringOps._
import uk.ac.cam.cl.azprime.verbs.VerbVoice
import uk.ac.cam.cl.azprime.verbs.VoiceModifier

object Action {
  def isPerceptionVerb(tokens: Array[String]) = {
    tokens.length > 1 &&
      tokens(0) == "be" &&
      tokens(1) != "the" && tokens(1) != "not"
  }
}

case class Action(value: String, override val label: String, exceptions: Set[String] = Set())
    extends FormulaicExpression(value, label) {
  lazy val stem = {
    if (isPerceptionVerb)
      tokens(0).value.toString
    else
      tokens(0).value.toString
  }
  lazy val isPerceptionVerb = Action.isPerceptionVerb(tokens.map(x => x.value.toString).toArray)
}

object Actions {
  val ActionLinePattern = """^(.*?)\s+([A-Z_]+?)\s*(?:\*\*\s{0,1}(.*?)\s{0,1}\*\*){0,1}\s*$""".r
}

class Actions(private val instream: InputStream) {
  def this(filepath: String)(implicit lexicon: Lexicon) {
    this(Class.
      forName("uk.ac.cam.cl.azprime.data.Actions").
      getResourceAsStream(filepath))
  }

  val lines = io.Source.fromInputStream(instream).getLines
  lazy val (actions, stems, perceptionVerbs, exceptions) = {
    import collection.mutable.Map
    import collection.mutable.Set
    import DataFile.isComment

    val exceptions: Map[String, Action] = Map()
    val perceptionVerbs: Set[String] = Set()
    val stems: Map[String, Set[Action]] = Map() withDefaultValue (Set[Action]())
    val actions = Set[Action]()

    // Lines look something like:
    // fall short		PROBLEM ** fell fallen **
    // force @SELF_ACC		PROBLEM

    for (line_ <- lines; line = line_.trim; if (!isComment(line))) {

      val Actions.ActionLinePattern(act, label, exList) = line
      // TODO if act has variables, replace them and create an action for all.
      val action = Action(act, label,
        (if (exList != null) exList.split("\\s+").map(_.trim).toSet else Set[String]().toSet))

      actions += action
      exceptions ++= action.exceptions.map(_ -> action)
      stems(action.stem) = stems(action.stem) + action
      if (action.isPerceptionVerb)
        perceptionVerbs += action.stem
    }
    (actions.toSet,
      stems.toMap,
      perceptionVerbs.toSet,
      exceptions.toMap)
  }

  lazy val lookup = actions.map(a => a.value -> a).groupByKey.valuesAsSet

  def matches(input: List[(String, String)], offset: Int, voice: VerbVoice = VerbVoice.NA)(implicit lexicon: Lexicon): Set[ExpressionMatch[Action]] = {
    val word = findStem(input(offset))
    val actset = if (stems contains word) {
      val expressions = stems(word)
      val matched =
        expressions map { ex =>
          FormulaicExpression.
            matches(ex, input, offset)(lexicon, findStem).
            map(x => ExpressionMatch(ex, x._1, x._2))
        }
      matched.flatten.toSet
    } else Set()
    if (actset.isEmpty) {
      voice match {
        // First three cases are special cases (ha ha) to deal with semantic verbs found elsewhere in sentence.
        case VerbVoice(_, VoiceModifier.Need) =>
          Set(ExpressionMatch(Action(word + " to", "NEED"), offset, offset + 1))
        case VerbVoice(_, VoiceModifier.Possessive) =>
          Set(ExpressionMatch(Action(word, "POSSESS"), offset, offset + 1))
        case VerbVoice(_, VoiceModifier.Copula) =>
          Set(ExpressionMatch(Action(word, "COPULA"), offset, offset + 1))
        case _ => Set()
      }
    } else actset.toSet
  }

  def findStem(wordWithPos: (String, String)) = {
    import uk.ac.cam.cl.azprime.verbs._
    val (word, pos) = wordWithPos

    def checkVariants(word: String) = {
      if (word =~ "s$" && stems.contains(word - 1))
        stems(word - 1).headOption
      else if (word =~ "ed$" && stems.contains(word - 2))
        stems(word - 2).headOption
      else if (word =~ "ed$" && stems.contains(word - 1))
        stems(word - 1).headOption
      else if (word =~ "ing$" && stems.contains(word - 3))
        stems(word - 3).headOption
      else None
    }

    if (exceptions contains word)
      exceptions(word).stem
    else
      word match {
        case BeVerb(v)   => "be"
        case HaveVerb(v) => "have"
        case DoVerb(v)   => "do"
        case _ => {
          {
            checkVariants(word)
          } orElse {
            if (word =~ "e$" || word =~ "h$" || word =~ "s$")
              checkVariants(word - 1)
            else None
          } orElse {
            if (((word =~ "ied$") || (word =~ "ies$")) && stems.contains(word - 3))
              stems(word - 3).headOption
            else None
          } map (_.stem) getOrElse (word)
        }
      }
  }
}