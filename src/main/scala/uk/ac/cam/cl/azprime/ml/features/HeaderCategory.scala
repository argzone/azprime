package uk.ac.cam.cl.azprime.ml.features

import uk.ac.cam.cl.azprime.resources.Headers
import uk.ac.cam.cl.azprime.xml.ParsedSciXml
import uk.ac.cam.cl.azprime.ml.ExtractedFeature
import uk.ac.cam.cl.azprime.ml.FeatureExtractor
import uk.ac.cam.cl.azprime.ml.ExtractedFeatures
import uk.ac.cam.cl.azprime.ml.SingleFeatureExtractor

case class HeaderCategory(val value: String) extends ExtractedFeature {
  type ValueType = String
  val label = "HEA"
}

class HeaderCategoryExtractor(headers: Headers) extends SingleFeatureExtractor {
  type Extracted = HeaderCategory
  def extract(sentence: ParsedSciXml.Sentence, document: ParsedSciXml.Document, features: ExtractedFeatures) = {
    val h = headers(document.header(sentence))
    new HeaderCategory(if (h != null) h else "nonstandard")
  }
}