package uk.ac.cam.cl.azprime.xml

import scala.xml.Node
import scala.xml.XML
import java.io.File
import scala.collection.immutable.ListMap
import scala.collection.immutable.ListSet
import com.sun.org.apache.xerces.internal.impl.io.MalformedByteSequenceException
import org.tempura.utils.EncodingUtils
import org.tempura.utils.EncodingUtils
import scala.xml.PrettyPrinter
import scala.xml.NodeSeq
import scala.xml.NamespaceBinding
import scala.xml.Elem

object ParsedSciXml {
  // For Java interop.
  trait ReplaceSentenceHandler {
    def apply(s: Sentence): Sentence
  }
  def replaceSentences(doc: Document, f: ReplaceSentenceHandler): Document = {
    replaceSentences(doc, f.apply _)
  }

  def replaceAbstract(doc: Document, f: Sentence => List[Sentence]): Document = {
    doc.copy(abstractSentences = doc.abstractSentences.flatMap(f));
  }

  // Sample usage: val someDoc2 = ParsedSciXml.replaceSentences(someDoc, sentence => sentence.copy(wordsWithPos = sentence.wordsWithPos.reverse))
  def replaceSentences(doc: Document, f: Sentence => Sentence): Document = {
    def transform[T <: HasSentences](hs: T): T = {
      hs match {
        case d: Division  => d.copy(content = d.content.map(transform[HasSentences])).asInstanceOf[T]
        case p: Paragraph => p.copy(sentences = p.sentences.map(f)).asInstanceOf[T]
      }
    }
    doc.copy(abstractSentences = doc.abstractSentences.map(f),
      divisions = doc.divisions.map(transform[Division]))
  }
  
  case class Sentence(
      val id: String,
      val wordsWithPosAndExtras: List[(String, String, Option[Map[String, String]])],
      val azCategory: String,
      val attributes: Map[String, String] = Map()) {

    lazy val wordsWithPos: List[(String, String)] = wordsWithPosAndExtras map { x => (x._1, x._2) }
    lazy val words = wordsWithPos.map(_._1)
    lazy val pos = wordsWithPos.map(_._2)
    lazy val length = wordsWithPos.length

    def withAzCategory(az: String) = {
      this.copy(azCategory = az)
    }

    /**
     * Merge all the words into a single string.
     */
    def mkString = {
      wordsWithPos.map(_._1).mkString(" ")
    }
  }

  trait HasSentences {
    def sentences: List[Sentence]
  }

  case class Paragraph(sentences: List[Sentence]) extends HasSentences {
  }

  case class Division(header: String,
                      depth: Int,
                      content: List[HasSentences], headerType: String = null) extends HasSentences {

    def sentences: List[Sentence] = {
      content.flatMap(_.sentences)
    }
  }

  case class Reference(text: String, xmlString: String) {
    override def toString = text.toString
    lazy val xml = {
    		val temp = {
    		  if (xmlString.startsWith("<REFERENCE")){
    		    xmlString
    		  }else{
    			  "<REFERENCE>" + xmlString + "</REFERENCE>"
    		  }
    		}
    		try{
    			XML.loadString(temp)
    		}catch{
    		  case e:Exception => {
    		    println(temp)
    		    println(e.printStackTrace())
    		  }
    		}
    }
  }
  
  case class Author(text: String, xmlString: String) {
    override def toString = text.toString
    lazy val xml = {
    		val temp = {
    		  if (xmlString.startsWith("<AUTHOR")){
    		    xmlString
    		  }else{
    		    "<AUTHOR>" + xmlString + "</AUTHOR>"
    		  }
    		}
    		try{
    			XML.loadString(temp)
    		}catch{
    		  case e:Exception => {
    		    println(temp)
    		    println(e.printStackTrace())
    		  }
    		}
    }
  }
  
  case class Document(val fileno: String, val title: String,
                      val authors: List[Author],
                      val abstractSentences: List[Sentence],
                      val divisions: List[Division],
                      val references: List[Reference],
                      val attributes: List[String]) extends HasSentences {

    // TODO (dk): Build lookup maps for the important stats, 
    //   and build a sequence of flattened sentences

    lazy val abstractText = abstractSentences.map(_.mkString).mkString("\n")

    lazy val text: String = {
      sentences.map(_.mkString).mkString("\n")
    }

    def allSentences = abstractSentences ++ sentences

    lazy val sentences: List[Sentence] = {
      divisions.flatMap(_.sentences)
    }

    lazy val sentenceIndexLookup = sentences.zipWithIndex.toMap

    // Builds a hash of sentence -> (sentence_loc_in_paragrah, paragraph_size) tuples.
    lazy val sentenceParaLookup = {
      var pcount = -1
      def collect(hs: HasSentences): List[(Sentence, Int, Int, Int)] = {
        hs match {
          case p: Paragraph =>
            pcount += 1
            p.sentences
              .zipWithIndex
              .map(x => (x._1, x._2, p.sentences.length, pcount))

          case d: Division =>
            d.content.flatMap(collect(_))
        }
      }
      val list = divisions.flatMap(collect(_))
      val abstractMap = abstractSentences.zipWithIndex.map(x => x._1 -> (x._2, abstractSentences.length, -1))
      ListMap() ++ abstractMap ++ list.map(x => x._1 -> (x._2, x._3, x._4))
    }

    // Builds a hash of sentence -> (div_header, sentence_loc_in_div, div_size) tuples.
    lazy val sentenceDivLookup = {
      def collect(hs: HasSentences): List[(Sentence, String, Int, Int)] = {
        hs match {
          case d: Division =>
            val plist = d.content
              .filter(_.isInstanceOf[Paragraph])
              .flatMap(_.sentences)

            val dlist = d.content
              .filter(_.isInstanceOf[Division])
              .flatMap(collect(_))

            plist.zipWithIndex
              .map(x => (x._1, d.header, x._2, plist.length)) ++ dlist

          case _ => List()
        }
      }
      val list = divisions.flatMap(collect(_))
      val abstractMap = abstractSentences.zipWithIndex.map(x => x._1 -> ("ABSTRACT", x._2, abstractSentences.length))
      ListMap() ++ abstractMap ++ list.map(x => x._1 -> (x._2, x._3, x._4))
    }

    lazy val allHeaders = {
      ListSet() ++ sentenceDivLookup.map(_._2._1)
    }

    def paragraphLocation(sentence: Sentence) = {
      val (_, _, loc) = sentenceParaLookup(sentence)
      loc
    }

    lazy val paragraphCount = {
      val (_, _, loc) = sentenceParaLookup.values.last
      loc
    }
    
    def isAbstractSentence(sentence: Sentence): Boolean = {
      sentenceDivLookup(sentence)._1 == "ABSTRACT"
    }

    //TODO: What if it's an abstract sentence?
    def locationWithinDocument(sentence: Sentence): (Int, Int) = {
      (sentenceIndexLookup(sentence), sentences.length)
    }

    def locationWithinParagraph(sentence: Sentence): (Int, Int) = {
      val (ploc, psize, _) = sentenceParaLookup(sentence)
      (ploc, psize)
    }

    def locationWithinRelativeDivision(sentence: Sentence): (Int, Int) = {
      val (_, dloc, dsize) = sentenceDivLookup(sentence)
      (dloc, dsize)
    }

    def firstAfterHeader(sentence: Sentence): Boolean = {
      val (_, dloc, _) = sentenceDivLookup(sentence)
      dloc == 0
    }

    def header(sentence: Sentence): String = {
      val (header, _, _) = sentenceDivLookup(sentence)
      header
    }
  }
}

object SciXmlReader {
  // XXX (dk): Hack to prevent doctype processing.
  def DoctypeIgnorantXML = {
    val f = javax.xml.parsers.SAXParserFactory.newInstance()
    f.setNamespaceAware(false)
    f.setValidating(false);
    //f.setFeature("http://apache.org/xml/features/disallow-doctype-decl", true);
    f.setFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd", false);
    XML.withSAXParser(f.newSAXParser())
  }

  def decodePos(wordWithPos: (String, String)): String = {
    wordWithPos._2 match {
      case "CREF"                   => "CREF"
      //case "XREF"                   => "XREF"
      case "EQN"                    => "EQN"
      case l if l.endsWith("-SELF") => "REF"
      case l if l.startsWith("REF") => "REF"
      case _                        => "POS"
    }
  }
}

object SciXmlParsing extends SciXmlParsing

trait SciXmlParsing {
  /**
   * Takes a list of <W C='PRP'>We</W>... and converts it into
   * a list of tuples (We,PRP), ... Handles other tags mixed in
   * as well.
   */
  def getWordsWithPosAndExtras(s: Node) = {
    import org.tempura.utils.XmlOps._
    def getRefPos(e: Node) = {
      e.attribute("SELF") match {
        case Some(on) => e.label + (if (on.text == "YES") "-SELF" else "")
        case None     => e.label
      }
    }
    val res = for (e <- s.child) yield {
      e match {
        // In the case that the scixml document has no W tags, we crudely split on space and add blank POS tags.
        case _: scala.xml.Text => e.text.split(" ").filter(_.trim().length != 0).map((_, "", None)).toList
        case e2 if e2.label == "XREF" =>
          // NOTE: Chemistry (AZ-2) use XREF tags to encode CREF, with richer information, 
          //       including attributes and content within the tag. We here convert it to 
          //       a CREF and create a NN token before it, as this is how AZ-1 CREF
          //       annotation worked. If the XREF has no content, though, we do not create
          //       another token preceeding it.
          val attrs = e.attributes.toStringMap
          if (e.text.isEmpty)
            List(("", "CREF", None))
          else
            List(
              (attrs("TYPE"), "NN", None),
              ("", "CREF", Some(attrs)))
        case _ =>
          List(e.label match {
            case "W"         => (e.text, (e \ "@C").text, None)
            case "REFAUTHOR" => (e.text, getRefPos(e), e.attributes.toStringMapOpt)
            case "REF"       => (e.text, getRefPos(e), e.attributes.toStringMapOpt)
            case "CREF"      => (e.text, "CREF", e.attributes.toStringMapOpt)
            case "EQN"       => (e.text, "EQN", e.attributes.toStringMapOpt)
            case l           => (e.text, l, e.attributes.toStringMapOpt)
          })
      }
    }

    res.flatten.toList
  }

  def parseSentence(s: Node): ParsedSciXml.Sentence = {
    import org.tempura.utils.XmlOps._
    new ParsedSciXml.Sentence((s \ "@ID").text, getWordsWithPosAndExtras(s), (s \ "@AZ").text,
      attributes = s.attributes.toStringMap.filterKeys(k => k != "AZ" && k != "ID"))
  }

  def parseParagraph(p: Node): ParsedSciXml.Paragraph = {
    val sentences = for (s <- p \ "S") yield parseSentence(s)
    new ParsedSciXml.Paragraph(sentences.toList)
  }

  def parseAbstract(a: Node) = {
    	val sentences = for (s <- a \ "A-S") yield parseSentence(s)
    	val list = sentences.toList
    	if (list.isEmpty && (! a.isEmpty)) {
    		List(parseSentence(<A-S>{ a text }</A-S>))
    	} else {
    		list
    	}
  }

  def parseDivision(d: Node): ParsedSciXml.Division = {
    val depth = if (d \ "@DEPTH" isEmpty) { 0 } else{(d \ "@DEPTH" text).toInt}
    val headerType = if (d \ "@TYPE" isEmpty){ null} else{d \ "@TYPE" text} 
    val header = d \ "HEADER" text
    def ignored(l: String) = {
      l match {
        case "HEADER" | "IMAGE" | "EXAMPLE-SENT" | "EQN" | "CAPTION" | "EXAMPLE" => true
        case _ => false
      }
    }
    val data = for (e <- d \ "_"; l = e.label; if !ignored(l)) yield {
      e.label match {
        case "P"   => parseParagraph(e)
        case "DIV" => parseDivision(e)
        case _     => throw new Exception("Unexpected element %s found!".format(e.label))
      }
    }
    new ParsedSciXml.Division(header, depth, data.toList, headerType)
  }
}

/**
 * This class reads in a SciXml file and parses it to
 * return a ParsedSciXmlDocument.
 */
class SciXmlReader(stream: java.io.InputStream, path: String = null) extends SciXmlParsing {

  def this(file: File) {
    this(new java.io.FileInputStream(file), file.getAbsolutePath())
  }

  import SciXmlReader._
  def parse() = {
    try {
      val root = try {
        DoctypeIgnorantXML.load(stream)
      } catch {
        case e: MalformedByteSequenceException =>
          println("Malformed UTF detected in file %s: %s".format(path, e.getMessage))
          // Try fixing and going once more
          //println("Attempting fix in-place (backup of original will be made)...")
          //EncodingUtils.stripNonValidXMLCharacters(path)
          //DoctypeIgnorantXML.loadFile(new java.io.File(path))
          throw e
      }

      val fileno = root \\ "FILENO" text
      val abstr = {  
        if ((root \ "ABSTRACT").text.equals("")){
          List[ParsedSciXml.Sentence]();
        }else{
           parseAbstract(root \ "ABSTRACT" head)
        }
      }
      val title = {
        if ((root \ "TITLE").text.equals("")) {
          root \\ "CURRENT_TITLE" text
        } else {
          root \ "TITLE" text
        }
      }
      
   //   val authors = (root \ "AUTHORS" \ "AUTHOR" map (_.text) toList) ++ (root \ "CURRENT_AUTHORLIST" \ "CURRENT_AUTHOR" map (_.text) toList)
     
   //   val authors = (root \ "AUTHORS" \ "AUTHOR" toList) ++ (root \ "CURRENT_AUTHORLIST" \ "CURRENT_AUTHOR" toList)
      val authors = (root \ "AUTHORS" \ "AUTHOR") ++ (root \ "CURRENT_AUTHORLIST" \ "CURRENT_AUTHOR") map (a => ParsedSciXml.Author(a.text,a.toString)) toList;
      
      val divs = for (div <- root \ "BODY" \ "DIV") yield parseDivision(div)
      // XXX: We lose additional formatting of refs here.
      val refs = ((root \ "REFERENCES") ++ (root \ "REFERENCELIST")) \ "REFERENCE" map (r => ParsedSciXml.Reference(r.text, r.toString)) toList;
      new ParsedSciXml.Document(fileno, title, authors, abstr, divs.toList, refs, List[String]())
    } catch {
      case e: org.xml.sax.SAXParseException =>
        println("Error with file %s: %s".format(path, e.getMessage))
        throw e
    }
  }
  
}

class SciXmlWriter(doc: ParsedSciXml.Document, outputPos: Boolean, outputAZ: Boolean, indentation: Int = 4) {
  import scalax.io.JavaConverters._
  import ParsedSciXml._
  import org.tempura.utils.StringOps._
  import scala.xml.{ XML, Node, Attribute, Text, Null }
  import org.tempura.utils.XmlOps._

  def sentenceAsXml(tagName: String)(s: Sentence): Node = {
    val words = (for ((word, pos, extras) <- s.wordsWithPosAndExtras) yield {
      if (pos =~ "^REF") {
        if (pos =~ "-SELF") {
          val tag = <TMP>{ word }</TMP>.copy(label = pos.takeWhile(_ != '-')) % Attribute(None, "SELF", Text("YES"), Null)
          tag.addAttributes(extras.getOrElse(Map()))
        } else {
          val tag = <TMP>{ word }</TMP>.copy(label = pos)
          tag.addAttributes(extras.getOrElse(Map()))
        }
      } else if (pos =~ "^EQN|^CREF") {
        <TMP/>.copy(label = pos)
      } else {
        if (outputPos) <W C={ pos }>{ word }</W>
        else new Text(word)
      }
    }).map(_.toString).mkString(" ")

    val tag = if (outputAZ){
    	<S ID={ s.id } AZ={ s.azCategory }>{ new scala.xml.Unparsed(words) }</S>.copy(label = tagName)
    }else{
         <S ID={ s.id }>{ new scala.xml.Unparsed(words) }</S>.copy(label = tagName)
    }
    tag.addAttributes(s.attributes)
  }

  def abstractAsXml(doc: Document): NodeSeq = {
    NodeSeq fromSeq doc.abstractSentences.map(sentenceAsXml("A-S") _).toSeq
  }

  def contentAsXml(doc: Document): NodeSeq = {
    def transform[T <: HasSentences](hs: T)(depth: Int): Node = {
      hs match {
        case d: Division => 
          val headerType = if (d.headerType == null || d.headerType.equals("")){ ""} else {" TYPE=" + d.headerType}
          if (d.headerType == null || d.headerType.equals("")){
            <DIV DEPTH={ "" + depth}>
            <HEADER>{ d.header.trim }</HEADER>
            { d.content.map(transform[HasSentences](_)(depth + 1)) }
            </DIV>
          }else{
            <DIV DEPTH={"" + depth} TYPE={d.headerType}>
            <HEADER>{ d.header.trim }</HEADER>
            { d.content.map(transform[HasSentences](_)(depth + 1)) }
            </DIV>
          }
          /*
          if (outputAZ){
             <DIV DEPTH={ "" + depth}>
            <HEADER>{ d.header.trim }</HEADER>
            { d.content.map(transform[HasSentences](_)(depth + 1)) }
            </DIV>
          }else{
            <DIV DEPTH={ "" + depth + headerType}>
            <HEADER>{ d.header.trim }</HEADER>
            { d.content.map(transform[HasSentences](_)(depth + 1)) }
            </DIV>
          }
          */
        case p: Paragraph => <P>
                               { p.sentences.map(sentenceAsXml("S") _) }
                             </P>
      }
    }
    doc.divisions.map(transform[Division](_)(0))
  }

  lazy val xmlString = {
    val body = contentAsXml(doc)
    val xml = <PAPER>
                <FILENO>{ doc.fileno }</FILENO>
    			{for (a <- doc.attributes) yield {
    			  try{
    			    XML.loadString(a)
    			  }catch{
    			     case e:Exception => {
    			    	 println(a)
    			    	 println(e.printStackTrace())
    			     }
    			}}
    			}
                <TITLE>{ doc.title }</TITLE>
                <AUTHORS>
                  {
                    doc.authors.map { a =>
       //               <AUTHOR>{ a }</AUTHOR>
                      {a.xml}
                    }
                  }
                </AUTHORS>
                <ABSTRACT>{ abstractAsXml(doc) }</ABSTRACT>
                <BODY>
                  { body }
                </BODY>
                <REFERENCES>
                  {
                    doc.references.map { r =>
                      if (r.xml != null)
                        //new scala.xml.Unparsed(r.xml)
                        {r.xml}
                      else
                        <REFERENCE>{ r.text.trim }</REFERENCE>
                    }
                  }
                </REFERENCES>
              </PAPER>
    val header = """|<?xml version='1.0' encoding='UTF-8'?>
            |<!DOCTYPE PAPER SYSTEM "/homes/sht25/dtd/s.dtd" >""".stripMargin
    val pp = new PrettyPrinter(Int.MaxValue, indentation)
    header + "\n" + pp.format(xml)
  }

  def write(file: File) {
    file.asOutput.write(xmlString)
  }

  def write(stream: java.io.OutputStream) {
    stream.write(xmlString.getBytes);
  }
}
