package uk.ac.cam.cl.azprime.ml.features

import uk.ac.cam.cl.azprime.xml.ParsedSciXml
import uk.ac.cam.cl.azprime.ml.ExtractedFeature
import uk.ac.cam.cl.azprime.ml.FeatureExtractor
import uk.ac.cam.cl.azprime.ml.ExtractedFeatures
import uk.ac.cam.cl.azprime.ml.SingleFeatureExtractor

object ParagraphLocationValues extends Enumeration {
  type Values = Value

  val First = Value("i")
  val Last = Value("f")
  val Middle = Value("m")
}

case class ParagraphLocation(val value: ParagraphLocationValues.Values) extends ExtractedFeature {
  type ValueType = ParagraphLocationValues.Values
  val label = "PAR"
}

class ParagraphLocationExtractor extends SingleFeatureExtractor {
  import ParagraphLocationValues._
  type Extracted = ParagraphLocation
  def extract(sentence: ParsedSciXml.Sentence, document: ParsedSciXml.Document, features: ExtractedFeatures) = {
    val (loc, total) = document.locationWithinParagraph(sentence)
    val cat =
      if (loc == 0) First
      else if (loc + 1 == total) Last
      else Middle
      
    new ParagraphLocation(cat)
  }
}