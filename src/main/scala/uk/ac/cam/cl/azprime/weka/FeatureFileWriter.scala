package uk.ac.cam.cl.azprime.weka

import uk.ac.cam.cl.azprime.ml.FeatureEngine
import uk.ac.cam.cl.azprime.xml.ParsedSciXml
import uk.ac.cam.cl.azprime.ml.ExtractedFeature

class FeatureFileWriter (data: List[(ParsedSciXml.Document, FeatureEngine.DocumentFeatures)])(ignored: Set[String]){
 import org.tempura.utils.StringOps._
  import scalax.io.JavaConverters._

  def this(document: ParsedSciXml.Document, sentences: FeatureEngine.DocumentFeatures)(ignored: Set[String]) = {
    this(List((document, sentences)))(ignored)
  }

  lazy val (samples, observedValues, observedTypes, discreteValues) = {
    import collection.mutable.Map
    import collection.mutable.Set

    val observedValues = Map[Class[_], Set[String]]().withDefaultValue(Set[String]())
    val observedTypes = Map[Class[_], Class[_]]()
    val discreteValues = Map[Class[_], Boolean]() withDefaultValue (true)

    def build(document: ParsedSciXml.Document, sentences: FeatureEngine.DocumentFeatures) = {
      val samples = for ((sentenceFeatures, sentence) <- sentences zip document.sentences) yield {
        val strlist = sentenceFeatures.filter(x => !ignored(x.label)).map { x =>
          val valstr = x.value.toString
          x.value match {
            case v: Number =>
              discreteValues(x.getClass) = false
              observedTypes(x.getClass) = v.getClass
            case v @ _ =>
              // We want to make strings "class" types as long as they:
              // a) have less than 50 unique values
              // b) do not have any spaces in them
              // c) are less than 25 characters long
              observedTypes(x.getClass) = classOf[String]
              if (valstr.indexOf(" ") > -1 || valstr.length > 25 ||
                observedValues(x.getClass).size > 50) {
                discreteValues(x.getClass) = false
                observedValues(x.getClass) = collection.mutable.Set()
              } else if (discreteValues(x.getClass)) {
                observedValues(x.getClass) = observedValues(x.getClass) + valstr
                discreteValues(x.getClass) = true
              }
          }

          x.getClass -> valstr
        }
        strlist.toMap[Class[_], String]
      }
      samples
    }

    (data.flatMap(x => build _ tupled x),
      observedValues toMap,
      observedTypes toMap,
      discreteValues toMap)
  }

  def writeFile(file: java.io.File) = {
    val buff = new StringBuilder()
    buff append "@relation 'az'\n\n"
    for (ot <- observedTypes.keys.toList.sortBy(x => x.getSimpleName)) {
      buff append "@attribute "
      buff append ot.getSimpleName
      if (discreteValues(ot) == true) {
        buff append " {"
        buff append observedValues(ot).mkString(",")
        buff append "}"
      } else if (classOf[Number].isAssignableFrom(observedTypes(ot))) {
        buff append " numeric"
      } else {
        buff append " string"
      }
      buff append "\n"
    }
    buff append "\n@data\n"
    for (sample <- samples) {
      // Adjust the feature vector so we have empty slots for missing ones
      val strlist = for (ot <- observedTypes.keys.toList.sortBy(x => x.getSimpleName)) yield {
        val v = if (sample.contains(ot)) sample(ot) else "?"
        if ((discreteValues(ot) == false && classOf[String] == observedTypes(ot)) ||
          v.indexOf(" ") > -1) v replaceAll ("'", """\\'""") wrap "'"
        else v
      }
      buff append strlist.mkString(",")
      buff append "\n"
    }
    file.asOutput.write(buff.toString)
    buff clear
  }
}