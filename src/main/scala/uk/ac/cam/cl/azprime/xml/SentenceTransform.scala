package uk.ac.cam.cl.azprime.xml

class SentenceTagger (tagger: edu.stanford.nlp.tagger.maxent.MaxentTagger) {
	//	lazy val factory = edu.stanford.nlp.process.PTBTokenizer.PTBTokenizerFactory.newCoreLabelTokenizerFactory("ptb3Escaping=false")

	val xmlRewriter = new scala.xml.transform.RewriteRule {
		def processText(str: String): String = {	    
			var start = 0;
			val pre = new StringBuilder();

			while(start < str.length() && (str.charAt(start) == '"' || str.charAt(start) == '\'')){
				pre.append(str.charAt(start))
				if (start + 1 < str.length()) pre.append(" ")
				start = start + 1;
			}

			var end = str.length()
			val post = scala.collection.mutable.ListBuffer[String]()

			while(end > start && (str.charAt(end-1) == '"' || str.charAt(end-1) == '\'' || str.charAt(end-1) == '.' || str.charAt(end-1) == '?' || str.charAt(end-1) == '!' || str.charAt(end-1) == '-' || str.charAt(end-1) == ',' )){
				post.prepend(" " + str.charAt(end-1))
				end = end - 1
			}

			pre.toString + str.substring(start,end) + post.mkString("")
		}

		override def transform(n: scala.xml.Node): Seq[scala.xml.Node] = n match {
			case n: scala.xml.Elem if n.label == "S" || n.label == "A-S" => n.asInstanceOf[scala.xml.Elem].copy(child = n.child flatMap (this transform))
			case n: scala.xml.Elem => n
			case n: scala.xml.Text => new scala.xml.Text((" " + n.text.split("\\s+").map(processText).mkString(" ") + " ").replaceAll("\\s+"," "))
		}
	}

	def addPosTags(doc: ParsedSciXml.Document): ParsedSciXml.Document = {
		ParsedSciXml.replaceSentences(doc,tagTokenisedSentence(_))
	}

	def tagUntokenisedSentence(sNode: scala.xml.Node): List[(String, String, Option[Map[String, String]])] = {
		import scala.collection.JavaConverters._
		import org.tempura.utils.XmlOps._

		//println("Tagging " + sNode)

		//  val tokenised = factory.getTokenizer(new java.io.StringReader(sNode.text)).tokenize()

		val transformedNodes = xmlRewriter transform sNode
		val transformedNode = transformedNodes(0)
		val text = transformedNode text
		val tokeniser = edu.stanford.nlp.process.WhitespaceTokenizer.newCoreLabelWhitespaceTokenizer(new java.io.StringReader(text))
		val tokenised = tokeniser.tokenize()
		//  println(transformedNode)
		// println(text)
		// println(tokenised.asScala.map(_.word()).mkString(" "))
		tagger.tagCoreLabels(tokenised)
		//  tokenised.asScala.foreach {x => println(x.beginPosition + " " + x.endPosition + " " + x.word)}

		val w_it = tokenised.asScala.iterator

		val nodes = transformedNode.child
		val node_it = nodes.iterator
		var curr_node = node_it.next()
		var endOfCurrNode = curr_node.text.length
		//  println ("End is " + endOfCurrNode + " " + text.charAt(endOfCurrNode-1))

		var words = List[(String, String, Option[Map[String, String]])]()

		while(w_it.hasNext){
			var word = w_it.next()
			if (word.beginPosition() >= endOfCurrNode){
				//		println(word.word + ": " + word.beginPosition() + " is greater than or equal to " + endOfCurrNode)
				curr_node = node_it.next()
				//		println("Chomped " + curr_node + ", " + node_it.hasNext)

				while(curr_node.text.length == 0){
					val tuple = (curr_node.text,curr_node.label,Option(curr_node.attributes.toStringMap))
					//		  	println(tuple)
					words = words :+ tuple
					curr_node = node_it.next()
					//		    println("Chomped " + curr_node + ", " + node_it.hasNext)
				}

				endOfCurrNode += curr_node.text.length
				//	    println ("End is " + endOfCurrNode + " " + text.charAt(endOfCurrNode-1))
			}else{
				//	  println(word.word + ": " + word.beginPosition() + " is less than " + endOfCurrNode)
			}

			if (curr_node.isInstanceOf[scala.xml.Text]){
				val tuple =  (word.word(),word.tag(),Option(Map[String,String]()))
				//		println(tuple)
				words = words :+ tuple	
			}else{
				val end = curr_node.text.length + word.beginPosition
				var sb = new StringBuilder()
				while(word.endPosition < end){
					word = w_it.next
					sb.append(word.word)
					//			println(word.word + " " + word.endPosition + " " + end)
				}

				val tuple = (curr_node.text,curr_node.label,Option(curr_node.attributes.toStringMap))
				//		println(tuple)
				words = words :+ tuple
			}
		}

		words
	}

	def tagTokenisedSentence(sen: ParsedSciXml.Sentence): ParsedSciXml.Sentence = {	  
		val string = sen.words.mkString(" ")
		if(string.matches("\\s*")){
			new ParsedSciXml.Sentence(sen.id,List[(String,String,Option[Map[String,String]])](),sen.azCategory,sen.attributes)
		}else{
			val tagged = tagger.tagTokenizedString(string).split(" ")

			def splitPos(output : String): (String,String) =  {
				val splitPoint = if (output.lastIndexOf("_") != -1){
					output.lastIndexOf("_");
				}else{
					output.lastIndexOf("/");
				}

				assert(splitPoint != -1,output.toString + "!" + string)
				(output.substring(0,splitPoint),output.substring(splitPoint+1))
			}

			val tagged_it = tagged.iterator

			val wordsWithPosAndExtras = sen.wordsWithPosAndExtras.map(x=>
				if (x._2.endsWith("REF")){
					x
				}else {
					assert(tagged_it.hasNext,sen.wordsWithPos + "\n" + tagged);
					val sp = splitPos(tagged_it.next())
							(sp._1,sp._2,x._3)
				}
			)

			new ParsedSciXml.Sentence(sen.id,wordsWithPosAndExtras,sen.azCategory,sen.attributes)
		}
	}
}

class SentenceSplitter () {
	val factory = edu.stanford.nlp.process.PTBTokenizer.PTBTokenizerFactory.newCoreLabelTokenizerFactory("ptb3Escaping=false")

	def splitString(par: scala.xml.Node, id: String): List[ParsedSciXml.Sentence] = {
		import scala.collection.JavaConverters._
		val prep = new edu.stanford.nlp.process.DocumentPreprocessor(new java.io.StringReader(par.text))
		prep.setTokenizerFactory(factory) 
		splitDocument(prep,par.child.toList)
	}

	def splitDocument(prep: edu.stanford.nlp.process.DocumentPreprocessor, nodes: List[scala.xml.Node]): List[uk.ac.cam.cl.azprime.xml.ParsedSciXml.Sentence] = {
		import scala.collection.JavaConverters._
		import org.tempura.utils.XmlOps._
		val sen_it = prep.asScala.iterator
		val node_it = nodes.iterator

		//	println(nodes)

		var out = List[uk.ac.cam.cl.azprime.xml.ParsedSciXml.Sentence]()

		if (node_it.hasNext){
			var curr_node = node_it.next()
				var endOfCurrNode = curr_node.text.length

				while(sen_it.hasNext){
					var words = List[(String, String, Option[Map[String, String]])]()

					val sen = sen_it.next().asScala.map(_.asInstanceOf[edu.stanford.nlp.ling.CoreLabel])

					val w_it = sen.iterator
					while(w_it.hasNext){
						var word = w_it.next()
						if (word.beginPosition() >= endOfCurrNode){
							curr_node = node_it.next()

							while(curr_node.text.length == 0){
								val tuple = (curr_node.text,curr_node.label,Option(curr_node.attributes.toStringMap))
								words = words :+ tuple
								curr_node = node_it.next()
							}

							endOfCurrNode += curr_node.text.length
						}

						if (curr_node.isInstanceOf[scala.xml.Text]){
							val tuple =  (word.word(),"",Option(Map[String,String]()))
							//				println(tuple)
							words = words :+ tuple
						}else{
							val end = curr_node.text.length + word.beginPosition
							var sb = new StringBuilder()
							while(word.endPosition < end){
								word = w_it.next
								sb.append(word.word)
							}

							val tuple = (curr_node.text,curr_node.label,Option(curr_node.attributes.toStringMap))
							//				println(tuple)
							words = words :+ tuple
						}
					}

					val new_sen = new uk.ac.cam.cl.azprime.xml.ParsedSciXml.Sentence("",words,"",Map())
					//		println(new_sen)
					out = out :+ new_sen
				}
		}
		out
	}

	def processText(text: scala.xml.Text, it: Iterator[edu.stanford.nlp.ling.CoreLabel]): List[(String, String, Option[Map[String, String]])] = {
		var list = List[(String, String, Option[Map[String, String]])]()
		var len = 0
		while(len < text.text.length){
			val next = it.next()
			list = list :+ (next.word(),"",None)
			len = next.endPosition()
		}
		list
	}

	def processElem(elem: scala.xml.Elem, it: Iterator[edu.stanford.nlp.ling.CoreLabel]): List[(String, String, Option[Map[String, String]])] = {
		import org.tempura.utils.XmlOps._
		var len = 0
		val sb = new StringBuilder()

		while (len < elem.text.length){
			val next = it.next()
					len = next.endPosition()
		}

		List((elem.text,elem.label,Option(elem.attributes.toStringMap)))
	}

	def makeSentence(haswords: List[edu.stanford.nlp.ling.CoreLabel], par: scala.xml.Node, id: String): ParsedSciXml.Sentence = {
		val nodes = par.child
		val it = haswords.iterator

		val words = nodes.flatMap(x => x match{
			case x: scala.xml.Text => processText(x,it)
			case x: scala.xml.Elem => processElem(x,it)
		}).toList

		new ParsedSciXml.Sentence(id,words,"",Map())	  
	}
}
