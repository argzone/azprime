package uk.ac.cam.cl.azprime.resources

import java.io.InputStream

case class Pattern(override val pattern: String, override val label: String)
  extends FormulaicExpression(pattern, label)

object Patterns {
  val PatternLinePattern = """(.*?)[\s\t]+([a-zA-Z_]+)""".r
}

class Patterns(private val instream: InputStream) {
  val lines = io.Source.fromInputStream(instream).getLines

  private val (_wordTriggers, _posTriggers, _fe) = {
    val fe = lines.map(_.trim).filter(!DataFile.isComment(_)).map(l => {
      val Patterns.PatternLinePattern(expression, category) = l
      Pattern(expression, category)
    }).toList
    val wt = fe.
      filter(_.trigger.isInstanceOf[FormulaicExpression.Word]).
      map(x => (x.trigger.value.toString, x)).
      groupBy(x => x._1).
      map(x => (x._1, x._2.map(_._2))).
      toMap withDefaultValue List()
    val pt = fe.
      filter(_.trigger.isInstanceOf[FormulaicExpression.POS]).
      map(x => (FormulaicExpression.posMappings(x.trigger.asInstanceOf[FormulaicExpression.POS].value), x)).
      groupBy(x => x._1).
      map(x => (x._1, x._2.map(_._2))).
      toMap withDefaultValue List()
    (wt, pt, fe)
  }

  val expressions: List[FormulaicExpression] = _fe

  val expressionsByLabel = expressions.groupBy(x => x.label)  

  /**
   * Key may be either:
   * - A normal word
   * - A variable that has been substituted for all its values (@VAR)
   */
  val wordTriggers: Map[String, List[FormulaicExpression]] = _wordTriggers

  /**
   * Slightly more work than for words. Here we iterate once over all pos keys,
   * testing if the pos expressions (regexes) match, and then for any/all matching,
   * we merge them into a single list of patterns. Given a certain input, the output
   * is always guaranteed, so caching of results could be done.
   */
  def posTriggers(pos: String): List[FormulaicExpression] = {
    import org.tempura.utils.StringOps._
    val matched = _posTriggers.keys.filter(pos =~ _)
    matched.flatMap(k => _posTriggers(k)).toList
  }

  /**
   * A set of all the expressions that must be manually checked.
   */
  val bruteForceExpressions = Set[FormulaicExpression]()

  // Takes a list of (word, pos) tuples
    def matches(input: List[(String, String)])(implicit lexicon: Lexicon) = {
    val matched =
      for (
        ((word, pos), idx) <- input zipWithIndex;
        expression <- wordTriggers(word) ++ posTriggers(pos) ++ bruteForceExpressions
      ) yield {
        val triggerOffset = expression.triggers.headOption.getOrElse(("",0))._2
        val startOffset = idx - triggerOffset
        FormulaicExpression.matches(expression, input, startOffset) map { x =>
          ExpressionMatch(expression, x._1, x._2)
        }
      }
    matched flatten
  }
}
