package uk.ac.cam.cl.azprime

import uk.ac.cam.cl.azprime.xml.ParsedSciXml
import uk.ac.cam.cl.util.IndexableDocument
import uk.ac.cam.cl.util.TfIdfScores
import uk.ac.cam.cl.azprime.resources.StopWords

object TfIdfTypes {
	type DocumentTfIdfScores = MappedTfIdfScores[ParsedSciXml.Document]
}

// Computes tfidf scores for all words in the bodies of all documents
// Provides a lookup for mapping ParsedSciXml.Document -> IndexableDocument
object BodyTfIdfScores {
  def apply(docs: List[ParsedSciXml.Document])(implicit stopwords: StopWords) = {
    new MappedTfIdfScores[ParsedSciXml.Document](docs, BodyTfIdfScores.getWords _)
  }
  def getWords(doc: ParsedSciXml.Document) = {
    // NOTE: this should probably be "doc.sentences" instead.
    doc.allSentences.flatMap(_.words)
  }
//  override val serialVersionUID = 3L
}

// Computes tfidf scores for all words in titles (and optionally headers) for all documents
// Provides a lookup for mapping ParsedSciXml.Document -> IndexableDocument
object TitleTfIdfScores {
  def apply(docs: List[ParsedSciXml.Document],
            includeHeaders: Boolean = true)(implicit stopwords: StopWords) = {
    new MappedTfIdfScores[ParsedSciXml.Document](docs, TitleTfIdfScores.getWords(includeHeaders) _)
  }
  // TODO (dk): This should properly segment the title additional characters, like -
  def getWords(includeHeaders: Boolean)(doc: ParsedSciXml.Document) = {
    val strings = doc.title :: (if (includeHeaders) doc.allHeaders.toList else List())
    val words = strings.flatMap(h => h.split(" "))
    words
  }
   
//  override val serialVersionUID = 2L
}

@serializable @SerialVersionUID(321478543795379732L)
final class MappedTfIdfScores[T] protected[azprime] (docsLookup: Map[T, IndexableDocument], getWords: T => Seq[String])(implicit stopwords: StopWords) {
  protected[azprime] def this(docs: List[T], getWords: T => Seq[String])(implicit stopwords: StopWords) {
    this(docs.zip(docs.map(d => new IndexableDocument(getWords(d)))).toMap, getWords)
  }

  def addDocument(doc: T) = {
    new MappedTfIdfScores[T](docsLookup + (doc -> new IndexableDocument(getWords(doc))), getWords)
  }

  val calculator = new TfIdfScores(docsLookup.values.toList)

  def termCounts = calculator.globalTermCounts

  def tfidf(word: String, doc: T) = {
    calculator.tfidf(word, docsLookup.getOrElse(doc, new IndexableDocument(getWords(doc))))
  }
  
//  val serialVersionUID= 1L
}