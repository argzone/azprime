package uk.ac.cam.cl.azprime.ml.features

import uk.ac.cam.cl.azprime.xml.ParsedSciXml._

class CitationBlockExtractor {    
	def extractCitations(doc:Document): Map[String,List[Int]] = {
      val cits = doc.allSentences.zipWithIndex.flatMap(x => getCitationsForSentence(x._1).map(s => (s,x._2)))
      val grouped = cits.groupBy(_._1).mapValues(_.map(_._2))
      grouped
	}
    
    def getCitationsForSentence(sen: Sentence): Set[String] = {
      val list = sen.wordsWithPosAndExtras.filter(_._2 == "REF").map({
          x => x._3.getOrElse(Map[String,String]()).getOrElse("REFID",x._1).toString
      })
      list.toSet
    }

}