package uk.ac.cam.cl.azprime.ml.features

import uk.ac.cam.cl.azprime.resources.StopWords
import uk.ac.cam.cl.azprime.xml.ParsedSciXml
import uk.ac.cam.cl.azprime.BodyTfIdfScores
import uk.ac.cam.cl.azprime.ml.ExtractedFeature
import uk.ac.cam.cl.azprime.ml.FeatureExtractor
import uk.ac.cam.cl.azprime.ml.ExtractedFeatures
import uk.ac.cam.cl.azprime.ml.SingleFeatureExtractor
import uk.ac.cam.cl.azprime.resources.Actions
import uk.ac.cam.cl.azprime.resources.Agents
import uk.ac.cam.cl.azprime.resources.Lexicon
import uk.ac.cam.cl.azprime.verbs.SemanticParse
import uk.ac.cam.cl.azprime.resources.Action
import uk.ac.cam.cl.azprime.resources.ExpressionMatch
import uk.ac.cam.cl.azprime.verbs.SemanticFiniteVerb
import uk.ac.cam.cl.azprime.ml.MultipleFeatureExtractor
import uk.ac.cam.cl.azprime.resources.FormulaicExpression
import uk.ac.cam.cl.azprime.resources.ExpressionMatch.BasicExpressionMatch

case class SemanticVerbs(val value: Map[SemanticFiniteVerb, (Option[ExpressionMatch[Action]], Option[BasicExpressionMatch], Option[Int])]) extends ExtractedFeature {
  type ValueType = Map[SemanticFiniteVerb, (Option[ExpressionMatch[Action]], Option[BasicExpressionMatch], Option[Int])]
  val label = "SEMVERBS"
}

class SemanticVerbsExtractor(scanWindowSize: Int)(implicit lexicon: Lexicon, actions: Actions, agents: Agents) extends SingleFeatureExtractor {
  type Extracted = SemanticVerbs
  def extract(sentence: ParsedSciXml.Sentence, document: ParsedSciXml.Document, features: ExtractedFeatures) = {

    val parse = new SemanticParse(sentence.wordsWithPos)(actions.perceptionVerbs, scanWindowSize)
    val verbs = {
      for (verb <- parse.semanticFiniteVerbs) yield {
        verb -> (
          parse actionforVerb verb,
          parse agentForVerb verb,
          parse negationForVerb verb)
      }
    }.toMap
    SemanticVerbs(verbs)
  }
}

// We separate this from the actual extraction of the semantic-verb to make processing clearer,
// and to more easily allow adding more features later that utilise the semantic verb extraction.

case class SemanticVerbTense(val value: String) extends ExtractedFeature {
  type ValueType = String
  val label = "SVTENSE"
}
case class SemanticVerbVoice(val value: String) extends ExtractedFeature {
  type ValueType = String
  val label = "SVVOICE"
}
case class SemanticVerbNeg(val value: String) extends ExtractedFeature {
  type ValueType = String
  val label = "SVNEG"
}
case class SemanticVerbAction(val value: String) extends ExtractedFeature {
  type ValueType = String
  val label = "SVACT"
}
case class SemanticVerbAgent(val value: String) extends ExtractedFeature {
  type ValueType = String
  val label = "SVAGT"
}
case class SemanticVerbExpressions(val value: String) extends ExtractedFeature {
  type ValueType = String
  val label = "SVPATTERNS"
}

class SimplifiedSemanticVerbsExtractor extends MultipleFeatureExtractor {
  type Extracted = ExtractedFeature

  // Declare our dependencies to require them to be run first.
  dependsOn[SemanticVerbsExtractor]

  def extract(sentence: ParsedSciXml.Sentence, document: ParsedSciXml.Document, features: ExtractedFeatures) = {
    val verbs = features[SemanticVerbs]

    // As the original system did, we only output info for the first verb with an agent.
    // If no verb had an agent, then we'll get back a None so empty strings will be output. 
    val firstVerbWithAgentOpt = verbs.value.find(x=>x._2._2.isDefined)
    val vals = for ((verb, (actionOpt, agentOpt, negOpt)) <- firstVerbWithAgentOpt) yield {
      val finite = sentence.words(verb.finitePosition)
      val semantic = if (verb.semanticPosition > -1) sentence.words(verb.semanticPosition) else ""
      val verbTense = "%s".format(verb.tense)
      val verbVoice = "%s".format(verb.voice)
      val verbNeg = "%s".format(if (negOpt.getOrElse(-1) > -1) "NEG" else "POS")
      val action = actionOpt.map(x => "%s".format(x.expression.pattern replaceAll (" ", "_"))) getOrElse ""
      val agent = agentOpt.map(x => "%s".format(x.expression.label)) getOrElse ""
      val verbAction = actionOpt.map(x => "%s".format(x.expression.label)) getOrElse ""
      val verbAgent = agentOpt.map(x => "%s".format(x.expression.pattern replaceAll (" ", "_"))) getOrElse ""
      val matchedExp = List(actionOpt, agentOpt).flatten.
        map(x => "%s".format(x.from(sentence.words).mkString("_"))) mkString " " replaceAll(",","")
      (verbTense, verbVoice, verbNeg, action, agent, verbAgent, verbAction, matchedExp)
    }
    
    // Output a list of a bunch of features that are string simplifications
    List(
      SemanticVerbTense(vals.map(_._1) mkString " "),
      SemanticVerbVoice(vals.map(_._2) mkString " "),
      SemanticVerbNeg(vals.map(_._3) mkString " "),
      SemanticVerbAction(vals.map(_._4) mkString " "),
      SemanticVerbAgent(vals.map(_._5) mkString " "),
      SemanticVerbExpressions(vals.map(_._8) mkString " "))
  }
}
