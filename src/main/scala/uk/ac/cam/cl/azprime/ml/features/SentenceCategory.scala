package uk.ac.cam.cl.azprime.ml.features

import uk.ac.cam.cl.azprime.xml.ParsedSciXml
import uk.ac.cam.cl.azprime.ml.ExtractedFeature
import uk.ac.cam.cl.azprime.ml.FeatureExtractor
import uk.ac.cam.cl.azprime.ml.ExtractedFeatures
import uk.ac.cam.cl.azprime.ml.SingleFeatureExtractor

case class SentenceCategory(val value: String) extends ExtractedFeature {
  type ValueType = String
  val label = "AZ"
}

class SentenceCategoryExtractor extends SingleFeatureExtractor {
  type Extracted = SentenceCategory
  def extract(sentence: ParsedSciXml.Sentence, document: ParsedSciXml.Document, features: ExtractedFeatures) = {
    new SentenceCategory(sentence.azCategory)
  }
}