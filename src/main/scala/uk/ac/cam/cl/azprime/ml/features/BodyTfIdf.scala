package uk.ac.cam.cl.azprime.ml.features

import uk.ac.cam.cl.azprime.xml.ParsedSciXml

import uk.ac.cam.cl.azprime.TfIdfTypes._
import uk.ac.cam.cl.azprime.ml.ExtractedFeature
import uk.ac.cam.cl.azprime.ml.FeatureExtractor
import uk.ac.cam.cl.azprime.ml.ExtractedFeatures
import uk.ac.cam.cl.azprime.ml.SingleFeatureExtractor
import uk.ac.cam.cl.azprime.resources.StopWords;

case class BodyTfIdf(value: Double) extends ExtractedFeature {
  type ValueType = Double
  val label = "TFI"
}

class BodyTfIdfExtractor(calc: DocumentTfIdfScores, normalize: Boolean = true)(implicit stoplist: StopWords) extends SingleFeatureExtractor {
  type Extracted = BodyTfIdf
  def extract(sentence: ParsedSciXml.Sentence, document: ParsedSciXml.Document, features: ExtractedFeatures) = {
    val score = sentence.words
      .filterNot(stoplist(_))
      .foldLeft(0d)((sum, word) => sum + calc.tfidf(word, document))

    if (normalize)
      new BodyTfIdf(score / sentence.words.length)
    else
      new BodyTfIdf(score)
  }
}