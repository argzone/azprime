package uk.ac.cam.cl.azprime.ml.features

import uk.ac.cam.cl.azprime.ml.ExtractedFeature
import uk.ac.cam.cl.azprime.ml.FeatureExtractor
import uk.ac.cam.cl.azprime.ml.ExtractedFeatures
import uk.ac.cam.cl.azprime.ml.SingleFeatureExtractor
import uk.ac.cam.cl.azprime.xml.ParsedSciXml
import uk.ac.cam.cl.azprime.resources.Patterns
import uk.ac.cam.cl.azprime.resources.Lexicon

case class SemanticCuePhrase(val value: String) extends ExtractedFeature {
	type ValueType = String
	val label = "SemanticCuePhrase"
}

class SemanticCuePhraseExtractor (implicit pat: Patterns, lex: Lexicon) extends SingleFeatureExtractor {
	type Extracted = ExtractedFeature
	
	def extract(sentence: ParsedSciXml.Sentence, document: ParsedSciXml.Document, features: ExtractedFeatures) = {
		val mat = pat.matches(sentence.wordsWithPos);
		if (mat.isEmpty == false){
		SemanticCuePhrase(mat.map(_.expression.featureString.replaceAll(",","")).mkString("!"))
		}else{
		  SemanticCuePhrase("");
		}
	}
}
