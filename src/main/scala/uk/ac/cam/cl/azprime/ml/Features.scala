package uk.ac.cam.cl.azprime.ml

import uk.ac.cam.cl.azprime.xml.ParsedSciXml

// TODO: Remove dependency on ParsedSciXml!!!

trait FeatureExtractor {
  type Extracted <: ExtractedFeature
  var _dependencies: List[Class[FeatureExtractor]] = Nil
  def dependsOn[T <: FeatureExtractor: ClassManifest] {
    val dep = classManifest[T].erasure.asInstanceOf[Class[FeatureExtractor]]
    _dependencies = dep :: _dependencies
  }
  lazy val dependencies = _dependencies.toSet

  def extractAll(sentence: ParsedSciXml.Sentence,
                 document: ParsedSciXml.Document,
                 features: ExtractedFeatures): Seq[Extracted]
}

trait SingleFeatureExtractor extends FeatureExtractor {
  override def extractAll(sentence: ParsedSciXml.Sentence,
                          document: ParsedSciXml.Document,
                          features: ExtractedFeatures): Seq[Extracted] = {
    List(extract(sentence, document, features))
  }
  def extract(sentence: ParsedSciXml.Sentence,
              document: ParsedSciXml.Document,
              features: ExtractedFeatures): Extracted
}

trait MultipleFeatureExtractor extends FeatureExtractor {
  override def extractAll(sentence: ParsedSciXml.Sentence,
                          document: ParsedSciXml.Document,
                          features: ExtractedFeatures): Seq[Extracted] = {
    extract(sentence, document, features)
  }
  
  def extract(sentence: ParsedSciXml.Sentence,
              document: ParsedSciXml.Document,
              features: ExtractedFeatures): Seq[Extracted]
}

object ExtractedFeatures {
  def apply(features: ExtractedFeature*) = {
    new ExtractedFeatures(features.map(f => f.getClass -> f).toMap)
  }
}

class ExtractedFeatures(protected val data: Map[Class[_], ExtractedFeature]) {
  def apply[T <: ExtractedFeature: ClassManifest](): T = {
    val cls = classManifest[T].erasure
    val fea = data(cls)
    fea.asInstanceOf[T]
  }
  def +(feature: ExtractedFeature) = {
    new ExtractedFeatures(data + (feature.getClass -> feature))
  }
  def ++(features: ExtractedFeatures) = {
    new ExtractedFeatures(data ++ features.data)
  }
  def values = data.values.toList
}

trait ExtractedFeature {
  type ValueType
  def value: ValueType
  def label: String
  
  def getComponents: Array[String] = {
    Array(label, String.valueOf(value))
  }
}

// TODO (dk): Implement all the below

case class SentenceAgent(val value: String) extends ExtractedFeature {
  type ValueType = String
  val label = "AGENT"
}

case class SentenceAction(val value: String) extends ExtractedFeature {
  type ValueType = String
  val label = "ACTION"
}
