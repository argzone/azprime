package uk.ac.cam.cl.azprime.ml.features

import uk.ac.cam.cl.azprime.resources.StopWords
import uk.ac.cam.cl.util.TermCounts
import uk.ac.cam.cl.azprime.xml.ParsedSciXml
import uk.ac.cam.cl.azprime.ml.ExtractedFeature
import uk.ac.cam.cl.azprime.ml.FeatureExtractor
import uk.ac.cam.cl.azprime.ml.ExtractedFeatures
import uk.ac.cam.cl.azprime.ml.SingleFeatureExtractor

case class TitleWords(val value: Double) extends ExtractedFeature {
  type ValueType = Double
  val label = "TIT"
}

// TODO: Ignore stereotypical headers
class TitleWordsExtractor(counts: TermCounts, normalize: Boolean = true)(implicit stoplist: StopWords) extends SingleFeatureExtractor {
  type Extracted = TitleWords
  def extract(sentence: ParsedSciXml.Sentence, document: ParsedSciXml.Document, features: ExtractedFeatures) = {
    val score = sentence.words
      .filter(!stoplist(_))
      .filter(counts(_) > 0)
      .foldLeft(0.0d)((sum, word) => sum + 1)

    // XXX (dk): Seems like we should be subtracting off the # of stop words present.
    if (normalize)
      new TitleWords(score / sentence.words.length)
    else
      new TitleWords(score)
  }
}