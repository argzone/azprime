package uk.ac.cam.cl.azprime.ml.features

import uk.ac.cam.cl.azprime.xml.ParsedSciXml
import uk.ac.cam.cl.azprime.ml.ExtractedFeature
import uk.ac.cam.cl.azprime.ml.FeatureExtractor
import uk.ac.cam.cl.azprime.ml.ExtractedFeatures
import uk.ac.cam.cl.azprime.ml.SingleFeatureExtractor

case class SentenceLength(val value: Int) extends ExtractedFeature {
  type ValueType = Int
  val label = "LEN"
}

class SentenceLengthExtractor extends SingleFeatureExtractor {
  type Extracted = SentenceLength
  def extract(sentence: ParsedSciXml.Sentence, document: ParsedSciXml.Document, features: ExtractedFeatures) = {
    new SentenceLength(sentence.length)
  }
}