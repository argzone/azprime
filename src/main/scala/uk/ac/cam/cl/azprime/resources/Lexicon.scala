package uk.ac.cam.cl.azprime.resources

import java.io.InputStream

object DataFile {
  def isComment(line: String) = (line startsWith "#") || (line isEmpty)
}

object Lexicon {
  import org.tempura.utils.StringOps._
  import uk.ac.cam.cl.azprime.verbs._

  def getVerbMorphology(word: String) = {

  }

  def getNounMorphology(wordstem: String) = {
    if (wordstem endsWith "s") {
      List(wordstem, wordstem + "es")
    } else if (wordstem endsWith "ey") {
      List(wordstem, wordstem + "s")
    } else if (wordstem endsWith "y") {
      List(wordstem, (wordstem - 1) + "ies")
    } else {
      List(wordstem, wordstem + "s")
    }
  }

  def getAdjectiveMorphology(wordstem: String) = {
    if (wordstem endsWith "ic") {
      List(wordstem, wordstem + "ally")
    } else if (wordstem endsWith "y") {
      List(wordstem,
        (wordstem - 1) + "ily",
        (wordstem - 1) + "ier", // modified; original did not chop
        (wordstem - 1) + "iest") // modified; original did not chop
    } else if (wordstem endsWith "le") {
      List(wordstem,
        (wordstem - 2) + "ly",
        wordstem + "r",
        wordstem + "st")
    } else if (wordstem endsWith "e") {
      List(wordstem,
        wordstem + "ly",
        (wordstem - 1) + "ly",
        wordstem + "r",
        wordstem + "st")
    } else {
      List(wordstem, wordstem + "ly")
    }
  }

  // XXX: Should probably be a setting optionally passed in.
  val ignoredClasses = Set("ARGUMENT", "INTEREST", "MAGNIFIER", "NEW", "TIME", "WANT")
}

class Lexicon(private val instream: InputStream) {
  val lines = io.Source.fromInputStream(instream).getLines
  lazy val (byclass, byindicator, lexicon) = {
    import collection.mutable.Map
    val byclass = Map[String, Set[String]]() withDefaultValue (Set())
    val byindicator = Map[String, Set[String]]() withDefaultValue (Set())
    val lexicon = collection.mutable.Set[String]()
    val fullphrases = Map[String, Set[String]]()
    for (line_ <- lines; line = line_.trim; if (!DataFile.isComment(line))) {
      // Split the line so that the last set of chars in a line is cls, and the rest (trailing spaces excluded), is on the left.
      val splitLine = """(.*?)(?=\s+)[\s\t]+([^\s]*?)""".r
      val splitLine(words, cls) = line
      var indicators = List[String]()
      if (!(Lexicon.ignoredClasses contains cls)) {
        if (!(cls startsWith "*")) {
          if (cls endsWith "_NOUN") {
            indicators ++= Lexicon.getNounMorphology(words)
          } else if (cls endsWith "_ADJ") {
            indicators ++= Lexicon.getAdjectiveMorphology(words)
          } else {
            indicators ++= List(words)
          }
        } else {
          // Irregular nouns/adjectives; read in as is
          indicators ++= List(words)
        }
        // Process indicators
        for (indicator <- indicators) {
          val cls_ = if (cls startsWith "*") cls.drop(1) else cls
          //println "REMEMBERING %s %s".format(indicator, class);
          byclass(cls_) ++= Set(indicator)
          lexicon += indicator
          byindicator(indicator) ++= Set(cls_)
          val splitwords = indicator split " "
          if (splitwords.length > 1) {
            for (word <- splitwords)
              fullphrases += word -> Set(indicator)
          }
        }
      }
    }
    (byclass.toMap.withDefaultValue(Set()),
      byindicator.toMap.withDefaultValue(Set()),
      lexicon.toSet)
  }
}