package uk.ac.cam.cl.azprime.resources

import java.io.InputStream

object StopWords {
  final val None = new StopWords(Set[String]())
}

@serializable @SerialVersionUID(5432185437953783973L)
class StopWords(val words: Set[String]) {
  def this(instream: InputStream) {
    this(io.Source.fromInputStream(instream)
      .getLines
      .map(_.split(" ")(0).toLowerCase)
      .toSet)
  }
  def asSet = words
  def contains(str: String) = words contains (str toLowerCase)
  def apply(str: String) = contains(str)
}