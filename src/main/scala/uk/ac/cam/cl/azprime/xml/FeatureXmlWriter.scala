package uk.ac.cam.cl.azprime.xml

import scalax.io.JavaConverters._
import uk.ac.cam.cl.azprime.ml.FeatureEngine
import scala.xml.{ XML, Node, Attribute, Text, Null }
import scala.xml.dtd.DocType
import scala.xml.dtd.SystemID
import scala.xml.PrettyPrinter

class FeatureXmlWriter(sentences: FeatureEngine.DocumentFeatures, document: ParsedSciXml.Document, ignored: Set[String]) {
  def writeFile(file: java.io.File) = {
    // XXX (dk): A bit convoluted, but incrementally adds an attribute for each feature
    //   to an initial <S/> element.
    val formatted = for ((sentenceFeatures, sentence) <- sentences zip document.sentences) yield {
      val base = <S/> % Attribute(None, "ID", Text(sentence.id), Null)
      sentenceFeatures.foldLeft(base)((sent, fea) => {
        if (ignored(fea.label))
          sent
        else
          sent % Attribute(None, fea.label, Text(fea.value.toString), Null)
      })
    }
    val xml = <FEATURES>{ formatted }</FEATURES>
    val pp = new PrettyPrinter(140, 4)
    val header = """<?xml version='1.0' encoding='UTF-8'?>
<!DOCTYPE FEATURES SYSTEM "/home/sht25/dtd/fea.dtd">"""
    file.asOutput.write(header + "\n" + pp.format(xml))
  }
}