package uk.ac.cam.cl.azprime.xml

import scala.xml.XML
import scala.xml.Elem
import scala.xml.Node
import scala.xml.NodeSeq

// TODO(dk): Give this a better name
object XmlUtils {

  def findTitles(root: NodeSeq, exhaustive: Boolean = true) = {
    findWordsWithin(root, "TITLE", exhaustive)
  }

  def findHeaders(root: NodeSeq, exhaustive: Boolean = true) = {
    findWordsWithin(root, "HEADER", exhaustive)
  }

  def findWordsWithin(root: NodeSeq, tag: String, exhaustive: Boolean = true) = {
    def find(e: Node) = {
      (e \ "W").map(_.text).mkString(" ")
    }
    val res = if (exhaustive)
      for (h <- root \\ tag) yield find(h)
    else
      for (h <- root \ tag) yield find(h)
    res
  }
}