package uk.ac.cam.cl.azprime.config

import org.tempura.utils.SimpleConfig

object Configuration {

  /* E.g.:
  new Configuration(
    debugging = true,
    files = new Filepaths(output = "trained/"))
  */
  // Does not validate fields but eval will throw an error if an invalid field is set.
  def load(filepath: String) = {
    val eval = new com.twitter.util.Eval()
    val config = 
      if (filepath.startsWith("classpath:")) 
        eval[Configuration](this.getClass().getResourceAsStream(filepath.substring("classpath:".length)))
      else
        eval[Configuration](new java.io.File(filepath))
    config
  }
}
  
case class Configuration(
  debugging: Boolean = false,
  features: EnabledFeatures = new EnabledFeatures,
  files: Filepaths = new Filepaths)

case class Filepaths(
  val output: String = "trained/",
  val actions: String = "/actions",
  val lexicon: String = "/lexicon",
  val indicators: String = "/indicators",
  val stoplist: String = "/stoplist",
  val patterns: String = "/patterns",
  val headers: String = "/headersort",
  val data: String = "data/",
  val bodytfidf: String = "/home/do242/Work/azprime/bodytfidf",
  val titletfidf: String = "/home/do242/Work/azprime/titletfidf",
  val datatype: String = "pos.xml")

case class EnabledFeatures(
  // location
  useLocation: Boolean = true,
  useParagraph: Boolean = true,
  useSection: Boolean = true,
  useHeader: Boolean = true,
  useOldLocation: Boolean = true,

  // title, tf/idf, sentence length
  useTfIdf: Boolean = true,
  useTitle: Boolean = true,
  useSentenceLength: Boolean = true,

  // citations
  useCitations: Boolean = true,
  useCitationType: Boolean = true, // Self / Other
  useCitationLocation: Boolean = true,
  useCitationNumber: Boolean = true,

  // agenthood, action 
  useAction: Boolean = true,
  useAgent: Boolean = true,

  // formulaic, cuephrases
  useFormulaic: Boolean = true,
  useCuePhrases: Boolean = true)

/*
case class FeatureM extends Feature {
  def run {
    println("foo")
  }
}
case class FeatureN extends Feature {
  def run {
    println("bar")
  }
}

class EnabledFeatures2(enabled: Class[Feature]*) {
}

object EnabledFeatures2 {
  def enable[T <: Feature: ClassManifest] = {
    classManifest[T].erasure.asInstanceOf[Class[Feature]]
  }
}

class EnabledFeaturez {
  private var enabled: List[Class[Feature]] = List()

  def use[T <: Feature: ClassManifest] {
    val a = classManifest[T].erasure.asInstanceOf[Class[Feature]]
    enabled = a :: enabled
  }

  def use2[T <: Feature](t: Class[T]) {
    enabled = t.asInstanceOf[Class[Feature]] :: enabled
  }

  implicit def enable[T <: Feature: ClassManifest] {
    classManifest[T].erasure.asInstanceOf[Class[Feature]]
  }

  lazy val _enabled = enabled;
}

object Test2 {
  import EnabledFeatures2._
  val f2 = new EnabledFeatures2(enable[FeatureM], enable[FeatureN])
}

object Test {
  val f = new EnabledFeaturez { use[FeatureM]; use2(classOf[FeatureN]) }
}
*/