package uk.ac.cam.cl.azprime.ml.features

import DocumentFinalSectionValues.FinalParagraph
import DocumentFinalSectionValues.FinalSection
import DocumentInitialSectionValues.InitialParagraph
import DocumentInitialSectionValues.InitialSection
import DocumentAndParagraphLocationValues.Other
import uk.ac.cam.cl.azprime.resources.Headers
import uk.ac.cam.cl.azprime.ml.ExtractedFeature
import uk.ac.cam.cl.azprime.ml.ExtractedFeatures
import uk.ac.cam.cl.azprime.ml.FeatureExtractor
import uk.ac.cam.cl.azprime.xml.ParsedSciXml
import uk.ac.cam.cl.azprime.ml.SingleFeatureExtractor

object DocumentAndParagraphLocationValues extends Enumeration {
  type Values = Value

  val InitialSecInitialPar = Value("i-INI")
  val InitialSecFinalPar = Value("f-INI")
  val InitialSecMiddlePar = Value("m-INI")
  val FinalSecInitialPar = Value("i-FINI")
  val FinalSecFinalPar = Value("f-FINI")
  val FinalSecMiddlePar = Value("m-FINI")
  val Other = Value("0")
}

case class DocumentAndParagraphLocation(val value: DocumentAndParagraphLocationValues.Values) extends ExtractedFeature {
  type ValueType = DocumentAndParagraphLocationValues.Values
  val label = "OLO"
}

class DocumentAndParagraphLocationExtractor(headers: Headers) extends SingleFeatureExtractor {
  import DocumentAndParagraphLocationValues._
  type Extracted = DocumentAndParagraphLocation

  // Declare our dependencies to require them to be run first.
  dependsOn[DocumentInitialSectionExtractor]
  dependsOn[DocumentFinalSectionExtractor]

  def extract(sentence: ParsedSciXml.Sentence, document: ParsedSciXml.Document, features: ExtractedFeatures) = {

    def sectionLocation() = {
      import DocumentInitialSectionValues._
      import DocumentFinalSectionValues._
      val initialSection = features[DocumentInitialSection]()
      val finalSection = features[DocumentFinalSection]()
      val res = initialSection.value match {
        case InitialSection | InitialParagraph => "INI"
        case _ => finalSection.value match {
          case FinalSection => "FINI"
          case FinalParagraph => "FINI"
          case _ => ""
        }
      }
      res
    }

    def locationInParagraph() = {
      val (loc, size) = document.locationWithinParagraph(sentence)
      loc match {
        case 0 => "i"
        case l if (l == size - 1) => "f"
        case _ => "m"
      }
    }

    val header = document.header(sentence)
    val sec = sectionLocation()
    val res = if (sec != "")
      DocumentAndParagraphLocationValues.withName("%s-%s".format(locationInParagraph(), sec))
    else
      Other
    new DocumentAndParagraphLocation(res)
  }
}