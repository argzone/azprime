package uk.ac.cam.cl.azprime

package object verbs {
  val BeVerb = """\b(is|isn't|aren't|wasn't|weren't|am|are|be|were|was|been)\b""".r
  val DoVerb = """\b(do|doesn't|don't|didn't|does|did|done)\b""".r
  val HaveVerb = """\b(have|hadn't|hasn't|haven't|has|had)\b""".r
  val WillVerb = """\bwill\b""".r
}