package uk.ac.cam.cl.azprime.xml

import scala.xml.Node

object SciXmlTagger{
  def getTagger(path: String): edu.stanford.nlp.tagger.maxent.MaxentTagger = {
    new edu.stanford.nlp.tagger.maxent.MaxentTagger(path)
  }
}

class SciXmlTagger(tagger: edu.stanford.nlp.tagger.maxent.MaxentTagger,stream: java.io.InputStream, path: String = null) extends SciXmlReader(stream,path){
	lazy val sentenceTagger = new SentenceTagger(tagger)
	
	def this (tagger: edu.stanford.nlp.tagger.maxent.MaxentTagger, file: java.io.File){
	  this(tagger,new java.io.FileInputStream(file), file.getAbsolutePath())
	}
  
  override def parseSentence(s: Node): ParsedSciXml.Sentence = {
	     import org.tempura.utils.XmlOps._
	     
	     if ((s \ "W") isEmpty){
	        new ParsedSciXml.Sentence((s \ "@ID").text, sentenceTagger.tagUntokenisedSentence(s), (s \ "@AZ").text, attributes = s.attributes.toStringMap)
	     }else{	
	    	 val sen = new ParsedSciXml.Sentence((s \ "@ID").text, getWordsWithPosAndExtras(s), (s \ "@AZ").text, attributes = s.attributes.toStringMap)
	     
	    	 if (sen.wordsWithPosAndExtras.exists(_._2 == "")){
	    		 sentenceTagger.tagTokenisedSentence(sen)
	    	 }else{
	    		 sen
	    	 }
	     }
	}
}

trait SentenceSplitting extends SciXmlParsing { self: SciXmlReader =>
  lazy val splitter = new SentenceSplitter()
  
  abstract override def parseParagraph(p: Node): ParsedSciXml.Paragraph = { 
    if ((p \ "S") isEmpty) {
    	new ParsedSciXml.Paragraph(splitter.splitString(p,""))
    } else {
      super.parseParagraph(p)
    }
  }
  
  abstract override def parseAbstract(a: Node): List[ParsedSciXml.Sentence] = {
    if ((a \ "A-S") isEmpty){
      splitter.splitString(a,"")
    }else{
      val sentences = for (s <- a \ "A-S") yield parseSentence(s)
      sentences.toList
    }
  }
  
  abstract override def parseDivision(d: Node): ParsedSciXml.Division = {    
    val depth = if ((d \ "@DEPTH") isEmpty){
      -1
    }else{
      (d \ "@DEPTH" text).toInt
    }
    val header = d \ "HEADER" text
    def ignored(l: String) = {
      l match {
        case "HEADER" | "IMAGE" | "EXAMPLE-SENT" | "EQN" | "CAPTION" | "EXAMPLE" => true
        case _ => false
      }
    }
    
    val data = for (e <- d \ "_"; l = e.label; if !ignored(l)) yield {
      e.label match {
        case "P"   => parseParagraph(e)
        case "DIV" => parseDivision(e)
        case _     => throw new Exception("Unexpected element %s found!".format(e.label))
      }
    }

    new ParsedSciXml.Division(header, depth, data.toList)
  }
}

object SciXmlProcessor{
  import ParsedSciXml._
  
  def toStructuredText(div: Division, depth: Integer): String = {
    
    def wrapString(in: String, wrapper: String, wrapDepth: Integer) : String = {
      if (wrapDepth == 0){
        ""
      }else{
    	  wrapper + wrapString(in,wrapper,wrapDepth-1) + wrapper
      }
    }
    
    def process[T <: HasSentences](hs: T): String = {
      hs match {
        case Division => toStructuredText(hs.asInstanceOf[Division],depth+1) + "\n"
        case Paragraph => hs.sentences.mkString("\n") + "\n"
      }
    }
    
    val title = div.header.replaceAll("|","")
    val content = div.content.foldLeft(new StringBuilder()){
      (sb,d) => sb.append(process(d))
    }.toString.replaceAll("|","")
    wrapString(title,"|",depth) + content
  }
  
  def toStructuredText(doc: Document): String = {
		  val str = doc.title + "\n\n|abstract|\n" + doc.abstractText.replaceAll("|","") + "\n\n" + doc.divisions.map(d => toStructuredText(d,1))
		  str
  }
  
  def renumberSentences(doc: Document): Document = {
    doc.copy(abstractSentences = renumberAbstractSentences(doc.abstractSentences),divisions = renumberBodySentences(doc.divisions,0))
  }
  
  def renumberSentence(sen: Sentence, i: Int, prefix: String): Sentence = {
   new Sentence(prefix + "-" + i.toString,sen.wordsWithPosAndExtras,sen.azCategory,sen.attributes) 
  }
  
  def renumberAbstractSentences(sens: List[Sentence]): List[Sentence] = {
    sens.zipWithIndex.map(x => renumberSentence(x._1,x._2,"AS"))
  }
  
  def renumberParagraphSentences(par: Paragraph, i: Int): (Paragraph,Int) = {
    val transformedSentences = par.sentences.zipWithIndex.map(x => renumberSentence(x._1,x._2+i,"S"))
    (new Paragraph(transformedSentences),i+transformedSentences.length)
  }
  
  def renumberBodySentences(divisions: List[Division], start: Int): List[Division] = {
    def transform[T <: HasSentences](hs: T, i: Int): (T,Int) = {
      hs match {
        case d: Division  => {
          val divCounterTuple = d.content.foldLeft(List[HasSentences](),i)((x,con) => {
            val transformed = transform(con,x._2)
            (x._1 :+ transformed._1,transformed._2)
          })
          val newDiv = new Division(d.header,d.depth,divCounterTuple._1)
          (newDiv.asInstanceOf[T],divCounterTuple._2)
        }
        
        case p: Paragraph => {
          val parCounterTuple = renumberParagraphSentences(hs.asInstanceOf[Paragraph],i)
          (parCounterTuple._1.asInstanceOf[T],parCounterTuple._2)
        }
      }
    }
    
    val listCounterTuple = divisions.foldLeft(List[Division](),start)((x,div) =>{
      val divCounterTuple = transform(div,x._2)
      (x._1 :+ divCounterTuple._1,divCounterTuple._2)
    })
    listCounterTuple._1
  }
  
  def main (args: Array[String]){
    val argMap = args.map(x => x.split("=")).map(x => (x(0),x(1))).toMap
    val pos = if (argMap.contains("pos")) java.lang.Boolean.parseBoolean(argMap.get("pos") getOrElse "") else false 
    val az = if (argMap.contains("az")) java.lang.Boolean.parseBoolean(argMap.get("az") getOrElse "") else false 
    val tag = if (argMap.contains("tag")) java.lang.Boolean.parseBoolean(argMap.get("tag") getOrElse "") else false 
    val split = if (argMap.contains("split")) java.lang.Boolean.parseBoolean(argMap.get("split") getOrElse "") else true 

    val textOut = if (argMap.contains("text")) java.lang.Boolean.parseBoolean(argMap.get("text") getOrElse "") else false 
    val dir = new java.io.File(argMap.get("dir") getOrElse "")
    val outDir = if (argMap.contains("outdir")) new java.io.File(argMap.get("outdir") getOrElse "") else dir
    val inSuffix = argMap.get("in") getOrElse ""
    val outSuffix = argMap.get("out") getOrElse ""
    val inFiles = dir.listFiles().filter(_.getName.endsWith(inSuffix))
    val indentation = if (argMap.contains("indent")) java.lang.Integer.parseInt(argMap.get("indent") getOrElse "") else 4
    val overwrite = if (argMap.contains("overwrite")) java.lang.Boolean.parseBoolean(argMap.get("overwrite") getOrElse "") else true 
    
    val macTagger = new java.io.File("/Users/do242/Work/Java/stanford-corenlp-2012-03-09/Models/edu/stanford/nlp/models/pos-tagger/english-bidirectional/english-bidirectional-distsim.tagger")
    val workTagger = new java.io.File("/anfs/bigdisc/do242/Java/stanford-corenlp-full-2012-11-12/edu/stanford/nlp/models/pos-tagger/english-bidirectional/english-bidirectional-distsim.tagger")
    
    val tagPath = if (argMap.contains("tagmodel")){ argMap.get("tagmodel") getOrElse ""} else if (macTagger.exists()){ macTagger.getPath() }else if (workTagger.exists()){ workTagger.getPath()} else{ ""}
    lazy val tagger = SciXmlTagger.getTagger(tagPath)
    
    if (inFiles.isEmpty) println("No input files found")
    
    inFiles.foreach({inFile =>
      val reader = if (tag && split){
    	  new SciXmlTagger(tagger,inFile) with SentenceSplitting
      }else if (split){
    	  new SciXmlReader(inFile) with SentenceSplitting
      }else if (tag){
        new SciXmlTagger(tagger,inFile)
      }else{
        new SciXmlReader(inFile)
      }

      val outFile = new java.io.File(outDir,inFile.getName().replaceAll(inSuffix + "$",outSuffix))      
     println("Reading " + inFile.getPath + ", writing to " + outFile.getPath)

      val doc = reader.parse();
      if (outFile.exists){
        if (overwrite){
          outFile.delete()
        }else{
          println(outFile.getPath + " exists...skipping")
        }
      } 
      
      if (outFile.exists == false){
      if (textOut){
       val docText = (doc.abstractText + "\n" + doc.text);
       import scalax.io.JavaConverters._
       outFile.asOutput.write(docText)
      }else{
       val renumbered = renumberSentences(doc);
       val writer = new SciXmlWriter(renumbered,pos,az,indentation)
       writer.write(outFile)
      }
    }
    })
  }
}