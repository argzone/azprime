package uk.ac.cam.cl.azprime.verbs
import org.tempura.utils.StringOps._
import uk.ac.cam.cl.azprime.resources.Agents
import uk.ac.cam.cl.azprime.resources.FormulaicExpression
import uk.ac.cam.cl.azprime.resources.Lexicon
import org.tempura.utils.CollectionOps._
import uk.ac.cam.cl.azprime.resources.FormulaicExpression.ValidPOS
import uk.ac.cam.cl.azprime.resources.FormulaicExpression.posMappings
import uk.ac.cam.cl.azprime.resources.ExpressionMatch
import uk.ac.cam.cl.azprime.resources.Actions

import scala.annotation.tailrec;

object SemanticParse {
  import uk.ac.cam.cl.azprime.verbs._

  def isVerb(pos: String) = {
    pos match {
      case "VBZ" => true
      case "VBP" => true
      case "VB"  => true
      case "VBD" => true
      case "VBN" => true
      case _     => false
    }
  }

  def isSyntacticBreak(pos: String) = {
    pos =~ """[,:\)\(]""" || pos =~ "WDT"
  }

  def alterPos(wordsWithPos: List[(String, String)]): List[(String, String)] = {
    wordsWithPos map { alterPos }
  }

  def alterPos(wordWithPos: (String, String)): (String, String) = {
    val (w, pos) = wordWithPos
    if (isVerb(pos) && w =~ BeVerb)
      (w, pos + "_BE")
    else if (isVerb(pos) && w =~ DoVerb)
      (w, pos + "_DO")
    else if (isVerb(pos) && w =~ HaveVerb)
      (w, pos + "_HAVE")
    else if (pos == "MD" && w =~ WillVerb)
      (w, pos + "_WILL")
    else (w, pos)
  }
}

object SemanticVerbType extends Enumeration {
  type Values = Value
  val Finite = Value("finite")
  val To = Value("to")
  val Base = Value("base")
  val Perceptron = Value("perc")
  val Participle = Value("participle")
  val Progressive = Value("ing")
  val NA = Value("nothing")
}

object VerbTense extends Enumeration {
  type Values = Value
  val Past = Value("PAST")
  val Present = Value("PRESENT")
  val Future = Value("FUTURE")
  val PastProgressive = Value("PAST_CONT")
  val PresentProgressive = Value("PRESENT_CONT")
  val FutureProgressive = Value("FUTURE_CONT")
  val PastPerfect = Value("PAST_PERFECT")
  val PresentPerfect = Value("PRESENT_PERFECT")
  val FuturePerfect = Value("FUTURE_PERFECT")

  // Hack
  def asPerfect(tense: VerbTense.Values) = {
    VerbTense.withName(tense.toString + "_PERFECT")
  }
  def asProgressive(tense: VerbTense.Values) = {
    VerbTense.withName(tense.toString + "_CONT")
  }

  def fromPos(pos: String) = {
    import org.tempura.utils.StringOps._
    if (pos =~ "^VBD") Past
    if (pos =~ "_WILL$") Future
    else Present
  }
}

object VoiceType extends Enumeration {
  type Values = Value
  val Passive = Value("PASSIVE")
  val Active = Value("ACTIVE")
  val NA = Value("")
}

object VoiceModifier extends Enumeration {
  type Values = Value
  val Possessive = Value("POSSESSIVE")
  val Copula = Value("COPULA")
  val CopulaCue = Value("COPULA_QUE")
  val Need = Value("NEED")
  val NA = Value("")
}

case class VerbVoice(voicetype: VoiceType.Values, modifier: VoiceModifier.Values = VoiceModifier.NA) {
  def asPossessive() = {
    this.copy(modifier = VoiceModifier.Possessive)
  }
  def asCopula() = {
    this.copy(modifier = VoiceModifier.Copula)
  }
  def asCopulaCue() = {
    this.copy(modifier = VoiceModifier.CopulaCue)
  }
  def asNeed() = {
    this.copy(modifier = VoiceModifier.Need)
  }
  def isActive = {
    this.voicetype == VoiceType.Active
  }
  def isPassive = {
    this.voicetype == VoiceType.Active
  }
  def hasCopulaCue() = {
    this.modifier == VoiceModifier.CopulaCue
  }
  def hasNeed() = {
    this.modifier == VoiceModifier.CopulaCue
  }
  def hasCopula() = {
    this.modifier == VoiceModifier.CopulaCue
  }
  def hasPossessive() = {
    this.modifier == VoiceModifier.CopulaCue
  }
  override def toString = {
    voicetype + (if (modifier != VoiceModifier.NA) "_" + modifier else "")
  }
}

object VerbVoice {
  val Active = VerbVoice(VoiceType.Active)
  val ActivePossessive = Active.asPossessive
  val ActiveCopula = Active.asCopula
  val ActiveCopulaCue = Active.asCopulaCue
  val ActiveNeed = Active.asNeed
  val Passive = VerbVoice(VoiceType.Passive)
  val PassiveCopulaCue = Passive.asCopulaCue
  val NA = VerbVoice(VoiceType.NA)
}

object BaseVerb extends Enumeration {
  type Values = Value
  val FullVerb = Value("full_verb")
  val Have = Value("have")
  val Be = Value("be")
  val Do = Value("do")
}

case class SemanticVerb(verbType: SemanticVerbType.Values, word: String = null, pos: String = null, position: Int = -1)

case class SemanticFiniteVerb(tense: VerbTense.Values, voice: VerbVoice,
                              isModal: Boolean, finitePosition: Int, semanticPosition: Int)

class SemanticParse(wordsWithPos_ : List[(String, String)])(perceptionVerbs: Set[String], scanWindowSize: Int = 6) {
  import uk.ac.cam.cl.azprime.verbs.{ VerbTense => Tense }
  import uk.ac.cam.cl.azprime.verbs.{ VerbVoice => Voice }
  import org.tempura.utils.CollectionOps._

  val wordsWithPos = SemanticParse.alterPos(wordsWithPos_)

  def semanticFiniteVerbs(): List[SemanticFiniteVerb] = {
    getSemanticFiniteVerbs(0)(List())
  }

  @tailrec
  final def getSemanticFiniteVerbs(startIdx: Int)(list: List[SemanticFiniteVerb]): List[SemanticFiniteVerb] = {
    val verb = nextSemanticFiniteVerb(startIdx)
    if (verb.isEmpty) return list
    if (verb.get.semanticPosition == -1) return list :+ verb.get
    getSemanticFiniteVerbs(verb.get.semanticPosition + 1)(list :+ verb.get)
  }

  private[azprime] def nextSemanticFiniteVerb(startIdx: Int) = {
    wordsWithPos.zipWithIndex drop (startIdx) findFirst { x =>
      val ((word, pos), idx) = x
      if (pos =~ "VB[PZ]$") handlePresentVerb(wordsWithPos, idx)
      else if (pos =~ "VBD$") handlePastVerb(wordsWithPos, idx)
      else if (pos =~ "VB[PZD]_DO") handleDoVerb(wordsWithPos, idx)
      else if (pos =~ "VB[PZD]_BE") handleBeVerb(wordsWithPos, idx)
      else if (pos =~ "VB[PZD]_HAVE") handleHaveVerb(wordsWithPos, idx)
      else if (pos =~ "^MD") handleModal(wordsWithPos, idx)
      else None
    }
  }

  def negationForVerb(verb: SemanticFiniteVerb)(implicit lexicon: Lexicon): Option[Int] = {
    negationPosition(verb.finitePosition)(scanWindowSize)
  }

  private[azprime] def negationPosition(idx: Int)(scanWindowSize: Int)(implicit lexicon: Lexicon): Option[Int] = {
    def findNegation(words: List[((String, String), Int)], reverse: Boolean): Option[Int] = {
      val found =
        (if (reverse) words.reverse else words).
          takeWhile(x => !SemanticParse.isSyntacticBreak(x._1._2)).
          find(x => lexicon.byclass("NEGATION") contains (x._1._1))
      if (found.isDefined) {
        val idx = found.get._2 // Index
        if (!(wordsWithPos(idx)._1 =~ "not" && 
              (wordsWithPos.length > idx + 1) && 
              wordsWithPos(idx + 1)._1 =~ "only")) {
          Some(idx)
        } else None
      } else None
    }

    // Scan backwards
    val start = math.max(0, idx - scanWindowSize)
    val before = wordsWithPos.zipWithIndex.drop(start).take(idx - start)
    findNegation(before, reverse = true) orElse {
      // Scan forwards
      val after = wordsWithPos.zipWithIndex.drop(idx).take(scanWindowSize)
      findNegation(after, reverse = false) orElse None
    }
  }

  def agentForVerb(verb: SemanticFiniteVerb)(implicit agents: Agents) = {
    def isClausalBoundary(wordWithPos: (String, String)) = {
      val (word, pos) = wordWithPos
      SemanticParse.isSyntacticBreak(pos) ||
        pos =~ "^WDT$" ||
        pos =~ "^IN$" ||
        pos =~ "^VB" ||
        word =~ "^that$"
    }

    // This is pretty gross. We're introducing a fake None to force matching to stop...
    val EarlyTermination = ExpressionMatch[FormulaicExpression](null, -1, -1)

    def stopAfter(wordWithPos: (String, String)) = {
      import FormulaicExpression._
      val (word, pos) = wordWithPos
      pos =~ posMappings(ValidPOS.CITE) ||
        pos =~ posMappings(ValidPOS.CREF) ||
        pos =~ posMappings(ValidPOS.SELFCITE)
      //|| pos =~ posMappings(ValidPOS.PRP)
    }

    def getAgent(idx: Int, dir: FormulaicExpression.MatchDirection.Values) = {
      val m = agents.matches(wordsWithPos, idx, matchDir = dir).headOption
      m orElse {
        if (stopAfter(wordsWithPos(idx)))
          Some(EarlyTermination)
        else
          None
      }
    }
    val withIdx = wordsWithPos.zipWithIndex
    val found =
      verb.voice match {
        case VerbVoice(VoiceType.Active, _) => {
          val agent = withIdx.
            take(verb.finitePosition).reverse.
            takeWhile(x => !isClausalBoundary(x._1)).
            takeUntil(x => stopAfter(x._1), inclusive = true).
            findFirst { x =>
              val ((word, pos), idx) = x
              if (agents patternEndsWithWord word)
                getAgent(idx, FormulaicExpression.MatchDirection.Backward)
              else
                None
            }
          agent
        }
        case VerbVoice(VoiceType.Passive, _) => {
          val agent = if (verb.semanticPosition == -1) None else
            withIdx.
              drop(verb.semanticPosition + 1).
              takeWhile(x => !isClausalBoundary(x._1)).
              takeUntil(x => stopAfter(x._1), inclusive = true).
              findFirst { x =>
                val ((word, pos), idx) = x
                // Previous word is "by"
                if (wordsWithPos(idx - 1)._1 =~ "by" && (agents patternStartsWithWord word))
                  getAgent(idx, FormulaicExpression.MatchDirection.Forward)
                else
                  None
              } orElse {
                withIdx.
                  take(verb.finitePosition).reverse.
                  takeWhile(x => !isClausalBoundary(x._1)).
                  findFirst { x =>
                    val ((word, pos), idx) = x
                    if (agents patternEndsWithWord word)
                      getAgent(idx, FormulaicExpression.MatchDirection.Backward)
                    else
                      None
                  }
              } orElse {
                // TODO: Search for "in ref" in whole sentence
                None
              }
          agent
        }
      }
    if (found == Some(EarlyTermination)) None else found
  }

  def actionforVerb(verb: SemanticFiniteVerb)(implicit lexicon: Lexicon, actions: Actions) = {
    val offset =
      if (verb.voice.hasCopulaCue) {
        if (verb.semanticPosition > 0 &&
          wordsWithPos(verb.semanticPosition - 1)._2 =~ "_BE$")
          verb.semanticPosition - 1
        else verb.semanticPosition
      } else verb.semanticPosition
    if (offset < 0) None
    else actions.matches(wordsWithPos, offset, verb.voice).headOption
  }

  private[azprime] def handlePresentVerb(wordsWithPos: List[(String, String)], idx: Int) = {
    Some(SemanticFiniteVerb(Tense.Present, Voice.Active, isModal = false, idx, idx))
  }

  private[azprime] def handlePastVerb(wordsWithPos: List[(String, String)], idx: Int) = {
    Some(SemanticFiniteVerb(Tense.Past, Voice.Active, isModal = false, idx, idx))
  }

  private[azprime] def handleHaveVerb(wordsWithPos: List[(String, String)], idx: Int) = {
    import SemanticVerbType._
    val (word, pos) = wordsWithPos(idx)
    val rc = rightContext(wordsWithPos, idx + 1)(scanWindowSize)
    val tense = Tense.fromPos(pos)
    val res = rc.verbType match {
      case Participle => {
        val tense2 = VerbTense.asPerfect(tense)
        if (rc.word =~ "been") {
          val rc2 = rightContext(wordsWithPos, rc.position + 1)(scanWindowSize)
          rc2.verbType match {
            case Perceptron =>
              SemanticFiniteVerb(tense2, Voice.PassiveCopulaCue, isModal = false, idx, rc2.position)
            case Participle =>
              SemanticFiniteVerb(tense2, Voice.Passive, isModal = false, idx, rc2.position)
            case _ =>
              SemanticFiniteVerb(tense2, Voice.ActiveCopula, isModal = false, idx, rc.position)
          }
        } else {
          SemanticFiniteVerb(tense2, Voice.Active, isModal = false, idx, rc.position)
        }
      }
      case NA | Finite | Progressive /*| Cue*/ =>
        SemanticFiniteVerb(tense, Voice.ActivePossessive, isModal = false, idx, idx)
      case To =>
        SemanticFiniteVerb(tense, Voice.ActiveNeed, isModal = true, idx, idx)
      case _ => null
    }
    if (res != null) Some(res) else None
  }

  private[azprime] def handleBeVerb(wordsWithPos: List[(String, String)], idx: Int) = {
    import SemanticVerbType._
    val (word, pos) = wordsWithPos(idx)
    val rc = rightContext(wordsWithPos, idx + 1)(scanWindowSize)
    val tense = Tense.fromPos(pos)
    val res = rc.verbType match {
      case Participle =>
        SemanticFiniteVerb(tense, Voice.Passive, isModal = false, idx, rc.position)
      case Progressive => {
        if (rc.position < idx + 2) {
          val tense2 = VerbTense.asProgressive(tense)
          if (rc.word =~ "being")
            SemanticFiniteVerb(tense2, Voice.ActiveCopula, isModal = false, idx, rc.position)
          else if (rc.word =~ "having")
            SemanticFiniteVerb(tense2, Voice.ActivePossessive, isModal = false, idx, rc.position)
          else
            SemanticFiniteVerb(tense2, Voice.Active, isModal = false, idx, rc.position)
        } else {
          SemanticFiniteVerb(tense, Voice.ActiveCopula, isModal = false, idx, idx)
        }
      }
      case Finite | NA | To | Base =>
        SemanticFiniteVerb(tense, Voice.ActiveCopula, isModal = false, idx, idx)
      case Perceptron =>
        if (rc.position <= idx + 2) {
          SemanticFiniteVerb(tense, Voice.PassiveCopulaCue, isModal = false, idx, rc.position)
        } else {
          SemanticFiniteVerb(tense, Voice.ActiveCopula, isModal = false, idx, idx)
        }
      case _ => null
    }
    if (res != null) Some(res) else None
  }

  private[azprime] def handleDoVerb(wordsWithPos: List[(String, String)], idx: Int) = {
    import SemanticVerbType._
    val (word, pos) = wordsWithPos(idx)
    val rc = rightContext(wordsWithPos, idx + 1)(scanWindowSize)
    val tense = Tense.fromPos(pos)
    val found = rc.verbType match {
      case Finite | NA | To | Participle | Progressive =>
        SemanticFiniteVerb(tense, Voice.Active, false, idx, idx)
      case Base if wordsWithPos(rc.position)._2 =~ "HAVE" =>
        SemanticFiniteVerb(tense, Voice.ActivePossessive, isModal = false, idx, rc.position)
      case Base =>
        SemanticFiniteVerb(tense, Voice.Active, isModal = false, idx, rc.position)
      case _ => null
    }
    if (found != null) Some(found) else None
  }

  private[azprime] def handleModal(wordsWithPos: List[(String, String)], idx: Int) = {
    import SemanticVerbType._
    val (word, pos) = wordsWithPos(idx)
    val base = nextBase(wordsWithPos, idx + 1)
    val (tense, modal) = {
      if (pos =~ "_WILL$") (Tense.Future, false)
      else (Tense.Present, true)
    }
    if (base.isDefined) {
      val baseIdx = base.get._2
      val found = base.get._1 match {
        case BaseVerb.Have => {
          val rc = rightContext(wordsWithPos, idx + 1)(scanWindowSize)
          rc.verbType match {
            case Participle =>
              SemanticFiniteVerb(VerbTense.asPerfect(tense), Voice.Active, modal, idx, rc.position)
            case NA | Progressive /*| Cue*/ =>
              SemanticFiniteVerb(tense, Voice.ActivePossessive, modal, idx, baseIdx)
            case To =>
              SemanticFiniteVerb(tense, Voice.ActiveNeed, modal, idx, baseIdx)
            case _ => null
          }
        }
        case BaseVerb.Be => {
          val rc = rightContext(wordsWithPos, baseIdx + 1)(scanWindowSize)
          rc.verbType match {
            case Participle =>
              SemanticFiniteVerb(tense, Voice.Passive, modal, idx, rc.position)
            case NA | Finite | To /*| Cue*/ =>
              SemanticFiniteVerb(tense, Voice.ActiveCopula, modal, idx, baseIdx)
            case Progressive =>
              SemanticFiniteVerb(VerbTense.asProgressive(tense), Voice.Active, modal, idx, rc.position)
            case Perceptron =>
              SemanticFiniteVerb(tense, Voice.PassiveCopulaCue, modal, idx, rc.position)
            case _ => null
          }
        }
        case BaseVerb.FullVerb | BaseVerb.Do =>
          SemanticFiniteVerb(tense, Voice.Active, isModal = modal, idx, baseIdx)
        case _ => null
      }
      if (found != null) Some(found) else None
    } else None
  }

  private[azprime] def nextBase(wordsWithPos: List[(String, String)], startIdx: Int): Option[(BaseVerb.Value, Int)] = {
    import org.tempura.utils.CollectionOps._
    import BaseVerb._

    val res = wordsWithPos.zipWithIndex drop (startIdx) findFirst { x =>
      val ((word, pos), idx) = x
      if (pos =~ "VB$") {
        Some(FullVerb, idx)
      } else if (pos =~ "VB_HAVE$") {
        Some(Have, idx)
      } else if (pos =~ "VB_BE$") {
        Some(Be, idx)
      } else if (pos =~ "VB_DO$") {
        Some(Do, idx)
      } else None
    }
    res
  }

  private[azprime] def isPerceptronVerb(word: String) = perceptionVerbs contains word

  private[azprime] def rightContext(wordsWithPos: List[(String, String)], startIdx: Int)(window: Int): SemanticVerb = {
    import org.tempura.utils.CollectionOps._
    import SemanticVerbType._
    val next = (x: ((String, String), Int)) => {
      val ((word, pos), idx) = x
      val res =
        if (pos =~ "(^VB[PZD])|(^MD)") SemanticVerb(Finite, word, pos, -1)
        else if (pos =~ "(^VB$)|(^VB_)") SemanticVerb(Base, word, pos, idx)
        else if (isPerceptronVerb(word)) SemanticVerb(Perceptron, word, pos, idx)
        else if (pos =~ "^VBN") SemanticVerb(Participle, word, pos, idx)
        else if (pos =~ "^VBG") SemanticVerb(Progressive, word, pos, idx)
        else if (pos =~ "[,:]") SemanticVerb(NA, word, pos, -1)
        else null
      if (res == null) None else Some(res)
    }
    val inspect = wordsWithPos.zipWithIndex drop (startIdx) take (window)
    val res = {
      inspect.headOption.map { x =>
        val ((word, pos), idx) = x
        if (word == "to")
          SemanticVerb(To, position = startIdx)
        else {
          inspect findFirst (next) getOrElse (SemanticVerb(NA))
        }
      } getOrElse { SemanticVerb(NA) }
    }
    res
  }
}