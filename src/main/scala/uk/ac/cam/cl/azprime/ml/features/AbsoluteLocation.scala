package uk.ac.cam.cl.azprime.ml.features

import uk.ac.cam.cl.azprime.xml.ParsedSciXml
import uk.ac.cam.cl.azprime.ml.ExtractedFeature
import uk.ac.cam.cl.azprime.ml.FeatureExtractor
import uk.ac.cam.cl.azprime.ml.ExtractedFeatures
import uk.ac.cam.cl.azprime.ml.SingleFeatureExtractor

object AbsoluteLocationValues extends Enumeration {
  type Values = Value
  val ABS = Value("ABS")
  val A = Value("A")
  val B = Value("B")
  val C = Value("C")
  val D = Value("D")
  val E = Value("E")
  val F = Value("F")
  val G = Value("G")
  val H = Value("H")
  val I = Value("I")
  val J = Value("J")
}

case class AbsoluteLocation(val value: AbsoluteLocationValues.Values) extends ExtractedFeature {
  type ValueType = AbsoluteLocationValues.Values
  val label = "LOC"
}

class AbsoluteLocationExtractor extends SingleFeatureExtractor {
  import AbsoluteLocationValues._
  type Extracted = AbsoluteLocation
  def extract(sentence: ParsedSciXml.Sentence, document: ParsedSciXml.Document, features: ExtractedFeatures) = {
	if (document.isAbstractSentence(sentence)){
	  new AbsoluteLocation(ABS)
	}else{
	  val (loc, total) = document.locationWithinDocument(sentence)
	  val nloc = (loc.toFloat / total * 20).toInt
	  val cat = nloc match {
      	case 0 => A
      	case 1 => B
      	case 2 => C
      	case 3 => D
      	case 4 | 5 => E
      	case pos if (pos >= 6 && pos <= 13) => F
      	case 14 | 15 => G
      	case 16 | 17 => H
      	case 18 => I
      	case 19 => J
      	case _ => throw new Exception("That's unpossible! (Seriously)")
	  }
	  new AbsoluteLocation(cat)
	}
  }
}