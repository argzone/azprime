package uk.ac.cam.cl.azprime.ml.features

import uk.ac.cam.cl.azprime.resources.Headers
import uk.ac.cam.cl.azprime.xml.ParsedSciXml
import uk.ac.cam.cl.azprime.ml.ExtractedFeature
import uk.ac.cam.cl.azprime.ml.FeatureExtractor
import uk.ac.cam.cl.azprime.ml.ExtractedFeatures
import uk.ac.cam.cl.azprime.ml.SingleFeatureExtractor

object DocumentFinalSectionValues extends Enumeration {
  type Values = Value

  val FinalSection = Value("1a")
  val FinalParagraph = Value("1c")
  val NotFinal = Value("0")
}

case class DocumentFinalSection(val value: DocumentFinalSectionValues.Values) extends ExtractedFeature {
  type ValueType = DocumentFinalSectionValues.Values
  val label = "FSEC"
}

class DocumentFinalSectionExtractor(headers: Headers, finalParagraphs: Int) extends SingleFeatureExtractor {
  import DocumentFinalSectionValues._
  type Extracted = DocumentFinalSection

  val Threshold = finalParagraphs

  def extract(sentence: ParsedSciXml.Sentence, document: ParsedSciXml.Document, features: ExtractedFeatures) = {
    val header = document.header(sentence)
    val res = if (headers.hasEnding(document.allHeaders)) {
      if (headers.ending(header)) FinalSection
      else NotFinal
    } else {
      if (document.paragraphLocation(sentence) >= document.paragraphCount - Threshold) FinalParagraph
      else NotFinal
    }
    new DocumentFinalSection(res)
  }
}