package uk.ac.cam.cl.azprime

import java.io.File
import scala.annotation.elidable
import uk.ac.cam.cl.azprime.config.Configuration
import uk.ac.cam.cl.azprime.resources.Resources
import uk.ac.cam.cl.azprime.ml.FeatureEngine
import uk.ac.cam.cl.azprime.xml.FeatureXmlWriter
import uk.ac.cam.cl.azprime.xml.SciXmlReader
import uk.ac.cam.cl.azprime.ml.features._
import uk.ac.cam.cl.azprime.ml.ExtractedFeature
import uk.ac.cam.cl.util.SimpleLogger
import uk.ac.cam.cl.azprime.resources.Agents
import uk.ac.cam.cl.azprime.weka.ArffFileWriter
import uk.ac.cam.cl.azprime.xml.ParsedSciXml.Document
import uk.ac.cam.cl.azprime.weka.FeatureFileWriter
import uk.ac.cam.cl.azprime.ml.FeatureExtractor

object AZGenerateTrainingData extends App {
  val configFile = if (args.length > 0) args(0) else "classpath:/cfg/config.scala"
  val config = Configuration.load(configFile)
  SimpleLogger.debugOn = config.debugging
  val prime = new AZPrime(config)
}

class AZWrapper(config: Configuration) {
  val resources = new Resources(config)
  val agents = new Agents(resources.patterns, resources.lexicon)

  val extractors = List(
    new SemanticCuePhraseExtractor()(resources.patterns, resources.lexicon),
    new AbsoluteLocationExtractor(),
    new DocumentInitialSectionExtractor(resources.headers, initialParagraphs = 7),
    new DocumentFinalSectionExtractor(resources.headers, finalParagraphs = 5),
    new DocumentAndParagraphLocationExtractor(resources.headers),
    new DivisionLocationExtractor(),
    new ParagraphLocationExtractor(),
    new HeaderCategoryExtractor(resources.headers),
    new SentenceCategoryExtractor(),
    new SentenceLengthExtractor(),
    new SentenceContentExtractor(),
    //		new BodyTfIdfExtractor(bodyTfIdfScores, normalize = false),
    //		new TitleWordsExtractor(titleTfIdfScores.termCounts, normalize = false),
    new SemanticVerbsExtractor(scanWindowSize = 5)(resources.lexicon, resources.actions, agents),
    new SimplifiedSemanticVerbsExtractor(),
    new DocumentIDExtractor(),
    new CitationTypeExtractor(),
    new SelfCitationExtractor(),
    new CitationLocationExtractor(),
    new CitationCountExtractor())
    
  val citationExtractor = new CitationBlockExtractor()

  val featureEngine = new FeatureEngine(List(), extractors);

  def getFeatures(doc: Document): Array[Array[ExtractedFeature]] = {
    val extracted = featureEngine.extractFeatures(doc)
    extracted.toArray[List[ExtractedFeature]].map(x => x.toArray[ExtractedFeature]).toArray[Array[ExtractedFeature]]
  }
  
  def getCitations(doc: Document): java.util.Map[String,Array[Int]] = {
    import collection.JavaConversions._
	citationExtractor.extractCitations(doc).mapValues(_.toArray)
  }
}

class AZPrime(config: Configuration) {
  import SimpleLogger._
  import org.tempura.console.utils._

  // Format YYYYmmdd-HHMMSS
  final val dateFormatStr = "%1$tY%1$tm%1$td-%1$tH%1$tM%1$tS"

  val resources = new Resources(config)
  val agents = new Agents(resources.patterns, resources.lexicon)
  //val files = List(resources.trainingFiles.head)
  val files = resources.trainingFiles
  implicit val stoplist = resources.stoplist

  debug("Collected %d files for training.".format(files.length))

  // Load the input files
  val data = files.map(new SciXmlReader(_).parse())
  val bodyTfIdfScores =
    if (new File("bodyTfIdfScores.bin").exists) {
      org.tempura.utils.ObjectIO.readObjectFromFile[MappedTfIdfScores[Document]]("bodyTfIdfScores.bin")
    } else {
      val scores = BodyTfIdfScores(data)
      // XXX: We never write the file so we never cache results.
      //org.tempura.utils.ObjectIO.writeObjectToFile(scores)("bodyTfIdfScores.bin")
      scores
    }
  val titleTfIdfScores = TitleTfIdfScores(data, includeHeaders = true)

  // Instantiate all the features to use
  val featureExtractors = List(
    new SemanticCuePhraseExtractor()(resources.patterns, resources.lexicon),
    new AbsoluteLocationExtractor(),
    new DocumentInitialSectionExtractor(resources.headers, initialParagraphs = 7),
    new DocumentFinalSectionExtractor(resources.headers, finalParagraphs = 5),
    new DocumentAndParagraphLocationExtractor(resources.headers),
    new DivisionLocationExtractor(),
    new ParagraphLocationExtractor(),
    new HeaderCategoryExtractor(resources.headers),
    new SentenceCategoryExtractor(),
    new SentenceLengthExtractor(),
    new SentenceContentExtractor(),
 //   new BodyTfIdfExtractor(bodyTfIdfScores, normalize = false),
    new TitleWordsExtractor(titleTfIdfScores.termCounts, normalize = false),
    new SemanticVerbsExtractor(scanWindowSize = 5)(resources.lexicon, resources.actions, agents),
    new SimplifiedSemanticVerbsExtractor(),
    new DocumentIDExtractor(),
    new CitationTypeExtractor(),
    new SelfCitationExtractor(),
    new CitationLocationExtractor(),
    new CitationCountExtractor())

  debug("Instantiated %d feature extractors.".format(featureExtractors.length))
  val featureEngine = new FeatureEngine(data, featureExtractors)

  debug("About to run feature engine.")
  var cnt = -1
  def updateProgress(progress: Int) {
    cnt += 1
    val out = "PROGRESS: " + mkProgressBar(50)(cnt, data.length)
    println(out)
  }
  updateProgress(0)
  val features = featureEngine.run(updateProgress)
  println()
  debug("Finished extracting; %d total training instances.".
    format(features.foldLeft(0)((a, b) => a + b.length)))

  // Make sure our output size is the same as our input size
  assert(features.length == files.length)

  val outfilename = config.files.output + "/" +
    ("all-" + dateFormatStr + ".arff").format(new java.util.Date())
  writeArffFile(outfilename, data zip features)
  debug("Done.")

  def writeArffFile(filename: String, data: List[(Document, FeatureEngine.DocumentFeatures)]) = {
    // Write out the features
    // TODO We need to modify the file writer to output a set of files' features.
    val outfile = new File(config.files.output, filename)
    new ArffFileWriter(data)(ignored = Set("ISEC", "FSEC", "SEMVERBS")).writeFile(outfile)
  }

  def writeFeatureFile(filename: String, data: List[(Document, FeatureEngine.DocumentFeatures)]) = {
    val outfile = new File(config.files.output, filename)
    new FeatureFileWriter(data)(ignored = Set("ISEC", "FSEC", "SEMVERBS")).writeFile(outfile)
  }

  def writeSciXmlFiles(filename: String, data: List[(Document, File, FeatureEngine.DocumentFeatures)]) = {
    data.map(x => {
      val (doc, infile, fea) = x
      val outfile = new File(config.files.output, infile + ".fea")
      new FeatureXmlWriter(fea, doc, ignored = Set("ISEC", "FSEC", "SEMVERBS")).writeFile(outfile)
    })
  }
}