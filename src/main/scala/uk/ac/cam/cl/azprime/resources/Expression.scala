package uk.ac.cam.cl.azprime.resources

import scala.annotation.tailrec

object FormulaicExpression {
  trait Token[T] {
    val value: T
    val isTrigger: Boolean
  }
  case class POS(value: ValidPOS.Values, isTrigger: Boolean) extends Token[ValidPOS.Values]
  case class Variable(value: String, isTrigger: Boolean) extends Token[String]
  case class Word(value: String, isTrigger: Boolean) extends Token[String]

  object ValidPOS extends Enumeration {
    type Values = Value
    val JJ = Value("JJ")
    val VV_ING = Value("VV_ING")
    val CD = Value("CD")
    val VV = Value("VV")
    val NN = Value("NN")
    val RB = Value("RB")
    val SELFCITE = Value("SELFCITE")
    val CREF = Value("CREF")
    val CITE = Value("CITE")
  }

  val validPOS = ValidPOS.values.map(_.toString)

  val posMappings = Map(
    ValidPOS.JJ -> "^JJ",
    ValidPOS.VV_ING -> "^VBG",
    ValidPOS.CD -> "^CD$",
    ValidPOS.VV -> "^VB$",
    ValidPOS.NN -> "^NN",
    ValidPOS.RB -> "^RB",
    ValidPOS.SELFCITE -> "-SELF$",
    ValidPOS.CREF -> "^CREF",
    ValidPOS.CITE -> "^REF")

  def posMatches(expectedPos: ValidPOS.Values)(pos: String) = {
    import org.tempura.utils.StringOps._
    pos =~ posMappings(expectedPos)
  }

  def isTrigger(str: String) = {
    if (str startsWith "^") (str drop 1, true)
    else (str, false)
  }

  val extractPOS: PartialFunction[(String, Boolean), POS] = {
    case (pos, trigger) if (validPOS.contains(pos)) => new POS(ValidPOS.withName(pos), trigger)
  }
  val extractVariable: PartialFunction[(String, Boolean), Variable] = {
    case (v, trigger) if (v.startsWith("@")) => new Variable(v drop 1, trigger)
  }
  val extractWord: PartialFunction[(String, Boolean), Word] = {
    case (w, trigger) => new Word(w, trigger)
  }

  object MatchDirection extends Enumeration {
    type Values = Value
    val Forward = Value("forward")
    val Backward = Value("backward")
  }

  /**
   * &is_formulaic( $formulaic_expression, $indx, $formu_trigger );
   * - checks if starting at $indx offset in a given sentence,
   *   if $formulaic_expression matches, having used $formu_trigger.
   * - Basically, it iterates over each token of the formulaic_expression,
   *   and scans the sentence starting at the index, matching one-by-one;
   *   there are three cases; either a POS matches (JJ, CD, etc.), a variable
   *   must be satisfied (@VAR), or it is a normal word that must be matched.
   *   POS: Simple mappings
   *   @VAR:
   *   - If variable name contains NOUN or ADJ we check the pos of the word in
   *     addition to matching the strings. RB can match JJ
   *   [word]: Simple match
   * - Finally, checks if the trigger is contained in the matched string (for sanity?)
   *
   * @returns end of matched tokens if success, or None otherwise
   */
  def matches(expression: FormulaicExpression,
              input: List[(String, String)],
              offset: Int,
              matchDir: MatchDirection.Values = MatchDirection.Forward)(implicit lexicon: Lexicon, lemmatiser: (Tuple2[String, String] => String) = { a => a._1 }): Option[(Int, Int)] = {
    import org.tempura.utils.StringOps._
    // Maps expression tokens to expected pos labels.
    val posExceptions = Set("RB")

    val (input_, tokens_) = {
      if (matchDir == MatchDirection.Forward)
        (input.drop(offset), expression.tokens)
      else
        (input.take(offset + 1).reverse, expression.tokens.reverse)
    }
    def isRB = FormulaicExpression.posMatches(ValidPOS.RB) _
    @tailrec
    def matchInput(input: List[(String, String)], tokens: List[Token[_]], inputOffset: Int, tokensOffset: Int): Option[Int] = {
      if (tokensOffset == tokens.length)
        Some(inputOffset)
      else if (inputOffset == input.length)
        None // End of input before end of tokens
      else {
        val (word_, pos) = input(inputOffset)
        val token = tokens(tokensOffset)
        val word = lemmatiser(word_, pos)
        val res = token match {
          case t: POS => pos =~ posMappings(t.value)
          case t: Variable =>
            val values = lexicon.byclass(t.value)
            if ((values contains word) || (values contains word_)) {
              t.value match {
                case d if d endsWith "NOUN" => pos =~ posMappings(ValidPOS.NN)
                case d if d endsWith "ADJ"  => pos =~ posMappings(ValidPOS.JJ) || posExceptions(pos)
                case _                      => true
              }
            } else false
          case t: Word => token.value == word || token.value == word_
        }
        if (!res && isRB(pos)) // No match but we're an adverb, try on next word of input w/ same token.
          matchInput(input, tokens, inputOffset + 1, tokensOffset)
        else if (res) // We matched so check next word/token
          matchInput(input, tokens, inputOffset + 1, tokensOffset + 1)
        else {
          None
        }
      }
    }

    // Aligns input starting at offset with formulaic expression tokens and compares them while
    //   equality holds. Then we check if the lengths are the same to see if it matched or
    //   not.
    val res2 = matchInput(input_, tokens_, 0, 0)
    if (res2.isDefined) {
      if (matchDir == MatchDirection.Forward)
        Some(offset, offset + res2.get)
      else
        Some(offset - res2.get + 1, offset)
    } else {
      None
    }
  }
}

abstract class FormulaicExpression(val pattern: String, val label: String) {
  lazy val (tokens, triggers) = {
    import FormulaicExpression._
    val parts = pattern split """\s+"""
    val exp = parts.
      map(isTrigger).
      map(extractPOS orElse extractVariable orElse extractWord)
    (exp.toList.asInstanceOf[List[FormulaicExpression.Token[_]]],
      exp.zipWithIndex.filter(x => x._1.isTrigger).toMap[FormulaicExpression.Token[_], Int])
  }
  lazy val trigger: FormulaicExpression.Token[_] = if (triggers.size > 0) triggers.head._1 else tokens.head
  
  val featureString =  label + "_" + pattern
}

case class ExpressionMatch[T <% FormulaicExpression](expression: T, start: Int, end: Int) {
  def from(words: List[String]) = {
    words.slice(start, end)
  }
  def toString(words: List[String]) = {
    from(words).mkString(" ")
  }
}

object ExpressionMatch {
  type BasicExpressionMatch = ExpressionMatch[FormulaicExpression]
}