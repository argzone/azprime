package uk.ac.cam.cl.azprime.ml.features

import uk.ac.cam.cl.azprime.xml.ParsedSciXml
import uk.ac.cam.cl.azprime.ml.ExtractedFeature
import uk.ac.cam.cl.azprime.ml.FeatureExtractor
import uk.ac.cam.cl.azprime.ml.ExtractedFeatures
import uk.ac.cam.cl.azprime.ml.SingleFeatureExtractor

object DivisionLocationValues extends Enumeration {
  type Values = Value

  val First = Value("first")
  val Last = Value("last")
  val SecondOrThird = Value("second_or_third")
  val SecondOrThirdToLast = Value("second_or_third_last")
  val FirstThird = Value("first_third")
  val SecondThird = Value("second_third")
  val LastThird = Value("last_third")
}

case class DivisionLocation(val value: DivisionLocationValues.Values) extends ExtractedFeature {
  type ValueType = DivisionLocationValues.Values
  val label = "DIV"
}

class DivisionLocationExtractor extends SingleFeatureExtractor {
  import DivisionLocationValues._
  type Extracted = DivisionLocation
  def extract(sentence: ParsedSciXml.Sentence, document: ParsedSciXml.Document, features: ExtractedFeatures) = {

	  	 val (loc, total) = document.locationWithinRelativeDivision(sentence)
    val cat =
      if (loc == 0) First
      else if (loc + 1 == total) Last
      else if (loc < 3) SecondOrThird
      else if (total - loc <= 3) SecondOrThirdToLast
      else if (loc.toFloat / total < 1.0 / 3) FirstThird
      else if (loc.toFloat / total > 2.0 / 3) LastThird
      else SecondThird

    new DivisionLocation(cat)
	}
  
}