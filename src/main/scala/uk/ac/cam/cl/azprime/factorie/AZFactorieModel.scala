package uk.ac.cam.cl.azprime.factorie

import cc.factorie._
import uk.ac.cam.cl.azprime.classify._
import perceptron.struct.Feature

class AZFactorieModel {  
  object SentenceDomain extends CategoricalDimensionTensorDomain[String]
  class Sentence(features:Seq[String], labelString:String) extends BinaryFeatureVectorVariable[String] with ChainLink[Sentence,Document] {
    def domain = SentenceDomain
    val label: Label = new Label(labelString, this)
    this ++= features
    
    override def toString: String = {
      labelString + "\t" + features.mkString(" ")
    }
  }
  
  object LabelDomain extends CategoricalDomain[String]
  class Label(labelname: String, val sentence: Sentence) extends LabeledCategoricalVariable(labelname) {
    def domain = LabelDomain
    def hasNext = sentence.hasNext && sentence.next.label != null
    def hasPrev = sentence.hasPrev && sentence.prev.label != null
    def next = sentence.next.label
    def prev = sentence.prev.label
  }
 
  class Document extends Chain[Document,Sentence]{
    override def toString: String = {
      this.asSeq.map(_.toString).mkString("\n")
    }
  }
  
  def initModel(): CombinedModel = {
    new CombinedModel(
      
    // Bias term on each individual label 
    new DotTemplateWithStatistics1[Label] {
      override def neighborDomain1 = LabelDomain
      lazy val weights = new la.DenseTensor1(LabelDomain.size)
    },
    
    // Transition factors between two successive labels
    new DotTemplateWithStatistics2[Label, Label] {
      override def neighborDomain1 = LabelDomain
      override def neighborDomain2 = LabelDomain
      //override def statisticsDomains = ((LabelDomain, LabelDomain))
      lazy val weights = new la.DenseTensor2(LabelDomain.size, LabelDomain.size)
      def unroll1(label: Label) = if (label.hasPrev) Factor(label.prev, label) else Nil
      def unroll2(label: Label) = if (label.hasNext) Factor(label, label.next) else Nil
    },
    // Factor between label and observed token
    new DotTemplateWithStatistics2[Label, Sentence] {
      override def neighborDomain1 = LabelDomain
      override def neighborDomain2 = SentenceDomain
      //override def statisticsDomains = ((LabelDomain, TokenDomain))
      lazy val weights = new la.DenseTensor2(LabelDomain.size, SentenceDomain.dimensionSize)
      def unroll1(label: Label) = Factor(label, label.sentence)
      def unroll2(sentence: Sentence) = throw new Error("Sentence values shouldn't change")
    }
  )
  }
 
  val persistentModel = initModel
    
  
  class WeightedTemplate(weights: java.util.Map[String,Double])(implicit am:Manifest[Label], tm:Manifest[Label#TargetType]) extends TupleTemplateWithStatistics2[Label,Label#TargetType] {
	     import collection.JavaConverters._  
    for (i <- weights.keySet().asScala) println(i + " " + weights.get(i))
    
    def unroll1(aimer:Label) = Factor(aimer, aimer.target)
  def unroll2(target:Label#TargetType) = throw new Error("Cannot unroll from the target variable.")
  def score(value1:Label#Value, value2:Label#TargetType#Value) = {
  //    println(value1 + " " + weights.get(value1.toString))
   //   println(value2 + " " + weights.get(value2.toString) + " " + (1000*weights.get(value2.toString)))
      if (value1 == value2) 1.0 else 0.0 // TODO
    }
  def accuracy(context: Iterable[Label]): Double = context.map(currentScore(_)).sum / context.size
  //def score(value1:A#Value, value2:A#TargetType#Value) = if (value1 == value2) 1.0 else 0.0
}
  
  def readDocument(features: Seq[java.util.Map[String,Double]], labels: Seq[String]): Document = {
     import collection.JavaConverters._
     val doc = new Document
     doc ++= features.zipWithIndex.map({x => new Sentence(x._1.keySet.asScala.toSeq,new String(labels(x._2)))})
     doc
  }
  
  def trainAndPredict(trainFeatures: Array[Array[java.util.Map[String,Double]]], trainLabels: Array[Array[String]], its: Int, testFeatures: Array[Array[java.util.Map[String,Double]]], testLabels: Array[Array[String]]): Array[Array[String]] = {    
	val localModel = initModel();
    
    val objective = new HammingTemplate[Label]
    val trainingDocuments = for (i <- List.range(0,trainFeatures.size)) yield readDocument(trainFeatures(i),trainLabels(i))
    val testDocuments = for (i <- List.range(0,testFeatures.size)) yield readDocument(testFeatures(i),testLabels(i))
    
    val trainingData = trainingDocuments.flatMap(_.links.map(_.label))
    val testData = testDocuments.flatMap(_.links.map(_.label))
    
    trainingData.foreach(_.setRandomly())
    testData.foreach(_.setRandomly())
    
    val learner = new optimize.SampleRankTrainer(new GibbsSampler(localModel, objective), new cc.factorie.optimize.MIRA)
    val predictor = new IteratedConditionalModes[Label](localModel, null)
   
    for (i <- 1 to its) {
      println("Iteration "+i) 
      learner.processContexts(trainingData)
      predictor.processAll(testData)
      println ("Train accuracy = "+ objective.accuracy(trainingData))
      println ("Test  accuracy = "+ objective.accuracy(testData))
    }
    
    predictor.temperature *= 0.1
    predictor.processAll(testData, 2)
    
    println ("Final Test  accuracy = "+ objective.accuracy(testData))
    
    testDocuments.map(_.links.map(_.label.categoryValue).toArray).toArray
  }
  
  
  def train(features: Array[Array[java.util.Map[String,Double]]], labels: Array[Array[String]], its: Int, weights: java.util.Map[String,Double]){
	  // The training objective
  val objective = new HammingTemplate[Label]
//    val objective = new WeightedTemplate(weights)
	val trainingDocuments = for (i <- List.range(0,features.size)) yield readDocument(features(i),labels(i))
	//   println(trainingDocuments(0).toString)
	   val learner = new optimize.SampleRankTrainer(new GibbsSampler(persistentModel, objective), new cc.factorie.optimize.MIRA)
       val trainLabels = trainingDocuments.flatMap(_.links.map(_.label))
       trainLabels.foreach(_.setRandomly())
       for (i <- 1 to its) {
    	   println("Iteration "+i) 
    	   learner.processContexts(trainLabels)
    	   println ("Train accuracy = "+ objective.accuracy(trainLabels))
       }
  }
  
  val predictor = new IteratedConditionalModes[Label](persistentModel, null)
  
  def predict(features: Array[java.util.Map[String,Double]], labels: Array[String]): Array[String] = {
	  val dummyLabels = for (i <- List.range(0,features.size)) yield "" // causes exception
	  val doc = readDocument(features,labels)
	  predictor.processAll(doc.links.map(_.label))
	  BP.inferChainMax(doc.asSeq.map(_.label), persistentModel)
	  doc.asSeq.map(_.label.categoryValue).toArray
  }
  
}