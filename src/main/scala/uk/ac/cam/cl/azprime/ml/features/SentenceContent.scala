package uk.ac.cam.cl.azprime.ml.features

import uk.ac.cam.cl.azprime.xml.ParsedSciXml
import uk.ac.cam.cl.azprime.ml.ExtractedFeature
import uk.ac.cam.cl.azprime.ml.FeatureExtractor
import uk.ac.cam.cl.azprime.ml.ExtractedFeatures
import uk.ac.cam.cl.azprime.ml.SingleFeatureExtractor

case class SentenceContent(val value: String) extends ExtractedFeature {
  type ValueType = String
  val label = "SEN"
}

// NOTE: Only one of these two extractors should be used at a time!

class SentenceContentExtractor extends SingleFeatureExtractor {
  type Extracted = SentenceContent
  def extract(sentence: ParsedSciXml.Sentence, document: ParsedSciXml.Document, features: ExtractedFeatures) = {
    new SentenceContent(sentence.words.mkString(" "))
  }
}

class SentenceContentNoCitesExtractor extends SingleFeatureExtractor {
  type Extracted = SentenceContent
  def extract(sentence: ParsedSciXml.Sentence, document: ParsedSciXml.Document, features: ExtractedFeatures) = {
    import org.tempura.utils.StringOps._
    def replaceRefs(wordWithPos: (String, String)) = {
      val (word, pos) = wordWithPos
      if (pos =~ "^REF"){ 
        "CITE" 
       	} else if (pos =~ "EQN"){
       		"EQN"
       	} else if (pos =~ "CREF") 
       		""       	
       	else{
       		word
       	}   
      }
    new SentenceContent(sentence.wordsWithPos.map(replaceRefs).mkString(" ").replaceAll(",",""))
  }
}