package uk.ac.cam.cl.azprime.resources

import java.io.InputStream
import scala.collection.immutable.ListSet

/**
 * A class to encapsulate the headersort file. Basically, it contains
 * A map of headers -> categories, and two arrays for categories that
 * occur at the beginning and ending of documents.
 */
class Headers(instream: InputStream) {
  val lines = io.Source.fromInputStream(instream).getLines
  private lazy val (starts, ends, headers) = {
    import collection.mutable.Map
    var starts = List[String]()
    var ends = List[String]()
    val headers = Map[String, String]()
    for (line_ <- lines; line = line_.trim; if (!DataFile.isComment(line))) {
      val Array(category, parts @ _*) = line split """\s+"""
      if (category == "AREA") {
        parts.toList match {
          case "beginning" :: rest => starts = rest
          case "end" :: rest => ends = rest
          case _ => throw new Exception("""AREA must be followed by "beginning" or "end".""")
        }
      } else {
        headers += parts.mkString(" ") -> category
      }
    }
    val startingHeaders = headers.filter(x => starts.contains(x._2)).keys.toSet
    val endingHeaders = headers.filter(x => ends.contains(x._2)).keys.toSet
    (startingHeaders, endingHeaders, headers.toMap withDefaultValue null)
  }
  def apply(header: String) = {
    headers(header.toLowerCase)
  }
  def starting(header: String) = {
    starts(header.toLowerCase)
  }
  def ending(header: String) = {
    ends(header.toLowerCase)
  }
  def hasStarting(headers: Iterable[String]) = {
    headers.exists(h => starts(h.toLowerCase))
  }
  def hasEnding(headers: Iterable[String]) = {
    headers.exists(h => ends(h.toLowerCase))
  }
}