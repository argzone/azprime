package uk.ac.cam.cl.azprime.ml

import java.io.File
import uk.ac.cam.cl.azprime.xml.XmlUtils
import uk.ac.cam.cl.azprime.xml.ParsedSciXml
import uk.ac.cam.cl.azprime.ml.features._
import uk.ac.cam.cl.azprime.xml.SciXmlReader
import scala.collection.immutable.ListSet
import scala.collection.immutable.ListMap

object FeatureEngine {
  type SentenceFeatures = List[ExtractedFeature]
  type DocumentFeatures = List[SentenceFeatures]

  def checkExtractorDependencies(extractors: List[FeatureExtractor]) = {
    val lookup = extractors.map(_.getClass).zip(extractors).toMap
    def recurse(stack: List[Class[FeatureExtractor]]): Unit = {
      lookup(stack.last).dependencies.foreach(d => {
        if (stack.contains(d)) {
          throw new Exception("Cyclic dependency in feature extractors found: %s".
            format((stack :+ d).map(_.getSimpleName).mkString("->")))
        }
        recurse(stack :+ d)
      })
    }
    extractors.foreach(e => {
      recurse(List(e.getClass.asInstanceOf[Class[FeatureExtractor]]))
    })
  }
}

// TODO (dk): move away from using ParsedSciXml.Document.
class FeatureEngine(documents: List[ParsedSciXml.Document], extractors: List[FeatureExtractor]) {

  // TODO (dk): validate that the extractor dependencies are sound, or throw exception
  FeatureEngine.checkExtractorDependencies(extractors)

  def run(callback: Int => Unit = (x => Unit)): List[FeatureEngine.DocumentFeatures] = {
    documents.zipWithIndex.par.map { x =>
      val fea = extractFeatures(x._1)
      callback(x._2)
      fea
    }.toList
  }

  // This method takes a document, and for each sentence, *collapses* the list
  // of feature extractors, creating a pipeline where every extractor has read-only 
  // access to all the extracted features that came before it.
  def extractFeatures(doc: ParsedSciXml.Document): List[FeatureEngine.SentenceFeatures] = {
    val extracted = doc.allSentences.map(s => {
      val initial = ExtractedFeatures()
      extractors.foldLeft(initial)((features, e) => {
        val ret = e.extractAll(s, doc, features)
        features ++ ExtractedFeatures(ret: _*)
      }).values
    })
    extracted
  }
}