package uk.ac.cam.cl.azprime.resources

import org.tempura.utils.CollectionOps._

class Agents(patterns: Patterns, lexicon: Lexicon) {

  val agentLabels = patterns.expressionsByLabel.filter(_._1 endsWith "_A")
  val agentExpressions = agentLabels.values.flatten.toList

  private def mapWordOrVarToken(exp: FormulaicExpression)(x: FormulaicExpression.Token[_]) = {
    x match {
      case x: FormulaicExpression.POS      => List()
      case x: FormulaicExpression.Word     => List(x.value -> exp)
      case x: FormulaicExpression.Variable => lexicon.byclass(x.value).map(_ -> exp).toList
    }
  }

  private def mapPosToken(exp: FormulaicExpression)(x: FormulaicExpression.Token[_]) = {
    x match {
      case x: FormulaicExpression.POS      => List(FormulaicExpression.posMappings(x.value) -> exp)
      case x: FormulaicExpression.Word     => List()
      case x: FormulaicExpression.Variable => List()
    }
  }

  import uk.ac.cam.cl.azprime.resources.FormulaicExpression;
  import uk.ac.cam.cl.azprime.resources.Lexicon;
  import uk.ac.cam.cl.azprime.resources.Patterns;

  val (wordStarts, wordEnds, posStarts_, posEnds_) = {

    val expWithFirstTokens = agentExpressions
      .map(exp => exp.tokens.headOption.map(exp -> _))
      .flatten

    val expWithLastTokens = agentExpressions
      .map(exp => exp.tokens.lastOption.map(exp -> _))
      .flatten

    val ws = expWithFirstTokens
      .flatMap({ case (exp, t) => mapWordOrVarToken(exp)(t) })
      .groupByKey
      .withDefaultValue(List())
    val we = expWithLastTokens
      .flatMap({ case (exp, t) => mapWordOrVarToken(exp)(t) })
      .groupByKey
      .withDefaultValue(List())
    val ps = expWithFirstTokens
      .flatMap({ case (exp, t) => mapPosToken(exp)(t) })
      .groupByKey
      .withDefaultValue(List())
    val pe = expWithLastTokens
      .flatMap({ case (exp, t) => mapPosToken(exp)(t) })
      .groupByKey
      .withDefaultValue(List())
    (ws, we, ps, pe)
  }

  def patternStartsWithWord(str: String) = wordStarts contains str
  def patternEndsWithWord(str: String) = wordEnds contains str

  def posStarts(pos: String) = patternsWithPos(posStarts_)(pos)
  def posEnds(pos: String) = patternsWithPos(posEnds_)(pos)

  def patternsWithPos(list: Map[String, List[FormulaicExpression]])(pos: String) = {
    import org.tempura.utils.StringOps._
    val matched = list.keys.filter(pos =~ _)
    matched.flatMap(k => list(k)).toList
  }

  def matches(input: List[(String, String)], offset: Int, matchDir: FormulaicExpression.MatchDirection.Values) = {
    val (word, pos) = input(offset)
    val expressions = {
      if (matchDir == FormulaicExpression.MatchDirection.Backward)
        wordEnds(word) ++ posEnds(pos)
      else
        wordStarts(word) ++ posStarts(pos)
    }

    val matched =
      for (expression <- expressions) yield {
        val res = FormulaicExpression.matches(expression, input, offset, matchDir)(lexicon)
        res.map(x => ExpressionMatch(expression, x._1, x._2))
      }
    matched flatten
  }
}
