package uk.ac.cam.cl.azprime

// NOT USED YET!
class WordWithPos(tuple: (String, String)) {
  def word = tuple._1
  def pos = tuple._2
}
object WordWithPos {
  implicit def toWordWithPos(tuple: (String, String)) = new WordWithPos(tuple)
}
