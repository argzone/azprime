package uk.ac.cam.cl.util

object SimpleLogger {
  var debugOn = false
  def debug(str: => String) = {
	  if (debugOn) println(str)
  }
}