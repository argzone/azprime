package uk.ac.cam.cl.util

import org.tempura.utils.NumericMergeMaps._
import scala.Array.canBuildFrom
import scala.collection.immutable.ListMap
import scala.xml.XML
import uk.ac.cam.cl.azprime.resources.StopWords

/**
 * Calculates term frequencies for a given sequence of terms. Terms can be any sequence of arbitrary strings.
 */
class TermCounts protected (counts: Map[String, Int]) {
  def this(terms: Seq[String])(implicit stopwords: StopWords = StopWords.None) = {
    this(TermCounts.getCounts(terms.filter(!stopwords(_))))
  }

  val sortedList = counts.toList.view.sortBy(_._2)(implicitly[Ordering[Int]].reverse)
  val sortedCounts = ListMap().withDefaultValue(0) ++ sortedList

  def apply(word: String) = sortedCounts(word)

  def +(that: TermCounts) = {
    new TermCounts((TermCounts.this.sortedCounts) merge that.sortedCounts)
  }

  def keys = {
    sortedCounts.keys
  }
}

object TermCounts {
  def getCounts(terms: Seq[String]) = {
    def getCounts(terms: Seq[String]): Map[String, Int] = {
      val res = (for ((word, reps) <- terms.map(normalizeTerm).groupBy(t => t))
        yield (word, reps.length)).toMap
      res
    }

    def normalizeTerm(term: String) = term.toLowerCase
    getCounts(terms.asInstanceOf[Seq[String]])
  }
}


// TODO (dk): Probably delete the stuff below here.

trait StringSequence {
  def getSeq(filepath: String): Seq[String]
}

object SciXmlWordSequence extends StringSequence {
  def getSeq(filepath: String): Seq[String] = {
    val root = XML.load(filepath)
    for (
      sentence <- root \ "S";
      word_ <- sentence \ "W";
      word = word_.text
    ) yield word
  }
}

object PlainTextWordSequence extends StringSequence {
  def getSeq(filepath: String): Seq[String] = {
    val text = io.Source.fromFile(filepath)
    (for (
      sentence <- text.getLines;
      word <- sentence split " "
    ) yield word).toSeq
  }
}

object PlainTextLineSequence extends StringSequence {
  def getSeq(filepath: String): Seq[String] = {
    val text = io.Source.fromFile(filepath)
    text.getLines.toSeq
  }
}