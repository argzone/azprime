package uk.ac.cam.cl.util

import uk.ac.cam.cl.azprime.resources.StopWords

case class IndexableDocument(terms: Seq[String])

// Rename StringCounts to TermFrequencies? or TermCounts?
// TODO (dk): normalize counts for each document?
@serializable
class TfIdfScores(docs: List[IndexableDocument])(implicit stopwords: StopWords) {
  // Main data structures for calculating tf and idf
  lazy val (
    localDocTermCounts,
    globalTermCounts,
    documentCountsByTerm
    ) = TfIdfScores.compileGlobalTermCounts(docs)

  // Hash of term -> idf score
  lazy val idf = {
    globalTermCounts.keys.map(t => (t, computeIdf(t))).toMap
  }

  private def computeIdf(term: String) = {
    val docCount = localDocTermCounts.size
    val idf = math.log(docCount / documentCountsByTerm(term))
    idf
  }

  def tf(word: String, doc: IndexableDocument) = {
    localDocTermCounts(doc)(word)
  }

  def tfidf(term: String, doc: IndexableDocument) = {
    val lowercased = term.toLowerCase
    if (stopwords(lowercased)) 0.0d
    else tf(lowercased, doc) * idf(lowercased)
  }
}

@serializable object TfIdfScores {
  def compileGlobalTermCounts(docs: List[IndexableDocument])(implicit stopwords: StopWords) = {
    val docCounts = docs.par.map(d => (d, new TermCounts(d.terms))).toList
    val (_, counts) = docCounts.unzip
    val (globalCounts, fileCounts) = compileTermCounts(counts)
    (docCounts.toMap, globalCounts, fileCounts)
  }

  // globalCounts: total numbers for a word across all docs
  // fileCounts: number of documents a word appears in
  private def compileTermCounts(counts: List[TermCounts]) = {
    val globalCounts = counts.reduceLeft((gbl, lcl) => gbl + lcl)
    val fileCounts = counts.flatMap(_.keys)
      .groupBy(k => k)
      .map(kv => (kv._1, kv._2.length))
      .toMap
    (globalCounts, fileCounts)
  }
}