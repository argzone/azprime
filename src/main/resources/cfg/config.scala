import uk.ac.cam.cl.azprime.config._

// See ac.uk.cam.cl.azprime.config.Configuration for possible values that can be overridden.
new Configuration(
  debugging = true,
  files = new Filepaths(output = "trained/"))