package uk.ac.cam.cl.azprime.classify;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Set;
import java.util.regex.Pattern;

import cc.mallet.fst.CRF;
import cc.mallet.fst.CRFTrainerByL1LabelLikelihood;
import cc.mallet.fst.CRFTrainerByLabelLikelihood;
import cc.mallet.fst.SimpleTagger;
import cc.mallet.fst.TokenAccuracyEvaluator;
import cc.mallet.fst.Transducer;
import cc.mallet.fst.TransducerEvaluator;
import cc.mallet.pipe.Pipe;
import cc.mallet.types.Instance;
import cc.mallet.types.InstanceList;
import cc.mallet.types.Sequence;

public class CRFClassifier extends AZClassifier{
	private CRF crf;
	private Pipe pipe;
	private double gaussianVariance;
	private int its;
	private Regulariser reg;
	private int[] orders;

	public CRFClassifier(int i, Regulariser r, int order, double gv, AZProblem azProb){
		super(azProb);
		gaussianVariance = gv;
		reg = r;
		its = i;

		orders = new int[order];
		for (int o = 0; o < order; o++){
			orders[o] = o+1;
		}

		crf = null;
		pipe = null;
	}


	@Override
	public void train(AZProblem azProb) {
		crf = null;

		ArrayList<String> trainStrings = new ArrayList<String>();
		for (AZDoc doc : azProb.docs){
			trainStrings.add(doc.getFeatureString(extractor));
		}

		pipe = new SimpleTagger.SimpleTaggerSentence2FeatureVectorSequence();
		pipe.getTargetAlphabet().lookupIndex("0");
		pipe.setTargetProcessing(true);
		InstanceList trainingData = new InstanceList(pipe);
		trainingData.addThruPipe(new FeatureIterator(trainStrings));

		TransducerEvaluator eval = new TokenAccuracyEvaluator(new InstanceList[]{trainingData}, new String[] {"Training"});

		crf = trainCRF(trainingData, null, eval, orders, "0", "\\s", ".*", true, its, gaussianVariance, null, reg);
	}

	// Borrowed from MALLET's SimpleTagger class
	private CRF trainCRF(InstanceList training, InstanceList testing,TransducerEvaluator eval, int[] orders,String defaultLabel,String forbidden, String allowed, boolean connected, int iterations, double param, CRF crf, Regulariser reg){
		Pattern forbiddenPat = Pattern.compile(forbidden);
		Pattern allowedPat = Pattern.compile(allowed);
		if (crf == null) {
			crf = new CRF(training.getPipe(), (Pipe)null);
			String startName =
					crf.addOrderNStates(training, orders, null,
							defaultLabel, forbiddenPat, allowedPat,
							connected);
			for (int i = 0; i < crf.numStates(); i++)
				crf.getState(i).setInitialWeight (Transducer.IMPOSSIBLE_WEIGHT);
			crf.getState(startName).setInitialWeight(0.0);
		}


		CRFTrainerByLabelLikelihood crft = null;

		if (reg == Regulariser.L2){
			crft = new CRFTrainerByLabelLikelihood (crf);
			crft.setGaussianPriorVariance(param);
		}else{
			CRFTrainerByL1LabelLikelihood crftemp = new CRFTrainerByL1LabelLikelihood(crf);
			crftemp.setL1RegularizationWeight(param);
			crft = crftemp;
		}

		boolean converged;
		for (int i = 1; i <= iterations; i++) {
			converged = crft.train (training, 1);
			if (i % 1 == 0 && eval != null) // Change the 1 to higher integer to evaluate less often
				eval.evaluate(crft);
			if (converged)
				break;
		}

		return crf;
	}

	@Override
	public int[][] predict(AZProblem prob){
		AZDoc[] docs = prob.docs; 

		int[][] predictions = new int[docs.length][];
		for (int i = 0; i < docs.length; i++){
			predictions[i] = predictDoc(docs[i]);
		}

		return predictions;
	}

	public int[] predictDoc(AZDoc doc) {
		assert pipe != null && crf != null : "Must train first!";

		ArrayList<String> testStrings = new ArrayList<String>();
		testStrings.add(doc.getFeatureString(extractor));

		InstanceList testData = new InstanceList(pipe);
		testData.addThruPipe(new FeatureIterator(testStrings));

		Instance t = testData.get(0);				
		Sequence input = (Sequence) t.getData();
		Sequence output = crf.transduce(input);

		assert input.size() == doc.features.length;

		int[] predictions = new int[input.size()];
		for (int s = 0; s < input.size(); s++){
			predictions[s] = Integer.parseInt((String) output.get(s));
		}

		return predictions;
	}

	@Override
	public int[][] crossValidate(AZProblem prob, Set<Integer>[] folds) {
		int[][] predictions = new int[prob.docs.length][];
		for (int f = 0; f < folds.length; f++){
			ArrayList<String> trainStrings = new ArrayList<String>();

			for (int i = 0; i < prob.docs.length; i++){
				AZDoc doc = prob.docs[i];
				if (folds[f].contains(i) == false){
					trainStrings.add(doc.getFeatureString(extractor));
				}
			}

			pipe = new SimpleTagger.SimpleTaggerSentence2FeatureVectorSequence();
			pipe.getTargetAlphabet().lookupIndex("0");
			pipe.setTargetProcessing(true);
			InstanceList trainingData = new InstanceList(pipe);
			trainingData.addThruPipe(new FeatureIterator(trainStrings));

			TransducerEvaluator eval = new TokenAccuracyEvaluator(new InstanceList[]{trainingData}, new String[] {"Training"});

			crf = trainCRF(trainingData, null, eval, orders, "0", "\\s", ".*", true, its, gaussianVariance, null, reg);

			for (int d : folds[f]){
				predictions[d] = predictDoc(prob.docs[d]);
			}
		}

		crf = null;
		pipe = null;
		
		return predictions;
	}

	@Override
	public int[][] crossValidateAndOptimise(AZProblem prob, Set<Integer>[] folds) {
		// TODO Auto-generated method stub
		return null;
	}


	private class FeatureIterator implements Iterator<Instance>{
		private ArrayList<String> features;
		private int i;

		public FeatureIterator(ArrayList<String> f){
			features = f;
			i = 0;
		}

		@Override
		public boolean hasNext() {
			return i < features.size();
		}

		@Override
		public Instance next() {
			Instance inst = new Instance(features.get(i),null,new Integer(i),null);
			i++;
			return inst;
		}

		@Override
		public void remove() {
			throw new IllegalStateException ("This Iterator<Instance> does not support remove().");
		}
	}
}
