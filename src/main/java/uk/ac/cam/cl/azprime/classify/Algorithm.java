package uk.ac.cam.cl.azprime.classify;

public enum Algorithm {
	LR,CRF,SVM,PERCEPTRON,FACTORIE
}
 