package uk.ac.cam.cl.azprime.cv;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;

public class AZEvaluator {
	private final static DecimalFormat df = new DecimalFormat("#.###");
	
	public static double[][] evaluate(int[][] gold, int[][] predicted, int numLabels, boolean print){
		return evaluate(gold,predicted,(ArrayList<String>)Arrays.asList(new String[numLabels+1]),print);
	}
		
	public static double[][] evaluate(int[][] gold, int[][] predictions, ArrayList<String> labelList, boolean print){
		/*
		 * Per-sentence accuracy
		 * Per-document accuracy
		 * Precision per label and average
		 * Recall per label and average
		 * F-score per label and average
		 */

		int numLabels = labelList.size();

		int[] posGold = new int[numLabels];
		int[] posPredicted = new int[numLabels];
		int[] posCorrect = new int[numLabels];
		int[] correctPerDoc = new int[predictions.length];
		int correct = 0;
		int sCount = 0;
		int[][] confusion = new int[numLabels][numLabels];

		double[][] scores = new double[numLabels+2][];
		scores[0] = new double[2];
		scores[scores.length-1] = new double[3];
		for (int l = 0; l < numLabels; l++){
			scores[l+1] = new double[3];
		}

		for (int d = 0; d < gold.length; d++){			
		//	sCount += gold[d].length;

			for (int s = 0; s < gold[d].length; s++){
				if (gold[d][s] == -1) continue;
				if (gold[d][s] == predictions[d][s]){
					correct++;
					correctPerDoc[d]++;
					posCorrect[gold[d][s]]++;
				}

				posGold[gold[d][s]]++;
				posPredicted[predictions[d][s]]++;
				confusion[gold[d][s]][predictions[d][s]]++;
				sCount++;
			}
		}

		scores[0][0] = ((double)correct)/sCount;
		for (int d = 0; d < correctPerDoc.length; d++){
			scores[0][1] += ((double) correctPerDoc[d])/predictions[d].length;
		}
		scores[0][1] /= predictions.length;

		double fSum = 0;
		double pSum = 0; 
		double rSum = 0;

		for (int l = 0; l < numLabels; l++){
			if (posPredicted[l] > 0){
				scores[l+1][0] = ((double)posCorrect[l])/posPredicted[l];
				pSum += scores[l+1][0];
			}

			if (posGold[l] > 0){
				// this should always be true
				scores[l+1][1] = ((double)posCorrect[l])/posGold[l];
				rSum += scores[l+1][1];
			}

			if (scores[l+1][0] > 0 || scores[l+1][1] > 0){
				scores[l+1][2] = 2*scores[l+1][0]*scores[l+1][1]/(scores[l+1][0] + scores[l+1][1]);
				fSum += scores[l+1][2];
			}			
		}

		scores[numLabels+1][0] = pSum/numLabels;
		scores[numLabels+1][1] = rSum/numLabels;
		scores[numLabels+1][2] = fSum/numLabels;
		
		if (print){
			System.out.println("Per-sentence accuracy\t" + df.format(scores[0][0]) + "(" + correct + "/" + sCount + ")");
			System.out.println("Per-document accuracy\t" + df.format(scores[0][1]));

			System.out.println("\n\n\tP\tR\tF");
			for (int l = 0; l < numLabels; l++){
				System.out.print(labelList.get(l) + "\t");
				System.out.print(df.format(scores[l+1][0]) + "\t" + df.format(scores[l+1][1]) + "\t" + df.format(scores[l+1][2])  + "\t" + posPredicted[l]  + "/" + posGold[l]);
				System.out.println();
			}
			System.out.println("\nAverage\t" + df.format(scores[numLabels+1][0]) + "\t" + df.format(scores[numLabels+1][1]) + "\t" + df.format(scores[numLabels+1][2]) + "\n\n");


			// Print confusion matrix
			for (int i = 0; i < numLabels; i++){
				System.out.print("\t" + labelList.get(i));
			}
			System.out.println();

			for (int l = 0; l < numLabels; l++){
				System.out.print(labelList.get(l));
				for (int m = 0; m < numLabels; m++){
					System.out.print("\t" + confusion[l][m]);
				}
				System.out.println();
			}
		}

		return scores;
	}
}
