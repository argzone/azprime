package uk.ac.cam.cl.azprime.cv;

import java.io.*;
import java.text.DecimalFormat;
import java.util.*;
import java.util.regex.Pattern;

import cc.mallet.fst.CRF;
import cc.mallet.fst.CRFTrainerByL1LabelLikelihood;
import cc.mallet.fst.CRFTrainerByLabelLikelihood;
import cc.mallet.fst.MEMM;
import cc.mallet.fst.MEMMTrainer;
import cc.mallet.fst.SimpleTagger;
import cc.mallet.fst.TokenAccuracyEvaluator;
import cc.mallet.fst.Transducer;
import cc.mallet.fst.TransducerEvaluator;
import cc.mallet.pipe.Pipe;
import cc.mallet.types.Instance;
import cc.mallet.types.InstanceList;
import cc.mallet.types.Sequence;

import de.bwaldvogel.liblinear.*;
//import cc.mallet.fst.*;


public class RunCV {
	private enum Method {CRF,SVM,LR,MEMM};
	private enum Decoder {VITERBI,BEAM,GREEDY};
	private enum Regulariser {L1,L2};
	private DecimalFormat df = new DecimalFormat("#.###");
	private boolean verbose;

	private class Pair<E1,E2> {
		private E1 first;
		private E2 second;

		public Pair(E1 f, E2 s){
			first = f;
			second = s;
		}

		public E1 first(){
			return first;
		}

		public E2 second(){
			return second;
		}

		public String toString(){
			return first.toString() + "," + second.toString();
		}
	}


	public static void main (String[] args){
		Method method = Method.LR;
		Decoder decoder = Decoder.VITERBI;
		int folds = 10;
		Set<String> inclusion = null;
		Set<String> exclusion = null;
		int ngram = 3;
		File in = new File("/home/do242/Work/azprime/trained/trained/all-20121002-195243.arff");
		boolean stop = false;
		boolean flat = false;
		double var = 10;
		//	boolean secondOrder = false;
		int history = 1;
		//	int history = 1;
		boolean fixedFolds = true;
		boolean oracle = false;
		boolean verbose = false;
		boolean ablation = false;
		int its = 500;
		int prune = 0;
		int beam = 10;
		Regulariser reg = Regulariser.L1;
		double c = 1.0;
		boolean optC = false;
		boolean optForF = true;
		boolean lookBack = false;
		boolean modWeights = false;

		for (int i = 0; i < args.length; i += 2){
			args[i] = args[i].toLowerCase();

			if (args[i].equals("--method")){
				method = Method.valueOf(args[i+1].toUpperCase());
			}else if (args[i].equals("--modweights")){
				modWeights = Boolean.parseBoolean(args[i+1]);
			}else if (args[i].equals("--reg")){
				reg = Regulariser.valueOf(args[i+1].toUpperCase());
			}else if (args[i].equals("--decoder")){
				decoder = Decoder.valueOf(args[i+1].toUpperCase());
			}else if (args[i].equals("--include")){			
				inclusion = new HashSet<String>(Arrays.asList(args[i+1].split(",")));
			}else if (args[i].equals("--exclude")){			
				exclusion = new HashSet<String>(Arrays.asList(args[i+1].split(",")));
			}else if (args[i].equals("--ngram")){
				ngram = Integer.parseInt(args[i+1]);
			}else if (args[i].equals("--beam")){
				beam = Integer.parseInt(args[i+1]);
			}else if (args[i].equals("--in")){
				in = new File(args[i+1]);
			}else if (args[i].equals("--stop")){
				stop = Boolean.parseBoolean(args[i+1]);
			}else if (args[i].equals("--ablation")){
				ablation = Boolean.parseBoolean(args[i+1]);
			}else if (args[i].equals("--fix")){
				fixedFolds = Boolean.parseBoolean(args[i+1]);
			}else if (args[i].equals("--flat")){
				flat = Boolean.parseBoolean(args[i+1]);
			}else if (args[i].equals("--oracle")){
				oracle = Boolean.parseBoolean(args[i+1]);
			}else if (args[i].equals("--var")){
				var = Double.parseDouble(args[i+1]);
			}else if (args[i].equals("--c")){
				c = Double.parseDouble(args[i+1]);
			}else if (args[i].equals("--order")){
				history = Integer.parseInt(args[i+1]);
			}else if (args[i].equals("--folds")){
				folds = Integer.parseInt(args[i+1]);
			}else if (args[i].equals("--its")){
				its = Integer.parseInt(args[i+1]);
			}else if (args[i].equals("--prune")){
				prune = Integer.parseInt(args[i+1]);
			}else if (args[i].equals("--history")){
				history = Integer.parseInt(args[i+1]);
			}else if (args[i].equals("--optc")){
				optC = Boolean.parseBoolean(args[i+1]);
			}else if (args[i].equals("--optforf")){
				optForF = Boolean.parseBoolean(args[i+1]);
			}else if (args[i].equals("--lookback")){
				lookBack = Boolean.parseBoolean(args[i+1]);
			}else{
				System.err.println("Unknown argument " + args[i]);
				System.exit(9);
			}
		}

		assert in != null && in.isFile() && in.canRead() : "Input file required";

		assert decoder != Decoder.VITERBI || history == 1 : "Viterbi only supported for history=1";

		new RunCV(in,folds,inclusion,exclusion,ngram,method,reg,stop,flat,var,c,history,fixedFolds,oracle,verbose,its,prune,beam,optC,optForF,lookBack,decoder,modWeights,ablation);
	}

    private HashMap<String,String> labelMapping;

    private void initMappings(){
	labelMapping = new HashMap<String,String>();
	labelMapping.put("TXT", "OWN");
	labelMapping.put("COGRO", "BKG");
	labelMapping.put("OTHR","OTH");
	labelMapping.put("PREV_OWN", "OTH");
	labelMapping.put("USE","BAS");
	labelMapping.put("SUPPORT","BAS");
	labelMapping.put("CODI","CTR");
	labelMapping.put("ANTISUPP", "CTR");
	labelMapping.put("GAP_WEAK","CTR");
	labelMapping.put("OWN_MTHD", "OWN");
	labelMapping.put("OWN_RES","OWN");
	labelMapping.put("OWN_CONC","OWN");
	labelMapping.put("OWN_FAIL","OWN");
	labelMapping.put("FUT","AIM");
	labelMapping.put("NOV_ADV","AIM");
	//      labelMapping.put("NOV_ADV","OWN");                                                                                                           
	labelMapping.put("OTH_GOOD","OTH");

	/*
	labelMap = new HashMap<String,Integer>();
	labelList = new ArrayList<String>();

	for (String label : new String[]{"OWN","BKG","OTH","BAS","CTR","AIM"}){
	    labelMap.put(label,labelList.size());
	    labelList.add(label);
	}
	*/
    }


	public RunCV(File in, int folds, Set<String> inclusion, Set<String> exclusion, int ngram, Method method, Regulariser reg, boolean stop, boolean flat, double var, double c, int history, boolean fixedFolds, boolean oracle, boolean verb, int its, int prune, int beam, boolean optC, boolean optForF, boolean lookBack, Decoder decoder, boolean modWeights, boolean ablation){	
		ArrayList<ArrayList<HashMap<String,HashSet<String>>>> allFeatures = null;
		verbose = verb;
		initMappings();
		try{
			System.out.println("Reading file " + in.getPath());
			allFeatures = readFeatureFile(in, ngram, stop);
		}catch(IOException e){
			e.printStackTrace();
			System.exit(9);
		}

		System.out.println("Read features for " + allFeatures.size() + " docs");
		
		HashSet<Integer>[] cvSplit = null;

		if (fixedFolds){
			cvSplit = fixedSplit(allFeatures.size(),folds);
		}else{
			cvSplit = split(allFeatures.size(),folds);
		}

		TreeSet<String>[][] docs = new TreeSet[allFeatures.size()][];
		int[][] labels = new int[allFeatures.size()][];
		HashMap<String,Integer> labelMap = new HashMap<String,Integer>();
		ArrayList<String> labelList = new ArrayList<String>();

		
		int[][] predictions = null;
		
		if (ablation){
			HashSet<String> featureTypes = new HashSet<String>();
			for (ArrayList<HashMap<String,HashSet<String>>> d : allFeatures){
				for (HashMap<String,HashSet<String>> s : d){
					featureTypes.addAll(s.keySet());
				}
			}
			
			for (String ft : featureTypes){
				exclusion = new HashSet<String>();
				exclusion.add(ft);
				inclusion = null;
				makeDocs(allFeatures,inclusion,exclusion,docs,labels,labelMap,labelList,lookBack);
				predictions = predict(docs,labels,cvSplit,folds,prune,method,flat,history,reg,its,var,c,beam,optC,optForF,oracle,decoder,labelList,modWeights);
				System.out.println("Excluding " + ft);
				double[][] scores = evaluate(predictions,labels,labelList,true);
				
				exclusion = null;
				inclusion = new HashSet<String>();
				inclusion.add(ft);
				
				makeDocs(allFeatures,inclusion,exclusion,docs,labels,labelMap,labelList,lookBack);
				predictions = predict(docs,labels,cvSplit,folds,prune,method,flat,history,reg,its,var,c,beam,optC,optForF,oracle,decoder,labelList,modWeights);
				System.out.println("Including " + ft);
				scores = evaluate(predictions,labels,labelList,true);
			}			
		}else{
			makeDocs(allFeatures,inclusion,exclusion,docs,labels,labelMap,labelList,lookBack);
			predictions = predict(docs,labels,cvSplit,folds,prune,method,flat,history,reg,its,var,c,beam,optC,optForF,oracle,decoder,labelList,modWeights);
			double[][] scores = evaluate(predictions,labels,labelList,true);
		}

		/*		
			if (flat){
				predictions = flatLibLinear(docs,new Parameter(SolverType.L1R_LR,1, Double.POSITIVE_INFINITY),labels,folds);
			}else{
				predictions = runLibLinear(docs,cvSplit,new Parameter(SolverType.L1R_LR,1, Double.POSITIVE_INFINITY),labels,history,oracle,viterbi,labelList,beam);
			}
			break;
		case SVM:
			if (flat){
				predictions = flatLibLinear(docs,new Parameter(SolverType.L2R_L2LOSS_SVC_DUAL,1, Double.POSITIVE_INFINITY),labels,folds);
			}else{
				predictions = runLibLinear(docs,cvSplit,new Parameter(SolverType.L2R_L2LOSS_SVC_DUAL,1, Double.POSITIVE_INFINITY),labels,history,oracle,viterbi,labelList,beam);
			}
		 */




		/*
		System.out.println("Per-sentence accuracy\t" + df.format(scores[0][0]));
		System.out.println("Per-document accuracy\t" + df.format(scores[0][1]));

		System.out.println("\n\n\tP\tR\tF");
		for (int l = 0; l < labelList.size(); l++){
			System.out.print(labelList.get(l));
			System.out.print("\t" + df.format(scores[l+1][0]) + "\t" + df.format(scores[l+1][1]) + "\t" + df.format(scores[l+1][2]));
			System.out.println();
		}
		System.out.println("\nAverage\t" + df.format(scores[labelList.size()+1][0]) + "\t" + df.format(scores[labelList.size()+1][1]) + "\t" + df.format(scores[labelList.size()+1][2]));
		 */
	}

	private int[][] predict(TreeSet<String>[][] docs, int[][] labels, HashSet<Integer>[] cvSplit, int folds, int prune, Method method, boolean flat, int history, Regulariser reg, int its, double var, double c, int beam, boolean optC, boolean optForF, boolean oracle, Decoder decoder, ArrayList<String> labelList, boolean modWeights){
		int[][] predictions = null;

		if (prune > 0){
			docs = pruneFeatures(docs,prune);
		}

		SolverType type = null;
		if (method == Method.LR){
			if (reg == Regulariser.L1){
				type = SolverType.L1R_LR;
			}else if (reg == Regulariser.L2){
				type = SolverType.L2R_LR;
			}
		}else if (method == Method.SVM){
			if (reg == Regulariser.L1){
				type = SolverType.L1R_L2LOSS_SVC;
			}else if (reg == Regulariser.L2){
				type = SolverType.L2R_L2LOSS_SVC_DUAL;
			}
		}


		switch (method){
		case CRF:
		case MEMM:
			predictions = runMalletCRF(docs,cvSplit,labels,var,history,its,method,reg);
			break;
		case LR:
		case SVM:
			if (flat){
				predictions = flatLibLinear(docs,new Parameter(type,c, Double.POSITIVE_INFINITY),labels,folds);
			}else{
				predictions = runLibLinear(docs,cvSplit,new Parameter(type,c, Double.POSITIVE_INFINITY),labels,history,oracle,labelList,beam,optC,optForF,decoder,modWeights);
			}
			break;
		}

		return predictions;
	}

	private double[][] evaluate(int[] predictions, int[] gold, int numLabels){
		int[] posGold = new int[numLabels];
		int[] posPredicted = new int[numLabels];
		int[] posCorrect = new int[numLabels];
		int correct = 0;

		double[][] scores = new double[numLabels+2][];
		scores[0] = new double[2];
		scores[scores.length-1] = new double[3];
		for (int l = 0; l < numLabels; l++){
			scores[l+1] = new double[3];
		}

		for (int s = 0; s < gold.length; s++){				
			if (gold[s] == predictions[s]){
				correct++;
				posCorrect[gold[s]-1]++;
			}

			posGold[gold[s]-1]++;
			posPredicted[predictions[s]-1]++;
		}


		scores[0][0] = ((double)correct)/gold.length;

		double fSum = 0;
		double pSum = 0; 
		double rSum = 0;

		for (int l = 0; l < numLabels; l++){
			if (posPredicted[l] > 0){
				scores[l+1][0] = ((double)posCorrect[l])/posPredicted[l];
				pSum += scores[l+1][0];
			}

			if (posGold[l] > 0){
				// this should always be true
				scores[l+1][1] = ((double)posCorrect[l])/posGold[l];
				rSum += scores[l+1][1];
			}

			if (scores[l+1][0] > 0 || scores[l+1][1] > 0){
				scores[l+1][2] = 2*scores[l+1][0]*scores[l+1][1]/(scores[l+1][0] + scores[l+1][1]);
				fSum += scores[l+1][2];
			}			
		}

		scores[numLabels+1][0] = pSum/numLabels;
		scores[numLabels+1][1] = rSum/numLabels;
		scores[numLabels+1][2] = fSum/numLabels;

		return scores;
	}

	private double[][] evaluate(int[][] predictions, int[][] gold, ArrayList<String> labelList, boolean print){
		/*
		 * Per-sentence accuracy
		 * Per-document accuracy
		 * Precision per label and average
		 * Recall per label and average
		 * F-score per label and average
		 */

		int numLabels = labelList.size();

		int[] posGold = new int[numLabels];
		int[] posPredicted = new int[numLabels];
		int[] posCorrect = new int[numLabels];
		int[] correctPerDoc = new int[predictions.length];
		int correct = 0;
		int sCount = 0;
		int[][] confusion = new int[numLabels][numLabels];

		double[][] scores = new double[numLabels+2][];
		scores[0] = new double[2];
		scores[scores.length-1] = new double[3];
		for (int l = 0; l < numLabels; l++){
			scores[l+1] = new double[3];
		}

		for (int d = 0; d < gold.length; d++){
			sCount += gold[d].length;

			for (int s = 0; s < gold[d].length; s++){				
				if (gold[d][s] == predictions[d][s]){
					correct++;
					correctPerDoc[d]++;
					posCorrect[gold[d][s]-1]++;
				}

				posGold[gold[d][s]-1]++;
				posPredicted[predictions[d][s]-1]++;
				confusion[gold[d][s]-1][predictions[d][s]-1]++;
			}
		}

		scores[0][0] = ((double)correct)/sCount;
		for (int d = 0; d < correctPerDoc.length; d++){
			scores[0][1] += ((double) correctPerDoc[d])/predictions[d].length;
		}
		scores[0][1] /= predictions.length;

		double fSum = 0;
		double pSum = 0; 
		double rSum = 0;

		for (int l = 0; l < numLabels; l++){
			if (posPredicted[l] > 0){
				scores[l+1][0] = ((double)posCorrect[l])/posPredicted[l];
				pSum += scores[l+1][0];
			}

			if (posGold[l] > 0){
				// this should always be true
				scores[l+1][1] = ((double)posCorrect[l])/posGold[l];
				rSum += scores[l+1][1];
			}

			if (scores[l+1][0] > 0 || scores[l+1][1] > 0){
				scores[l+1][2] = 2*scores[l+1][0]*scores[l+1][1]/(scores[l+1][0] + scores[l+1][1]);
				fSum += scores[l+1][2];
			}			
		}

		scores[numLabels+1][0] = pSum/numLabels;
		scores[numLabels+1][1] = rSum/numLabels;
		scores[numLabels+1][2] = fSum/numLabels;

		if (print){
			System.out.println("Per-sentence accuracy\t" + df.format(scores[0][0]) + "(" + correct + "/" + sCount + ")");
			System.out.println("Per-document accuracy\t" + df.format(scores[0][1]));

			System.out.println("\n\n\tP\tR\tF");
			for (int l = 0; l < numLabels; l++){
				System.out.print(labelList.get(l) + "\t");
				System.out.print(df.format(scores[l+1][0]) + "\t" + df.format(scores[l+1][1]) + "\t" + df.format(scores[l+1][2])  + "\t" + posPredicted[l]  + "/" + posGold[l]);
				System.out.println();
			}
			System.out.println("\nAverage\t" + df.format(scores[numLabels+1][0]) + "\t" + df.format(scores[numLabels+1][1]) + "\t" + df.format(scores[numLabels+1][2]) + "\n\n");


			// Print confusion matrix
			for (int i = 0; i < numLabels; i++){
				System.out.print("\t" + labelList.get(i));
			}
			System.out.println();

			for (int l = 0; l < numLabels; l++){
				System.out.print(labelList.get(l));
				for (int m = 0; m < numLabels; m++){
					System.out.print("\t" + confusion[l][m]);
				}
				System.out.println();
			}
		}

		return scores;
	}

	private void makeDocs(ArrayList<ArrayList<HashMap<String,HashSet<String>>>> allFeatures, Set<String> inclusion, Set<String> exclusion, TreeSet<String>[][] docs, int[][] labels, HashMap<String,Integer> labelMap, ArrayList<String> labelList, boolean lookBack){		
		//HashMap<String,HashSet<String>> seen = new HashMap<String,HashSet<String>>();

		HashSet<String> seen = new HashSet<String>();

		for (int d = 0; d < docs.length; d++){
			docs[d] = new TreeSet[allFeatures.get(d).size()];
			labels[d] = new int[docs[d].length];
			for (int s = 0; s < docs[d].length; s++){
				docs[d][s] = new TreeSet<String>();
			}		

			for (int s = 0; s < docs[d].length; s++){
				HashMap<String,HashSet<String>> sets = allFeatures.get(d).get(s);
				for (String key : sets.keySet()){
					if (key.equals("SentenceCategory")){
						String feat = sets.get(key).iterator().next();
						if (labelMapping.containsKey(feat)){
							feat = labelMapping.get(feat);
						}
						if (labelMap.containsKey(feat) == false){
							labelMap.put(feat, labelMap.size()+1);
							labelList.add(feat);
							System.out.println(feat);
						}
						labels[d][s] = labelMap.get(feat);
						continue;
					}

					if (inclusion != null && inclusion.contains(key) == false) continue;
					if (exclusion != null && exclusion.contains(key)) continue;

					for (String feat : sets.get(key)){	
						docs[d][s].add(key + ":" + feat);
					}

					if (lookBack && key.startsWith("Semantic") && s < docs[d].length-1){
						for (String feat : sets.get(key)){	
							docs[d][s+1].add("Prior" + key + ":" + feat);
						}
						seen.add("Prior" + key);
					}

					seen.add(key);
				}
			}
		}

		System.out.println("Using features:");
		for (String key : seen){
			System.out.println(key);
		}

		//	return docs;
	}

	private ArrayList<ArrayList<HashMap<String,HashSet<String>>>> readFeatureFile(File in, int ngram, boolean stop) throws IOException{
		ArrayList<ArrayList<HashMap<String,HashSet<String>>>> features = new ArrayList<ArrayList<HashMap<String,HashSet<String>>>>();

		Set<String> stopList = new HashSet<String>();
		if (stop){
			BufferedReader stopReader = new BufferedReader(new FileReader(new File("/anfs/bigdisc/do242/NGram/black.txt")));

			while (true){
				String line = stopReader.readLine();
				if (line == null){
					stopReader.close();
					break;
				}
				stopList.add(line);
			}

			System.out.println("Read " + stopList.size() + " stop words");
		}

		BufferedReader reader = new BufferedReader(new FileReader(in));
		HashSet<Integer> isNumeric = new HashSet<Integer>();
		HashSet<Integer> isString = new HashSet<Integer>();
		int idIdx = -1;
		ArrayList<String> attributes = new ArrayList<String>();
		//	HashMap<String,ArrayList<Double>> numerics = new HashMap<String,ArrayList<Double>>();
		HashMap<String,Double> totals = new HashMap<String,Double>();
		HashMap<String,Double> max = new HashMap<String,Double>();

		//int currDoc = -1;
		String currDoc = null;
		ArrayList<HashMap<String,HashSet<String>>> docFeatures = null;
		int sentenceCount = 0;

		while(true){
			String line = reader.readLine();
			if (line == null){
				reader.close();
				break;				
			}

			if (line.startsWith("@data") || line.startsWith("@relation") || line.matches("\\s*")){
				continue;
			}

			if (line.startsWith("@attribute")){
				String[] split = line.split("\\s+");
				if (split[1].equals("SentenceCategory")){
					//		labelIdx = attributes.size();
				}else if (split[1].equals("DocumentID")){
					idIdx = attributes.size();
				}

				if (split[2].equals("numeric")){					
					isNumeric.add(attributes.size());
					//numerics.put(split[1], new ArrayList<Double>());
					totals.put(split[1],0.0);
					max.put(split[1],Double.NEGATIVE_INFINITY);
					totals.put("Log" + split[1],0.0);
					max.put("Log" + split[1], Double.NEGATIVE_INFINITY);
				}else if (split[2].equals("string")){
					isString.add(attributes.size());
				}

				attributes.add(split[1]);
			}else{
				HashMap<String,HashSet<String>> senFeatures = new HashMap<String,HashSet<String>>();
				String[] split = line.split(",");
				//int thisDoc = -1;
				String thisDoc = null;

				for (int i = 0; i < split.length; i++){
					HashSet<String> values = new HashSet<String>();
					if (idIdx == i){
						//thisDoc = Integer.parseInt(split[i].replaceAll("'", ""));
						thisDoc = split[i];
						continue;
					}

					if (attributes.get(i).equals("SemanticCuePhrase")){
						values.addAll(Arrays.asList(split[i].split("!")));
					}else if (isString.contains(i)){
						values = processString(split[i],ngram,stopList);
					}else if (isNumeric.contains(i)){
						//	numerics.get(attributes.get(i)).add(Double.parseDouble(split[i]));
						values.add(split[i]);
						double val = Double.parseDouble(split[i]);
												
						totals.put(attributes.get(i), totals.get(attributes.get(i)) + val);
						if (val > max.get(attributes.get(i))){
							max.put(attributes.get(i),val);
						}
						
						if (val > 0){
							String type = "Log" + attributes.get(i);
							double log = Math.log(val);
							totals.put(type,totals.get(type)+ log);
							if (log > max.get(type)){
								max.put(type, log);
							}
						}
					}else{
						values.add(split[i]);
					}
					senFeatures.put(attributes.get(i), values);
				}

				assert thisDoc != null;
				sentenceCount++;

				if (thisDoc.equals(currDoc)){
					docFeatures.add(senFeatures);
				}else{
					if (currDoc != null) features.add(docFeatures);
					docFeatures = new ArrayList<HashMap<String,HashSet<String>>>();
					docFeatures.add(senFeatures);
					currDoc = thisDoc;
				}
			}
		}

		if (docFeatures.isEmpty() == false) features.add(docFeatures);

		for (int f : isNumeric){
			String feat = attributes.get(f);
			if (feat.equals("DocumentID")) continue;
			double ave = totals.get(feat)/sentenceCount;
			double binSize = max.get(feat)/10;

			for (ArrayList<HashMap<String,HashSet<String>>> doc : features){
				for (HashMap<String,HashSet<String>> sen :doc){
					double val = Double.parseDouble(sen.get(feat).iterator().next());
					if (val > ave){
						HashSet<String> set = new HashSet<String>();
						set.add("1");
						sen.put(feat, set);
					}else{
						sen.put(feat,new HashSet<String>());
					}

					int bin = (int) (val/binSize);
					HashSet<String> set = new HashSet<String>();
					set.add(Integer.toString(bin));
					sen.put(feat + "Bin", set);
				}
			}
		}

		return features;
	}

	private HashSet<String> processString(String str, int ngram, Set<String> stopList){
		int start = 0;
		while(start < str.length()){
			char pre = str.charAt(start);
			if (pre == '"' || pre == '\''){
				start++;
				continue;
			}
			break;
		}


		int end = str.length();

		while(end > start){
			char post = str.charAt(end-1);
			if (post == '"' || post == '\'' || post == '.' || post == '?' || post == '!' || post == '-' ){
				end--;
				continue;
			}
			break;
		}

		str = str.substring(start,end);

		HashSet<String> set = new HashSet<String>();

		if (str.length() <= 0) return set;

		String[] words = str.split("\\s+");

		for (int i = 0; i < words.length; i++){
			for (int n = 0; n < ngram; n++){
				if (i - n < 0) continue;
				StringBuilder sb = new StringBuilder();
				for (int w = i - n; w <= i; w++){
					if (stopList.contains(words[w])) continue;
					sb.append(words[w] + ":");
				}

				String feat = sb.toString();
				set.add(feat);
			}
		}

		return set;
	}

	private int[][] flatLibLinear(TreeSet<String>[][] features, Parameter param, int[][] labels, int folds){
		if (param.getEps() == Double.POSITIVE_INFINITY) {
			if (param.getSolverType() == SolverType.L2R_LR || param.getSolverType() == SolverType.L2R_L2LOSS_SVC) {
				param.setEps(0.01);
			} else if (param.getSolverType() == SolverType.L2R_L2LOSS_SVC_DUAL || param.getSolverType() == SolverType.L2R_L1LOSS_SVC_DUAL
					|| param.getSolverType() == SolverType.MCSVM_CS || param.getSolverType() == SolverType.L2R_LR_DUAL) {
				param.setEps(0.1);
			} else if (param.getSolverType() == SolverType.L1R_L2LOSS_SVC || param.getSolverType() == SolverType.L1R_LR) {
				param.setEps(0.01);
			}
		}

		HashMap<String,Integer> featureMap = new HashMap<String,Integer>();

		int[][] predictions = new int[features.length][];	    

		//  FeatureNode[][][] x = new FeatureNode[features.length][][];

		ArrayList<FeatureNode[]> x = new ArrayList<FeatureNode[]>();
		ArrayList<Integer> y = new ArrayList<Integer>();
		ArrayList<Integer> dList = new ArrayList<Integer>();
		ArrayList<Integer> sList = new ArrayList<Integer>();

		for (int d = 0; d < features.length; d++){
			predictions[d] = new int[features[d].length];
			for (int s = 0; s < features[d].length; s++){
				y.add(labels[d][s]);
				dList.add(d);
				sList.add(s);
				TreeSet<Integer> ordered = new TreeSet<Integer>();
				for (String feat : features[d][s]){
					if (featureMap.containsKey(feat) == false) featureMap.put(feat, featureMap.size()+1);
					ordered.add(featureMap.get(feat));
				}
				FeatureNode[] fn = new FeatureNode[ordered.size()];
				int i = 0;
				for (int of : ordered){
					fn[i] = new FeatureNode(of,1);
					i++;
				}

				x.add(fn);
			}
		}	    	    

		HashSet<Integer>[] cvSplit = split(x.size(),folds);

		Linear.disableDebugOutput();

		for (int fold = 0; fold < cvSplit.length; fold++){
			int max_index = 0;

			int[] trainY = new int[x.size()-cvSplit[fold].size()];
			FeatureNode[][] trainX = new FeatureNode[trainY.length][];
			HashMap<Integer,FeatureNode[]> testX = new HashMap<Integer,FeatureNode[]>();

			int count = 0;
			for (int i = 0; i < x.size(); i++){
				if (cvSplit[fold].contains(i) == false){
					trainX[count] = x.get(i);
					trainY[count] = y.get(i);
					for (FeatureNode fn : trainX[count]){
						if (fn.index > max_index) max_index = fn.index;
					}
					count++;
				}else{
					testX.put(i, x.get(i));
				}
			}

			try{
				dumpFile(trainX,trainY,new File("/anfs/bigtmp/dump" + fold));
			}catch(IOException e){
				System.exit(9);
			}


			Problem prob = new Problem();
			prob.n = max_index;
			prob.x = trainX;
			prob.y = trainY;
			prob.l = trainX.length;

			Model model = Linear.train(prob, param);

			for (int i : testX.keySet()){
				predictions[dList.get(i)][sList.get(i)] = Linear.predict(model, testX.get(i));
			}		    
		}

		return predictions;
	}

	/*
	private ArrayList<ArrayList<Integer>> makeCandidates (int num_labels, int length){
		ArrayList<ArrayList<Integer>> list = new ArrayList<ArrayList<Integer>>();

		for (int i = 0; i < length; i++){
			ArrayList<ArrayList<Integer>> temp = new ArrayList<ArrayList<Integer>>();

			for (ArrayList<Integer> al : list){
				for (int l = 0; l < num_labels; l++){
					ArrayList<Integer> tl = new ArrayList<Integer>(al);
					tl.add(l);
					temp.add(tl);
				}
			}

			list = temp;
		}

		return list;
	}
	 */

	private class CapacityMap<K extends Comparable<K>,V>{
		private int capacity;
		private TreeMap<K,HashSet<V>> map;
		private int size = 0;

		public CapacityMap(int c){
			capacity = c;
			assert capacity > 0;
			map = new TreeMap<K,HashSet<V>>();
		}

		public int size(){ return size;}

		public HashSet<V> get(K key){
			if (map.containsKey(key)){
				return map.get(key);
			}else{
				return null;
			}
		}

		public void addAll(Map<V,K> map){
			for (V val : map.keySet()){
				K key = map.get(val);
				put(key,val);
			}
		}

		public void put(K key, V val){
			if (size < capacity){
				if (map.containsKey(key)){
					map.get(key).add(val);
				}else{
					HashSet<V> set = new HashSet<V>();
					set.add(val);
					map.put(key,set);
				}
				size++;
			}else if (key.compareTo(map.firstKey()) > 0){
				HashSet<V> hs = map.get(map.firstKey());
				hs.remove(hs.iterator().next());
				if (hs.isEmpty()) map.remove(map.firstKey());
				if (map.containsKey(key) == false){
					map.put(key, new HashSet<V>());
				}
				map.get(key).add(val);
			}
		}

		public ArrayList<V> getValues(){
			ArrayList<V> list = new ArrayList<V>();
			for (K key : map.descendingKeySet()){ 
				list.addAll(map.get(key));
			}

			return list;
		}

		public TreeMap<K,HashSet<V>> getMap(){
			return new TreeMap<K,HashSet<V>>(map);
		}

		public ArrayList<Pair<K,V>> getEntries(){
			ArrayList<Pair<K,V>> list = new ArrayList<Pair<K,V>>();
			for (K key : map.descendingKeySet()){ 
				for (V val : map.get(key)){
					list.add(new Pair<K,V>(key,val));
				}
			}
			return list;
		}
	}

	private void crossValidateParameterLibLinear(Problem prob, Parameter param, boolean optF, int numLabels){
		int start = -4; 
		int end = 12;

		double bestScore = Double.NEGATIVE_INFINITY;
		int bestC = 0;

		int[] target = new int[prob.l];

		for (int c = start; c <= end; c += 4){
			param.setC(Math.pow(2,c));
			Linear.crossValidation(prob,param,10,target);

			double[][] scores = evaluate(target,prob.y,numLabels);

			if (optF){
				if (scores[numLabels+1][2] > bestScore){
					bestScore = scores[numLabels+1][2];
					bestC = c;
				}			
			}else{
				if (scores[0][0] > bestScore){
					bestScore = scores[0][0];
					bestC = c;
				}
			}

			System.out.println("c = " + c + ": Accuracy = " + scores[0][0] + ", Precision = " + scores[numLabels+1][0] + ", Recall = " + scores[numLabels+1][1] + ", F-Score = " + scores[numLabels+1][2] + ", best C = " + bestC);
		}

		int sliceC = bestC;
		for (int c = sliceC - 2; c <= sliceC + 2; c+= 2){
			if (c == sliceC) continue;

			param.setC(Math.pow(2,c));
			Linear.crossValidation(prob,param,10,target);

			double[][] scores = evaluate(target,prob.y,numLabels);

			if (optF){
				if (scores[numLabels+1][2] > bestScore){
					bestScore = scores[numLabels+1][2];
					bestC = c;
				}			
			}else{
				if (scores[0][0] > bestScore){
					bestScore = scores[0][0];
					bestC = c;
				}
			}

			System.out.println("c = " + c + ": Accuracy = " + scores[0][0] + ", Precision = " + scores[numLabels+1][0] + ", Recall = " + scores[numLabels+1][1] + ", F-Score = " + scores[numLabels+1][2] + ", best C = " + bestC);
		}

		param.setC(Math.pow(2,bestC));
	}

	private int[][] runLibLinear(TreeSet<String>[][] features, HashSet<Integer>[] cvSplit, Parameter param, int[][] labels, int history, boolean oracle, ArrayList<String> labelList, int beam, boolean optC, boolean optForF, Decoder decoder, boolean modWeights){
		if (param.getEps() == Double.POSITIVE_INFINITY) {
			if (param.getSolverType() == SolverType.L2R_LR || param.getSolverType() == SolverType.L2R_L2LOSS_SVC) {
				param.setEps(0.01);
			} else if (param.getSolverType() == SolverType.L2R_L2LOSS_SVC_DUAL || param.getSolverType() == SolverType.L2R_L1LOSS_SVC_DUAL
					|| param.getSolverType() == SolverType.MCSVM_CS || param.getSolverType() == SolverType.L2R_LR_DUAL) {
				param.setEps(0.1);
			} else if (param.getSolverType() == SolverType.L1R_L2LOSS_SVC || param.getSolverType() == SolverType.L1R_LR) {
				param.setEps(0.01);
			}
		}

		/*
		ArrayList<ArrayList<Integer>> candidates = null;
		if (history > 0 && viterbi) candidates = makeCandidates(labelList.size()+1,history);
		 */

		HashMap<String,Integer> featureMap = new HashMap<String,Integer>();

		int[][] predictions = new int[features.length][];	    
		FeatureNode[][][] x = new FeatureNode[features.length][][];

		for (int d = 0; d < features.length; d++){
			predictions[d] = new int[features[d].length];
			x[d] = new FeatureNode[features[d].length][];
			for (int s = 0; s < features[d].length; s++){
				x[d][s] = new FeatureNode[features[d][s].size()];

				TreeSet<Integer> ordered = new TreeSet<Integer>();
				for (String feat : features[d][s]){
					if (featureMap.containsKey(feat) == false) featureMap.put(feat, featureMap.size()+1);
					ordered.add(featureMap.get(feat));
				}	

				int i = 0;
				for (int of : ordered){
					x[d][s][i] = new FeatureNode(of,1);
					i++;
				}    			
			}
		}

		System.out.println("Read " + featureMap.size() + " features");
		Linear.disableDebugOutput();

		for (int fold = 0; fold < cvSplit.length; fold++){
			int max_index = 0;

			ArrayList<Integer> yList = new ArrayList<Integer>();
			ArrayList<FeatureNode[]> xList = new ArrayList<FeatureNode[]>();

			HashMap<Integer,FeatureNode[][]> testX = new HashMap<Integer,FeatureNode[][]>();

			int[] labelCounts = new int[labelList.size()+1];

			for (int d = 0; d < features.length; d++){
				if (cvSplit[fold].contains(d)){
					testX.put(d, x[d]);
				}else{
					for (int s = 0; s < features[d].length; s++){
						if (history > 0){
							StringBuilder sb = new StringBuilder();
							sb.append("History");
							int old = x[d][s].length - 1;
							if (old < 0){
								System.out.println("no features for " + d + "," + s + ": " + features[d][s]);
							}
							FeatureNode[] newX = new FeatureNode[x[d][s].length + history];
							System.arraycopy(x[d][s],0,newX,0,x[d][s].length);

							for (int h = 1; h <= history; h++){
								if (s - h < 0){
									sb.append(":B");
								}else{
									sb.append(":" + labels[d][s-h]);
								}

								String feat = sb.toString();
								if (featureMap.containsKey(feat) == false) featureMap.put(feat, featureMap.size()+1);
								newX[old + h] = new FeatureNode(featureMap.get(feat),1.0);
								assert old+h-1 < 0 || newX[old+h].index > newX[old+h-1].index;
							}						

							xList.add(newX);
						}else{
							xList.add(x[d][s]);
						}

						//for (FeatureNode fn : x[d][s]){
						for (FeatureNode fn : xList.get(xList.size()-1)){
							if (fn.index > max_index) max_index = fn.index;
						}
						yList.add(labels[d][s]);
					}
				}
			}

			System.out.println("Fold " + fold + ": " + xList.size() + " sentences for training, " + testX.size() + " documents for testing" + cvSplit[fold]);	    

			Problem prob = new Problem();
			prob.l = xList.size();
			prob.n = max_index;
			prob.x = new FeatureNode[prob.l][];		  
			prob.y = new int[prob.l];			

			for (int i = 0; i < prob.l; i++){
				prob.x[i] = xList.get(i);
				prob.y[i] = yList.get(i);
				labelCounts[prob.y[i]]++;
			}

			if (modWeights){
				double[] weights = new double[labelList.size()];
				int[] weightLabels = new int[labelList.size()];
				for (int l = 0; l < weights.length; l++){
					weightLabels[l] = l+1;
					weights[l] = prob.l/((double)labelCounts[l+1]);
				}
				param.setWeights(weights, weightLabels);
			}

			if (optC){
				crossValidateParameterLibLinear(prob,param,optForF,labelList.size());
			}

			Model model = Linear.train(prob, param);
			int[] modelLabels = model.getLabels();
			/*
		    double[] w = model.getFeatureWeights();
		    for (int i = 0; i < w.length; i++){
		    	if (w[i] != 0) System.out.println(i +":"+w[i]);
		    }
			 */


			for (int d : testX.keySet()){		
				if (decoder == Decoder.BEAM && history > 0){
					//ArrayList<Pair<ArrayList<Integer>,Double>> lattice = new ArrayList<Pair<ArrayList<Integer>,Double>>();
					//lattice.add(new Pair<ArrayList<Integer>,Double>(new ArrayList<Integer>(),0.0));

					CapacityMap<Double,ArrayList<Integer>> lattice = new CapacityMap<Double,ArrayList<Integer>>(beam);
					lattice.put(0.0, new ArrayList<Integer>());

					for (int s = 0; s < testX.get(d).length; s++){
						//	System.out.println("Sentence " + s + ", lattice size is " + lattice.size());
						HashMap<String,Double[]> historyScores = new HashMap<String,Double[]>();
						//	ArrayList<Pair<ArrayList<Integer>,Double>> temp = new ArrayList<Pair<ArrayList<Integer>,Double>>();
						CapacityMap<Double,ArrayList<Integer>> temp = new CapacityMap<Double,ArrayList<Integer>>(beam);
						StringBuilder sb = new StringBuilder();
						sb.append("History");
						for (Pair<Double, ArrayList<Integer>> p : lattice.getEntries()){
							ArrayList<String> historyFeatures = new ArrayList<String>();

							for (int h = 1; h <= history; h++){
								if (s - h < 0){
									sb.append(":B");
								}else{
									sb.append(":" + p.second().get(s-h));
								}
								historyFeatures.add(sb.toString());
							}

							double[] scores = new double[labelList.size()];
							if (historyScores.containsKey(sb.toString())){
								Double[] cache = historyScores.get(sb.toString());
								assert cache.length == scores.length;
								for (int i = 0; i < scores.length; i++){
									scores[i] = cache[i];
								}
							}else{
								int old = x[d][s].length;
								FeatureNode[] newX = new FeatureNode[testX.get(d)[s].length + history];
								System.arraycopy(testX.get(d)[s],0,newX,0,testX.get(d)[s].length);

								for (int h = 0; h < historyFeatures.size(); h++){
									String feat = historyFeatures.get(h);
									if (featureMap.containsKey(feat) == false) featureMap.put(feat, featureMap.size()+1);
									newX[old + h] = new FeatureNode(featureMap.get(feat),1.0);
									assert old+h <= 0 || newX[old+h].index > newX[old+h-1].index;
								}

								Linear.predictProbability(model, newX, scores);
							}

							for (int l = 0; l < scores.length; l++){
								int label = modelLabels[l];
								ArrayList<Integer> sequence = new ArrayList<Integer>(p.second());
								double sequenceScore = p.first()+Math.log(scores[l]);
								sequence.add(label);
								//	temp.add(new Pair<ArrayList<Integer>,Double>(sequence,sequenceScore));
								temp.put(sequenceScore, sequence);
							}

							lattice = temp;
						}					
					}

					double maxScore = Double.NEGATIVE_INFINITY;
					ArrayList<Integer> maxSequence = null;
					for (Pair<Double,ArrayList<Integer>> p : lattice.getEntries()){
						if (p.first() > maxScore){
							maxScore = p.first();
							maxSequence = p.second();
						}
					}

					for (int s = 0; s < maxSequence.size(); s++){
						predictions[d][s] = maxSequence.get(s);
					}
				}else if (decoder == Decoder.VITERBI){
					double[][] delta = new double[testX.get(d).length][labelList.size()];
					int[][] phi = new int[testX.get(d).length][labelList.size()];

					// Forward
					for (int s = 0; s < testX.get(d).length; s++){
						for (int k = 0; k < labelList.size(); k++){
							int old = x[d][s].length;
							FeatureNode[] newX = new FeatureNode[testX.get(d)[s].length + 1];
							System.arraycopy(testX.get(d)[s],0,newX,0,testX.get(d)[s].length);
							double[] scores = new double[labelList.size()];
							String feat = "History:" + modelLabels[k];
							if (featureMap.containsKey(feat) == false) featureMap.put(feat, featureMap.size()+1);
							newX[old] = new FeatureNode(featureMap.get(feat),1.0);
							assert old <= 0 || newX[old].index > newX[old-1].index;
							Linear.predictProbability(model, newX, scores);
							for (int l = 0; l < labelList.size(); l++){
								double de;
								if (s == 0){
									de = scores[l];
								}else{
									de = scores[l]*delta[s-1][k];
								}
								if (de > delta[s][l]){
									delta[s][l] = de;
									phi[s][l] = k;
								}
							}
						}						
					}

					double last = 0;
					for (int l = 0; l < labelList.size(); l++){
						if (delta[testX.get(d).length-1][l] > last){
							last = delta[testX.get(d).length-1][l];
							predictions[d][testX.get(d).length-1] = l;
						}
					}

					// Backward
					for (int s = testX.get(d).length-2; s >= 0; s--){
						predictions[d][s] = phi[s+1][predictions[d][s+1]];
					}

					for (int s = 0; s < testX.get(d).length; s++){
						predictions[d][s] = modelLabels[predictions[d][s]];
					}
				}else{
					for (int s = 0; s < testX.get(d).length; s++){
						FeatureNode[] test = null;

						if (history > 0){
							StringBuilder sb = new StringBuilder();
							sb.append("History");
							int old = x[d][s].length - 1;
							FeatureNode[] newX = new FeatureNode[testX.get(d)[s].length + history];
							System.arraycopy(testX.get(d)[s],0,newX,0,testX.get(d)[s].length);

							for (int h = 1; h <= history; h++){
								if (s - h < 0){
									sb.append(":B");
								}else{
									if (oracle){
										sb.append(":" + labels[d][s-h]);
									}else{
										sb.append(":" + predictions[d][s-h]);
									}
								}

								String feat = sb.toString();
								if (featureMap.containsKey(feat) == false) featureMap.put(feat, featureMap.size()+1);
								newX[old + h] = new FeatureNode(featureMap.get(feat),1.0);
								assert newX[old+h].index > newX[old+h-1].index;
							}						

							test = newX;
						}else{
							test = testX.get(d)[s];
						}

						predictions[d][s] = Linear.predict(model, test);
					}
				}
			}
		}



		return predictions;
	}

	private void dumpFile(FeatureNode[][] x, int[] y, File f) throws IOException{
		PrintWriter writer = new PrintWriter(new BufferedWriter(new FileWriter(f)));
		for (int i = 0; i < y.length; i++){
			writer.print(y[i]);
			for (FeatureNode n : x[i]){
				writer.print(" " + n.index + ":" + n.value);
			}
			writer.println();
		}
		writer.close();
	}

	// Technically peeking, need to fix if we're using this
	private TreeSet<String>[][] pruneFeatures (TreeSet<String>[][] features, int th){
		TreeSet<String>[][] pruned = new TreeSet[features.length][];

		HashMap<String,Integer> counts = new HashMap<String,Integer>();

		for (TreeSet<String>[] d : features){
			HashSet<String> docFeatures = new HashSet<String>();
			for (TreeSet<String> s : d){
				for (String feat : s){
					docFeatures.add(feat);
				}
			}

			for (String feat : docFeatures){
				int c = counts.containsKey(feat) ? counts.get(feat) : 0;
				counts.put(feat,c+1);
			}
		}

		for (int d = 0; d < features.length; d++){
			pruned[d] = new TreeSet[features[d].length];
			for (int s = 0; s < features[d].length; s++){
				pruned[d][s] = new TreeSet<String>();
				for (String feat : features[d][s]){
					if (counts.get(feat) >= th) pruned[d][s].add(feat);
				}
			}
		}

		int count = 0;
		for (String feat : counts.keySet()){
			if (counts.get(feat) >= th) count++;
		}

		System.out.println("Pruned from " + counts.size() + " features to " + count);

		return pruned;
	}

	private int[][] runMalletCRF(TreeSet<String>[][] features, HashSet<Integer>[] cvSplit, int[][] labels, double gaussianVariance, int order, int its, Method method, Regulariser reg){
		//		int its = 500;


		/*
		try{
			PrintWriter writer = new PrintWriter(new BufferedWriter(new FileWriter("/home/do242/Work/azprime/trained/trained/crf.mallet.txt")));
			for (int d = 0; d < features.length; d++){
				for (int s = 0; s < features[d].length; s++){
					for (String feat : features[d][s]){
						writer.print(feat + " ");
					}
					writer.println(labels[d][s]);
				}			
				writer.println();
			}
			writer.close();

		}catch(IOException e){
			System.exit(9);
		}
		 */

		int[][] predictions = new int[features.length][];

		ArrayList<String> malletStrings = new ArrayList<String>();
		for (int d = 0; d < features.length; d++){
			StringBuilder sb = new StringBuilder();
			for (int s = 0; s < features[d].length; s++){
				for (String f : features[d][s]){
					sb.append(f + " ");
				}
				sb.append(labels[d][s] + "\n");
			}
			malletStrings.add(sb.toString());
			predictions[d] = new int[features[d].length];
		}

		for (int fold = 0; fold < cvSplit.length; fold++){
			System.out.println("Fold " + fold);

			ArrayList<String> trainStrings = new ArrayList<String>();
			ArrayList<String> testStrings = new ArrayList<String>();
			ArrayList<Integer> testIDs = new ArrayList<Integer>();

			for (int d = 0; d < features.length; d++){
				if (cvSplit[fold].contains(d)){
					testStrings.add(malletStrings.get(d));
					testIDs.add(d);
				}else{
					trainStrings.add(malletStrings.get(d));
				}				
			}

			Pipe p = new SimpleTagger.SimpleTaggerSentence2FeatureVectorSequence();
			p.getTargetAlphabet().lookupIndex("0");
			p.setTargetProcessing(true);
			InstanceList trainingData = new InstanceList(p);
			trainingData.addThruPipe(new FeatureIterator(trainStrings));
			InstanceList testData = new InstanceList(p);
			testData.addThruPipe(new FeatureIterator(testStrings));

			System.out.println(p.getDataAlphabet().size() + "  features");
			System.out.println(p.getTargetAlphabet().size() + " labels");
			System.out.println(trainingData.size() + " training instances of " + trainStrings.size());
			System.out.println(testData.size() + " testing instances of " + testStrings.size());

			TransducerEvaluator eval = new TokenAccuracyEvaluator(new InstanceList[]{trainingData,testData}, new String[] {"Training","Testing"});

			int[] orders = new int[order];
			for (int o = 0; o < order; o++){
				orders[o] = o+1;
			}


			CRF crf = null;


			//		switch (method) {
			//		case CRF:
			crf = trainCRF(trainingData, testData, eval, orders, "0", "\\s", ".*", true, its, gaussianVariance, null, reg);
			//			break;
			//		case MEMM:
			//			crf = trainMEMM(trainingData);
			//			break;
			//		}


			//	CRF crf = SimpleTagger.train(trainingData, testData, eval, orders, "0", "\\s", ".*", true, its, gaussianVariance, null);
			for (Instance t : testData){
				int d = testIDs.get((Integer) t.getName());
				Sequence input = (Sequence) t.getData();
				Sequence output = crf.transduce(input);
				for (int s = 0; s < features[d].length; s++){
					predictions[d][s] = Integer.parseInt((String) output.get(s));
				}
			}			
		}

		return predictions;

	}

	// Borrowed from MALLET's SimpleTagger class
	private static CRF trainCRF(InstanceList training, InstanceList testing,TransducerEvaluator eval, int[] orders,String defaultLabel,String forbidden, String allowed, boolean connected, int iterations, double param, CRF crf, Regulariser reg){
		Pattern forbiddenPat = Pattern.compile(forbidden);
		Pattern allowedPat = Pattern.compile(allowed);
		if (crf == null) {
			crf = new CRF(training.getPipe(), (Pipe)null);
			String startName =
					crf.addOrderNStates(training, orders, null,
							defaultLabel, forbiddenPat, allowedPat,
							connected);
			for (int i = 0; i < crf.numStates(); i++)
				crf.getState(i).setInitialWeight (Transducer.IMPOSSIBLE_WEIGHT);
			crf.getState(startName).setInitialWeight(0.0);
		}


		CRFTrainerByLabelLikelihood crft = null;

		if (reg == Regulariser.L2){
			crft = new CRFTrainerByLabelLikelihood (crf);
			crft.setGaussianPriorVariance(param);
		}else{
			CRFTrainerByL1LabelLikelihood crftemp = new CRFTrainerByL1LabelLikelihood(crf);
			crftemp.setL1RegularizationWeight(param);
			crft = crftemp;
		}

		boolean converged;
		for (int i = 1; i <= iterations; i++) {
			converged = crft.train (training, 1);
			if (i % 1 == 0 && eval != null) // Change the 1 to higher integer to evaluate less often
				eval.evaluate(crft);
			if (converged)
				break;
		}

		return crf;
	}

	// MEMM is broken in Mallet, don't bother trying it
	public static CRF trainMEMM(InstanceList training){
		MEMM memm = new MEMM(training.getAlphabet(), training.getTargetAlphabet());
		memm.addStatesForThreeQuarterLabelsConnectedAsIn(training);
		MEMMTrainer memmTrainer = new MEMMTrainer(memm);
		memmTrainer.train(training);		    
		return memm;
	}

	private HashSet<Integer>[] fixedSplit(int n, int f){
		HashSet<Integer>[] folds = new HashSet[f];
		for (int fold = 0; fold < f; fold++){
			folds[fold] = new HashSet<Integer>();
		}
		int foldSize = n/f;

		for (int i = 0; i < n; i++){
			int j = i/foldSize;
			folds[j].add(i);				
		}
		return folds;
	}

	private HashSet<Integer>[] split(int n, int f){
		Random rand = new Random();	

		int[] perm = new int[n];
		int[] fold_start = new int[f];
		int i;
		for(i=0;i<n;i++) perm[i]=i;
		for(i=0;i<n;i++)
		{
			int j = i + rand.nextInt(n-i);
			//int j = i+(int)(Math.random()*(l-i));
			do {int _=perm[i]; perm[i]=perm[j]; perm[j]=_;} while(false);
		}
		for(i=0;i<f;i++)
			fold_start[i]=i*n/f;

		HashSet<Integer>[] folds = new HashSet[f];
		for (int fold = 0; fold < f; fold++){
			int end;
			if (fold+1 == f){
				end = n;			
			}else{
				end = fold_start[fold+1];
			}
			folds[fold] = new HashSet<Integer>();
			for (i = fold_start[fold]; i < end; i++){
				folds[fold].add(perm[i]);
			}
		}

		return folds;
	}

	private class FeatureIterator implements Iterator<Instance>{
		private ArrayList<String> features;
		private int i;

		public FeatureIterator(ArrayList<String> f){
			features = f;
			i = 0;
		}

		@Override
		public boolean hasNext() {
			return i < features.size();
		}

		@Override
		public Instance next() {
			Instance inst = new Instance(features.get(i),null,new Integer(i),null);
			i++;
			return inst;
		}

		@Override
		public void remove() {
			throw new IllegalStateException ("This Iterator<Instance> does not support remove().");
		}

	}
}
