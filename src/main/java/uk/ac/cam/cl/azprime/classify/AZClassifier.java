package uk.ac.cam.cl.azprime.classify;

import java.util.List;
import java.util.Map;
import java.util.Set;

public abstract class AZClassifier { 
	//Map<String,Integer> featureMap;
	//List<String> featureList;
	AZFeatureExtractor extractor;
	List<String> labelList;
	
	public AZClassifier(AZProblem az){
		extractor = az.extractor;
		labelList = az.labelList;
	}
	
	public abstract void train(AZProblem prob);
	public abstract int[][] predict(AZProblem prob);
	public abstract int[][] crossValidate(AZProblem prob, Set<Integer>[] folds); 
	public abstract int[][] crossValidateAndOptimise(AZProblem prob, Set<Integer>[] folds);
	
	public void setVerbose(boolean v){} // bit of a hack
}
