package uk.ac.cam.cl.azprime.classify;

import java.io.File;
import java.io.IOException;
import java.util.*;

import perceptron.struct.BeamSearchUpdate;

public class RunAZ {
	public static void main(String[] args) {
		File[] train = null;
		File[] test = null;
		boolean cv = false;
		Set<String> more2Ignore = new HashSet<String>();
		File modelPath = null;
		Algorithm alg = Algorithm.LR;
		Regulariser reg = Regulariser.L1;
		double parameter = 1;
		int its = 1000;
		int history = 1;
		int typeHistory = 1;
		File dict = null;
		File[] trainSRL = null;
		File[] testSRL = null;
		Decoder decoder = Decoder.VITERBI;
		int beamSize = 20;
		BeamSearchUpdate update = BeamSearchUpdate.EARLY;
		File[] outDirs = null;
		String[] sFeatures = null;
		boolean mapLabels = true;
		boolean verbose = false;
		boolean trainAbstracts = false;
		boolean testAbstracts = false;
		
		for (int i = 0; i < args.length; i+=2){
			if (args[i].equals("--train")){
				String[] temp = args[i+1].split(",");
				train = new File[temp.length];
				for (int f = 0; f < temp.length; f++){
					train[f] = new File(temp[f]);
					assert train[f].exists();
				}
			}else if (args[i].equals("--test")){
				String[] temp = args[i+1].split(",");
				test = new File[temp.length];
				for (int f = 0; f < temp.length; f++){
					test[f] = new File(temp[f]);
					assert test[f].exists();
				}
			}else if (args[i].equals("--out")){
				String[] temp = args[i+1].split(",");
				outDirs = new File[temp.length];
				for (int f = 0; f < temp.length; f++){
					outDirs[f] = new File(temp[f]);
				}
			}else if (args[i].equals("--trainsrl")){
				String[] temp = args[i+1].split(",");
				trainSRL = new File[temp.length];
				for (int f = 0; f < temp.length; f++){
					trainSRL[f] = new File(temp[f]);
					assert trainSRL[f].exists();
				}
			}else if (args[i].equals("--testsrl")){
				String[] temp = args[i+1].split(",");
				testSRL = new File[temp.length];
				for (int f = 0; f < temp.length; f++){
					testSRL[f] = new File(temp[f]);
					assert testSRL[f].exists();
				}
			}else if (args[i].equals("--cv")){
				cv = Boolean.parseBoolean(args[i+1]);
			}else if (args[i].equals("--verbose")){
				verbose = Boolean.parseBoolean(args[i+1]);
			}else if (args[i].equals("--ignore")){
				more2Ignore.addAll(Arrays.asList(args[i+1].split(",")));
			}else if (args[i].equals("--algorithm")){
				alg = Algorithm.valueOf(args[i+1].toUpperCase());
			}else if (args[i].equals("--reg")){
				reg = Regulariser.valueOf(args[i+1].toUpperCase());
			}else if (args[i].equals("--decoder")){
				decoder = Decoder.valueOf(args[i+1].toUpperCase());
			}else if (args[i].equals("--its")){
				its = Integer.parseInt(args[i+1]);
			}else if (args[i].equals("--history")){
				history = Integer.parseInt(args[i+1]);
			}else if (args[i].equals("--typehistory")){
				typeHistory = Integer.parseInt(args[i+1]);
			}else if (args[i].equals("--beam")){
				beamSize = Integer.parseInt(args[i+1]);
			}else if (args[i].equals("--parameter")){
				parameter = Double.parseDouble(args[i+1]);
			}else if (args[i].equals("--dict")){
				dict = new File(args[i+1]);
			}else if (args[i].equals("--struct")){
				sFeatures = args[i+1].split(",");
			}else if (args[i].equals("--maplabels")){
				mapLabels = Boolean.parseBoolean(args[i+1]);
			}else if (args[i].equals("--trainAbstracts")){
				trainAbstracts = Boolean.parseBoolean(args[i+1]);
			}else if (args[i].equals("--testAbstracts")){
				testAbstracts = Boolean.parseBoolean(args[i+1]);
			}else{
				System.err.println("Unknown argument " + args[i]);
				System.exit(9);
			}
		}

		assert train != null;
		assert trainSRL == null || trainSRL.length == train.length;
		assert test == null || testSRL == null || test.length == testSRL.length;

		new RunAZ(train,test,trainSRL,testSRL,outDirs,cv,more2Ignore,modelPath,alg,parameter,its,reg,decoder,history,typeHistory,sFeatures,dict,beamSize,update,mapLabels,verbose,trainAbstracts,testAbstracts);
	}

	public RunAZ(File[] trainingDirs, File[] testDirs, File[] trainingSRL, File[] testSRL, File[] outDirs, boolean cv, Set<String> more2Ignore, File modelPath, Algorithm alg, double parameter, int its, Regulariser reg, Decoder decoder, int history, int typeHistory, String[] sFeatures, File dict, int beamSize, BeamSearchUpdate update, boolean mapLabels, boolean verbose, boolean trainAbstracts, boolean testAbstracts){
		AZProblem trainingProblem = new AZProblem(trainingDirs,trainingSRL,more2Ignore,history,typeHistory,sFeatures,dict,mapLabels,trainAbstracts);
		AZEvaluator eval = new AZEvaluator(trainingProblem.labelList);

		trainingProblem.extractor.printLabels();
		
		AZClassifier classifier = null;
		switch(alg){
		case CRF:
			classifier = new CRFClassifier(its,reg,history,parameter,trainingProblem);
			break;
		case SVM:
		case LR:
			classifier = new LibLinearClassifier(alg,reg,history,parameter,trainingProblem,decoder,beamSize);
			break;
		case PERCEPTRON:
			classifier = new StructuredPerceptronClassifier(its,history,trainingProblem,decoder,beamSize,update,eval);
			break;
		case FACTORIE:
			classifier = new FactorieClassifier(trainingProblem,its);
			break;
		}

		classifier.setVerbose(verbose);
		
		if (cv){
			System.out.println("Cross-Validating...");
			int[][] predictions = classifier.crossValidate(trainingProblem,fixedSplit(trainingProblem.docs.length,10));
			int[][] gold = trainingProblem.getGold();
			if (testAbstracts){
				int[][] abstractPredictions = new int[predictions.length][];
				int[][] abstractGold = new int[gold.length][];
				for (int d = 0; d < gold.length; d++){
					int abstractLength = trainingProblem.docs[d].abstractLength;
					abstractPredictions[d] = Arrays.copyOfRange(predictions[d],0,abstractLength);
					abstractGold[d] = Arrays.copyOfRange(gold[d],0,abstractLength);
				}
				eval.evaluate(abstractPredictions, abstractGold, true);
			}else{
				eval.evaluate(predictions,gold,true);	
			}
		}

		if (testDirs != null && testDirs.length > 0){
			System.out.println("Training...");
			classifier.train(trainingProblem);

			for (int d = 0; d < testDirs.length; d++){ 
				File dir = testDirs[d];
				System.out.println("Testing on directory " + dir.getPath());
				//	List<File> testList = Arrays.asList(dir.listFiles(filter));
				//	AZDoc[] testDocs = readData(testList,false,more2Ignore);
				AZProblem testProblem = new AZProblem(new File[]{dir},testSRL,trainingProblem,testAbstracts);
				int[][] predictions = classifier.predict(testProblem);
				int[][] gold = testProblem.getGold();
				eval.evaluate(predictions,gold,true);

				if (outDirs != null){
					System.out.println("Writing to directory " + outDirs[d].getPath());
			//		if (outDirs[d].exists() == false) outDirs[d].mkdir();
					try{
						testProblem.write(outDirs[d],predictions);
					}catch(IOException e){
						e.printStackTrace();
						System.exit(9);
					}
				}
			}
		}		
	}

	private HashSet<Integer>[] fixedSplit(double n, int f){
		HashSet<Integer>[] folds = new HashSet[f];
		for (int fold = 0; fold < f; fold++){
			folds[fold] = new HashSet<Integer>();
		}
		int foldSize = (int) Math.ceil(n/f);

		for (int i = 0; i < n; i++){
			int j = i/foldSize;
			folds[j].add(i);				
		}
		return folds;
	}
}
