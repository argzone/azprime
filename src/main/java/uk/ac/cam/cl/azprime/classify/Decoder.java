package uk.ac.cam.cl.azprime.classify;

public enum Decoder {
	VITERBI,BEAM,GREEDY
}
