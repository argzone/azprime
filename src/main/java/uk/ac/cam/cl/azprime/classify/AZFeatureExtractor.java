package uk.ac.cam.cl.azprime.classify;

import edu.stanford.nlp.tagger.maxent.MaxentTagger;
import gnu.trove.map.TObjectIntMap;
import gnu.trove.map.hash.TIntIntHashMap;
import gnu.trove.map.hash.TObjectIntHashMap;
import gnu.trove.procedure.TIntIntProcedure;
import gnu.trove.set.hash.TIntHashSet;

import java.util.*;
import java.util.zip.GZIPInputStream;
import java.io.*;

import perceptron.struct.Feature;
import perceptron.struct.StructuredFeatureExtractor;

import uk.ac.cam.cl.azprime.ml.ExtractedFeature;
import uk.ac.cam.cl.azprime.xml.SciXmlReader;
import uk.ac.cam.cl.azprime.xml.ParsedSciXml.Document;
import uk.ac.cam.cl.azprime.xml.SciXmlWriter;
import uk.ac.cam.cl.azprime.xml.SentenceTagger;
import uk.ac.cam.cl.azprime.xml.SentenceSplitter;
import uk.ac.cam.cl.azprime.AZWrapper;

public class AZFeatureExtractor implements StructuredFeatureExtractor<Feature[]>{
	private interface AZExtractable{
		public abstract void extract(Map<Integer,Double> map, int[] sequence, AZFeatureExtractor ext, String d);
		public abstract void extract(Map<Integer,Double> map, int[] sequence, int i, AZFeatureExtractor ext, String d);
		public boolean isStructFeature = true;
		public boolean isNonlocalFeature = false;
	}

	private enum AZFeature implements AZExtractable{
		HISTORY{
			@Override
			public void extract(Map<Integer, Double> map, int[] sequence, AZFeatureExtractor ext, String d) {
				for (int i = 0; i <= sequence.length; i++){
					extract(map,sequence,i,ext,d);
				}	
			}

			@Override
			public void extract(Map<Integer, Double> map, int[] sequence, int i, AZFeatureExtractor ext, String d) {
				StringBuilder sb = new StringBuilder("History");
				for (int h = 1; h <= ext.history; h++){							
					int b = i-h;
					if (b < 0){
						sb.append(":B");
						ext.incFeature(sb.toString(),1.0,map);
						break;
					}else{
						sb.append(":" + sequence[b]);
						ext.incFeature(sb.toString(),1.0,map);
					}
				}				
			}
		},

		TYPE_HISTORY{
			public boolean isNonlocalFeature = true;

			@Override
			public void extract(Map<Integer, Double> map, int[] sequence, AZFeatureExtractor ext, String d) {
				for (int i = 0; i <= sequence.length; i++){
					extract(map,sequence,i,ext,d);
				}	
			}

			@Override
			public void extract(Map<Integer, Double> map, int[] sequence, int i, AZFeatureExtractor ext, String d) {
				StringBuilder sb = new StringBuilder("TypeHistory");
				int seen = 0;
				int last = -1;
				int h = 0;
				while(seen < ext.typeHistory){
					h++;
					int b = i-h;
					if (b < 0){
						sb.append(":B");
						ext.incFeature(sb.toString(),1.0,map);
						break;
					}else{
						if (sequence[b] != last){
							sb.append(":" + sequence[b]);
							ext.incFeature(sb.toString(),1.0,map);
							seen++;
						}
						last = sequence[b];				
					}
				}
			}
		},

		SINGLE_OCCURRENCE{
			public boolean isNonlocalFeature = true;

			@Override
			public void extract(Map<Integer, Double> map, int[] sequence, AZFeatureExtractor ext, String d) {
				extract(map,sequence,sequence.length,ext,d);
			}

			@Override
			public void extract(Map<Integer, Double> map, int[] sequence, int i, AZFeatureExtractor ext, String d) {
				TIntHashSet set = new TIntHashSet();
				for (int j = 0; j < i; j++){
					set.add(sequence[j]);
				}

				for (int l : set._set){
					String feat = "SingleOccurrence:" + l;
					ext.addFeature(feat,1.0,map);
				}	
			}
		},

		CITATION_AGREEMENT{
			public boolean isNonlocalFeature = true;

			@Override
			public void extract(Map<Integer, Double> map, int[] sequence, AZFeatureExtractor ext, String d) {
				for (int i = 1; i < sequence.length; i++){
					extract(map,sequence,i,ext,d);
				}
			}

			@Override
			public void extract(Map<Integer, Double> map, int[] sequence, int i, AZFeatureExtractor ext, String d) {
				HashSet<Integer> links = ext.pathsToDocs.get(d).citationLinks[i];
				for (int j : links){
					if (j >= i) continue;
					if (sequence[j] == sequence[i]){
						ext.addFeature("CitationAgreement",1.0,map);
						ext.addFeature("CitationAgreement:" + sequence[j],1.0,map);
					}
				}
			}
		},

		CITATION_LINK{
			public boolean isNonlocalFeature = true;

			@Override
			public void extract(Map<Integer, Double> map, int[] sequence,AZFeatureExtractor ext, String d) {
				for (int i = 1; i < sequence.length; i++){
					extract(map,sequence,i,ext,d);
				}
			}

			@Override
			public void extract(Map<Integer, Double> map, int[] sequence,int i, AZFeatureExtractor ext, String d) {
				if (i == 0) return;
				HashSet<Integer> links = ext.pathsToDocs.get(d).citationLinks[i];
				int lastIdx = -1;
				for (int j : links){
					if (j >= i) continue;
					ext.addFeature("CitationLink:" + sequence[j],1.0,map);
					if (j > lastIdx){
						lastIdx = j;
					}
				}	
				if (lastIdx >= 0) ext.addFeature("CitationLink:Last:" + sequence[lastIdx],1.0,map);
			}
		}
	}

	private int history = 1;
	private int typeHistory = 1;
	//private boolean useSingleOccurrence;
	private TObjectIntMap<String> featureMap;
	private ArrayList<String> featureList;
	private Map<String,Double> aveMap;
	private Map<String,Double> binMap;
	private HashSet<String> featureTypes;
	private Set<String> dictionary;

	private AZWrapper wrapper;
	private Set<String> ignore;
	private HashMap<String,String> labelMapping;
	HashMap<String,Integer> labelMap;
	List<String> labelList;
	private boolean usingStructFeatures = false;
	private boolean usingNonlocalFeatures = false;
	private Set<AZFeature> sFeatures;
	private Map<String,AZDoc> pathsToDocs;


	@Override
	public String getLabelName(int l){
		return labelList.get(l);
	}

	private void addFeatureType(AZFeature f){
		sFeatures.add(f);
		if (f.isStructFeature) usingStructFeatures = true;
		if (f.isNonlocalFeature) usingNonlocalFeatures = true;
	}

	private void removeFeatureType(AZFeature f){
		sFeatures.remove(f);
		usingStructFeatures = false;
		usingNonlocalFeatures = false;
		for (AZFeature ff : sFeatures){
			if (ff.isStructFeature) usingStructFeatures = true;
			if (ff.isNonlocalFeature) usingNonlocalFeatures = true;
		}
	}

	public void setHistory(int h){
		addFeatureType(AZFeature.HISTORY);
		history = h;;
	}

	public void setTypeHistory(int h){
		addFeatureType(AZFeature.TYPE_HISTORY);
		typeHistory = h;
	}

	public void addFeature(String feat){
		addFeatureType(AZFeature.valueOf(feat.toUpperCase()));
	}

	public boolean usingStructuralFeatures(){
		return usingStructFeatures;
	}

	public boolean usingNonlocalFeatures(){
		return usingNonlocalFeatures;
	}

	public Set<String> allFeatureNames(){
		return featureMap.keySet();
	}

	public AZFeatureExtractor(File config) throws IOException{
		readConfig(config);
		initFeatures();
		initMappings(true);
	}

	public AZFeatureExtractor(AZWrapper w, boolean mapLabels){
		wrapper = w;
		initFeatures();
		initMappings(mapLabels);
	}

	public AZFeatureExtractor(boolean mapLabels){
		initFeatures();
		initMappings(mapLabels);
	}

	@Override
	public String getFeatureName(int idx){
		return featureList.get(idx);
	}

	public int lastFeature(){
		return featureList.size()-1;
	}

	public int getFeatureIndex(String feat){
		return featureMap.containsKey(feat) ? featureMap.get(feat) : -1;
	}

	private void initMappings(boolean mapLabels){
		labelMapping = new HashMap<String,String>();

		if (mapLabels){
			labelMapping.put("TXT", "OWN");
			labelMapping.put("COGRO", "BKG");
			labelMapping.put("OTHR","OTH");
			labelMapping.put("PREV_OWN", "OTH");
			labelMapping.put("USE","BAS");
			labelMapping.put("SUPPORT","BAS");
			labelMapping.put("CODI","CTR");
			labelMapping.put("ANTISUPP", "CTR");
			labelMapping.put("GAP_WEAK","CTR");
			labelMapping.put("OWN_MTHD", "OWN");
			labelMapping.put("OWN_RES","OWN");
			labelMapping.put("OWN_CONC","OWN");
			labelMapping.put("OWN_FAIL","OWN");
			labelMapping.put("FUT","AIM");
			//	labelMapping.put("NOV_ADV","AIM");
			labelMapping.put("NOV_ADV","OWN");
			labelMapping.put("OTH_GOOD","OTH");
		}else{
			labelMapping.put("OTH_GOOD","OTH");	
		}

		labelMap = new HashMap<String,Integer>();
		labelList = new ArrayList<String>();
		labelList.add("");	

		pathsToDocs = new HashMap<String,AZDoc>();
	}

	private void initFeatures(){
		featureMap = new TObjectIntHashMap<String>();
		featureList = new ArrayList<String>();
		featureList.add("");
		featureTypes = new HashSet<String>();
		sFeatures = new HashSet<AZFeature>();
		aveMap = new HashMap<String,Double>();
		binMap = new HashMap<String,Double>();
		ignore = new HashSet<String>(Arrays.asList(new String[]{"ID"}));
	}

	public void printLabels(){
		for (int i = 1; i < labelList.size(); i++){
			System.out.println(i + " " + labelList.get(i));
		}
	}

	public void addToIgnoreList(Collection<String> more2Ignore){
		ignore.addAll(more2Ignore);
	}

	public void listFeatureTypes(){
		for (String ft : featureTypes){
			System.out.println(ft);
		}
	}

	private void readConfig(File f) throws IOException{
		BufferedReader reader = new BufferedReader(new FileReader(f));

		while (true){
			String line = reader.readLine();
			if (line == null){
				reader.close();
				break;
			}
		}
	}

	private void incFeature(String feat, double val, Map<Integer,Double> map){
		if (featureMap.containsKey(feat) == false){
			featureMap.put(feat, featureList.size());
			featureList.add(feat);
		}

		int idx = featureMap.get(feat);
		map.put(idx,map.containsKey(idx) ? map.get(idx) + val : val);
	}

	private void addFeature(String feat, double val, Map<Integer,Double> map){
		if (featureMap.containsKey(feat) == false){
			featureMap.put(feat, featureList.size());
			featureList.add(feat);
		}

		map.put(featureMap.get(feat),val);	
	}


	@Override
	public TreeMap<Integer,Double> getNewGlobalFeatures(Feature[][] e, int[] labels, String d) {
		HashMap<Integer,Double> map = new HashMap<Integer,Double>();

		for (AZFeature f : sFeatures){
			f.extract(map,labels,this,d);
		}

		return new TreeMap<Integer,Double>(map);
	}

	@Override
	public TreeMap<Integer,Double> getNewLocalFeatures(Feature[][] e, int[] labels, int i, String d){
		HashMap<Integer,Double> map = new HashMap<Integer,Double>();

		for (AZFeature f : sFeatures){
			f.extract(map,labels,i,this,d);
		}

		return new TreeMap<Integer,Double>(map);
	}



	@Override
	public Feature[] getGlobalStructFeatures(Feature[][] e, int[] labels, String d) {
		HashMap<Integer,Double> map = new HashMap<Integer,Double>();

		for (AZFeature f : sFeatures){
			f.extract(map,labels,this,d);
		}

		TreeMap<Integer,Double> sorted = new TreeMap<Integer,Double>(map);

		Feature[] features = new Feature[sorted.size()];
		int c = 0;
		for(Map.Entry<Integer, Double> entry : sorted.entrySet()){
			features[c] = new Feature(entry.getKey(),entry.getValue());
			c++;
		}

		return features;
	}

	@Override
	public Feature[] getLocalStructFeatures(Feature[] e, int[] labels, int i, String d){
		HashMap<Integer,Double> map = new HashMap<Integer,Double>();

		for (AZFeature f : sFeatures){
			f.extract(map,labels,i,this,d);
		}

		TreeMap<Integer,Double> sorted = new TreeMap<Integer,Double>(map);

		Feature[] features = new Feature[sorted.size()];
		int c = 0;
		for(Map.Entry<Integer, Double> entry : sorted.entrySet()){
			features[c] = new Feature(entry.getKey(),entry.getValue());
			c++;
		}

		return features;
	}

	public AZDoc[] readDataWithSRL(List<File> fileList, List<File> srlList, boolean addFeatures) throws IOException{
		AZDoc[] docs = readData(fileList,addFeatures,false);
		readSRL(docs,srlList,addFeatures);
		return docs;
	}

	public AZDoc[] readData(List<File> fileList, boolean addFeatures,boolean abstractOnly){
		ArrayList<ArrayList<Integer>> yList = new ArrayList<ArrayList<Integer>>();

		HashSet<String> numerics = new HashSet<String>(Arrays.asList(new String[]{"TFI","LEN","TIT"}));
		HashSet<String> mustSplit = new HashSet<String>(Arrays.asList(new String[]{"SemanticCuePhrase"}));
		HashSet<String> lookbackTypes = new HashSet<String>(Arrays.asList(new String[]{"SemanticCuePhrase","SVPATTERNS","SVVOICE","SVTENSE","SVACT"}));
		HashMap<String,Double> max = new HashMap<String,Double>();
		for (String type : numerics){
			max.put(type,Double.NEGATIVE_INFINITY);
			max.put("Log" + type,Double.NEGATIVE_INFINITY);
		}
		HashMap<String,Double> totals = new HashMap<String,Double>();
		for (String type : numerics){
			totals.put(type,0.0);
			totals.put("Log" + type,0.0);
		}

		ArrayList<ArrayList<HashMap<String,Double>>> numericFeatures = new ArrayList<ArrayList<HashMap<String,Double>>>();
		//	ArrayList<ArrayList<TreeSet<Integer>>> stringFeatures = new ArrayList<ArrayList<TreeSet<Integer>>>();
		ArrayList<ArrayList<TreeSet<String>>> stringFeatures = new ArrayList<ArrayList<TreeSet<String>>>();
		ArrayList<String> paths = new ArrayList<String>();

		ArrayList<HashSet<Integer>[]> citationLinkList = new ArrayList<HashSet<Integer>[]>();
		ArrayList<Integer> abstractLengths = new ArrayList<Integer>();

		SentenceTagger tagger = null;

		for (File f : fileList){
			System.out.println("Reading file " + f.getPath());
			Document doc = new SciXmlReader(f).parse();

			if (f.getName().endsWith("simone.xml")){
				File posFile = new File(f.getPath().replaceAll("\\.simone\\.xml", ".pos.xml"));
				if (posFile.exists() == false){ 
					System.out.println("Have to POS tag " + f.getPath());

					if (tagger == null){
						try{
							tagger = new SentenceTagger(new MaxentTagger("/anfs/bigdisc/do242/Java/stanford-corenlp-2011-09-16/edu/stanford/nlp/models/pos-tagger/wsj3t0-18-bidirectional/bidirectional-distsim-wsj-0-18.tagger"));						}catch(Exception e){
								e.printStackTrace();
								System.exit(9);
							}
					}

					doc = tagger.addPosTags(doc);
					SciXmlWriter writer = new SciXmlWriter(doc,true,true,4);
					writer.write(posFile);
				}
			}

			ArrayList<HashMap<String,Double>> docNumericFeatures = new ArrayList<HashMap<String,Double>>();
			ArrayList<TreeSet<String>> docStringFeatures = new ArrayList<TreeSet<String>>();

			ExtractedFeature[][] extractedFeatures = wrapper.getFeatures(doc);
			TreeSet<String> lookbackSet = new TreeSet<String>();
			int lastSen = -1;
			ArrayList<Integer> docY = new ArrayList<Integer>();

			LOOP: for (int s = 0; s < extractedFeatures.length; s++){
				TreeSet<String> stringSet = new TreeSet<String>();
				HashMap<String,Double> numericMap = new HashMap<String,Double>();
				int y = -1;				

				for (String feat : lookbackSet){

					stringSet.add(feat);					
				}

				lookbackSet = new TreeSet<String>();
				int isAbstract = -1;

				for (ExtractedFeature ef : extractedFeatures[s]){
					String[] components = ef.getComponents();
					String type = components[0];
					//		System.out.println("Extracted feature " + type + ":" + components[1]);
					if (ignore.contains(type)) continue;	

					if (addFeatures) featureTypes.add(type);

					if (type.equals("LOC")){
						if (components[1].equals("ABS")){
							isAbstract = 1;
						}else{
							isAbstract = 0;
							if (abstractOnly){
								break LOOP;
							}
						}
					}

					if (numerics.contains(type)){
						double val = Double.parseDouble(components[1]);

						if (addFeatures){
							if (val > max.get(type)) max.put(type,val);
							totals.put(type, totals.get(type) + val);							
						}

						numericMap.put(type, val);

						if (val > 0){
							String logType = "Log" + type;

							if (addFeatures){
								if (val > max.get(logType)) max.put(logType,Math.log(val));
								totals.put(logType, totals.get(logType) + Math.log(val));
							}

							numericMap.put(logType, Math.log(val));
						}
					}else if (type.equals("SEN")){
						Set<String> ngrams = processString(components[1],3);
						for (String ng : ngrams){
							if (dictionary != null && dictionary.contains(ng) == false) continue;
							String feat = type + ":" + ng;

							stringSet.add(feat);
						}
					}else if (type.equals("AZ")){
						String label = components[1];
						if (label.equals("")) continue LOOP;

						if (labelMapping.containsKey(label)){
							label = labelMapping.get(label);
						}

						if (labelMap.containsKey(label) == false){
							labelMap.put(label,labelList.size());
							labelList.add(label);
						}

						y = labelMap.get(label);
					}else if (mustSplit.contains(type)){
						if (components[1].equals("")) continue;
						for (String sp : components[1].split("!")){
							String feat = type + ":" + sp;

							stringSet.add(feat);
							if (lookbackTypes.contains(type)){
								lookbackSet.add("Lookback:" + feat);
							}
						}
					}else{
						if (components[1].equals("")) continue;
						String feat = type + ":" + components[1];

						stringSet.add(feat);
						if (lookbackTypes.contains(type)){
							lookbackSet.add("Lookback:" + feat);
						}
					}
				}

				if (isAbstract == 1){
					HashSet<String> absFeatSet = new HashSet<String>();
					for (String feat : stringSet){
						absFeatSet.add("ABSTRACT:" + feat);
					}
					stringSet.addAll(absFeatSet);
				}

				docY.add(y);
				docStringFeatures.add(stringSet);
				docNumericFeatures.add(numericMap);

				lastSen = docY.size()-1;
			}	

			if (docY.isEmpty()){
				System.out.println("No annotation for file " + f.getPath());
				continue;
			}

			yList.add(docY);
			stringFeatures.add(docStringFeatures);
			numericFeatures.add(docNumericFeatures);
			paths.add(f.getPath());
			abstractLengths.add(doc.abstractSentences().size());

			Map<String,int[]> docCitationMap = new HashMap<String,int[]>(wrapper.getCitations(doc));
			HashSet<Integer>[] docCitationLinks = new HashSet[extractedFeatures.length];
			for (int s = 0; s < extractedFeatures.length; s++) docCitationLinks[s] = new HashSet<Integer>();
			for (int[] ref : docCitationMap.values()){
				for (int s = 0; s < ref.length-1; s++){
					for (int t = s+1; t < ref.length; t++){
						docCitationLinks[ref[s]].add(ref[t]);
						docCitationLinks[ref[t]].add(ref[s]);
					}
				}
			}
			citationLinkList.add(docCitationLinks);
		}		

		for (int d = 0; d < stringFeatures.size(); d++){
			for (int s = 0; s < stringFeatures.get(d).size(); s++){
				for (String type : numericFeatures.get(d).get(s).keySet()){
					featureTypes.add(type);
					if (addFeatures){
						if (aveMap.containsKey(type) == false){
							double ave = totals.get(type)/yList.size();
							aveMap.put(type, ave);
							System.out.println("Average for " + type + ": " + ave);
						}

						if (binMap.containsKey(type) == false){
							double binSize = max.get(type)/10;
							binMap.put(type,binSize);
							System.out.println("Bin size for " + type + ": " + binSize);
						}
					}


					if (numericFeatures.get(d).get(s).get(type) > aveMap.get(type)){
						String feat = type;
						/*
						if (featureMap.containsKey(feat) == false){
							featureMap.put(feat,featureMap.size()+1);
							featureList.add(feat);
							if (featureTypes.containsKey(type) == false) featureTypes.put(type, new TreeSet<Integer>()); 
							featureTypes.get(type).add(featureMap.get(feat));
						}
						 */
						stringFeatures.get(d).get(s).add(feat);
					}

					int bin = (int) (numericFeatures.get(d).get(s).get(type)/binMap.get(type));
					String feat = type + "Bin:" + bin;
					featureTypes.add(type + "Bin");

					stringFeatures.get(d).get(s).add(feat);
				}
			}
		}

		/*
		try{
			PrintWriter writer = new PrintWriter(new BufferedWriter(new FileWriter("/local/scratch-2/do242/a1.txt")));
			for (TreeSet<String> set : stringFeatures.get(0)){
				for (String s : set){
					writer.print(s + " ");
				}
				writer.println();
			}
			writer.close();
		}catch(IOException e){
			e.printStackTrace();
			System.exit(9);
		}
		 */

		AZDoc[] docs = new AZDoc[stringFeatures.size()];
		for (int d = 0; d < stringFeatures.size(); d++){
			AZDoc doc = new AZDoc();
			doc.path = paths.get(d);
			doc.abstractLength = abstractLengths.get(d);
			pathsToDocs.put(doc.path, doc);
			doc.citationLinks = citationLinkList.get(d);
			doc.features = new Feature[stringFeatures.get(d).size()][];
			doc.labels = new int[stringFeatures.get(d).size()];
			for (int s = 0; s < doc.labels.length; s++){
				doc.labels[s] = yList.get(d).get(s);
				int i = 0;
				doc.features[s] = new Feature[stringFeatures.get(d).get(s).size()];
				TreeSet<Integer> ordered = new TreeSet<Integer>();
				for (String feat : stringFeatures.get(d).get(s)){
					if (featureMap.containsKey(feat) == false){
						featureMap.put(feat,featureMap.size()+1);
						featureList.add(feat);
					}
					ordered.add(featureMap.get(feat));
				}

				for (int idx : ordered){
					doc.features[s][i] = new Feature(idx,1.0);
					i++;
				}
			}

			docs[d] = doc;
		}

		return docs;
	}

	public AZDoc[] readHardcodedFeatures(Set<String>[][] features, String[][] labels, String[] paths){
		AZDoc[] docs = new AZDoc[features.length];
		for (int d = 0; d < features.length; d++){
			AZDoc doc = new AZDoc();

			doc.path = paths[d];
			pathsToDocs.put(doc.path, doc);
			doc.citationLinks = new HashSet[0];
			doc.features = new Feature[features[d].length][];
			doc.labels = new int[features[d].length];

			for (int s = 0; s < features[d].length; s++){
				String label = labels[d][s];
				if (label.equals("")){
					doc.labels[s] = -1; // We allow unlabelled data, but it may break some classifiers
				}else{
					if (labelMapping.containsKey(label)){
						label = labelMapping.get(label);
					}

					if (labelMap.containsKey(label) == false){
						labelMap.put(label,labelList.size());
						labelList.add(label);
					}

					doc.labels[s] = labelMap.get(label);
				}

				TreeSet<Integer> ordered = new TreeSet<Integer>();
				for (String feat : features[d][s]){
					if (featureMap.containsKey(feat) == false){
						featureMap.put(feat,featureMap.size()+1);
						featureList.add(feat);
					}
					ordered.add(featureMap.get(feat));
				}

				doc.features[s] = new Feature[ordered.size()];
				int i = 0;
				for (int idx : ordered){
					doc.features[s][i] = new Feature(idx,1.0);
					i++;
				}
			}

			docs[d] = doc;
		}
		return docs;
	}

	private String replacePTB(String str){
		str = str.replaceAll("-LRB-","(");
		str = str.replaceAll("-RRB-", ")");
		str = str.replaceAll("-LSB-","[");
		str = str.replaceAll("-RSB-", "]");		
		str = str.replaceAll("-LCB-","{");
		str = str.replaceAll("-RCB-", "}");
		return str;
	}

	private Set<String> processString(String str, int ngram){
		str = replacePTB(str).replaceAll("\\d", "0");

		int start = 0;
		while(start < str.length()){
			char pre = str.charAt(start);
			if (pre == '"' || pre == '\''){
				start++;
				continue;
			}
			break;
		}


		int end = str.length();

		while(end > start){
			char post = str.charAt(end-1);
			if (post == '"' || post == '\'' || post == '.' || post == '?' || post == '!' || post == '-' ){
				end--;
				continue;
			}
			break;
		}

		str = str.substring(start,end);

		TreeSet<String> set = new TreeSet<String>();

		if (str.length() <= 0) return set;

		String[] words = str.split("\\s+");

		for (int i = 0; i < words.length; i++){
			for (int n = 0; n < ngram; n++){
				if (i - n < 0) continue;
				StringBuilder sb = new StringBuilder();
				for (int w = i - n; w <= i; w++){
					sb.append(words[w]);
					if (w < i) sb.append(":");
				}

				String feat = sb.toString();
				set.add(feat);
			}
		}

		return set;
	}

	private void readSRL(AZDoc[] docs, List<File> files, boolean addFeatures) throws IOException{
		for (int i = 0; i < files.size(); i++){
			if (files.get(i) == null){
				continue;
			}
			File file = new File(files.get(i),"fesWLabels");
			if (file.exists() == false){
				System.err.println("No fesWLabels for " + file.getPath());

				file = new File(files.get(i),"fes");
				if (file.exists() == false){
					System.err.println("No fes for " + file.getPath());
					continue;
				}

				continue;
			}
			BufferedReader reader = new BufferedReader(new FileReader(file));
			TreeSet<Integer>[] features = new TreeSet[docs[i].features.length];
			for (int s = 0; s < features.length; s++) features[s] = new TreeSet<Integer>();

			File mapFile = new File(files.get(i),"id_map");
			HashMap<Integer,Integer> idMap = new HashMap<Integer,Integer>();
			if (mapFile.exists()){
				BufferedReader idReader = new BufferedReader(new FileReader(mapFile));
				while (true){
					String line = idReader.readLine();
					if (line == null){
						idReader.close();
						break;
					}

					String[] split = line.split("\\s+");
					idMap.put(Integer.parseInt(split[0]), Integer.parseInt(split[1]) + docs[i].abstractLength);
				}
			}


			while (true){
				String line = reader.readLine();
				if (line == null){
					reader.close();
					break;
				}

				String[] split = line.split("\t");
				int sid = Integer.parseInt(split[6]);
				if (idMap != null){
					sid = idMap.get(sid);
				}
				String frame = split[2];
				String feature = "SRL:" + frame;
				int numRoles = Integer.parseInt(split[1])-1;

				if (featureMap.containsKey(feature) == false){
					featureMap.put(feature, featureList.size());
					featureList.add(feature);
				}

				features[sid].add(featureMap.get(feature));

				for (int r = 7; r < split.length; r+=2){
					feature = "SRL:" + frame + ":" + split[r];

					if (featureMap.containsKey(feature) == false){
						featureMap.put(feature, featureList.size());
						featureList.add(feature);
					}

					features[sid].add(featureMap.get(feature));
				}

				if (split.length > 7+2*numRoles){
					String[] temp = split[7+2*numRoles].split(",");
					for (String rule : temp){
						feature = "SRL_RULE:" + rule;
						if (featureMap.containsKey(feature) == false){
							featureMap.put(feature, featureList.size());
							featureList.add(feature);
						}

						features[sid].add(featureMap.get(feature));
					}
				}
			}

			for (int s = 0; s < features.length; s++){
				Feature[] old = docs[i].features[s];
				Feature[] temp = new Feature[old.length + features[s].size()];
				System.arraycopy(old,0,temp,0,old.length);
				int idx = old.length;
				for (int f : features[s]){
					temp[idx] = new Feature(f,1.0);
					idx++;
				}
				docs[i].features[s] = temp;
			}
		}
	}

	public void readDictionary(File f) throws IOException{
		BufferedReader reader = null;
		if (f.getPath().endsWith("gz")){
			reader = new BufferedReader(new InputStreamReader(new GZIPInputStream(new FileInputStream(f))));
		}else{
			reader = new BufferedReader(new FileReader(f));
		}
		dictionary = new HashSet<String>();
		while (true){
			String line = reader.readLine();
			if (line == null){
				reader.close();
				break;
			}
			String[] split = line.split("\\s+");
			dictionary.add(split[0]);
		}
		System.out.println("Read whitelist of " + dictionary.size() + " ngrams");
	}

	// Labels will often be shorter than e if we're scoring a partial sequence
	@Override
	public Feature[] getGlobalBaseFeatures(Feature[][] e, int[] labels) {
		TIntIntHashMap map = new TIntIntHashMap();

		for (int s = 0; s < labels.length; s++){
			for (Feature feat : e[s]){
				String newFeat = featureList.get(feat.index) + ":" + labelList.get(labels[s]);
				if (featureMap.containsKey(newFeat)){
					map.adjustOrPutValue(featureMap.get(newFeat), 1, 1);
				}else{
					featureMap.put(newFeat,featureList.size());
					featureList.add(newFeat);
					map.adjustOrPutValue(featureMap.get(newFeat), 1, 1);
				}
			}
		}	

		final Feature[] ret = new Feature[map.size()];

		map.forEachEntry(new TIntIntProcedure(){
			private int count = 0;

			@Override
			public boolean execute(int arg0, int arg1) {
				ret[count] = new Feature(arg0,arg1);
				count++;
				return true;
			}		
		});

		return ret;
	}

	@Override
	public Feature[][] convertBaseFeatures(Feature[] e){
		Feature[][] ret = new Feature[labelList.size()][e.length];
		for (int l = 1; l <= labelMap.size(); l++){
			for (int f = 0; f < e.length; f++){
				String newFeat = featureList.get(e[f].index) + ":" + labelList.get(l);
				if (featureMap.containsKey(newFeat) == false){
					featureMap.put(newFeat,featureList.size());
					featureList.add(newFeat);
				}

				ret[l][f] = new Feature(featureMap.get(newFeat),e[f].value);	
			}

		}
		return ret;
	}

	@Override
	public Feature[][][] convertBaseFeatures(Feature[][] e){
		Feature[][][] ret = new Feature[e.length][labelList.size()][];

		for (int s = 0; s < e.length; s++){
			for (int l = 1; l <= labelMap.size(); l++){
				ret[s][l] = new Feature[e[s].length];
				for (int f = 0; f < e[s].length; f++){
					String newFeat = featureList.get(e[s][f].index) + ":" + labelList.get(l);
					if (featureMap.containsKey(newFeat) == false){
						featureMap.put(newFeat,featureList.size());
						featureList.add(newFeat);
					}

					ret[s][l][f] = new Feature(featureMap.get(newFeat),e[s][f].value);					
				}
			}
		}

		return ret;
	}

	@Override
	public Feature[] getGlobalFeatures(Feature[][] e, int[] labels, String d){
		Feature[] bFeatures = getGlobalBaseFeatures(e,labels);
		Feature[] sFeatures = getGlobalStructFeatures(e,labels,d);
		Feature[] allFeatures = new Feature[bFeatures.length+sFeatures.length];
		System.arraycopy(bFeatures,0,allFeatures,0,bFeatures.length);
		System.arraycopy(sFeatures,0,allFeatures,bFeatures.length,sFeatures.length);
		Arrays.sort(allFeatures);
		return allFeatures;
	}

	@Override
	public Feature[] getLocalBaseFeatures(Feature[] e) {
		return e;
	}
}
