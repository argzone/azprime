package uk.ac.cam.cl.azprime.system;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.UnsupportedEncodingException;
import java.util.*;

import uk.ac.cam.cl.azprime.AZWrapper;
import uk.ac.cam.cl.azprime.config.Configuration;
import uk.ac.cam.cl.azprime.ml.ExtractedFeature;
import uk.ac.cam.cl.azprime.xml.ParsedSciXml.*;
import uk.ac.cam.cl.azprime.xml.SciXmlReader;
import de.bwaldvogel.liblinear.*;

public class AZClassifier {
	private Model model;
	private AZWrapper wrapper;
	private Map<String,Integer> featureMap;
	private List<String> labelList;
	private int[] modelLabels;
	private Map<String,Double> aveMap;
	private Map<String,Double> binMap;
	private final HashSet<String> numerics = new HashSet<String>(Arrays.asList(new String[]{"TFI","LEN","TIT"}));
	private final HashSet<String> mustSplit = new HashSet<String>(Arrays.asList(new String[]{"SemanticCuePhrase"}));
	private final HashSet<String> ignore = new HashSet<String>(Arrays.asList(new String[]{"ID","SEMVERBS"}));
	private final HashSet<String> lookbackTypes = new HashSet<String>(Arrays.asList(new String[]{"SemanticCuePhrase","SVPATTERNS","SVVOICE","SVTENSE","SVACT"}));


	public static void main (String[] args){
		String dir = args[0];
		/*
		try{
			AZClassifier classifier = new AZClassifier("classpath:/cfg/config.scala",dir + "/target/classes/models/");
			Document doc = new SciXmlReader(new File(dir + "/data/cl/9405023.pos.xml")).parse();
			String[] labels = classifier.classify(doc);
			Configuration config = Configuration.load("classpath:/cfg/config.scala");
			AZWrapper wrapper = new AZWrapper(config);	
			ExtractedFeature[][] features = wrapper.getFeatures(doc);
			assert labels.length == features.length;
			double correct = 0;
			for (int s = 0; s < labels.length; s++){
				String gold = "";
				for (ExtractedFeature ef : features[s]){
					String[] components = ef.getComponents();
					if (components[0].equals("AZ")){
						gold = components[1];
					}
				}
				System.out.println("Predicted: " + labels[s] + ", Gold: " + gold);
				if (labels[s].equals(gold)) correct++;
			}

			System.out.println(correct + "/" + labels.length + " correct");
		}catch(Exception e){
			e.printStackTrace();
			System.exit(9);
		}
		 */
		AZClassifier classifier = null;

		try{
			classifier = new AZClassifier("classpath:/cfg/config.scala",dir + "/target/classes/models");
		}catch(Exception e){
			e.printStackTrace();
			System.exit(9);
		}

		String[] strings = {"Furthermore , combining CFS and filtering features can improve model performance and reduce the number of features being considered in model building , at t-test of 1 , dataset2431 yields models with 769 features , R = 0.777 and MSE = 0.022 and dataset 579 yields models with 542 features , R = 0.622 and MSE = 0.072 .",
				"Predictive SVM models are able to be produced from individual or combinations of features , and methods such as feature filtering or CFS can improve model performance .",
				"However , minimizing feature sets sizes can result in distinct sub sets of features being selected with nearly equal model performance among feature subsets .",
				"(Kitamura and Matsumoto, 1996) has used the Dice coefficient (Kay and Roschesen, 1993) which was weighted by the logarithm of the frequency of the word pair as the Table 2: Examples of translation pairs extracted by our method Japanese English &#0;&#2;"
		};

		String[] labels = classifier.classify(strings,"related work");
		for (int i = 0; i < labels.length; i++) System.out.println(labels[i]);

		for (int i = 0; i < labels.length; i++){
			System.out.println(labels[i] + " " + strings[i]);
		}
	}

	public AZClassifier(String configFilepath, String modelsDirpath) throws IOException, ClassNotFoundException{
		Configuration config = Configuration.load(configFilepath);
		wrapper = new AZWrapper(config);		
		readObjects(modelsDirpath);
	}

	/*
	public AZClassifier() throws IOException, ClassNotFoundException{
		this("classpath:/cfg/config.scala", "/models");
	}
	 */

	public AZClassifier() throws IOException, ClassNotFoundException{
		this("classpath:/cfg/config.scala", "classpath:/models");
	}

	@SuppressWarnings("unchecked")
	private void readObjects(String dir) throws IOException, ClassNotFoundException{
		if (dir.startsWith("classpath:")){
			String pathInJar = dir.substring("classpath:".length());
			
			InputStream stream = getClass().getResourceAsStream(pathInJar + "/az.model");
			BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
			model = Model.load(reader);
			
			ObjectInputStream ois = new ObjectInputStream(getClass().getResourceAsStream(pathInJar + "/az.featuremap.bin"));
			featureMap = (Map<String,Integer>) ois.readObject();
			ois.close();
			
			ois = new ObjectInputStream(getClass().getResourceAsStream(pathInJar + "/az.labellist.bin"));
			labelList = (List<String>) ois.readObject();
			ois.close();
			
			ois = new ObjectInputStream(getClass().getResourceAsStream(pathInJar + "/az.avemap.bin"));
			aveMap = (Map<String,Double>) ois.readObject();
			ois.close();
			
			ois = new ObjectInputStream(getClass().getResourceAsStream(pathInJar + "/az.binmap.bin"));
			binMap = (Map<String,Double>) ois.readObject();
			ois.close();
		}else{
			model = Model.load(new File(dir,"az.model"));
			ObjectInputStream ois = new ObjectInputStream(new FileInputStream(dir + "/az.featuremap.bin"));
			featureMap = (Map<String,Integer>) ois.readObject();
			ois.close();
			ois = new ObjectInputStream(new FileInputStream(dir + "/az.labellist.bin"));
			labelList = (List<String>) ois.readObject();
			ois.close();
			ois = new ObjectInputStream(new FileInputStream(dir + "/az.avemap.bin"));
			aveMap = (Map<String,Double>) ois.readObject();
			ois.close();
			ois = new ObjectInputStream(new FileInputStream(dir + "/az.binmap.bin"));
			binMap = (Map<String,Double>) ois.readObject();
			ois.close();
		}
		modelLabels = model.getLabels();
	}



	public String[] classify(Document doc){
		FeatureNode[][] features = getFeatures(doc);

		int[] predictions = predict(features);
		String[] strings = new String[predictions.length];
		for (int i = 0; i < strings.length; i++){
			strings[i] = labelList.get(predictions[i]);
		}

		return strings;
	}	

	private String replaceCitations(String str, String replacement){
		str = str.replaceAll("\\\\\\[[A-Z][A-Z]+[0-9]{2}\\\\\\]", replacement);

		if (str.matches(".*\\d\\d\\d\\d.*") == false){
			return str;
		}

		final String author = "((van|von|Van|de|del|della) )?[A-Z]([a-zA-Z?]|\\w['-]\\w|[a-zA-Z?] [^a-zA-Z0-9,;\\[\\]\\(\\)] [a-zA-Z?])*[a-zA-Z]";
		final String year = "\\d\\d\\d\\d[a-z]?";
		final String yearList = year + "( ?[,;] ?" + year + ")*( and " + year + ")?";
		final String authorList = author + "( ?,? " + author + ")*( et al ?\\.?| and others| and " + author + ")?";

		str = str.replaceAll("\\s+", " ");

		str = str.replaceAll("[\\[\\(][^\\)\\]]*" + authorList + " ?,? " + yearList + "( ?[,;] " + authorList + " ?,? " + yearList + ")* ?[\\)\\]]" , replacement);
		str = str.replaceAll(authorList + " ?,? [\\(\\[] ?" + yearList + " ?[\\)\\]]", replacement);

		return str;
	}

	public String[] classify(String[] strings, String header){
		StringBuffer sb = new StringBuffer();
		sb.append("<?xml version='1.0' encoding='UTF-8'?><!DOCTYPE PAPER SYSTEM \"/homes/sht25/dtd/s.dtd\" ><PAPER><FILENO>0</FILENO><AUTHORS></AUTHORS>");

		if (header != null && header.equals("") == false){
			sb.append("<ABSTRACT></ABSTRACT><BODY><DIV DEPTH=\"0\"><HEADER ID='H-1'>" + header + "</HEADER><P>");
		}else{
			sb.append("<ABSTRACT></ABSTRACT><BODY><DIV DEPTH=\"0\"><P>");
		}
		for (int i = 0; i < strings.length; i++){
			strings[i] = strings[i].replaceAll("&#\\d+;", "");
			strings[i] = replaceCitations(strings[i],"<REF>Author</REF>");
			sb.append("<S ID=\"" + i + "\">" + strings[i] + "</S>");
		}

		sb.append("</P></DIV></BODY><REFERENCES> </REFERENCES></PAPER>");

		Document doc = null;
		try {
			doc = new SciXmlReader(new ByteArrayInputStream(sb.toString().getBytes("UTF-8")),null).parse();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return classify(doc);
	}

	protected int[] predict(FeatureNode[][] features){
		int[] predictions = new int[features.length];

		double[][] delta = new double[features.length][modelLabels.length];
		int[][] phi = new int[features.length][modelLabels.length];

		// Forward
		for (int s = 0; s < features.length; s++){
			for (int k = 0; k < modelLabels.length; k++){
				int old = features[s].length;
				FeatureNode[] newX = new FeatureNode[features[s].length + 1];
				System.arraycopy(features[s],0,newX,0,features[s].length);
				double[] scores = new double[modelLabels.length];
				String feat = "History:" + modelLabels[k];
				if (featureMap.containsKey(feat) == false) featureMap.put(feat, featureMap.size()+1);
				newX[old] = new FeatureNode(featureMap.get(feat),1.0);

				// Actually, the features don't need to be in order for prediction
				//assert old <= 0 || newX[old].index > newX[old-1].index : old + "," + newX[old].toString() + "," + newX[old-1].toString();

				Linear.predictProbability(model, newX, scores);
				for (int l = 0; l < modelLabels.length; l++){
					double de;
					if (s == 0){
						de = scores[l];
					}else{
						de = scores[l]*delta[s-1][k];
					}
					if (de > delta[s][l]){
						delta[s][l] = de;
						phi[s][l] = k;
					}
				}
			}						
		}

		double last = 0;
		for (int l = 0; l < modelLabels.length; l++){
			if (delta[features.length-1][l] > last){
				last = delta[features.length-1][l];
				predictions[features.length-1] = l;
			}
		}

		// Backward
		for (int s = features.length-2; s >= 0; s--){
			predictions[s] = phi[s+1][predictions[s+1]];
		}

		for (int s = 0; s < features.length; s++){
			predictions[s] = modelLabels[predictions[s]];
		}
		return predictions;
	}

	private FeatureNode[][] getFeatures(Document doc){		
		ExtractedFeature[][] extractedFeatures = wrapper.getFeatures(doc);
		FeatureNode[][] lrFeatures = new FeatureNode[extractedFeatures.length][];

		HashSet<String> lookbackSet = new HashSet<String>();

		for (int s = 0; s < extractedFeatures.length; s++){		
			TreeSet<Integer> orderedSet = new TreeSet<Integer>();

			for (String feat : lookbackSet){
				if (featureMap.containsKey(feat) == false) featureMap.put(feat,featureMap.size()+1);
				orderedSet.add(featureMap.get(feat));
			}

			lookbackSet = new HashSet<String>();

			for (ExtractedFeature ef : extractedFeatures[s]){
				String[] components = ef.getComponents();
				String type = components[0];
				if (ignore.contains(type)) continue;
				if (numerics.contains(type)){
					double val = Double.parseDouble(components[1]);
					if (val > aveMap.get(type)){
						String feat = type;
						if (featureMap.containsKey(feat) == false) continue;
						orderedSet.add(featureMap.get(feat));						
					}

					int bin = (int) (val/binMap.get(type));

					String feat = type + "Bin:" + bin;
					if (featureMap.containsKey(feat) == false) continue;
					orderedSet.add(featureMap.get(feat));		

					String logType = "Log" + type;
					double logVal = Math.log(val);
					if (aveMap.containsKey(logType)){
						if (logVal > aveMap.get(logType)){
							feat = logType;
							if (featureMap.containsKey(feat) == false) continue;
							orderedSet.add(featureMap.get(feat));						
						}
					}

					if (binMap.containsKey(logType)){
						bin = (int) (logVal/binMap.get(logType));
						feat = logType + "Bin:" + bin;
						if (featureMap.containsKey(feat) == false) continue;
						orderedSet.add(featureMap.get(feat));	
					}
				}else if (type.equals("SEN")){
					Set<String> ngrams = processString(components[1],3);
					for (String ng : ngrams){
						String feat = type + ":" + ng;
						if (featureMap.containsKey(feat) == false) continue;
						orderedSet.add(featureMap.get(feat));
					}
				}else if (type.equals("AZ")){
					continue;
				}else if (mustSplit.contains(type)){
					for (String sp : components[1].split("!")){
						String feat = type + ":" + sp;
						if (featureMap.containsKey(feat) == false) continue;
						orderedSet.add(featureMap.get(feat));
						if (lookbackTypes.contains(type)){
							lookbackSet.add("Lookback:" + feat);
						}
					}
				}else{
					String feat = type + ":" + components[1];
					if (featureMap.containsKey(feat) == false) featureMap.put(feat,featureMap.size()+1);
					orderedSet.add(featureMap.get(feat));
					if (lookbackTypes.contains(type)){
						lookbackSet.add("Lookback:" + feat);
					}
				}

				lrFeatures[s] = new FeatureNode[orderedSet.size()];
				int i = 0;
				for (int idx : orderedSet){
					lrFeatures[s][i] = new FeatureNode(idx,1.0);
					i++;
				}
			}
		}

		return lrFeatures;
	}

	private HashSet<String> processString(String str, int ngram){
		str = str.replaceAll("-[LR]RB-", "").replaceAll("\\d", "0");

		int start = 0;
		while(start < str.length()){
			char pre = str.charAt(start);
			if (pre == '"' || pre == '\''){
				start++;
				continue;
			}
			break;
		}


		int end = str.length();

		while(end > start){
			char post = str.charAt(end-1);
			if (post == '"' || post == '\'' || post == '.' || post == '?' || post == '!' || post == '-' ){
				end--;
				continue;
			}
			break;
		}

		str = str.substring(start,end);

		HashSet<String> set = new HashSet<String>();

		if (str.length() <= 0) return set;

		String[] words = str.split("\\s+");

		for (int i = 0; i < words.length; i++){
			for (int n = 0; n < ngram; n++){
				if (i - n < 0) continue;
				StringBuilder sb = new StringBuilder();
				for (int w = i - n; w <= i; w++){
					sb.append(words[w] + ":");
				}

				String feat = sb.toString();
				set.add(feat);
			}
		}

		return set;
	}
}
