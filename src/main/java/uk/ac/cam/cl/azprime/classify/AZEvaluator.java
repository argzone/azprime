package uk.ac.cam.cl.azprime.classify;

import java.text.DecimalFormat;
import java.util.List;
import java.util.Set;

import myutil.evaluation.Evaluator;

public class AZEvaluator extends Evaluator<int[][]>{
	private DecimalFormat df = new DecimalFormat("#.###");
	private List<String> labelList;
	
	public AZEvaluator(List<String> labels){
		labelList = labels;
	}
	
	public double[][] evaluateCV(int[][] predictions, int[][] gold, boolean print, Set<Integer>[] split){
		int k = split.length;
		double[] f = new double[k];
		double[] a = new double[k];
		
		for (int i = 0; i < k; i++){
			int[][] g = new int[split[i].size()][];
			int[][] p = new int[g.length][];
			int count = 0;
			for (int d : split[i]){
				g[count] = gold[d];
				p[count] = predictions[d];
				count++;
			}
			
			double[][] eval = evaluate(p,g,false);
			f[i] = eval[eval.length-1][2];
			a[i] = eval[0][0];
		}
		
		if (print){
			System.out.println("By fold:\n\tAcc\tF");
			for (int i = 0; i < k; i++){
				System.out.println(i + "\t" + a[i] + "\t" + f[i]);
			}
			System.out.println();
		}
		
		return new double[][]{f,a};
	}
	
	@Override
	public double[][] evaluate(int[][] predictions, int[][] gold, boolean print){
		/*
		 * Per-sentence accuracy
		 * Per-document accuracy
		 * Precision per label and average
		 * Recall per label and average
		 * F-score per label and average
		 */

		int numLabels = labelList.size()-1;

		int[] posGold = new int[numLabels];
		int[] posPredicted = new int[numLabels];
		int[] posCorrect = new int[numLabels];
		int[] correctPerDoc = new int[predictions.length];
		int correct = 0;
		int sCount = 0;
		int[][] confusion = new int[numLabels][numLabels];

		double[][] scores = new double[numLabels+2][];
		scores[0] = new double[2];
		scores[scores.length-1] = new double[3];
		for (int l = 0; l < numLabels; l++){
			scores[l+1] = new double[3];
		}

		
		assert predictions.length == gold.length;
		for (int d = 0; d < gold.length; d++){
			assert predictions[d].length == gold[d].length: predictions[d].length + "," + gold[d].length;
			sCount += gold[d].length;

			for (int s = 0; s < gold[d].length; s++){				
				if (gold[d][s] == predictions[d][s]){
					correct++;
					correctPerDoc[d]++;
					posCorrect[gold[d][s]-1]++;
				}

				posGold[gold[d][s]-1]++;
				posPredicted[predictions[d][s]-1]++;
				confusion[gold[d][s]-1][predictions[d][s]-1]++;
			}
		}

		scores[0][0] = ((double)correct)/sCount;
		for (int d = 0; d < correctPerDoc.length; d++){
			scores[0][1] += ((double) correctPerDoc[d])/predictions[d].length;
		}
		scores[0][1] /= predictions.length;

		double fSum = 0;
		double pSum = 0; 
		double rSum = 0;

		for (int l = 0; l < numLabels; l++){
			if (posPredicted[l] > 0){
				scores[l+1][0] = ((double)posCorrect[l])/posPredicted[l];
				pSum += scores[l+1][0];
			}

			if (posGold[l] > 0){
				// this should always be true
				scores[l+1][1] = ((double)posCorrect[l])/posGold[l];
				rSum += scores[l+1][1];
			}

			if (scores[l+1][0] > 0 || scores[l+1][1] > 0){
				scores[l+1][2] = 2*scores[l+1][0]*scores[l+1][1]/(scores[l+1][0] + scores[l+1][1]);
				fSum += scores[l+1][2];
			}			
		}

		scores[numLabels+1][0] = pSum/numLabels;
		scores[numLabels+1][1] = rSum/numLabels;
		scores[numLabels+1][2] = fSum/numLabels;

		if (print){
			System.out.println("Per-sentence accuracy\t" + df.format(scores[0][0]) + "(" + correct + "/" + sCount + ")");
			System.out.println("Per-document accuracy\t" + df.format(scores[0][1]));

			System.out.println("\n\n\tP\tR\tF");
			for (int l = 1; l <= numLabels; l++){
				System.out.print(labelList.get(l) + "\t");
				System.out.print(df.format(scores[l][0]) + "\t" + df.format(scores[l][1]) + "\t" + df.format(scores[l][2])  + "\t" + posPredicted[l-1]  + "/" + posGold[l-1]);
				System.out.println();
			}
			System.out.println("\nAverage\t" + df.format(scores[numLabels+1][0]) + "\t" + df.format(scores[numLabels+1][1]) + "\t" + df.format(scores[numLabels+1][2]) + "\n\n");


			// Print confusion matrix
			for (int i = 1; i <= numLabels; i++){
				System.out.print("\t" + labelList.get(i));
			}
			System.out.println();

			for (int l = 1; l <= numLabels; l++){
				System.out.print(labelList.get(l));
				for (int m = 0; m < numLabels; m++){
					System.out.print("\t" + confusion[l-1][m]);
				}
				System.out.println();
			}
		}

		return scores;
	}

	public double[][] evaluate(int[] predictions, int[] gold, int numLabels){
		int[] posGold = new int[numLabels];
		int[] posPredicted = new int[numLabels];
		int[] posCorrect = new int[numLabels];
		int correct = 0;

		double[][] scores = new double[numLabels+2][];
		scores[0] = new double[2];
		scores[scores.length-1] = new double[3];
		for (int l = 0; l < numLabels; l++){
			scores[l+1] = new double[3];
		}

		for (int s = 0; s < gold.length; s++){				
			if (gold[s] == predictions[s]){
				correct++;
				posCorrect[gold[s]-1]++;
			}

			posGold[gold[s]-1]++;
			posPredicted[predictions[s]-1]++;
		}


		scores[0][0] = ((double)correct)/gold.length;

		double fSum = 0;
		double pSum = 0; 
		double rSum = 0;

		for (int l = 0; l < numLabels; l++){
			if (posPredicted[l] > 0){
				scores[l+1][0] = ((double)posCorrect[l])/posPredicted[l];
				pSum += scores[l+1][0];
			}

			if (posGold[l] > 0){
				// this should always be true
				scores[l+1][1] = ((double)posCorrect[l])/posGold[l];
				rSum += scores[l+1][1];
			}

			if (scores[l+1][0] > 0 || scores[l+1][1] > 0){
				scores[l+1][2] = 2*scores[l+1][0]*scores[l+1][1]/(scores[l+1][0] + scores[l+1][1]);
				fSum += scores[l+1][2];
			}			
		}

		scores[numLabels+1][0] = pSum/numLabels;
		scores[numLabels+1][1] = rSum/numLabels;
		scores[numLabels+1][2] = fSum/numLabels;

		return scores;
	}
}
