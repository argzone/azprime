package uk.ac.cam.cl.azprime.classify;

import java.util.*;

import perceptron.struct.Feature;

import uk.ac.cam.cl.azprime.factorie.AZFactorieModel;

public class FactorieClassifier extends AZClassifier{
	private AZFactorieModel model;
	private int iterations;

	public FactorieClassifier(AZProblem az, int its) {
		super(az);
		model = new AZFactorieModel();
		iterations = its;
	}

	@Override
	public void train(AZProblem prob) {
		Map<String,Object>[][] features = new Map[prob.docs.length][];
		Map<String,Object> weights = new HashMap<String,Object>();
		int n = 0;
		String[][] labels = new String[prob.docs.length][];
		for (int i = 0; i < labels.length; i++){
			AZDoc doc = prob.docs[i];
			labels[i] = new String[doc.labels.length];
			features[i] = new Map[doc.labels.length];
			
			for (int s = 0; s < doc.labels.length; s++){
				labels[i][s] = prob.extractor.labelList.get(doc.labels[s]);
				if (weights.containsKey(labels[i][s]) == false){
					weights.put(labels[i][s],1.0);
				}else{
					weights.put(labels[i][s],((Double)weights.get(labels[i][s]))+1);
				}
				features[i][s] = new HashMap<String,Object>();
				for (Feature feat : doc.features[s]){
					features[i][s].put(prob.extractor.getFeatureName(feat.index), feat.value);
				}
				n++;
			}
		}
		
		for (String label : weights.keySet()){
			weights.put(label,1.0/((Double)weights.get(label)));
		}
		
		System.out.println("Training");
		model.train(features,labels,iterations,weights);
	}

	@Override
	public int[][] predict(AZProblem prob) {
		int[][] predictions = new int[prob.docs.length][];
		for (int d = 0; d < prob.docs.length; d++){
			AZDoc doc = prob.docs[d];
			Map<String,Object>[] features = new Map[doc.features.length];
			String[] dummyLabels = new String[doc.features.length]; // not sure how to avoid providing this in FactorIE
			for (int s = 0; s < features.length; s++){
				features[s] = new HashMap<String,Object>();
				dummyLabels[s] = prob.extractor.labelList.get(doc.labels[s]);
				for (Feature feat : doc.features[s]){
					features[s].put(prob.extractor.getFeatureName(feat.index), feat.value);
				}
			}
			String[] labels = model.predict(features,dummyLabels);
			predictions[d] = new int[features.length];
			for (int s = 0; s < labels.length; s++){
				predictions[d][s] = prob.extractor.labelMap.get(labels[s]);
			}
		}
		return predictions;
	}

	@Override
	public int[][] crossValidate(AZProblem prob, Set<Integer>[] folds) {
		int[][] predictions = new int[prob.docs.length][];
		for (int f = 0; f < folds.length; f++){
			ArrayList<Integer> idxList = new ArrayList<Integer>();
			ArrayList<AZDoc> testDocs = new ArrayList<AZDoc> ();
			ArrayList<AZDoc> trainDocs = new ArrayList<AZDoc> ();
			
			for (int d = 0; d < prob.docs.length; d++){
				if (folds[f].contains(d)){
					idxList.add(d);
					testDocs.add(prob.docs[d]);
				}else{
					trainDocs.add(prob.docs[d]);
				}
			}

			String[][] trainLabels = new String[trainDocs.size()][];
			Map<String,Object>[][] trainFeatures = new Map[trainDocs.size()][];
			for (int d = 0; d < trainLabels.length; d++){
				AZDoc doc = trainDocs.get(d);
				trainLabels[d] = new String[doc.labels.length];
				trainFeatures[d] = new Map[trainLabels[d].length];
				for (int s = 0; s < trainLabels[d].length; s++){
					trainLabels[d][s] = prob.extractor.labelList.get(doc.labels[s]);
					trainFeatures[d][s] = new HashMap<String,Object>(doc.features[s].length);
					for (Feature feat : doc.features[s]){
						trainFeatures[d][s].put(prob.extractor.getFeatureName(feat.index), feat.value);
					}
				}
			}
			
			String[][] testLabels = new String[testDocs.size()][];
			Map<String,Object>[][] testFeatures = new Map[testDocs.size()][];
			for (int d = 0; d < testLabels.length; d++){
				AZDoc doc = testDocs.get(d);
				testLabels[d] = new String[doc.labels.length];
				testFeatures[d] = new Map[testLabels[d].length];
				for (int s = 0; s < testLabels[d].length; s++){
					testLabels[d][s] = prob.extractor.labelList.get(doc.labels[s]);
					testFeatures[d][s] = new HashMap<String,Object>(doc.features[s].length);
					for (Feature feat : doc.features[s]){
						testFeatures[d][s].put(prob.extractor.getFeatureName(feat.index), feat.value);
					}
				}
			}
			
			System.out.println("Fold " + f + ": " + trainDocs.size() + " training docs, " + testDocs.size() + " test docs");
			
			String[][] predictedLabels = model.trainAndPredict(trainFeatures,trainLabels,iterations,testFeatures,testLabels);

			for (int d = 0; d < predictedLabels.length; d++){
				int idx = idxList.get(d);
				predictions[idx] = new int[prob.docs[idx].labels.length];
				assert predictions[idx].length == predictedLabels[d].length;
				for (int s = 0; s < predictions[idx].length; s++){
					predictions[idx][s] = prob.extractor.labelMap.get(predictedLabels[d][s]);
				}
			}
		}
		
		return predictions;
	}

	@Override
	public int[][] crossValidateAndOptimise(AZProblem prob, Set<Integer>[] folds) {
		// TODO Auto-generated method stub
		return null;
	}

}
