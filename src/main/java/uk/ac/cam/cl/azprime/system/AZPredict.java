package uk.ac.cam.cl.azprime.system;

import java.io.*;
import java.util.*;

import myutil.config.CommandLineParser;

import uk.ac.cam.cl.azprime.xml.*;
import uk.ac.cam.cl.azprime.xml.ParsedSciXml.Document;
import uk.ac.cam.cl.azprime.AZWrapper;
import uk.ac.cam.cl.azprime.config.Configuration;
import uk.ac.cam.cl.azprime.ml.ExtractedFeature;

import de.bwaldvogel.liblinear.*;

public class AZPredict {
	private HashMap<String,Integer> featureMap;
	private ArrayList<String> labelList;
	private Model model;
	private AZWrapper wrapper;
	private Map<String,Double> aveMap;
	private Map<String,Double> binMap;
	private ArrayList<String> featureList;
	private HashMap<String,String> labelMapping;
	private HashMap<String,Integer> labelMap;

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		CommandLineParser clp = new CommandLineParser();
		clp.parse(args);

		File inputDir = clp.getFile("in", null);
		File outputDir = clp.getFile("out", null);
		File modelDir = clp.getFile("model",null);
		Boolean outputSciXml = clp.getBoolean("output-scixml", false);
		Boolean overwrite = clp.getBoolean("overwrite", false);
		Boolean outputPos = clp.getBoolean("output-pos", true);
		String inputSuffix = clp.getString("in-suffix", "pos.xml");
		String outputSuffix = clp.getString("out-suffix", "predictions.txt");

		HashSet<String> ignore = new HashSet<String>(Arrays.asList(clp.getStringArray("ignore")));

		new AZPredict(inputDir,outputDir,modelDir,ignore,inputSuffix,outputSuffix,outputSciXml, outputPos, overwrite);
	}

	public AZPredict(File inputDir, File outputDir, File modelDir, Set<String> ignore, final String inputSuffix, String outputSuffix, Boolean outputSciXml, Boolean outputPos, Boolean overwrite){
		if (modelDir == null) modelDir = new File(getClass().getResource("/models").getPath());
		Configuration config = Configuration.load("classpath:/cfg/config.scala");
		wrapper = new AZWrapper(config);

		System.out.println("Reading model from " + modelDir.getPath());
		try{
			readObjects(modelDir);
		}catch(Exception e){
			e.printStackTrace();
			System.exit(9);
		}

		System.out.println("Reading data from " + inputDir.getPath());

		AZClassifier classifier = null;

		try{
			classifier = new AZClassifier("classpath:/cfg/config.scala",modelDir.getPath());
		}catch(Exception e){
			e.printStackTrace();
			System.exit(9);
		}

		File[] files = inputDir.listFiles(new FilenameFilter(){
			@Override
			public boolean accept(File dir, String name) {
				return name.endsWith(inputSuffix);
			}			
		});

		for (File file : files){
			String inPath = file.getPath();

			File outfile;
			if (outputDir != null) {
				if (!outputDir.exists()) outputDir.mkdirs();
				String filename = file.getName().substring(0, file.getName().lastIndexOf(inputSuffix));
				outfile = new File(outputDir, filename + outputSuffix);
			} else {
				String outPath = inPath.substring(0,inPath.length() - inputSuffix.length()) + outputSuffix;
				outfile = new File(outPath);
			}

			System.out.println("Reading from " + inPath + ", predicting and writing to " + outfile.getPath());

			final String[] prediction = predict(file,classifier,ignore);

			try {
				if (overwrite && outfile.exists()) outfile.delete();
				if (outputSciXml) {
					Document doc = new SciXmlReader(file).parse();
					final List<String> preds = new ArrayList<String>();
					Collections.addAll(preds, prediction);
					Document predDoc = ParsedSciXml.replaceSentences(doc, new ParsedSciXml.ReplaceSentenceHandler() {
						@Override
						public ParsedSciXml.Sentence apply(ParsedSciXml.Sentence s) {
							return s.withAzCategory(preds.remove(0));
						}
					});
					new SciXmlWriter(predDoc, outputPos, true, 4).write(outfile);
				} else{
					writePrediction(prediction, outfile);
				}
			}catch(IOException e){
				e.printStackTrace();
				System.exit(9);
			}
		}
	}

	private void writePrediction(String[] prediction, File f) throws IOException{
		PrintWriter writer = new PrintWriter(new BufferedWriter(new FileWriter(f)));
		
		for (String p : prediction){
			writer.println(p);
		}
		
		writer.close();
	}

	private String[] predict(File f, AZClassifier classifier,Set<String> ignore){

		FeatureNode[][] features = readData(new File[]{f},ignore);
		int[] prediction = classifier.predict(features);
		String[] predictionLabels = new String[prediction.length];
		for (int i = 0; i < prediction.length; i++){
			predictionLabels[i] = labelList.get(prediction[i] - 1); // Check this
		}

		return predictionLabels;
	}

	private FeatureNode[][] readData(File[] files, Set<String> more2Ignore){
		HashSet<String> numerics = new HashSet<String>(Arrays.asList(new String[]{"TFI","LEN","TIT"}));
		HashSet<String> mustSplit = new HashSet<String>(Arrays.asList(new String[]{"SemanticCuePhrase"}));
		HashSet<String> ignore = new HashSet<String>(Arrays.asList(new String[]{"ID","SEMVERBS","AZ"}));
		ignore.addAll(more2Ignore);
		HashSet<String> lookbackTypes = new HashSet<String>(Arrays.asList(new String[]{"SemanticCuePhrase","SVPATTERNS","SVVOICE","SVTENSE","SVACT"}));
		HashMap<String,Double> max = new HashMap<String,Double>();
		for (String type : numerics){
			max.put(type,Double.NEGATIVE_INFINITY);
			max.put("Log" + type,Double.NEGATIVE_INFINITY);
		}
		HashMap<String,Double> totals = new HashMap<String,Double>();
		for (String type : numerics){
			totals.put(type,0.0);
			totals.put("Log" + type,0.0);
		}

		ArrayList<HashMap<String,Double>> numericFeatures = new ArrayList<HashMap<String,Double>>();
		ArrayList<TreeSet<Integer>> stringFeatures = new ArrayList<TreeSet<Integer>>();

		for (File f : files){
			Document doc = new SciXmlReader(f).parse();

			ExtractedFeature[][] extractedFeatures = wrapper.getFeatures(doc);
			HashSet<String> lookbackSet = new HashSet<String>();
			int lastSen = -1;

			LOOP: for (int s = 0; s < extractedFeatures.length; s++){
				TreeSet<Integer> stringSet = new TreeSet<Integer>();
				HashMap<String,Double> numericMap = new HashMap<String,Double>();
				int y = -1;				

				for (String feat : lookbackSet){
					if (featureMap.containsKey(feat) == false){
						featureMap.put(feat,featureMap.size()+1);
						featureList.add(feat);
					}

					stringSet.add(featureMap.get(feat));					
				}

				lookbackSet = new HashSet<String>();

				for (ExtractedFeature ef : extractedFeatures[s]){
					String[] components = ef.getComponents();
					String type = components[0];
					//		System.out.println("Extracted feature " + type + ":" + components[1]);
					if (ignore.contains(type)) continue;		

					if (numerics.contains(type)){
						double val = Double.parseDouble(components[1]);

						numericMap.put(type, val);

						if (val > 0){
							String logType = "Log" + type;

							numericMap.put(logType, Math.log(val));
						}
					}else if (type.equals("SEN")){
						Set<String> ngrams = processString(components[1],3);
						for (String ng : ngrams){
							String feat = type + ":" + ng;
							if (featureMap.containsKey(feat) == false){
								featureMap.put(feat,featureMap.size()+1);
								featureList.add(feat);
							}
							stringSet.add(featureMap.get(feat));
						}
					}else if (mustSplit.contains(type)){
						for (String sp : components[1].split("!")){
							String feat = type + ":" + sp;
							if (featureMap.containsKey(feat) == false){
								featureMap.put(feat,featureMap.size()+1);
								featureList.add(feat);
							}
							stringSet.add(featureMap.get(feat));
							if (lookbackTypes.contains(type)){
								lookbackSet.add("Lookback:" + feat);
							}
						}
					}else{
						String feat = type + ":" + components[1];
						if (featureMap.containsKey(feat) == false){
							featureMap.put(feat,featureMap.size()+1);
							featureList.add(feat);
						}
						stringSet.add(featureMap.get(feat));
						if (lookbackTypes.contains(type)){
							lookbackSet.add("Lookback:" + feat);
						}
					}
				}

				stringFeatures.add(stringSet);
				numericFeatures.add(numericMap);

				lastSen = stringFeatures.size()-1;
			}			
		}		

		for (int s = 0; s < stringFeatures.size(); s++){
			for (String type : numericFeatures.get(s).keySet()){
				if (numericFeatures.get(s).get(type) > aveMap.get(type)){
					String feat = type;
					if (featureMap.containsKey(feat) == false){
						featureMap.put(feat,featureMap.size()+1);
						featureList.add(feat);
					}
					stringFeatures.get(s).add(featureMap.get(feat));
				}

				int bin = (int) (numericFeatures.get(s).get(type)/binMap.get(type));
				String feat = type + "Bin:" + bin;
				if (featureMap.containsKey(feat) == false){
					featureMap.put(feat,featureMap.size()+1);
					featureList.add(feat);
				}
				stringFeatures.get(s).add(featureMap.get(feat));
			}
		}


		FeatureNode[][] features = new FeatureNode[stringFeatures.size()][];

		for (int s = 0; s < features.length; s++){
			features[s] = new FeatureNode[stringFeatures.get(s).size()];
			int i = 0;
			for (int idx : stringFeatures.get(s)){
				features[s][i] = new FeatureNode(idx,1.0);
				i++;
			}
		}

		return features;
	}

	private HashSet<String> processString(String str, int ngram){
		//	Set<String> ignore = new HashSet<String>(Arrays.asList(new String[]{"-LRB-","-RRB-","-RRB"}));
		int start = 0;

		str = str.replaceAll("-[LR]RB-", "").replaceAll("\\d", "0");

		while(start < str.length()){
			char pre = str.charAt(start);
			if (pre == '"' || pre == '\''){
				start++;
				continue;
			}
			break;
		}


		int end = str.length();

		while(end > start){
			char post = str.charAt(end-1);
			if (post == '"' || post == '\'' || post == '.' || post == '?' || post == '!' || post == '-' ){
				end--;
				continue;
			}
			break;
		}

		str = str.substring(start,end);

		HashSet<String> set = new HashSet<String>();

		if (str.length() <= 0) return set;

		String[] words = str.split("\\s+");

		for (int i = 0; i < words.length; i++){			
			for (int n = 0; n < ngram; n++){
				if (i - n < 0) continue;
				StringBuilder sb = new StringBuilder();
				for (int w = i - n; w <= i; w++){
					sb.append(words[w] + ":");
				}

				String feat = sb.toString();
				set.add(feat);
			}
		}

		return set;
	}


	private void readObjects(File dir) throws IOException, ClassNotFoundException{
		ObjectInputStream ois = new ObjectInputStream(new FileInputStream(dir.getPath() + "/az.featuremap.bin"));
		featureMap = (HashMap<String,Integer>) ois.readObject();
		ois.close();
		System.out.println("Read " + featureMap.size() + " features");
		String[] temp = new String[featureMap.size()+1];
		for (String f : featureMap.keySet()){
			temp[featureMap.get(f)] = f;
		}
		featureList = new ArrayList<String>(Arrays.asList(temp));

		ois = new ObjectInputStream(new FileInputStream(dir.getPath() + "/az.labellist.bin"));
		labelList = (ArrayList<String>) ois.readObject();
		ois.close();
		labelMap = new HashMap<String,Integer>();
		for (int i = 0; i < labelList.size(); i++){
			labelMap.put(labelList.get(i),i);
		}
		ois = new ObjectInputStream(new FileInputStream(dir.getPath() + "/az.avemap.bin"));
		aveMap = (Map<String,Double>) ois.readObject();
		ois.close();
		ois = new ObjectInputStream(new FileInputStream(dir.getPath() + "/az.binmap.bin"));
		binMap = (Map<String,Double>) ois.readObject();
		ois.close();
	}
}
