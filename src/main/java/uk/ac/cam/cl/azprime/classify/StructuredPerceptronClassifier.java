package uk.ac.cam.cl.azprime.classify;

import java.util.*;

import myutil.evaluation.Evaluator;

import perceptron.struct.*;

public class StructuredPerceptronClassifier extends AZClassifier {
	private StructuredPerceptron<Feature[]> perceptron;
	private int defaultIterations;

	
	public StructuredPerceptronClassifier(int its, int history, AZProblem az, Decoder decoder, int beamSize, BeamSearchUpdate update, Evaluator<int[][]> eval) {
		super(az);
		switch(decoder){
		case BEAM: 
			perceptron = new StructuredPerceptronWithBeamSearch<Feature[]>(az.extractor,beamSize,update);
			break;
		case VITERBI: 
			perceptron = new StructuredPerceptronWithViterbi<Feature[]>(az.extractor);
			break;
		default:
			System.err.println(decoder.toString() + " not yet supported in StructuredPerceptronClassifier");
			System.exit(99);
		}
		perceptron.setEvaluator(eval);
		defaultIterations = its;
	}
	 
	public void setVerbose(boolean v){
		perceptron.setVerbose(v); 
	}
	
	public void setDefaultIterations(int its){ defaultIterations = its;}

	@Override
	public void train(AZProblem prob) {
		train(prob,defaultIterations);
	}

	public void train(AZProblem prob, int its){
		Feature[][][] x = new Feature[prob.docs.length][][];
		int[][] y = new int[x.length][];
		String[] ids = prob.getDocIDs();

		for (int d = 0; d < x.length; d++){
			x[d] = prob.docs[d].features;
			y[d] = prob.docs[d].labels;					
		}

		/*

		SparseVector[][] x = new SparseVector[prob.docs.length][];
		int[][] y = new int[x.length][];

		for (int d = 0; d < x.length; d++){
			AZNode[][] features = prob.docs[d].features;
			x[d] = new SparseVector[features.length];
			y[d] = new int[x[d].length];
			System.arraycopy(prob.docs[d].labels,0,y[d],0,x[d].length);

			for (int s = 0; s < features.length; s++){
				int[] idx = new int[features[s].length];
				double[] val = new double[idx.length];

				for (int i = 0; i < idx.length; i++){
					idx[i] = features[s][i].index;
					val[i] = features[s][i].value;
				}

				x[d][s] = new SparseVector(idx,val);
			}
		}
		 */

		perceptron.train(x,y,ids,its);
	}

	@Override
	public int[][] predict(AZProblem prob) {
		Feature[][][] x = new Feature[prob.docs.length][][];
		int[][] predictions = new int[x.length][];

		for (int d = 0; d < x.length; d++){
			predictions[d] = perceptron.predict(x[d],prob.docs[d].path);
		}

		return predictions;
	}

	@Override
	public int[][] crossValidate(AZProblem azProb, Set<Integer>[] folds) {
		AZDoc[] docs = azProb.docs;
		
		int[][] predictions = new int[docs.length][];

		for (int f = 0; f < folds.length; f++){
			Feature[][][] x = new Feature[docs.length-folds[f].size()][][];
			int[][] y = new int[x.length][];	
			System.out.println("Fold " + f + ": " + x.length + " training, " + folds[f].size() + " testing" );

			int trainCount = 0;
			int testCount = 0;

			int[] idx = new int[folds[f].size()];
			Feature[][][] testX = new Feature[folds[f].size()][][];
			int[][] testY = new int[folds[f].size()][];
			
			String[] trainIDs = new String[x.length];
			String[] testIDs = new String[idx.length];

			for (int d = 0; d < docs.length; d++){
				if (folds[f].contains(d)){
					idx[testCount] = d;
					testX[testCount] = docs[d].features;
					testY[testCount] = docs[d].labels;
					testIDs[testCount] = docs[d].path;
					testCount++;
				}else{
					y[trainCount] = docs[d].labels;					
					AZDoc doc = docs[d];
					x[trainCount] = new Feature[doc.labels.length][];
					trainIDs[trainCount] = doc.path;

					for (int s = 0; s < docs[d].labels.length; s++){	
						if (extractor.usingStructuralFeatures()){
							TreeMap<Integer,Double> sFeatures = extractor.getNewLocalFeatures(doc.features,doc.labels,s,doc.path);
							x[trainCount][s] = new Feature[doc.features[s].length + sFeatures.size()];							
							int c = doc.features[s].length;
							System.arraycopy(doc.features[s],0,x[trainCount][s],0,c);

							for(Map.Entry<Integer, Double> entry : sFeatures.entrySet()){
								x[trainCount][s][c] = new Feature(entry.getKey(),entry.getValue());
								c++;
							}
						}else{
							x[trainCount][s] = doc.features[s];
						}				
					}
					trainCount++;
				}
			}

			perceptron.setDevData(testX,testY,testIDs);
			perceptron.train(x, y,trainIDs,defaultIterations);

			for (int d = 0; d < idx.length; d++){
				predictions[idx[d]] = perceptron.predict(testX[d],testIDs[d]);
			}
		}

		return predictions;			
	}


	@Override
	public int[][] crossValidateAndOptimise(AZProblem prob, Set<Integer>[] folds) {
		// TODO Auto-generated method stub
		return null;
	}

}
