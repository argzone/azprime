package uk.ac.cam.cl.azprime.classify;

import java.util.*;
import java.io.*;

import myutil.collections.CapacityMap;
import myutil.collections.Pair;

import perceptron.struct.Feature;

import de.bwaldvogel.liblinear.*;

public class LibLinearClassifier extends AZClassifier {
	private Parameter param;
	private Model model;
	private SolverType type;
	private double C;
	private Decoder decoder;
	private AZFeatureExtractor extractor;
	private int beamSize;

	public LibLinearClassifier(Algorithm alg, Regulariser reg, int h, double parameter, AZProblem azProb, Decoder dec, int bs){
		super(azProb);
		Linear.disableDebugOutput();

		extractor = azProb.extractor;
		extractor.setHistory(h);
		decoder = dec;
		C = parameter;
		beamSize = bs;
		

		if (alg == Algorithm.LR){
			if (reg == Regulariser.L1){
				type = SolverType.L1R_LR;
			}else if (reg == Regulariser.L2){
				type = SolverType.L2R_LR;
			}
		}else if (alg == Algorithm.SVM){
			if (reg == Regulariser.L1){
				type = SolverType.L1R_L2LOSS_SVC;
			}else if (reg == Regulariser.L2){
				type = SolverType.L2R_L2LOSS_SVC_DUAL;
			}
		}

		param = new Parameter(type,C, Double.POSITIVE_INFINITY);
		if (param.getSolverType() == SolverType.L2R_LR || param.getSolverType() == SolverType.L2R_L2LOSS_SVC) {
			param.setEps(0.01);
		} else if (param.getSolverType() == SolverType.L2R_L2LOSS_SVC_DUAL || param.getSolverType() == SolverType.L2R_L1LOSS_SVC_DUAL
				|| param.getSolverType() == SolverType.MCSVM_CS || param.getSolverType() == SolverType.L2R_LR_DUAL) {
			param.setEps(0.1);
		} else if (param.getSolverType() == SolverType.L1R_L2LOSS_SVC || param.getSolverType() == SolverType.L1R_LR) {
			param.setEps(0.01);
		}
	}

	@Override
	public void train(AZProblem azProb) {
		AZDoc[] docs = azProb.docs; 

		Problem prob = new Problem();

		int l = 0;
		for (AZDoc doc : docs) l += doc.labels.length;
		System.out.println(docs.length + " documents, " + l + " sentences");

		prob.x = new FeatureNode[l][];
		prob.y = new int[l];
		prob.l = l;

		int i = 0;
		for (AZDoc doc : docs){
			for (int s = 0; s < doc.labels.length; s++){
				prob.y[i] = doc.labels[s];			
				assert prob.y[i] > 0;

				if (extractor.usingStructuralFeatures()){
					TreeMap<Integer,Double> sFeatures = extractor.getNewLocalFeatures(doc.features,doc.labels,s,doc.path);
					prob.x[i] = new FeatureNode[doc.features[s].length + sFeatures.size()];
					int c = doc.features[s].length;
					for (int f = 0; f < c; f++){
						prob.x[i][f] = new FeatureNode(doc.features[s][f].index,doc.features[s][f].value);
					}

					for(Map.Entry<Integer, Double> entry : sFeatures.entrySet()){
						prob.x[i][c] = new FeatureNode(entry.getKey(),entry.getValue());
						if (c > 0) assert prob.x[i][c].index > prob.x[i][c-1].index;
						c++;
					}
				}else{
					prob.x[i] = new FeatureNode[doc.features[s].length];
					for (int f = 0; f < prob.x[i].length; f++){
						prob.x[i][f] = new FeatureNode(doc.features[s][f].index,doc.features[s][f].value);
					}
				}
				
				for (int f = 1; f < prob.x[i].length; f++){
					 assert prob.x[i][f].index > prob.x[i][f-1].index;
				}

				i++;
			}
		}

		setWeights(prob.y);

		try{
			PrintWriter writer = new PrintWriter(new BufferedWriter(new FileWriter("/anfs/bigtmp/features.txt")));
			for (int j = 0; j < prob.l; j++){
				writer.print(prob.y[j]);
				for (FeatureNode n : prob.x[j]){
					writer.print(" " + n.index + ":" + n.value);
				}
				writer.println();
			}
			writer.close();
		}catch(IOException e){
			e.printStackTrace();
			System.exit(9);
		}

		prob.n = extractor.lastFeature();
		model = Linear.train(prob, param);
		printWeights(model);
	}

	public void printWeights(Model model){
		double[] weights = model.getFeatureWeights();
		//	CapacityMap<Double,Integer>[] topWeights = new CapacityMap[model.getNrClass()];
		int numLabels = model.getNrClass();
		int[] mLabels = model.getLabels();

		HashSet<String> zeroWeights = new HashSet<String>(extractor.allFeatureNames());

		for (int l = 0; l < numLabels; l++){
			//	topWeights[l] = new CapacityMap<Double,Integer>(20);
			CapacityMap<Double,Integer> topWeights = new CapacityMap<Double,Integer>(20);
			for (int idx = 1; idx <= extractor.lastFeature(); idx++){
				double w = weights[(idx-1)*numLabels + l];
				topWeights.put(w, idx);
				if (w != 0){
				//	zeroWeights.remove(featureList.get(idx));
					zeroWeights.remove(extractor.getFeatureName(idx));
				}
			}

			System.out.println("Top weights for " + labelList.get(mLabels[l]) + ":");
			for (Pair<Double,Integer> p : topWeights.getEntries()){
	//			System.out.println(featureList.get(p.second()) + "\t" + p.first());
				System.out.println(extractor.getFeatureName(p.second()) + "\t" + p.first());
			}
			System.out.println();
		}

		System.out.println(zeroWeights.size() + " feature weights were zero for all labels");
	}

	@Override
	public int[][] predict(AZProblem azProb) {
		AZDoc[] docs = azProb.docs; 

		int[][] predictions = new int[docs.length][];
		for (int i = 0; i < docs.length; i++){
			predictions[i] = predictDoc(docs[i],model);
		}

		return predictions;
	}

	private int[] predictDoc(AZDoc doc, Model predictModel){
		switch(decoder){
		case VITERBI:	
			return predictDocViterbi(doc,predictModel);
		case BEAM:
			return predictDocBeam(doc,predictModel);
		default: 
			return null;
		}
	}

	
	private int[] predictDocBeam(AZDoc doc, Model predictModel){
		int[] modelLabels = predictModel.getLabels();
		CapacityMap<Double,int[]> beam = new CapacityMap<Double,int[]>(beamSize);
		beam.put(0.0, new int[doc.features.length]);

		for (int s = 0; s < doc.features.length; s++){
			CapacityMap<Double,int[]> temp = new CapacityMap<Double,int[]>(beamSize);
			for (Pair<Double,int[]> entry : beam.getEntries()){				
				double[] probs = new double[modelLabels.length];
				FeatureNode[] newX = null;

				if (extractor.usingStructuralFeatures()){
					Feature[] structFeatures = extractor.getLocalStructFeatures(doc.features[s], entry.second(), s, doc.path);
					newX = new FeatureNode[doc.features[s].length + structFeatures.length];
					int i = 0;
					while (i < doc.features[s].length){
						newX[i] = new FeatureNode(doc.features[s][i].index,doc.features[s][i].value);
						i++;
					}

					for (Feature f : structFeatures){
						newX[i] = new FeatureNode(f.index,f.value);
						i++;
					}
				}else{
					newX = new FeatureNode[doc.features[s].length];
					for (int i = 0; i < newX.length; i++){
						newX[i] = new FeatureNode(doc.features[s][i].index,doc.features[s][i].value);
					}
				}

				Linear.predictProbability(predictModel,newX,probs);
				for (int l = 0; l < modelLabels.length; l++){
					double score = entry.first() + Math.log(probs[l]);
					if (temp.isEmpty() || score > temp.lastKey()){
						int[] newSequence = new int[s+1];
						System.arraycopy(entry.second(), 0, newSequence, 0, s);
						newSequence[s] = modelLabels[l];
						temp.put(score, newSequence);
					}
				}
			}


			beam = temp;
		}

		return beam.lastValue();
	}

	private int[] predictDocViterbi(AZDoc doc, Model predictModel){	
		//	FeatureNode[][] features = new FeatureNode[doc.features.length][];		
		int[] predictions = new int[doc.features.length];
		int[] modelLabels = predictModel.getLabels();

		if (extractor.usingStructuralFeatures() == false){
			for (int s = 0; s < doc.features.length; s++){
				FeatureNode[] features = new FeatureNode[doc.features[s].length];
				for (int x = 0; x < features.length; x++){
					features[x] = new FeatureNode(doc.features[s][x].index,doc.features[s][x].value);
				}

				predictions[s] = Linear.predict(predictModel, features);
			}
		}else{
			double[][] delta = new double[doc.features.length][modelLabels.length];
			int[][] phi = new int[doc.features.length][modelLabels.length];

			// Forward
			for (int s = 0; s < doc.features.length; s++){
				FeatureNode[] baseFeatures = new FeatureNode[doc.features[s].length];
				for (int i = 0; i < baseFeatures.length; i++){
					baseFeatures[i] = new FeatureNode(doc.features[s][i].index,doc.features[s][i].value);
				}


				for (int k = 0; k < modelLabels.length; k++){
					Feature[] structFeatures = extractor.getLocalStructFeatures(doc.features[s], predictions, s, doc.path);
					FeatureNode[] newX = new FeatureNode[baseFeatures.length + structFeatures.length];
					
					System.arraycopy(baseFeatures,0,newX,0,baseFeatures.length);
					int i = baseFeatures.length;

					for (Feature f : structFeatures){
						newX[i] = new FeatureNode(f.index,f.value);
						i++;
					}

					double[] scores = new double[labelList.size()];

					Linear.predictProbability(predictModel, newX, scores);
					for (int l = 0; l < modelLabels.length; l++){
						double de;
						if (s == 0){
							de = scores[l];
						}else{
							de = scores[l]*delta[s-1][k];
						}
						if (de > delta[s][l]){
							delta[s][l] = de;
							phi[s][l] = k;
						}
					}
				}						
			}

			double last = 0;
			for (int l = 0; l < modelLabels.length; l++){
				if (delta[doc.features.length-1][l] > last){
					last = delta[doc.features.length-1][l];
					predictions[doc.features.length-1] = l;
				}
			}

			// Backward
			for (int s = doc.features.length-2; s >= 0; s--){
				predictions[s] = phi[s+1][predictions[s+1]];
			}

			for (int s = 0; s < doc.features.length; s++){
				predictions[s] = modelLabels[predictions[s]];
			}
		}

		return predictions;
	}

	@Override
	public int[][] crossValidate(AZProblem azProb, Set<Integer>[] folds) {
		AZDoc[] docs = azProb.docs;
		int[][] predictions = new int[docs.length][];

		for (int f = 0; f < folds.length; f++){
			ArrayList<Integer> idxList = new ArrayList<Integer>();
			ArrayList<AZDoc> testDocs = new ArrayList<AZDoc> ();
			Problem prob = new Problem();

			prob.l = 0;
			prob.n = extractor.lastFeature();

			for (int d = 0; d < docs.length; d++){
				if (folds[f].contains(d) == false){
					prob.l += docs[d].labels.length;
				}
			}

			prob.x = new FeatureNode[prob.l][];
			prob.y = new int[prob.l];				

			int i = 0;

			//			System.out.print("Fold " + f + ": ");
			for (int d = 0; d < docs.length; d++){
				if (folds[f].contains(d)){
					idxList.add(d);
					testDocs.add(docs[d]);
					//		System.out.print(docs[d].path + ", ");
				}else{
					AZDoc doc = docs[d];

					for (int s = 0; s < docs[d].labels.length; s++){
						prob.y[i] = doc.labels[s];			
						assert prob.y[i] > 0;

						if (extractor.usingStructuralFeatures()){
							TreeMap<Integer,Double> sFeatures = extractor.getNewLocalFeatures(doc.features,doc.labels,s,doc.path);
							prob.x[i] = new FeatureNode[doc.features[s].length + sFeatures.size()];
							int c = doc.features[s].length;
							for (int fe = 0; fe < c; fe++){
								prob.x[i][fe] = new FeatureNode(doc.features[s][fe].index,doc.features[s][fe].value);
							}

							for(Map.Entry<Integer, Double> entry : sFeatures.entrySet()){
								prob.x[i][c] = new FeatureNode(entry.getKey(),entry.getValue());
								c++;
							}
						}else{
							prob.x[i] = new FeatureNode[doc.features[s].length];
							for (int fe = 0; fe < prob.x[i].length; fe++){
								prob.x[i][fe] = new FeatureNode(doc.features[s][fe].index,doc.features[s][fe].value);
							}
						}

						i++;
					}
				}
			}
			//		System.out.println();

			prob.n = extractor.lastFeature();
			setWeights(prob.y);

			Model cvModel = Linear.train(prob, param);

			for (int d = 0; d < idxList.size(); d++){
				predictions[idxList.get(d)] = predictDoc(testDocs.get(d),cvModel);
			}
		}

		return predictions;			
	}

	@Override
	public int[][] crossValidateAndOptimise(AZProblem azProb, Set<Integer>[] folds) {
		// TODO Auto-generated method stub
		return null;
	}

	private void setWeights(int[] y){
		int[] labelCounts = new int[labelList.size()];
		HashSet<Integer> nonZeroSet = new HashSet<Integer>();
		for (int label : y){
			labelCounts[label]++;
			nonZeroSet.add(label);
		}

		double[] weights = new double[nonZeroSet.size()];
		int[] weightLabels = new int[nonZeroSet.size()];
		int count = 0;
		
		for (int l : nonZeroSet){
			weightLabels[count] = l;
			weights[count] = y.length/((double)labelCounts[l]);
			count++;
		}
		
		param.setWeights(weights, weightLabels);
	}
}
