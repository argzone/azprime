package uk.ac.cam.cl.azprime.postprocess;

import java.io.*;
import java.util.*;
import java.util.regex.Pattern;

public class SVMFeatureFile {
	Pattern prePattern = Pattern.compile("^['\"]*(.+)");
	Pattern postPattern = Pattern.compile("(.+)[?.!'\";:]");
	
	public static void main (String[] args){
		File in = null;
		File out = null;
		HashSet<String> inclusion = null;
		int ngram = 1;
		boolean stop = false;

		for (int i = 0; i < args.length; i+=2){
			if (args[i].equals("--in")){
				in = new File(args[i+1]);
			}else if (args[i].equals("--out")){
				out = new File(args[i+1]);
			}else if (args[i].equals("--inclusion")){
				inclusion = new HashSet<String>(Arrays.asList(args[i+1].split(",")));
			}else if (args[i].equals("--ngram")){
				ngram = Integer.parseInt(args[i+1]);
			}else if (args[i].equals("--stop")){
				stop = Boolean.parseBoolean(args[i+1]);
			}else{
				System.exit(9);
			}
		}

		assert in != null;
		assert out != null;

		SVMFeatureFile ff = new SVMFeatureFile();

		try{
			ff.process(in, out, inclusion, ngram, stop);	
		}catch(IOException e){
			e.printStackTrace();
			System.exit(9);
		}
	}

	public void process(File in, File out, Set<String> inclusion, int ngram, boolean stop) throws IOException{
		ArrayList<HashMap<String,Integer>> maps = new ArrayList<HashMap<String,Integer>>();

		BufferedReader reader = new BufferedReader(new FileReader(in));
		HashSet<Integer> isNumeric = new HashSet<Integer>();
		HashSet<Integer> isString = new HashSet<Integer>();
		int labelIdx = -1;
		ArrayList<String> attributes = new ArrayList<String>();
		ArrayList<HashMap<String,HashMap<Integer,Integer>>> instances = new ArrayList<HashMap<String,HashMap<Integer,Integer>>>();
		
		Set<String> stopList = new HashSet<String>();
		if (stop){
			BufferedReader stopReader = new BufferedReader(new FileReader(new File("/anfs/bigdisc/do242/NGram/black.txt")));
			
			while (true){
				String line = stopReader.readLine();
				if (line == null){
					stopReader.close();
					break;
				}
				stopList.add(line);
			}
		}

		while(true){
			String line = reader.readLine();
			if (line == null){
				reader.close();
				break;
			}

			if (line.startsWith("@data") || line.startsWith("@relation") || line.matches("\\s*")){
				continue;
			}

			if (line.startsWith("@attribute")){
				HashMap<String,Integer> map = new HashMap<String,Integer>();

				String[] lineSplit = line.split(" ");

				if (lineSplit[1].equals("SentenceCategory")) labelIdx = maps.size();
				attributes.add(lineSplit[1]);
				
				String features = lineSplit[2];

				if(features.startsWith("{")){
					String[] split = features.split(",");				
					split[0] = split[0].substring(1);
					split[split.length-1] = split[split.length-1].substring(0,split[split.length-1].length()-1);

					for (int i = 0; i < split.length; i++){
						map.put(split[i], i+1);
					}
				}else if (features.equals("numeric")){
					map.put("numeric", 1);
					isNumeric.add(maps.size());
				}else if (features.equals("string")){
					isString.add(maps.size());
				}

				maps.add(map);
				continue;
			}

			String[] split = line.split(",");
			assert split.length == maps.size();

			HashMap<String,HashMap<Integer,Integer>> instance = new HashMap<String,HashMap<Integer,Integer>>();
			
			for (int i = 0; i < split.length; i++){
				if (maps.get(i).containsKey(split[i])){
					HashMap<Integer,Integer> one = new HashMap<Integer,Integer>();
					one.put(maps.get(i).get(split[i]), 1);
					instance.put(attributes.get(i),one);
				}else if (isNumeric.contains(i)){
					HashMap<Integer,Integer> one = new HashMap<Integer,Integer>();
					one.put(maps.get(i).get("numeric"), (int)Double.parseDouble(split[i]));					
				}else if (isString.contains(i)){
					instance.put(attributes.get(i),new HashMap<Integer,Integer>());
					processString(split[i],instance.get(attributes.get(i)),maps.get(i),ngram,stopList);
				}
			}			
			
			instances.add(instance);
		}
		
		int[] offsets = new int[maps.size()];
		for (int i = 0; i < offsets.length-1; i++){
			System.out.println(attributes.get(i) + ": " + maps.get(i).size() + " features");
			offsets[i+1] = offsets[i] + maps.get(i).size();
		}
		System.out.println(attributes.get(maps.size()-1) + ": " + maps.get(maps.size()-1).size() + " features");
		
		PrintWriter writer = new PrintWriter(new BufferedWriter(new FileWriter(out)));
		
		for (int i = 0; i < instances.size(); i++){
			writer.print(instances.get(i).get("SentenceCategory").keySet().iterator().next());
			
			for (int f = 0; f < maps.size(); f++){
				if (f == labelIdx) continue;
				String feat = attributes.get(f);
				if (inclusion != null && inclusion.contains(feat) == false) continue;
				
				if (instances.get(i).containsKey(feat) == false) continue;
				
				TreeMap<Integer,Integer> ordered = new TreeMap<Integer,Integer>(instances.get(i).get(feat));
				
				for (int idx : ordered.keySet()){
					writer.print(" " + (idx+offsets[f]) + ":" + ordered.get(idx));
				}
			}
			
			writer.println();
		}
		
		System.out.println("Wrote " + instances.size() + " instances");
		writer.close();
	}
	
	private void processString(String str, HashMap<Integer,Integer> instance, HashMap<String,Integer> map, int ngram, Set<String> stopList){
		int start = 0;
		while(start < str.length()){
			char pre = str.charAt(start);
			if (pre == '"' || pre == '\''){
				start++;
				continue;
			}
			break;
		}
		

		int end = str.length();
		
		while(end > start){
			char post = str.charAt(end-1);
			if (post == '"' || post == '\'' || post == '.' || post == '?' || post == '!' || post == '-' ){
				end--;
				continue;
			}
			break;
		}
		
		str = str.substring(start,end);
		
		if (str.length() <= 0) return;
		
		String[] words = str.split("\\s+");
		
		for (int i = 0; i < words.length; i++){
			for (int n = 0; n < ngram; n++){
				if (i - n < 0) continue;
				StringBuilder sb = new StringBuilder();
				for (int w = i - n; w <= i; w++){
					if (stopList.contains(words[w])) continue;
					sb.append(words[w] + ":");
				}
				
				String feat = sb.toString();
				if (map.containsKey(feat) == false){
					map.put(feat,map.size());
				}
				
				instance.put(map.get(feat),1);	
			}
		}
		
/*		for (String w : words){
			if (map.containsKey(w) == false){
				map.put(w,map.size());
			}
			
			instance.put(map.get(w),1);			
		}*/		
	}
}
