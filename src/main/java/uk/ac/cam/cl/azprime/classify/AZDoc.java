package uk.ac.cam.cl.azprime.classify;

import java.util.HashSet;
import java.util.List;

import perceptron.struct.Feature;

public class AZDoc {
	public int[] labels;
	public Feature[][] features;
	public String path;
	public HashSet<Integer>[] citationLinks;
	public int abstractLength;

	public String getFeatureString(List<String> featureList){
		StringBuilder sb = new StringBuilder();
		for (int s = 0; s < features.length; s++){
			for (Feature n : features[s]){
				sb.append(featureList.get(n.index) + " ");
			}				
			sb.append(labels[s] + "\n");
		}
		return sb.toString();
	}
	
	public String getFeatureString(AZFeatureExtractor extractor){
		StringBuilder sb = new StringBuilder();
		for (int s = 0; s < features.length; s++){
			for (Feature n : features[s]){
				sb.append(extractor.getFeatureName(n.index) + " ");
			}				
			sb.append(labels[s] + "\n");
		}
		return sb.toString();
	}
	
	
}
