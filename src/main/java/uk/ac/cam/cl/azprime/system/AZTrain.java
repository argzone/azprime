package uk.ac.cam.cl.azprime.system;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.text.DecimalFormat;
import java.util.*;

import uk.ac.cam.cl.azprime.AZWrapper;
import uk.ac.cam.cl.azprime.config.Configuration;
import uk.ac.cam.cl.azprime.cv.AZEvaluator;
import uk.ac.cam.cl.azprime.ml.ExtractedFeature;
import uk.ac.cam.cl.azprime.xml.ParsedSciXml.Document;
import uk.ac.cam.cl.azprime.xml.SciXmlReader;
import uk.ac.cam.cl.azprime.xml.SciXmlWriter;
//import uk.ac.cam.cl.azprime.xml.SentenceTagger;

import de.bwaldvogel.liblinear.*;
import edu.stanford.nlp.tagger.maxent.MaxentTagger;

public class AZTrain {
	private HashMap<String,Integer> featureMap;
	private ArrayList<String> labelList;
	private Model model;
	private AZWrapper wrapper;
	private Map<String,Double> aveMap;
	private Map<String,Double> binMap;
	private ArrayList<String> featureList;
	private HashMap<String,String> labelMapping;
	private HashMap<String,Integer> labelMap;

	public static void main(String[] args){
		File[] train = null;
		File[] test = null;
		boolean cv = false;
		File modelDir = null;
		HashSet<String> ignore = new HashSet<String>();

		for (int i = 0; i < args.length; i+=2){
			if (args[i].equals("--train")){
				String[] temp = args[i+1].split(",");
				train = new File[temp.length];
				for (int f = 0; f < temp.length; f++){
					train[f] = new File(temp[f]);
					assert train[f].exists();
				}
			}else if (args[i].equals("--test")){
				String[] temp = args[i+1].split(",");
				test = new File[temp.length];
				for (int f = 0; f < temp.length; f++){
					test[f] = new File(temp[f]);
					assert test[f].exists();
				}
			}else if (args[i].equals("--cv")){
				cv = Boolean.parseBoolean(args[i+1]);
			}else if (args[i].equals("--modeldir")){
				modelDir = new File(args[i+1]);
				assert modelDir.isDirectory();
			}else if (args[i].equals("--ignore")){
				ignore.addAll(Arrays.asList(args[i+1].split(",")));
			}else{
				System.err.println("Unknown argument " + args[i]);
			}
		}

		assert train != null || test != null : "Train or test must be non-null";

		new AZTrain(cv,train,test,modelDir,ignore);
	}

	private void initMappings(){
		labelMapping = new HashMap<String,String>();
		labelMapping.put("TXT", "OWN");
		labelMapping.put("COGRO", "BKG");
		labelMapping.put("OTHR","OTH");
		labelMapping.put("PREV_OWN", "OTH");
		labelMapping.put("USE","BAS");
		labelMapping.put("SUPPORT","BAS");
		labelMapping.put("CODI","CTR");
		labelMapping.put("ANTISUPP", "CTR");
		labelMapping.put("GAP_WEAK","CTR");
		labelMapping.put("OWN_MTHD", "OWN");
		labelMapping.put("OWN_RES","OWN");
		labelMapping.put("OWN_CONC","OWN");
		labelMapping.put("OWN_FAIL","OWN");
		labelMapping.put("FUT","AIM");
	//	labelMapping.put("NOV_ADV","AIM");
		labelMapping.put("NOV_ADV","OWN");
		labelMapping.put("OTH_GOOD","OTH");

		labelMap = new HashMap<String,Integer>();
		labelList = new ArrayList<String>();

		for (String label : new String[]{"OWN","BKG","OTH","BAS","CTR","AIM"}){
			labelMap.put(label,labelList.size());
			labelList.add(label);
		}
	}

	/*
	public AZTrain(boolean cv){
		this(cv,new File[]{new File("/home/do242/Work/azprime/data/chemistry"),new File("/home/do242/Work/azprime/data/cl")},new File[0]);
	}

	 */

	public AZTrain (boolean cv, File[] trainingDirectories, File[] testDirectories, File modelDir, Set<String> ignore){
		Configuration config = Configuration.load("classpath:/cfg/config.scala");
		if (modelDir == null) modelDir = new File(getClass().getResource("/models").getPath());
		wrapper = new AZWrapper(config);		
		if (trainingDirectories == null && testDirectories == null){
			trainingDirectories = new File[]{new File(config.files().data() + "/cl2"),new File(config.files().data() + "/cl"),new File(config.files().data() + "/chemistry")};
			testDirectories = new File[0];
		}
		//	File[] trainingDirectories = new File[]{new File("/home/do242/Work/azprime/data/chemistry"),new File("/home/do242/Work/azprime/data/cl")};
		//	File[] trainingDirectories = new File[]{new File("/home/do242/Work/azprime/data/chemistry")};

		initMappings();

		if (trainingDirectories != null){
			Problem prob = readData(collectFiles(trainingDirectories),true,ignore);
			Parameter param = initParams(prob.y);

			System.out.print("Read labels");
			for (int l = 0; l < labelList.size(); l++) System.out.print(" " + (l+1) + ":" + labelList.get(l));
			System.out.println();		

			System.out.println("Read " + prob.l + " sentences, " + featureMap.size() + " features");

			Linear.disableDebugOutput();

			if (cv){
				int[] predictions = new int[prob.l];
				Linear.crossValidation(prob, param, 10, predictions);
				evaluate(predictions,prob.y,true);
			}

			model = Linear.train(prob, param);

			printWeights(model);

			if (modelDir != null){
				try{
					writeObjects(modelDir);
				}catch(IOException e){
					e.printStackTrace();
					System.exit(9);
				}
			}
		}else{
			try{
				readObjects(modelDir);
			}catch(Exception e){
				e.printStackTrace();
				System.exit(9);
			}
		}

		if (testDirectories != null){
			AZClassifier classifier = null;

			try{
				classifier = new AZClassifier("classpath:/cfg/config.scala",modelDir.getPath());
			}catch(Exception e){
				e.printStackTrace();
				System.exit(9);
			}

			for (File dir : testDirectories){
				predict(dir,classifier,ignore);
			}
		}
	}

	private void readObjects(File dir) throws IOException, ClassNotFoundException{
		ObjectInputStream ois = new ObjectInputStream(new FileInputStream(dir.getPath() + "/az.featuremap.bin"));
		featureMap = (HashMap<String,Integer>) ois.readObject();
		ois.close();
		System.out.println("Read " + featureMap.size() + " features");
		String[] temp = new String[featureMap.size()+1];
		for (String f : featureMap.keySet()){
			temp[featureMap.get(f)] = f;
		}
		featureList = new ArrayList<String>(Arrays.asList(temp));

		ois = new ObjectInputStream(new FileInputStream(dir.getPath() + "/az.labellist.bin"));
		labelList = (ArrayList<String>) ois.readObject();
		ois.close();
		labelMap = new HashMap<String,Integer>();
		for (int i = 0; i < labelList.size(); i++){
			labelMap.put(labelList.get(i),i);
		}
		ois = new ObjectInputStream(new FileInputStream(dir.getPath() + "/az.avemap.bin"));
		aveMap = (Map<String,Double>) ois.readObject();
		ois.close();
		ois = new ObjectInputStream(new FileInputStream(dir.getPath() + "/az.binmap.bin"));
		binMap = (Map<String,Double>) ois.readObject();
		ois.close();
	}


	private void predict(File dir, AZClassifier classifier,Set<String> ignore){
		File[] files = dir.listFiles(new FilenameFilter(){
			@Override
			public boolean accept(File dir, String name) {
				return name.endsWith("pos.xml");
			}			
		});

		int[][] predictions = new int[files.length][];
		int[][] gold = new int[files.length][];

		for (int i = 0; i < files.length; i++){
			Problem prob = readData(new File[]{files[i]},false,ignore);
			predictions[i] = classifier.predict(prob.x);

			gold[i] = new int[prob.l];
			for (int s = 0; s < prob.l; s++){
				gold[i][s] = prob.y[s]-1;
			}
		}		

		AZEvaluator.evaluate(gold, predictions, labelList, true);
	}

	private void printWeights(Model model){
		double[] weights = model.getFeatureWeights();
		//	CapacityMap<Double,Integer>[] topWeights = new CapacityMap[model.getNrClass()];
		int numLabels = model.getNrClass();
		int[] mLabels = model.getLabels();

		HashSet<String> zeroWeights = new HashSet<String>(featureMap.keySet());

		for (int l = 0; l < numLabels; l++){
			//	topWeights[l] = new CapacityMap<Double,Integer>(20);
			CapacityMap<Double,Integer> topWeights = new CapacityMap<Double,Integer>(50);
			for (int idx = 1; idx < featureList.size(); idx++){
				double w = weights[(idx-1)*numLabels + l];
				topWeights.put(w, idx);
				if (w != 0){
					zeroWeights.remove(featureList.get(idx));
				}
			}

			System.out.println("Top weights for " + labelList.get(mLabels[l]-1) + ":");
			for (Pair<Double,Integer> p : topWeights.getEntries()){
				System.out.println(featureList.get(p.second()) + "\t" + p.first());				
			}
			System.out.println();
		}

		System.out.println(zeroWeights.size() + " feature weights were zero for all labels");
	}

	private File[] collectFiles(File[] dirs){
		ArrayList<File> files = new ArrayList<File>();
		//SentenceTagger tagger = null;

		for (File dir : dirs){
			File[] contents = dir.listFiles();
			for (File f : contents){
				if (f.getName().endsWith("pos.xml")){
					files.add(f);
//				}else if (f.getName().endsWith("simone.xml")){
//					String name = f.getName();
//					String posFileName = name.substring(0,name.indexOf("simone.xml")) + "pos.xml";
//					File posFile = new File(dir,posFileName);
//					if (posFile.exists()) continue;
//
//					System.out.println("Adding POS tags to " + f.getPath());
//
//					Document doc = new SciXmlReader(f).parse();
//					if (tagger == null){
//						try{
//							tagger = new SentenceTagger(new MaxentTagger("/anfs/bigdisc/do242/Java/stanford-corenlp-2011-09-16/edu/stanford/nlp/models/pos-tagger/wsj3t0-18-bidirectional/bidirectional-distsim-wsj-0-18.tagger"));
//						}catch(Exception e){
//							e.printStackTrace();
//							System.exit(9);
//						}
//					}
//
//					doc = tagger.addPosTags(doc);
//					new SciXmlWriter(doc,true).write(posFile);
				}
			}
		}				

		return files.toArray(new File[files.size()]);
	}

	private Problem readData(File[] files, boolean addFeatures, Set<String> more2Ignore){
		if (addFeatures){
			aveMap = new HashMap<String,Double>();
			binMap = new HashMap<String,Double>();
			featureMap = new HashMap<String,Integer>();
			featureList = new ArrayList<String>();
			featureList.add("");
		}
		ArrayList<Integer> yList = new ArrayList<Integer>();

		HashSet<String> numerics = new HashSet<String>(Arrays.asList(new String[]{"TFI","LEN","TIT"}));
		HashSet<String> mustSplit = new HashSet<String>(Arrays.asList(new String[]{"SemanticCuePhrase"}));
		HashSet<String> ignore = new HashSet<String>(Arrays.asList(new String[]{"ID","SEMVERBS"}));
		ignore.addAll(more2Ignore);
		HashSet<String> lookbackTypes = new HashSet<String>(Arrays.asList(new String[]{"SemanticCuePhrase","SVPATTERNS","SVVOICE","SVTENSE","SVACT"}));
		HashMap<String,Double> max = new HashMap<String,Double>();
		for (String type : numerics){
			max.put(type,Double.NEGATIVE_INFINITY);
			max.put("Log" + type,Double.NEGATIVE_INFINITY);
		}
		HashMap<String,Double> totals = new HashMap<String,Double>();
		for (String type : numerics){
			totals.put(type,0.0);
			totals.put("Log" + type,0.0);
		}

		ArrayList<HashMap<String,Double>> numericFeatures = new ArrayList<HashMap<String,Double>>();
		ArrayList<TreeSet<Integer>> stringFeatures = new ArrayList<TreeSet<Integer>>();

		for (File f : files){
			Document doc = new SciXmlReader(f).parse();
			//if (doc.containsPOS() == false){
			//	if (tagger == null){
			//		tagger = new MaxentTagger("/anfs/bigdisc/do242/Java/stanford-corenlp-2011-09-16/edu/stanford/nlp/models/pos-tagger/wsj3t0-18-bidirectional");
			//	}
			//	
			//	doc = doc.tagForPOS(tagger);
			//}
			ExtractedFeature[][] extractedFeatures = wrapper.getFeatures(doc);
			HashSet<String> lookbackSet = new HashSet<String>();
			int lastSen = -1;

			LOOP: for (int s = 0; s < extractedFeatures.length; s++){
				TreeSet<Integer> stringSet = new TreeSet<Integer>();
				HashMap<String,Double> numericMap = new HashMap<String,Double>();
				int y = -1;				

				for (String feat : lookbackSet){
					if (featureMap.containsKey(feat) == false){
						featureMap.put(feat,featureMap.size()+1);
						featureList.add(feat);
					}

					stringSet.add(featureMap.get(feat));					
				}

				lookbackSet = new HashSet<String>();

				for (ExtractedFeature ef : extractedFeatures[s]){
					String[] components = ef.getComponents();
					String type = components[0];
					//		System.out.println("Extracted feature " + type + ":" + components[1]);
					if (ignore.contains(type)) continue;		

					if (numerics.contains(type)){
						double val = Double.parseDouble(components[1]);

						if (addFeatures){
							if (val > max.get(type)) max.put(type,val);
							totals.put(type, totals.get(type) + val);
						}

						numericMap.put(type, val);

						if (val > 0){
							String logType = "Log" + type;

							if (addFeatures){
								if (val > max.get(logType)) max.put(logType,Math.log(val));
								totals.put(logType, totals.get(logType) + Math.log(val));
							}

							numericMap.put(logType, Math.log(val));
						}
					}else if (type.equals("SEN")){
						Set<String> ngrams = processString(components[1],3);
						for (String ng : ngrams){
							String feat = type + ":" + ng;
							if (featureMap.containsKey(feat) == false){
								featureMap.put(feat,featureMap.size()+1);
								featureList.add(feat);
							}
							stringSet.add(featureMap.get(feat));
						}
					}else if (type.equals("AZ")){
						String label = components[1];
						if (labelMapping.containsKey(label)){
							label = labelMapping.get(label);
						}

						if (label.equals("")){
							if (addFeatures){
								continue LOOP;
							}else{
								y = -1;
							}
						}else{
							y = labelMap.get(label);
						}
					}else if (mustSplit.contains(type)){
						for (String sp : components[1].split("!")){
							String feat = type + ":" + sp;
							if (featureMap.containsKey(feat) == false){
								featureMap.put(feat,featureMap.size()+1);
								featureList.add(feat);
							}
							stringSet.add(featureMap.get(feat));
							if (lookbackTypes.contains(type)){
								lookbackSet.add("Lookback:" + feat);
							}
						}
					}else{
						String feat = type + ":" + components[1];
						if (featureMap.containsKey(feat) == false){
							featureMap.put(feat,featureMap.size()+1);
							featureList.add(feat);
						}
						stringSet.add(featureMap.get(feat));
						if (lookbackTypes.contains(type)){
							lookbackSet.add("Lookback:" + feat);
						}
					}
				}

				if (s == 0){
					String feat = "History:B";
					if (featureMap.containsKey(feat) == false){
						featureMap.put(feat,featureMap.size()+1);
						featureList.add(feat);
					}
					stringSet.add(featureMap.get(feat));
				}else{
					String feat = "History:" + yList.get(lastSen);
					if (featureMap.containsKey(feat) == false){
						featureMap.put(feat,featureMap.size()+1);
						featureList.add(feat);
					}
					stringSet.add(featureMap.get(feat));
				}

				/*
				FeatureNode[] x = new FeatureNode[orderedFeatures.size()];

				int i = 0;
				for (int idx : orderedFeatures.keySet()){
					x[i] = new FeatureNode(idx,orderedFeatures.get(idx));
					i++;
				}
				 */

				//	assert y > 0 : y;
				//xList.add(x);
				yList.add(y+1);
				stringFeatures.add(stringSet);
				numericFeatures.add(numericMap);

				lastSen = yList.size()-1;
			}			
		}		

		for (int s = 0; s < yList.size(); s++){
			for (String type : numericFeatures.get(s).keySet()){
				if (addFeatures){
					if (aveMap.containsKey(type) == false){
						double ave = totals.get(type)/yList.size();
						aveMap.put(type, ave);
						System.out.println("Average for " + type + ": " + ave);
					}

					if (binMap.containsKey(type) == false){
						double binSize = max.get(type)/10;
						binMap.put(type,binSize);
						System.out.println("Bin size for " + type + ": " + binSize);
					}
				}


				if (numericFeatures.get(s).get(type) > aveMap.get(type)){
					String feat = type;
					if (featureMap.containsKey(feat) == false){
						featureMap.put(feat,featureMap.size()+1);
						featureList.add(feat);
					}
					stringFeatures.get(s).add(featureMap.get(feat));
				}

				int bin = (int) (numericFeatures.get(s).get(type)/binMap.get(type));
				String feat = type + "Bin:" + bin;
				if (featureMap.containsKey(feat) == false){
					featureMap.put(feat,featureMap.size()+1);
					featureList.add(feat);
				}
				stringFeatures.get(s).add(featureMap.get(feat));
			}
		}

		Problem prob = new Problem();

		prob.y = new int[yList.size()];
		prob.x = new FeatureNode[yList.size()][];
		prob.l = yList.size();
		prob.n = featureMap.size();

		for (int s = 0; s < prob.l; s++){
			prob.y[s] = yList.get(s);
			prob.x[s] = new FeatureNode[stringFeatures.get(s).size()];
			int i = 0;
			for (int idx : stringFeatures.get(s)){
				prob.x[s][i] = new FeatureNode(idx,1.0);
				i++;
			}
		}

		return prob;
	}


	private double[][] evaluate(int[] predictions, int[] gold, boolean print){
		/*
		 * Per-sentence accuracy
		 * Per-document accuracy
		 * Precision per label and average
		 * Recall per label and average
		 * F-score per label and average
		 */

		int numLabels = labelList.size()-1;
		DecimalFormat df = new DecimalFormat("#.###");

		int[] posGold = new int[numLabels];
		int[] posPredicted = new int[numLabels];
		int[] posCorrect = new int[numLabels];
		int correct = 0;
		int[][] confusion = new int[numLabels][numLabels];

		double[][] scores = new double[numLabels+2][];
		scores[0] = new double[2];
		scores[scores.length-1] = new double[3];
		for (int l = 0; l < numLabels; l++){
			scores[l+1] = new double[3];
		}

		for (int s = 0; s < gold.length; s++){				
			if (gold[s] == predictions[s]){
				correct++;
				posCorrect[gold[s]-1]++;
			}

			posGold[gold[s]-1]++;
			posPredicted[predictions[s]-1]++;
			confusion[gold[s]-1][predictions[s]-1]++;
		}


		scores[0][0] = ((double)correct)/gold.length;

		double fSum = 0;
		double pSum = 0; 
		double rSum = 0;

		for (int l = 0; l < numLabels; l++){
			if (posPredicted[l] > 0){
				scores[l+1][0] = ((double)posCorrect[l])/posPredicted[l];
				pSum += scores[l+1][0];
			}

			if (posGold[l] > 0){
				// this should always be true
				scores[l+1][1] = ((double)posCorrect[l])/posGold[l];
				rSum += scores[l+1][1];
			}

			if (scores[l+1][0] > 0 || scores[l+1][1] > 0){
				scores[l+1][2] = 2*scores[l+1][0]*scores[l+1][1]/(scores[l+1][0] + scores[l+1][1]);
				fSum += scores[l+1][2];
			}			
		}

		scores[numLabels+1][0] = pSum/numLabels;
		scores[numLabels+1][1] = rSum/numLabels;
		scores[numLabels+1][2] = fSum/numLabels;

		if (print){
			System.out.println("Per-sentence accuracy\t" + df.format(scores[0][0]) + "(" + correct + "/" + gold.length + ")");

			System.out.println("\n\n\tP\tR\tF");
			for (int l = 0; l < numLabels; l++){
				System.out.print(labelList.get(l+1) + "\t");
				System.out.print(df.format(scores[l+1][0]) + "\t" + df.format(scores[l+1][1]) + "\t" + df.format(scores[l+1][2])  + "\t" + posPredicted[l]  + "/" + posGold[l]);
				System.out.println();
			}
			System.out.println("\nAverage\t" + df.format(scores[numLabels+1][0]) + "\t" + df.format(scores[numLabels+1][1]) + "\t" + df.format(scores[numLabels+1][2]) + "\n\n");


			// Print confusion matrix
			for (int i = 0; i < numLabels; i++){
				System.out.print("\t" + labelList.get(i));
			}
			System.out.println();

			for (int l = 0; l < numLabels; l++){
				System.out.print(labelList.get(l+1));
				for (int m = 0; m < numLabels; m++){
					System.out.print("\t" + confusion[l][m]);
				}
				System.out.println();
			}
		}

		return scores;
	}


	private Parameter initParams(int[] y){
		Parameter param = new Parameter(SolverType.L1R_LR,1.0, Double.POSITIVE_INFINITY);
		param.setEps(0.01);

		int[] labelCounts = new int[labelList.size()+1];
		for (int label : y){
			labelCounts[label]++;
		}

		double[] weights = new double[labelList.size()-1];
		int[] weightLabels = new int[labelList.size()-1];
		for (int l = 1; l < labelCounts.length-1; l++){
			weightLabels[l-1] = l;
			weights[l-1] = y.length/((double)labelCounts[l]);
		}
		param.setWeights(weights, weightLabels);

		return param;
	}

	private HashSet<String> processString(String str, int ngram){
		//	Set<String> ignore = new HashSet<String>(Arrays.asList(new String[]{"-LRB-","-RRB-","-RRB"}));
		int start = 0;

		str = str.replaceAll("-[LR]RB-", "").replaceAll("\\d", "0");

		while(start < str.length()){
			char pre = str.charAt(start);
			if (pre == '"' || pre == '\''){
				start++;
				continue;
			}
			break;
		}


		int end = str.length();

		while(end > start){
			char post = str.charAt(end-1);
			if (post == '"' || post == '\'' || post == '.' || post == '?' || post == '!' || post == '-' ){
				end--;
				continue;
			}
			break;
		}

		str = str.substring(start,end);

		HashSet<String> set = new HashSet<String>();

		if (str.length() <= 0) return set;

		String[] words = str.split("\\s+");

		for (int i = 0; i < words.length; i++){			
			for (int n = 0; n < ngram; n++){
				if (i - n < 0) continue;
				StringBuilder sb = new StringBuilder();
				for (int w = i - n; w <= i; w++){
					sb.append(words[w] + ":");
				}

				String feat = sb.toString();
				set.add(feat);
			}
		}

		return set;
	}

	private void writeObjects(File dir) throws IOException{
		model.save(new File(dir,"az.model"));
		ObjectOutputStream ois = new ObjectOutputStream(new FileOutputStream(dir.getPath() + "/az.featuremap.bin"));
		ois.writeObject(featureMap);
		ois.close();
		ois = new ObjectOutputStream(new FileOutputStream(dir.getPath() + "/az.labellist.bin"));
		ois.writeObject(labelList);
		ois.close();
		ois = new ObjectOutputStream(new FileOutputStream(dir.getPath() + "/az.avemap.bin"));
		ois.writeObject(aveMap);
		ois.close();
		ois = new ObjectOutputStream(new FileOutputStream(dir.getPath() + "/az.binmap.bin"));
		ois.writeObject(binMap);
		ois.close();
	}

	private class CapacityMap<K extends Comparable<K>,V>{
		private int capacity;
		private TreeMap<K,HashSet<V>> map;
		private int size = 0;

		public CapacityMap(int c){
			capacity = c;
			assert capacity > 0;
			map = new TreeMap<K,HashSet<V>>();
		}

		public HashSet<V> get(K key){
			if (map.containsKey(key)){
				return map.get(key);
			}else{
				return null;
			}
		}

		public void addAll(Map<V,K> map){
			for (V val : map.keySet()){
				K key = map.get(val);
				put(key,val);
			}
		}

		public void put(K key, V val){
			if (size < capacity){
				if (map.containsKey(key)){
					map.get(key).add(val);
				}else{
					HashSet<V> set = new HashSet<V>();
					set.add(val);
					map.put(key,set);
				}
				size++;
			}else if (key.compareTo(map.firstKey()) > 0){
				HashSet<V> hs = map.get(map.firstKey());
				hs.remove(hs.iterator().next());
				if (hs.isEmpty()) map.remove(map.firstKey());
				if (map.containsKey(key) == false){
					map.put(key, new HashSet<V>());
				}
				map.get(key).add(val);
			}
		}

		public ArrayList<V> getValues(){
			ArrayList<V> list = new ArrayList<V>();
			for (K key : map.descendingKeySet()){ 
				list.addAll(map.get(key));
			}

			return list;
		}

		public TreeMap<K,HashSet<V>> getMap(){
			return new TreeMap<K,HashSet<V>>(map);
		}

		public ArrayList<Pair<K,V>> getEntries(){
			ArrayList<Pair<K,V>> list = new ArrayList<Pair<K,V>>();
			for (K key : map.descendingKeySet()){ 
				for (V val : map.get(key)){
					list.add(new Pair<K,V>(key,val));
				}
			}
			return list;
		}
	}

	private class Pair<E1,E2> {
		private E1 first;
		private E2 second;

		public Pair(E1 f, E2 s){
			first = f;
			second = s;
		}

		public E1 first(){
			return first;
		}

		public E2 second(){
			return second;
		}

		public String toString(){
			return first.toString() + "," + second.toString();
		}

	}
}
