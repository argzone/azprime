package uk.ac.cam.cl.azprime.classify;

import java.io.*;
import java.util.*;

import uk.ac.cam.cl.azprime.AZWrapper;
import uk.ac.cam.cl.azprime.config.Configuration;

public class AZProblem {
	AZDoc[] docs;
	AZFeatureExtractor extractor;
	List<String> labelList;
	String[] ids;	
	
	String[] getDocIDs(){
		if (ids == null){
			ids = new String[docs.length];
			for (int d = 0; d < docs.length; d++){
				ids[d] = docs[d].path;
			}
		}
		
		return ids;
	}
	
	public List<String> getLabelList(){
		return labelList;
	}
		
	public AZProblem(File[] dirs, File[] srl, AZProblem prob){
		this(dirs,srl,prob,false);
	}
	
	public AZProblem(File[] dirs, File[] srl, AZProblem prob, boolean abstractOnly){
		FilenameFilter filter = new FilenameFilter(){
			@Override
			public boolean accept(File arg0, String arg1) {
				return arg1.endsWith(".pos.xml");
			}				
		};
		
		FilenameFilter backupFilter = new FilenameFilter(){
			@Override
			public boolean accept(File arg0, String arg1) {
				return arg1.endsWith(".simone.xml");
			}				
		};

		extractor = prob.extractor;
		labelList = extractor.labelList;

		ArrayList<File> fileList = new ArrayList<File>();
		ArrayList<File> srlList = new ArrayList<File>();

		for (int d = 0; d < dirs.length; d++){
			File[] contents = dirs[d].listFiles(filter);
			
			if (contents.length == 0){
				System.out.println("No pos.xml files found in " + dirs[d].getPath() + ", looking for files without tags");
				contents = dirs[d].listFiles(backupFilter);
			}
			
			if (contents.length == 0){
				System.out.println("No simone.xml files found in " + dirs[d].getPath() + ", skipping directory");
				continue;
			}
			
			List<File> list = new ArrayList<File>();

			for (File f : contents){
				if (srl != null){
					String fName = f.getName();
					File srlFile = new File(srl[d],fName.replaceAll("\\.pos\\.xml$", ".words.txt."));
				//	File srlFile = new File(srl[d],fName.substring(0,fName.indexOf(".pos.xml")) + ".nxml");
					if (srlFile.exists()){
						srlList.add(srlFile);
						list.add(f);
					}else{
						System.out.println("No SRL file " + srlFile.getPath());
						list.add(f);
						srlList.add(null);
					}
				}else{
					list.add(f);
				}
			}


			fileList.addAll(list);
		}

		if (srl != null){
			try{
				docs = extractor.readDataWithSRL(fileList,srlList,false);
			} catch (IOException e) {
				e.printStackTrace();
				System.exit(9);
			}
		}else{
			docs = extractor.readData(fileList, false,abstractOnly);
		}
	}

	public AZProblem(File[] dirs, File[] srl, Set<String> more2Ignore, int history, int typeHistory, String[] sFeatures, File dict,boolean mapLabels){
		this(dirs,srl,more2Ignore,history,typeHistory,sFeatures,dict,mapLabels,false);
	}
	
	public AZProblem(File[] dirs, File[] srl, Set<String> more2Ignore, int history, int typeHistory, String[] sFeatures, File dict,boolean mapLabels,boolean abstractOnly){
		Configuration config = Configuration.load("classpath:/cfg/config.scala");
		AZWrapper wrapper = new AZWrapper(config);
		extractor = new AZFeatureExtractor(wrapper,mapLabels);
		labelList = extractor.labelList;
		extractor.setHistory(history);
		extractor.setTypeHistory(typeHistory);
		
		if (sFeatures != null){
			for (String sf : sFeatures) extractor.addFeature(sf);
		}
		
		System.out.println(extractor.usingStructuralFeatures() + "," + extractor.usingNonlocalFeatures());
		
		if (dict != null){
			try{
				extractor.readDictionary(dict);
			}catch(IOException e){
				e.printStackTrace();
				System.exit(9);
			}
		}
		
		extractor.addToIgnoreList(more2Ignore);

		FilenameFilter filter = new FilenameFilter(){
			@Override
			public boolean accept(File arg0, String arg1) {
				return arg1.endsWith(".pos.xml");
			}				
		};
		
		FilenameFilter backupFilter = new FilenameFilter(){
			@Override
			public boolean accept(File arg0, String arg1) {
				return arg1.endsWith(".simone.xml");
			}				
		};


		ArrayList<File> fileList = new ArrayList<File>();
		ArrayList<File> srlList = new ArrayList<File>();

		for (int d = 0; d < dirs.length; d++){
			File[] contents = dirs[d].listFiles(filter);
			
			if (contents.length == 0){
				System.out.println("No pos.xml files found in " + dirs[d].getPath() + ", looking for files without tags");
				contents = dirs[d].listFiles(backupFilter);
			}
			
			if (contents.length == 0){
				System.out.println("No simone.xml files found in " + dirs[d].getPath() + ", skipping directory");
				continue;
			}
						
			List<File> list = new ArrayList<File>();

			for (File f : contents){
				if (srl != null){
					String fName = f.getName();
					File srlFile = new File(srl[d],fName.replaceAll("\\.pos\\.xml$", ".words.txt."));
				//	File srlFile = new File(srl[d],fName.substring(0,fName.indexOf(".pos.xml")) + ".nxml");
					if (srlFile.exists()){
						srlList.add(srlFile);
						list.add(f);
					}else{
						System.out.println("No SRL file " + srlFile.getPath());
						list.add(f);
						srlList.add(null);
					}
				}else{
					list.add(f);
				}
			}


			fileList.addAll(list);
			//fileList.addAll(Arrays.asList(d.listFiles(filter)));
		}

		if (srl != null){
			try{
				docs = extractor.readDataWithSRL(fileList,srlList,true);
			} catch (IOException e) {
				e.printStackTrace();
				System.exit(9);
			}
		}else{
			docs = extractor.readData(fileList,true,abstractOnly);
		}

		System.out.println("Read " + docs.length + " documents, " + extractor.lastFeature() + " features");
		extractor.listFeatureTypes();
		System.out.println();
	}
	
	public AZProblem(Set<String>[][] features, String[][] labels, String[] paths, int history){
		extractor = new AZFeatureExtractor(false);
		docs = extractor.readHardcodedFeatures(features, labels, paths);
		extractor.setHistory(history);
		labelList = extractor.labelList;
		System.out.println("Read " + docs.length + " documents, " + extractor.lastFeature() + " features");		
	}
	
	private AZProblem(){}
	
	public AZProblem subProblem(Collection<Integer> ds){
		AZProblem prob = new AZProblem();
		prob.docs = new AZDoc[ds.size()];
		prob.extractor = extractor;
		prob.labelList = labelList;
		prob.ids = new String[ds.size()];
		int count = 0;
		for (int d : ds){
			prob.docs[count] = docs[d];
			prob.ids[count] = ids[d];
			count++;
		}
		return prob;
	}
	
	public AZProblem(AZDoc[] ds, AZFeatureExtractor ext, List<String> ll, String[] i){
		docs = ds;
		extractor = ext;
		labelList = ll;
		ids = i;
	}

	public int[][] getGold(){
		int[][] gold = new int[docs.length][];

		for (int d = 0; d < gold.length; d++){			
			gold[d] = docs[d].labels;
		}

		return gold;
	}

	public void write(File dir, int[][] predictions) throws IOException{
		if (dir.exists() == false) dir.mkdir();
	}
	
	public int size(){
		return docs.length;
	}
}
