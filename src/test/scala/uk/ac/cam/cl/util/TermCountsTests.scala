package uk.ac.cam.cl.util

import org.junit.runner.RunWith
import org.scalatest.FunSuite
import org.scalatest.junit.JUnitRunner
import org.scalatest.matchers.ShouldMatchers
import uk.ac.cam.cl.azprime.resources.StopWords

@RunWith(classOf[JUnitRunner])
class TermCountsTests extends FunSuite with ShouldMatchers {

  test("can get term counts for single word terms") {
    val words = Seq("Dog", "Man", "Dog", "Cat")
    implicit val stopwords = new StopWords(Set("cat"))
    val wc = new TermCounts(words)
    expect(2) {
      wc("dog")
    }
    expect(1) {
      wc("man")
    }
    expect(0) {
      wc("cat")
    }
  }

  test("can combine term count frequencies") {
    val words1 = Seq("Dog", "Man", "Dog", "Cat")
    val words2 = Seq("Dog", "Lion")
    implicit val stopwords = new StopWords(Set("cat"))
    val wc1 = new TermCounts(words1)
    val wc2 = new TermCounts(words2)
    val wc12 = wc1 + wc2
    expect(3) {
      wc12("dog")
    }
    expect(1) {
      wc12("man")
    }
    expect(1) {
      wc12("lion")
    }
    expect(0) {
      wc12("cat")
    }
  }

  test("can get term counts for multiword terms") {
    val terms = Seq("Dog on the moon", "DOG ON THE MOON", "Dog Is a Bunny", "Cat is a man?")
    val wc = new TermCounts(terms)
    expect(2) {
      wc("dog on the moon")
    }
    expect(1) {
      wc("dog is a bunny")
    }
    expect(1) {
      wc("cat is a man?")
    }
  }
}