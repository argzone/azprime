package uk.ac.cam.cl.util

import org.junit.runner.RunWith
import org.scalatest.FunSuite
import org.scalatest.junit.JUnitRunner
import org.scalatest.matchers.ShouldMatchers
import uk.ac.cam.cl.azprime.resources.StopWords

@RunWith(classOf[JUnitRunner])
class TfIdfScoresTests extends FunSuite with ShouldMatchers {

  test("can calculate tf*idf for set of documents") {
    implicit val stoplist = new StopWords(Set("robot"))
    val doc1 = new IndexableDocument(Seq("alien", "cat", "man"))
    val doc2 = new IndexableDocument(Seq("aliens", "aliens", "kid", "man"))
    val tfidf = new TfIdfScores(List(doc1, doc2));
    val alien = tfidf.tfidf("alien", doc1)
    val cat = tfidf.tfidf("cat", doc1)
    val man = tfidf.tfidf("man", doc1)
    val aliens = tfidf.tfidf("aliens", doc2)
    
    expect(1 * math.log(2.0 / 1)) {
      alien
    }
    expect(1 * math.log(2.0 / 1)) {
      cat
    }
    expect(1 * math.log(2.0 / 2)) {
      man
    }
    expect(2 * math.log(2.0 / 1)) {
      aliens
    }
  }
}