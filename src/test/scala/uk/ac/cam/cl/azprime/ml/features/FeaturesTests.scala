package uk.ac.cam.cl.azprime.ml.features

import java.io.File
import org.scalatest.FunSuite
import org.scalatest.matchers.ShouldMatchers
import uk.ac.cam.cl.azprime.xml.ParsedSciXml.Division
import uk.ac.cam.cl.azprime.xml.ParsedSciXml.Paragraph
import uk.ac.cam.cl.azprime.xml.ParsedSciXml.Sentence
import uk.ac.cam.cl.azprime.xml.SciXmlReader
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import uk.ac.cam.cl.azprime.ml.FeatureEngine
import uk.ac.cam.cl.azprime.xml.FeatureXmlWriter
import uk.ac.cam.cl.azprime.ml.features._
import uk.ac.cam.cl.azprime.ml.ExtractedFeature
import uk.ac.cam.cl.azprime.ml.FeatureExtractor
import uk.ac.cam.cl.azprime.xml.ParsedSciXml
import uk.ac.cam.cl.azprime.ml.ExtractedFeatures
import uk.ac.cam.cl.azprime.config.Configuration
import uk.ac.cam.cl.azprime.resources.Resources
import uk.ac.cam.cl.util.TermCounts
import uk.ac.cam.cl.azprime.BodyTfIdfScores
import uk.ac.cam.cl.azprime.TitleTfIdfScores
import uk.ac.cam.cl.azprime.TfIdfTypes._
import uk.ac.cam.cl.azprime.ml.SingleFeatureExtractor

@RunWith(classOf[JUnitRunner])
class FeaturesTests extends FunSuite with ShouldMatchers {

  def loadFile(filename: String, size: Int) = {
    val inurl = this.getClass.getResource(filename)
    expect(true) { inurl != null }

    val infile = new File(inurl.getFile)
    val reader = new SciXmlReader(infile)
    val doc = reader.parse()
    expect(size) { doc.sentences.length } // Simple sanity check
    doc
  }

  def loadTestFile = {
    loadFile("/9405001.pos.xml", 158)
  }

  def load2ndTestFile = {
    loadFile("/9410033.pos.xml", 176)
  }

  def runTestFile = {
    val doc = loadTestFile
    val config = new Configuration
    val resources = new Resources(config)
    implicit val stoplist = resources.stoplist
    val titleTfIdfScores = TitleTfIdfScores(List(doc), includeHeaders = true)
    val featureExtractors =
      new AbsoluteLocationExtractor ::
        new DivisionLocationExtractor ::
        new ParagraphLocationExtractor ::
        new DocumentInitialSectionExtractor(resources.headers, initialParagraphs = 7) ::
        new DocumentFinalSectionExtractor(resources.headers, finalParagraphs = 5) ::
        new DocumentAndParagraphLocationExtractor(resources.headers) ::
        new SentenceLengthExtractor ::
        new BodyTfIdfExtractor(BodyTfIdfScores(List(doc))) ::
        new HeaderCategoryExtractor(resources.headers) ::
        new TitleWordsExtractor(titleTfIdfScores.termCounts) ::
        new CitationTypeExtractor ::
        new SelfCitationExtractor ::
        new CitationLocationExtractor ::
        new CitationCountExtractor ::
        Nil
    val p = new FeatureEngine(List(doc), featureExtractors)
    val features = p.run()
    features
  }

  def saveFeatures(features: List[FeatureEngine.DocumentFeatures]) = {
    val doc = loadTestFile
    val features = runTestFile
    val outfile = File.createTempFile("azp-", ".tmp", new File("./"))
    //outfile.deleteOnExit
    new FeatureXmlWriter(features(0), doc, ignored = Set("ISEC", "FSEC")).writeFile(outfile)
    outfile
  }

  // Spot check three locations (top, middle, bottom) 
  test("can extract absolute location features") {
    val doc = loadTestFile
    val extractor = new AbsoluteLocationExtractor()
    val feature = extractor.extract(doc.sentences(0), doc, null)
    assert(AbsoluteLocationValues.A === feature.value)

    val feature2 = extractor.extract(doc.sentences(doc.sentences.length / 2), doc, null)
    assert(AbsoluteLocationValues.F === feature2.value)

    val feature3 = extractor.extract(doc.sentences.last, doc, null)
    assert(AbsoluteLocationValues.J === feature3.value)
  }

  test("can extract header categories") {
    val doc = loadTestFile
    val config = new Configuration
    val resources = new Resources(config)
    val extractor = new HeaderCategoryExtractor(resources.headers);
    {
      val feature = extractor.extract(doc.sentences(0), doc, null)
      expect("introduction") { feature.value }
    }
    {
      val feature = extractor.extract(doc.sentences(67), doc, null)
      expect("nonstandard") { feature.value }
    }
    {
      val feature = extractor.extract(doc.sentences(154), doc, null)
      expect("conclusion") { feature.value }
    }
  }

  test("can extract title word counts from sentences") {
    val doc = loadTestFile
    val config = new Configuration
    val resources = new Resources(config)
    implicit val stoplist = resources.stoplist
    val titleTfIdfScores = TitleTfIdfScores(List(doc), includeHeaders = true)
    val extractor = new TitleWordsExtractor(titleTfIdfScores.termCounts, normalize = false);
    {
      val feature = extractor.extract(doc.sentences(22), doc, null)
      // XXX (dk): Should be 3 / 17 but because words aren't lemmatized, plurals don't match singulars.
      assert(feature.value === 2.0)
    }
  }

  test("can extract tf*idf scores from sentences.") {
    val doc1 = loadTestFile
    val config = new Configuration
    val resources = new Resources(config)
    implicit val stoplist = resources.stoplist;
    {
      val bodyTfIdfScores = BodyTfIdfScores(List(doc1))
      val extractor = new BodyTfIdfExtractor(bodyTfIdfScores, normalize = true);
      {
        val feature = extractor.extract(doc1.sentences(22), doc1, null)
        // XXX (dk): Since we only have one document, all words get reduced to 0.
        assert(feature.value === 0.0d)
      }
    }
    val doc2 = load2ndTestFile;
    {
      val bodyTfIdfScores = BodyTfIdfScores(List(doc1, doc2))
      val extractor = new BodyTfIdfExtractor(bodyTfIdfScores, normalize = true);
      {
        val feature = extractor.extract(doc1.sentences(22), doc1, null)
        // XXX (dk): This value isn't actually verified, but it should be...
        assert(feature.value > 4.4d)
      }
    }
  }

  // Do a spot check for the first division to make sure the extracted values look sound.
  test("can extract division location features") {
    val doc = loadTestFile
    val extractor = new DivisionLocationExtractor();
    {
      val feature = extractor.extract(doc.sentences(0), doc, null)
      assert(DivisionLocationValues.First === feature.value)
    }
    for (s <- doc.sentences.drop(1).take(2)) {
      val feature = extractor.extract(s, doc, null)
      assert(DivisionLocationValues.SecondOrThird === feature.value)
    }
    {
      val feature = extractor.extract(doc.sentences(3), doc, null)
      assert(DivisionLocationValues.FirstThird === feature.value)
    }
    {
      val feature = extractor.extract(doc.sentences(17), doc, null)
      assert(DivisionLocationValues.SecondThird === feature.value)
    }
    {
      val feature = extractor.extract(doc.sentences(29), doc, null)
      assert(DivisionLocationValues.LastThird === feature.value)
    }
    for (s <- doc.sentences.drop(30).take(2)) {
      val feature = extractor.extract(s, doc, null)
      assert(DivisionLocationValues.SecondOrThirdToLast === feature.value)
    }
    {
      val feature = extractor.extract(doc.sentences(32), doc, null)
      assert(DivisionLocationValues.Last === feature.value)
    }
  }

  // Test the first paragraph to see if the locations are accurate.
  test("can extract paragraph location features") {
    val doc = loadTestFile
    val extractor = new ParagraphLocationExtractor()

    {
      val feature = extractor.extract(doc.sentences(0), doc, null)
      assert(ParagraphLocationValues.First === feature.value)
    }
    for (s <- doc.sentences.drop(1).take(3)) {
      val feature = extractor.extract(s, doc, null)
      assert(ParagraphLocationValues.Middle === feature.value)
    }
    {
      val feature = extractor.extract(doc.sentences(4), doc, null)
      assert(ParagraphLocationValues.Last === feature.value)
    }
  }

  // Test the first paragraph to see if the locations are accurate.
  test("can extract document and paragraph location features") {
    val doc = loadTestFile
    val config = new Configuration
    val resources = new Resources(config)
    val extractorI = new DocumentInitialSectionExtractor(resources.headers, initialParagraphs = 7)
    val extractorF = new DocumentFinalSectionExtractor(resources.headers, finalParagraphs = 5)
    val extractor = new DocumentAndParagraphLocationExtractor(resources.headers)
    val feaI = extractorI.extract(doc.sentences(0), doc, null)
    val feaF = extractorF.extract(doc.sentences(0), doc, null)
    val extracted = ExtractedFeatures(feaI, feaF);
    {
      val feature = extractor.extract(doc.sentences(0), doc, extracted)
      assert(DocumentAndParagraphLocationValues.InitialSecInitialPar === feature.value)
    }
    for (s <- doc.sentences.drop(1).take(3)) {
      val feature = extractor.extract(s, doc, extracted)
      assert(DocumentAndParagraphLocationValues.InitialSecMiddlePar === feature.value)
    }
    {
      val feature = extractor.extract(doc.sentences(4), doc, extracted)
      assert(DocumentAndParagraphLocationValues.InitialSecFinalPar === feature.value)
    }
  }

  test("can extract citation type features") {
    val doc = loadTestFile
    val extractor = new CitationTypeExtractor;
    {
      val feature = extractor.extract(doc.sentences(0), doc, null)
      assert(CitationTypeValues.None === feature.value)
    }
    {
      val feature = extractor.extract(doc.sentences(14), doc, null)
      assert(CitationTypeValues.Citation === feature.value)
    }
    {
      val feature = extractor.extract(doc.sentences(49), doc, null)
      assert(CitationTypeValues.Author === feature.value)
    }
  }

  test("can extract self citation features") {
    val doc = loadTestFile
    val extractorType = new CitationTypeExtractor;
    val extractor = new SelfCitationExtractor;
    {
      val ctype = extractorType.extract(doc.sentences(0), doc, null)
      val extracted = ExtractedFeatures(ctype);
      val feature = extractor.extract(doc.sentences(0), doc, extracted)
      assert(SelfCitationValues.NA === feature.value)
    }
    {
      val ctype = extractorType.extract(doc.sentences(8), doc, null)
      val extracted = ExtractedFeatures(ctype);
      val feature = extractor.extract(doc.sentences(8), doc, extracted)
      assert(SelfCitationValues.Other === feature.value)
    }
    {
      val ctype = extractorType.extract(doc.sentences(14), doc, null)
      val extracted = ExtractedFeatures(ctype);
      val feature = extractor.extract(doc.sentences(14), doc, extracted)
      assert(SelfCitationValues.Self === feature.value)
    }
    {
      val ctype = extractorType.extract(doc.sentences(49), doc, null)
      val extracted = ExtractedFeatures(ctype);
      val feature = extractor.extract(doc.sentences(49), doc, extracted)
      assert(SelfCitationValues.Author === feature.value)
    }
  }

  test("can extract citation location features") {
    val doc = loadTestFile
    val extractorType = new CitationTypeExtractor;
    val extractor = new CitationLocationExtractor;
    {
      val s = doc.sentences(0)
      val ctype = extractorType.extract(s, doc, null)
      val extracted = ExtractedFeatures(ctype);
      val feature = extractor.extract(s, doc, extracted)
      assert(CitationLocationValues.NA === feature.value)
    }
    {
      val s = doc.sentences(8)
      val ctype = extractorType.extract(s, doc, null)
      val extracted = ExtractedFeatures(ctype);
      val feature = extractor.extract(s, doc, extracted)
      assert(CitationLocationValues.SentenceEnd === feature.value)
    }
    {
      val s = doc.sentences(14)
      val ctype = extractorType.extract(s, doc, null)
      val extracted = ExtractedFeatures(ctype);
      val feature = extractor.extract(s, doc, extracted)
      assert(CitationLocationValues.SentenceStart === feature.value)
    }
    {
      val s = doc.sentences(49)
      val ctype = extractorType.extract(s, doc, null)
      val extracted = ExtractedFeatures(ctype);
      val feature = extractor.extract(s, doc, extracted)
      assert(CitationLocationValues.Author === feature.value)
    }
  }

  test("can extract citation count features") {
    val doc = loadTestFile
    val extractorType = new CitationTypeExtractor;
    val extractor = new CitationCountExtractor;
    {
      val s = doc.sentences(0)
      val ctype = extractorType.extract(s, doc, null)
      val extracted = ExtractedFeatures(ctype);
      val feature = extractor.extract(s, doc, extracted)
      assert(CitationCountValues.None === feature.value)
    }
    {
      val s = doc.sentences(8)
      val ctype = extractorType.extract(s, doc, null)
      val extracted = ExtractedFeatures(ctype);
      val feature = extractor.extract(s, doc, extracted)
      assert(CitationCountValues.Many === feature.value)
    }
    {
      val s = doc.sentences(14)
      val ctype = extractorType.extract(s, doc, null)
      val extracted = ExtractedFeatures(ctype);
      val feature = extractor.extract(s, doc, extracted)
      assert(CitationCountValues.One === feature.value)
    }
    {
      val s = doc.sentences(49)
      val ctype = extractorType.extract(s, doc, null)
      val extracted = ExtractedFeatures(ctype);
      val feature = extractor.extract(s, doc, extracted)
      assert(CitationCountValues.Author === feature.value)
    }
  }

  test("can output feature xml") {
    val outfile = saveFeatures(runTestFile)
    //io.Source.fromFile(outfile).getLines.foreach(println)
    val root = SciXmlReader.DoctypeIgnorantXML.loadFile(outfile)
    assert(158 === (root \\ "S").length)
    assert(158 === (root \\ "S" \\ ("@LOC")).length)
    assert(158 === (root \\ "S" \\ ("@DIV")).length)
  }

  ignore("ouput is the same as previous system for 1 test file") {
    val outfile = saveFeatures(runTestFile)
    val testurl = this.getClass.getResource("/9405001.fea.xml")
    expect(true) { testurl != null }
    val testfile = testurl.getFile
    val root = SciXmlReader.DoctypeIgnorantXML.loadFile(outfile)
    val root2 = SciXmlReader.DoctypeIgnorantXML.loadFile(testfile)

    // Compare all attributes in root with the same element in root2.
    // We ignore attributes not being output in root but in roo2.
    val ss1 = (root \\ "S")
    val ss2 = (root2 \\ "S")
    assert(ss1.length === ss2.length)
    var passed = true
    for (((s, s2), idx) <- ss1 zip ss2 zipWithIndex; (attr, value) <- s.attributes.asAttrMap) {
      val value2 = (s2 \\ ("@" + attr) text)
      if (value2 != value) {
        println("S %d - %s didn't match (%s != %s)".format(idx, attr, value, value2))
        passed = false
      }
    }
    if (!passed) assert(true === false)
  }

  case class UselessFeature(val value: String) extends ExtractedFeature {
    type ValueType = String
    val label = ""
  }

  test("can detect cycles in feature extractors") {
    {
      class UselessFeatureExtractorA extends SingleFeatureExtractor {
        type Extracted = UselessFeature
        dependsOn[UselessFeatureExtractorB]
        def extract(sentence: ParsedSciXml.Sentence,
          document: ParsedSciXml.Document,
          features: ExtractedFeatures) = {
          new UselessFeature("")
        }
      }
      class UselessFeatureExtractorB extends SingleFeatureExtractor {
        type Extracted = UselessFeature
        dependsOn[UselessFeatureExtractorC]
        def extract(sentence: ParsedSciXml.Sentence,
          document: ParsedSciXml.Document,
          features: ExtractedFeatures) = {
          new UselessFeature("")
        }
      }
      class UselessFeatureExtractorC extends SingleFeatureExtractor {
        type Extracted = UselessFeature
        dependsOn[UselessFeatureExtractorA]
        def extract(sentence: ParsedSciXml.Sentence,
          document: ParsedSciXml.Document,
          features: ExtractedFeatures) = {
          new UselessFeature("")
        }
      }
      val featureExtractors = new UselessFeatureExtractorA() ::
        new UselessFeatureExtractorB() ::
        new UselessFeatureExtractorC() :: Nil
      val doc = loadTestFile
      intercept[Exception] {
        val trainer = new FeatureEngine(List(doc), featureExtractors)
      }
    }
    {
      class UselessFeatureExtractorA extends SingleFeatureExtractor {
        type Extracted = UselessFeature
        dependsOn[UselessFeatureExtractorB]
        dependsOn[UselessFeatureExtractorC]
        def extract(sentence: ParsedSciXml.Sentence,
          document: ParsedSciXml.Document,
          features: ExtractedFeatures) = {
          new UselessFeature("")
        }
      }
      class UselessFeatureExtractorB extends SingleFeatureExtractor {
        type Extracted = UselessFeature
        dependsOn[UselessFeatureExtractorC]
        def extract(sentence: ParsedSciXml.Sentence,
          document: ParsedSciXml.Document,
          features: ExtractedFeatures) = {
          new UselessFeature("")
        }
      }
      class UselessFeatureExtractorC extends SingleFeatureExtractor {
        type Extracted = UselessFeature
        def extract(sentence: ParsedSciXml.Sentence,
          document: ParsedSciXml.Document,
          features: ExtractedFeatures) = {
          new UselessFeature("")
        }
      }
      val featureExtractors = new UselessFeatureExtractorC() ::
        new UselessFeatureExtractorB() ::
        new UselessFeatureExtractorA() :: Nil
      val doc = loadTestFile
      val trainer = new FeatureEngine(List(doc), featureExtractors)
      expect(true) { trainer != null }
    }
  }

  // Once this functionality is added, enable this.
  ignore("can resolve out of order extractor dependencies") {
    {
      class UselessFeatureExtractorA extends SingleFeatureExtractor {
        type Extracted = UselessFeature
        dependsOn[UselessFeatureExtractorB]
        dependsOn[UselessFeatureExtractorC]
        def extract(sentence: ParsedSciXml.Sentence,
          document: ParsedSciXml.Document,
          features: ExtractedFeatures) = {
          new UselessFeature("")
        }
      }
      class UselessFeatureExtractorB extends SingleFeatureExtractor {
        type Extracted = UselessFeature
        dependsOn[UselessFeatureExtractorC]
        def extract(sentence: ParsedSciXml.Sentence,
          document: ParsedSciXml.Document,
          features: ExtractedFeatures) = {
          new UselessFeature("")
        }
      }
      class UselessFeatureExtractorC extends SingleFeatureExtractor {
        type Extracted = UselessFeature
        def extract(sentence: ParsedSciXml.Sentence,
          document: ParsedSciXml.Document,
          features: ExtractedFeatures) = {
          new UselessFeature("")
        }
      }
      val featureExtractors = new UselessFeatureExtractorA() ::
        new UselessFeatureExtractorB() ::
        new UselessFeatureExtractorC() :: Nil
      val doc = loadTestFile
      val trainer = new FeatureEngine(List(doc), featureExtractors)
      expect(true) { trainer != null }
    }
  }
}