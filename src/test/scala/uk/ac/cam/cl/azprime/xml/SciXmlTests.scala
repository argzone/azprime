package uk.ac.cam.cl.azprime.xml

import org.scalatest.FunSuite
import org.scalatest.matchers.ShouldMatchers
import uk.ac.cam.cl.azprime.resources.StopWords
import uk.ac.cam.cl.azprime.xml.ParsedSciXml._
import java.io.File
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import java.net.URL

object SciXmlTestsData {
  val sampleSentence = <S ID='S-0' IA='BKG' AZ='BKG'>
                         <W C='NNS'>Data</W>
                         <W C='NN'>sparseness</W>
                         <W C='VBZ'>is</W>
                         <W C='DT'>an</W>
                         <W C='JJ'>inherent</W>
                         <W C='NN'>problem</W>
                         <W C='IN'>in</W>
                         <W C='JJ'>statistical</W>
                         <W C='NNS'>methods</W>
                         <W C='IN'>for</W>
                         <W C='JJ'>natural</W>
                         <W C='NN'>language</W>
                         <W C='NN'>processing.</W>
                       </S>

  val sampleSentence2 = <S ID='S-1' IA='BKG' AZ='BKG'>
                          <W C='JJ'>Internal</W>
                          <W C='NNS'>decisions</W>
                          <W C='VBP'>are</W>
                          <W C='VBN'>made</W>
                          <W C='IN'>on</W>
                          <W C='DT'>the</W>
                          <W C='NN'>basis</W>
                          <W C='IN'>of</W>
                          <W C='DT'>the</W>
                          <W C='JJ'>specified</W>
                          <W C='NN'>input.</W>
                        </S>

  val sampleParagraph = <P>{ sampleSentence }</P>

  val sampleParagraph2 = <P>{ sampleSentence2 }</P>

  val sampleDivision = <DIV DEPTH='1'>
                         <HEADER ID='H-0'>Introduction</HEADER>
                         { sampleParagraph }
                       </DIV>

  val sampleNestedDivision = <DIV DEPTH='1'>
                               <HEADER ID='H-0'>Related Work</HEADER>
                               { sampleParagraph }
                               <DIV DEPTH="2">
                                 <HEADER ID='H-0'>Competition</HEADER>
                                 { sampleParagraph2 }
                               </DIV>
                             </DIV>

  def loadFile(filename: String, fromClasspath: Boolean = true) = {
    val infile = if (fromClasspath) {
      val inurl = this.getClass.getResource(filename)
      assert(inurl != null)
      new File(inurl.getFile)
    } else {
      new File(filename)
    }

    assert(infile != null)
    val reader = new SciXmlReader(infile)
    val doc = reader.parse()
    doc
  }

  def loadSampleFile = loadFile("/9405001.pos.xml")
  def loadSampleFile2 = loadFile("/9410033.pos.xml")
}

@RunWith(classOf[JUnitRunner])
class SciXmlWriterTests extends FunSuite with ShouldMatchers {
  import SciXmlTestsData._

  test("replaceSentences iterates over sentences in proper order.") {
    val doc = loadSampleFile
    val allSentences = doc.allSentences
    var idx = 0
    def f(s: Sentence): Sentence = {
      val s2 = allSentences(idx)
      idx += 1
      assert(s === s2)
      s2
    }
    val doc2 = ParsedSciXml.replaceSentences(doc, f _)
    assert(doc === doc2)
  }

  test("can write a SciXml File") {
    val doc = loadSampleFile
    val writer = new SciXmlWriter(doc, true, true)
    val out = File.createTempFile("TMP", "TMP")
    out.deleteOnExit()
    writer.write(out)
    //println(new SciXmlWriter(doc, false).xmlString)
    // TODO: Test output
    val doc2 = loadFile(out.getAbsolutePath, fromClasspath = false)
    assert(doc2.title === doc.title)
    // TODO: Do not squash abstract text
    //assert(doc2.abstractText === doc.abstractText)
    assert(doc2.sentences.length === doc.sentences.length)
    for ((s1, s2) <- doc.sentences zip doc2.sentences) {
      assert(s1.words.length === s2.words.length)
      assert(s1 === s2)
    }
  }
}

@RunWith(classOf[JUnitRunner])
class SciXmlReaderTests extends FunSuite with ShouldMatchers {
  import SciXmlTestsData._

  test("can parse a SciXml File") {
    val doc = loadSampleFile
    expect(158) { doc.sentences.length }

    expect("Data sparseness is an inherent problem in statistical methods for natural language processing.") {
      doc.sentences(0).mkString
    }
    expect("NNS") { // POS for first word, "Data"
      doc.sentences(0).wordsWithPos(0)._2
    }
    expect("BKG") {
      doc.sentences(0).azCategory
    }
  }

  test("Can replace sentence content.") {
    def inverseSentence(s: Sentence): Sentence = {
      s.copy(wordsWithPosAndExtras = s.wordsWithPosAndExtras.reverse)
    }
    def changePos(newPos: String)(s: Sentence): Sentence = {
      s.copy(wordsWithPosAndExtras = s.wordsWithPosAndExtras.map(x => (x._1, newPos, x._3)))
    }
    val doc = loadSampleFile
    val doc2 = ParsedSciXml.replaceSentences(doc, changePos("BOO") _)
    for (s <- doc2.sentences; (w, pos) <- s.wordsWithPos) {
      expect("BOO") {
        pos
      }
    }
  }

  test("can retrieve headers for sentences") {
    val doc = loadSampleFile
    expect("Introduction") {
      doc.header(doc.sentences(0))
    }

    expect("Introduction") {
      doc.header(doc.sentences(10))
    }
  }
  test("can retrieve headers for sentences in nested divs") {
    val doc = loadSampleFile2
    expect("When to Trigger Default Handling") {
      doc.header(doc.sentences(59))
    }

    expect("General Discussion of Defaults") {
      doc.header(doc.sentences(35))
    }
  }

  test("can retrieve the location of sentences within a paragraph") {
    val doc = loadSampleFile
    expect((0, 5)) {
      doc.locationWithinParagraph(doc.sentences(0))
    }
    expect((0, 5)) {
      doc.locationWithinParagraph(doc.sentences(5))
    }
  }

  test("can retrieve the location of sentences within a division") {
    val doc = loadSampleFile
    expect((0, 33)) {
      doc.locationWithinRelativeDivision(doc.sentences(0))
    }
    expect((5, 33)) {
      doc.locationWithinRelativeDivision(doc.sentences(5))
    }
  }

  def testSentence(sentence: Sentence) = {
    assert(13 === sentence.wordsWithPos.length)
    assert("BKG" === sentence.azCategory)
  }

  def testParagraph(paragraph: Paragraph) = {
    assert(1 === paragraph.sentences.length)
    val sentence = paragraph.sentences(0)
    testSentence(sentence)
  }

  def testDivision(div: Division, header: String) = {
    assert(header === div.header)
    testSentence(div.sentences(0))
  }

  test("can parse a sentence") {
    val parsed = SciXmlParsing.parseSentence(sampleSentence)
    testSentence(parsed)
  }

  test("can parse a paragraph") {
    val parsed = SciXmlParsing.parseParagraph(sampleParagraph)
    testParagraph(parsed)
  }

  test("can parse a division") {
    val parsed = SciXmlParsing.parseDivision(sampleDivision)
    testDivision(parsed, "Introduction")
  }

  test("can parse a nested division") {
    val parsed = SciXmlParsing.parseDivision(sampleNestedDivision)
    assert(parsed.header === "Related Work")
    assert(parsed.content.filter(_.isInstanceOf[Division]).head.asInstanceOf[Division].header === "Competition")
  }
}