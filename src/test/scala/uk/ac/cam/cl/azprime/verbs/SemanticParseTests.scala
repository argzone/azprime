package uk.ac.cam.cl.azprime.verbs

import org.scalatest.FunSuite
import org.scalatest.matchers._
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import uk.ac.cam.cl.azprime.config.Configuration
import java.io.ByteArrayInputStream
import uk.ac.cam.cl.azprime.resources.Action
import uk.ac.cam.cl.azprime.resources.Actions
import uk.ac.cam.cl.azprime.resources.Agents
import uk.ac.cam.cl.azprime.resources.ExpressionMatch
import uk.ac.cam.cl.azprime.resources.Lexicon
import uk.ac.cam.cl.azprime.resources.Pattern
import uk.ac.cam.cl.azprime.resources.Resources
import org.scalatest.PrivateMethodTester
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner

object TestUtils extends FunSuite with ShouldMatchers {
  import java.io.File
  import uk.ac.cam.cl.azprime.xml.SciXmlReader

  def loadFile(filename: String, size: Int) = {
    val inurl = this.getClass.getResource(filename)
    expect(true) { inurl != null }

    val infile = new File(inurl.getFile)
    val reader = new SciXmlReader(infile)
    val doc = reader.parse()
    expect(size) { doc.sentences.length } // Simple sanity check
    doc
  }

  def loadTestFile = {
    loadFile("/9405001.pos.xml", 158)
  }

  def load2ndTestFile = {
    loadFile("/9410033.pos.xml", 176)
  }

  def loadTroubleFile = loadFile("/144823.pos.xml", 1605)
}

object SharedData {
  val sent1 = List(
    "the" -> "DT",
    "problem" -> "NN",
    "was" -> "VBD",
    "solved." -> "VBN")

  val sent2 = List("the" -> "DT",
    "counts" -> "NNS",
    "used" -> "VBN",
    "in" -> "IN",
    "this" -> "DT",
    "model" -> "NN",
    "and" -> "CC",
    "in" -> "IN",
    "ours" -> "PRP",
    "were" -> "VBD",
    "obtained" -> "VBN",
    "from" -> "IN",
    "40.5" -> "CD",
    "million" -> "CD",
    "words" -> "NNS",
    "of" -> "IN",
    "WSJ" -> "NNP",
    "text" -> "NN",
    "from" -> "IN",
    "the" -> "DT",
    "years" -> "NNS",
    "1987" -> "CD",
    "-" -> ":",
    "89" -> "CD",
    "." -> ".")

  // S-2
  val sent3 = List("the" -> "DT",
    "most" -> "RBS",
    "likely" -> "JJ",
    "analysis" -> "NN",
    "will" -> "MD",
    "be" -> "VB",
    "taken" -> "VBN",
    "to" -> "TO",
    "be" -> "VB",
    "the" -> "DT",
    "one" -> "NN",
    "that" -> "WDT",
    "contains" -> "VBZ",
    "the" -> "DT",
    "most" -> "RBS",
    "frequent" -> "JJ",
    "configurations." -> "NN")
  val sent3verbs = List(
    SemanticFiniteVerb(VerbTense.Future, VerbVoice.Passive, false, 4, 6) -> ("taken", "VBN"),
    SemanticFiniteVerb(VerbTense.Present, VerbVoice.Active, false, 12, 12) -> ("contains", "VBZ"))

  // S-8
  val sent4 = List("for" -> "IN",
    "example" -> "NN",
    "," -> ",",
    "in" -> "IN",
    "the" -> "DT",
    "bigram" -> "NN",
    "models" -> "VBZ", // Should actually be NN but...
    "that" -> "IN",
    "we" -> "PRP",
    "study" -> "VBP",
    "here" -> "RB",
    "," -> ",",
    "the" -> "DT",
    "probability" -> "NN",
    "<EQN/>" -> "EQN",
    "of" -> "IN",
    "a" -> "DT",
    "conditioned" -> "JJ",
    "word" -> "NN",
    "<EQN/>" -> "EQN",
    "that" -> "WDT",
    "has" -> "VBZ",
    "never" -> "RB",
    "occurred" -> "VBN",
    "in" -> "IN",
    "training" -> "NN",
    "following" -> "VBG",
    "the" -> "DT",
    "conditioning" -> "NN",
    "word" -> "NN",
    "<EQN/>" -> "EQN",
    "is" -> "VBZ",
    "calculated" -> "VBN",
    "from" -> "IN",
    "the" -> "DT",
    "probability" -> "NN",
    "of" -> "IN",
    "<EQN/>" -> "EQN",
    "," -> ",",
    "as" -> "IN",
    "estimated" -> "VBN",
    "by" -> "IN",
    "<EQN/>" -> "EQN",
    "'" -> "''",
    "s" -> "NN",
    "frequency" -> "NN",
    "in" -> "IN",
    "the" -> "DT",
    "corpus" -> "NN",
    "Jelinek et al. 1992" -> "REF",
    "," -> ",",
    "Katz 1987" -> "REF",
    "." -> ".")
  val sent4verbs = List(
    SemanticFiniteVerb(VerbTense.Present, VerbVoice.Active, false, 6, 6) -> ("models", "VBZ"),
    SemanticFiniteVerb(VerbTense.Present, VerbVoice.Active, false, 9, 9) -> ("study", "VBP"),
    SemanticFiniteVerb(VerbTense.PresentPerfect, VerbVoice.Active, false, 21, 23) -> ("occurred", "VBN"),
    SemanticFiniteVerb(VerbTense.Present, VerbVoice.Passive, false, 31, 32) -> ("calculated", "VBN"))
  val sent4agents = List(
    None,
    Some(ExpressionMatch(Pattern("@SELF_NOM", "US_A"), 8, 8)),
    None,
    None)
  val sent4actions = List(
    Some(ExpressionMatch(Action("model", "SOLVE", Set("modelled", "modelling")), 6, 7)),
    Some(ExpressionMatch(Action("study", "INTEREST", Set()), 9, 10)),
    None,
    Some(ExpressionMatch(Action("calculate", "RESEARCH", Set()), 32, 33)))
  val sent4neg = List(
    false,
    false,
    true,
    false)

  val sent5 = List("however" -> "RB",
    "," -> ",",
    "the" -> "DT",
    "overall" -> "JJ",
    "form" -> "NN",
    "of" -> "IN",
    "the" -> "DT",
    "model" -> "NN",
    "<CREF/>" -> "CREF",
    "does" -> "VBZ",
    "not" -> "RB",
    "depend" -> "VB",
    "on" -> "IN",
    "this" -> "DT",
    "assumption" -> "NN",
    "," -> ",",
    "and" -> "CC",
    "we" -> "PRP",
    "will" -> "MD",
    "next" -> "RB",
    "investigate" -> "VB",
    "an" -> "DT",
    "estimate" -> "NN",
    "for" -> "IN",
    "<EQN/>" -> "EQN",
    "derived" -> "VBN",
    "by" -> "IN",
    "averaging" -> "VBG",
    "estimates" -> "NNS",
    "for" -> "IN",
    "the" -> "DT",
    "conditional" -> "JJ",
    "probabilities" -> "NNS",
    "that" -> "WDT",
    "<EQN/>" -> "EQN",
    "follows" -> "VBZ",
    "words" -> "NNS",
    "that" -> "WP",
    "are" -> "VBP",
    "distributionally" -> "RB",
    "similar" -> "JJ",
    "to" -> "TO",
    "<EQN/>" -> "EQN",
    "." -> ".")
  val sent5verbs = List(SemanticFiniteVerb(VerbTense.Present, VerbVoice.Active, false, 9, 11) -> ("depend", "VB"),
    SemanticFiniteVerb(VerbTense.Future, VerbVoice.Active, false, 18, 20) -> ("investigate" -> "VB"),
    SemanticFiniteVerb(VerbTense.Present, VerbVoice.Active, false, 35, 35) -> ("follows" -> "VBZ"),
    SemanticFiniteVerb(VerbTense.Present, VerbVoice.ActiveCopula, false, 38, 38) -> ("are" -> "VBP")) // should be Present/Passive??
  val sent5agents = List(
    None,
    Some(ExpressionMatch(Pattern("@SELF_NOM", "US_A"), 17, 17)),
    None,
    None)
  val sent5actions = List(
    Some(ExpressionMatch(Action("depend on", "NEED", Set()), 11, 13)),
    Some(ExpressionMatch(Action("investigate", "RESEARCH", Set()), 20, 21)),
    None,
    Some(ExpressionMatch(Action("be similar to", "SIMILAR", Set()), 38, 42)))
  val sent5neg = List(
    true,
    false,
    false,
    false)

  // From 9410033; sent id 12
  val sent6 = List("Elements" -> "NNS",
    "are" -> "VBP",
    "uttered" -> "VBN",
    "before" -> "IN",
    "the" -> "DT",
    "processing" -> "NN",
    "or" -> "CC",
    "input" -> "NN",
    "consumption" -> "NN",
    "has" -> "VBZ",
    "been" -> "VBN",
    "finished" -> "NN",
    "." -> ".")
  val sent6verbs = List(SemanticFiniteVerb(VerbTense.Present, VerbVoice.Passive, false, 1, 2) -> ("uttered" -> "VBN"),
    SemanticFiniteVerb(VerbTense.PresentPerfect, VerbVoice.ActiveCopula, false, 9, 10) -> ("been" -> "VBN"))
  val sent6agents = List(None, None)
  val sent6actions = List(None,
    Some(ExpressionMatch(Action("be", "COPULA", Set()), 10, 11)))
  val sent6neg = List(false, false)


  val troubleSentences = List(
    List("Consumers" -> "NNS", "are" -> "VBP", "able" -> "JJ", "to" -> "TO", "distinguish" -> "VB", "on-line" -> "JJ", "health" -> "NN", "services" -> "NNS", "that" -> "WDT", "follow" -> "VBP", "the" -> "DT", "Hi-Ethics" -> "NNP", "principles" -> "NNS", "from" -> "IN", "those" -> "DT", "that" -> "WDT", "do" -> "VBP", "not" -> "RB"))
  }

@RunWith(classOf[JUnitRunner])
class SemanticParseTests extends FunSuite with ShouldMatchers with PrivateMethodTester {
  import SharedData._

  test("can find negation") {
    val resources = new Resources(new Configuration)
    val sp = new SemanticParse(sent4)(Set(), 6)
    val res = sp.negationPosition(24)(5)(resources.lexicon)
    assert(res.isDefined === true)
    assert(res.get === 22)
  }

  test("can parse trouble sentences without incident") {
    import TestUtils._
    //val d = loadTroubleFile
    val resources = new Resources(new Configuration)
    implicit val agents = new Agents(resources.patterns, resources.lexicon)
    implicit val actions = resources.actions
    implicit val lexicon = resources.lexicon
    //for (s <- d.sentences; sent = SemanticParse.alterPos(s.wordsWithPos)) {
    for (s <- troubleSentences; sent = SemanticParse.alterPos(s)) {
      val sp = new SemanticParse(sent)(Set(), 6)
      val verbs = sp.semanticFiniteVerbs
      val ac = verbs map (sp.actionforVerb)
      val ag = verbs map (sp.agentForVerb)
      val neg = verbs map (x => sp.negationPosition(x.finitePosition)(5)(lexicon))
    }
  }

  test("can alter verb POS") {
    assert(SemanticParse.alterPos(("are", "VBP"))._2 === "VBP_BE")
    assert(SemanticParse.alterPos(("will", "MD"))._2 === "MD_WILL")
    assert(SemanticParse.alterPos(("have", "VB"))._2 === "VB_HAVE")
    assert(SemanticParse.alterPos(("does", "VBZ"))._2 === "VBZ_DO")

    val modified = SemanticParse.alterPos(sent1)
    assert(modified(2)._2 === "VBD_BE")
  }

  test("can find all semantic verbs") {
    val sp = new SemanticParse(sent3)(Set(), 6)

    {
      val Some((baseVerb, idx)) = sp.nextBase(sp.wordsWithPos, 0)
      assert(baseVerb === BaseVerb.Be)
      assert(idx === 5)
    }

    {
      val sent = SemanticParse.alterPos(sent3)
      val Some((baseVerb, idx)) = sp.nextBase(sent, 6)
      assert(baseVerb === BaseVerb.Be)
      assert(idx === 8)
    }

    {
      val sent = SemanticParse.alterPos(sent3)
      val verb = sp.rightContext(sent, 5)(6)
      assert(verb.verbType === SemanticVerbType.Base)
      assert(verb.word === "be")
    }

    {
      val verb = sp.nextSemanticFiniteVerb(0)
      assert(verb.isDefined === true)
      assert(sp.wordsWithPos(verb.get.semanticPosition)._1 === "taken")
      assert(verb.get.voice === VerbVoice.Passive)
      assert(verb.get.tense === VerbTense.Future);
      {
        // "most			MANY", "analysis		*WORK_NOUN"
        // "@MANY JJ ^@WORK_NOUN					GENERAL_A"
        val resources = new Resources(new Configuration)
        val agents = new Agents(resources.patterns, resources.lexicon)
        val agent = sp.agentForVerb(verb.get)(agents)
        //println(agents.ends.filter(x => x._2.exists(y => y.label endsWith "GENERAL_A")).map(x => x._1).toList.sortBy(x => x))
        //println(verb)
        //println(agent)
        assert(agent.isDefined === true)
        assert(agent.get.start === 1)
        assert(agent.get.end === 3)
      }
    }
    {
      val resources = new Resources(new Configuration)
      val agents = new Agents(resources.patterns, resources.lexicon)
      val verb = sp.nextSemanticFiniteVerb(6)
      assert(verb.isDefined === true)
      assert(sp.wordsWithPos(verb.get.semanticPosition)._1 === "contains")
      assert(verb.get.voice === VerbVoice.Active)
      assert(verb.get.tense === VerbTense.Present)
      val agent = sp.agentForVerb(verb.get)(agents)
      assert(agent === None)
    }
  }

  test("can find all semantic verbs with agents and actions and negation") {
    val resources = new Resources(new Configuration)
    implicit val agents = new Agents(resources.patterns, resources.lexicon)
    implicit val actions = resources.actions
    implicit val lexicon = resources.lexicon

    val sentences = sent3 :: sent4 :: sent5 :: sent6 :: Nil
    val verbslist = sent3verbs :: sent4verbs :: sent5verbs :: sent6verbs :: Nil
    val actionlist = List() :: sent4actions :: sent5actions :: sent6actions :: Nil
    val agentlist = List() :: sent4agents :: sent5agents :: sent6agents :: Nil
    val neglist = List() :: sent4neg :: sent5neg :: sent6neg :: Nil
    for {
      ((((sent_, verbs_), agents_), actions_), neg_) <- sentences zip verbslist zip agentlist zip actionlist zip neglist;
      sent = SemanticParse.alterPos(sent_)
    } {
      val sp = new SemanticParse(sent)(Set(), 6)
      val verbs = sp.semanticFiniteVerbs
      val ac = verbs map (sp.actionforVerb)
      val ag = verbs map (sp.agentForVerb)
      val neg = verbs map (x => sp.negationPosition(x.finitePosition)(5)(lexicon))
      // DEBUG
      //println("s: " + sent.map(_._1).mkString(" "))
      //verbs zip verbs_ foreach { x =>
      //  val action = sp.actionforVerb(x._1)
      //  val agent = sp.agentForVerb(x._1)
      //  println("sv: " + x._1)
      //  println("ac: " + action)
      //  println("ag: " + agent)
      //  println()
      //}
      verbs zip verbs_ foreach { x =>
        // Make sure words are equal
        if (x._1.semanticPosition > -1)
          assert(sent(x._1.semanticPosition)._1 === x._2._2._1)
        // Make sure extracted semanticfiniteverbs are equal.
        assert(x._1 === x._2._1)
      }
      neg_ zip neg foreach { x =>
        assert(x._1 === x._2.isDefined)
      }
      actions_ zip ac foreach { x =>
        assert(x._1 === x._2)
      }
      agents_ zip ag foreach { x =>
        assert(x._1 === x._2)
      }
    }
  }

  test("can get actions for a sentence via semantic parses") {

  }
}

@RunWith(classOf[JUnitRunner])
class ActionsTests extends FunSuite with ShouldMatchers {
  import SharedData._

  val actionWithException = """formalize		RESEARCH ** formalise formalising formalises formalised **"""

  test("can load actions with exceptions") {
    val actions = new Actions(new ByteArrayInputStream(actionWithException.getBytes("UTF-8")))
    val lexicon = new Lexicon(new ByteArrayInputStream("".getBytes("UTF-8")))
    assert(actions.lookup.size === 1)
    assert(actions.lookup contains ("formalize"))
    assert(actions.lookup("formalize").head.label === "RESEARCH")
    assert(actions.lookup("formalize").head.exceptions.size === 4)
    assert("formalize" === actions.findStem("formalize" -> "VB"))
    assert("formalize" === actions.findStem("formalise" -> "VB"))
    assert("formalize" === actions.findStem("formalising" -> "VBP"))
    assert("formalize" === actions.findStem("formalised" -> "VB"))
  }

  val actionWithoutException = """formalize		RESEARCH"""

  test("can load actions without exceptions") {
    val actions = new Actions(new ByteArrayInputStream(actionWithoutException.getBytes("UTF-8")))
    val lexicon = new Lexicon(new ByteArrayInputStream("".getBytes("UTF-8")))
    assert(actions.lookup.size === 1)
    assert(actions.lookup contains ("formalize"))
    assert(actions.lookup("formalize").head.label === "RESEARCH")
    assert(actions.lookup("formalize").head.exceptions.size === 0)
  }

  test("can get actions") {
    val instream = this.getClass.getResourceAsStream("/actions")
    val actions = new Actions(instream)
    val lexicon = new Lexicon(new ByteArrayInputStream("".getBytes("UTF-8")))
    assert("formalize" === actions.findStem("formalize" -> "VB"))
    assert("formalize" === actions.findStem("formalise" -> "VB"))
    assert("maximize" === actions.findStem("maximise" -> "VB"))
    assert("maximize" === actions.findStem("maximising" -> "VBP"))
    assert("model" === actions.findStem("models" -> "VBZ"))

    // "models"
    val res = actions.matches(sent4, 6)(lexicon)
    assert(res.size === 1)
    assert(res.head.expression.label === "SOLVE")

    // "calculated"
    val res2 = actions.matches(sent4, 32)(lexicon)
    assert(res2.size === 1)
    assert(res2.head.expression.label === "RESEARCH")
  }
}