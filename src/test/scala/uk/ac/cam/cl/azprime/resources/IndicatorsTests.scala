package uk.ac.cam.cl.azprime.resources

import org.scalatest.FunSuite
import org.scalatest.matchers._

import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class IndicatorsTests extends FunSuite with ShouldMatchers {

  test("can parse indicator format") {
    val indicator = Indicators.parseIndicator("1 paper		P	paper		in this paper")
    assert(indicator.trigger === "paper")
    assert(indicator.phrase === "in this paper")
    assert(indicator.score === 1)
    assert(indicator.category === "P")
    assert(indicator.category2 === "paper")
  }

  test("can create indicators from file") {
    val instream = this.getClass.getResourceAsStream("/indicators")
    val ind = new Indicators(instream)
  }

  test("can match indicators") {
    val indicators = Map(
      "we" ->
        Set(Indicators.parseIndicator("1 we		0	outline		we outline")),
      "in this paper" ->
        Set(Indicators.parseIndicator("1 paper		P	paper		in this paper")))

    {
      val matched = Indicators.matchingIndicators("in this paper", indicators)
      assert(matched.size === 1)
      assert(matched(0).trigger === "paper")
      assert(matched(0).phrase === "in this paper")
    }
    {
      val matched = Indicators.matchingIndicators("in this paper we outline our approach.", indicators)
      assert(matched.size === 2)
    }
  }

  test("can match input against loaded file") {
    val instream = this.getClass.getResourceAsStream("/indicators")
    val ind = new Indicators(instream);
    {
      val res = ind.find("outcome of our experiment")
      assert(res.isDefined === true)
      val (a, b) = res.get
      assert(a.score === 2)
      assert(a === b)
    }

    {
      val res = ind.find("in this paper we outline that...")
      assert(res.isDefined === true)
      val (a, b) = res.get
      assert(a.score === 3)
      assert(a === b)
    }
  }
}