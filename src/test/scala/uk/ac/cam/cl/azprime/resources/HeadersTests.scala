package uk.ac.cam.cl.azprime.resources

import org.scalatest.FunSuite
import org.scalatest.matchers._
import java.io.ByteArrayInputStream

class HeadersTests extends FunSuite with ShouldMatchers {

  val headers = """|
    |AREA beginning introduction background motivation problem_stat
    |AREA end conclusion discussion
    |
    |introduction  introduction 
    |
    |problem_stat problem statement
    |problem_stat goal of this
    |
    |related other work
    |related past work""".stripMargin

  test("headers can be parsed") {
    val hdr = new Headers(new ByteArrayInputStream(headers.getBytes()))
    expect("introduction") {
      hdr("introduction")
    }
    expect("related") {
      hdr("past work")
    }
    expect("problem_stat") {
      hdr("goal of this")
    }
  }
}