package uk.ac.cam.cl.azprime.resources

import org.scalatest.FunSuite
import org.scalatest.matchers._
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import java.io.ByteArrayInputStream
import uk.ac.cam.cl.azprime.resources.FormulaicExpression.POS;
import uk.ac.cam.cl.azprime.resources.FormulaicExpression.Variable;
import uk.ac.cam.cl.azprime.resources.FormulaicExpression.Word;

@RunWith(classOf[JUnitRunner])
class PatternsTests extends FunSuite with ShouldMatchers {

  val sent1 = List(
    "the" -> "DT",
    "problem" -> "NN",
    "of" -> "IN",
    "how" -> "WRB",
    "to" -> "TO",
    "eat." -> "VB")

  val patterns1 = List(
    "@SELF_POSS ^@WORK_NOUN					US_A",
    "the ^@SOLUTION_NOUN @GIVEN in this @PRESENTATION_NOUN	US_A",
    "as ^@GIVEN earlier in this @TXT_NOUN		NO_TXTSTRUCT_F",
    "a ^similar NN to CITE				SIMILARITY_F",
    "@SELF_POSS JJ ^@WORK_NOUN				US_A")

  val lexicon1 = List(
    "account			WORK_NOUN",
    "algorithm		WORK_NOUN",
    "answer			SOLUTION_NOUN",
    "noted			GIVEN",
    "paragraph		TXT_NOUN",
    "my		SELF_POSS")

  test("can create formulaic expressions") {
    val expressions = patterns1.map(_.trim).map(x => {
      val Patterns.PatternLinePattern(pattern, category) = x
      Pattern(pattern, category)
    })
    assert(expressions.length === 5)
    assert(expressions(0).tokens(0).getClass === classOf[FormulaicExpression.Variable])
    assert(expressions(2).tokens(0).getClass === classOf[FormulaicExpression.Word])
    assert(expressions(4).tokens(1).getClass === classOf[FormulaicExpression.POS])
    assert(expressions(2).tokens(0).isTrigger === false)
    assert(expressions(2).tokens(1).isTrigger === true)
  }

  test("can create patterns from file") {
    val instream = this.getClass.getResourceAsStream("/patterns")
    val patterns = new Patterns(instream)
    val lexicon = new Lexicon(new ByteArrayInputStream("".getBytes("UTF-8")))

    val matched1 = patterns.matches(sent1)(lexicon)
    assert(matched1.length === 1)
    assert(matched1(0).start === 0) // start
    assert(matched1(0).end === 5) // end
    FormulaicExpression.matches(
      matched1(0).expression,
      sent1,
      matched1(0).expression.tokens.length,
      matchDir = FormulaicExpression.MatchDirection.Backward)(lexicon)
  }

  test("can get at agents") {
    val pdata = patterns1.mkString("\n")
    val ldata = lexicon1.mkString("\n")
    val pstream = new ByteArrayInputStream(pdata.getBytes("UTF-8"))
    val lstream = new ByteArrayInputStream(ldata.getBytes("UTF-8"))
    val patterns = new Patterns(pstream)
    val lexicon = new Lexicon(lstream)
    val agents = new Agents(patterns, lexicon)
    assert(agents.agentExpressions.size === 3)
    assert(agents.patternStartsWithWord("my") === true)
    assert(agents.patternStartsWithWord("the") === true)
    assert(agents.patternEndsWithWord("algorithm") === true)
  }
}