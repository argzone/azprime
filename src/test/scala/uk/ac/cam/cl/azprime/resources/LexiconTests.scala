package uk.ac.cam.cl.azprime.resources

import org.scalatest.FunSuite
import org.scalatest.matchers._

class LexiconTests extends FunSuite with ShouldMatchers {

  val nounTests = Map(
    "shoe" -> List("shoe", "shoes"),
    "valley" -> List("valley", "valleys"),
    "theatre" -> List("theatre", "theatres"),
    "patty" -> List("patty", "patties"),
    "dress" -> List("dress", "dresses"))

  val adjectiveTests = Map(
    "pretty" -> List("pretty", "prettily", "prettier", "prettiest"),
    "considerable" -> List("considerable", "considerably", "considerabler", "considerablest"),
    "alcoholic" -> List("alcoholic", "alcoholically"),
    "extreme" -> List("extreme", "extremely", "extremly", "extremer", "extremest"))

  test("noun morphology generation") {
    for ((noun, result) <- nounTests)
      expect(result) {
        Lexicon.getNounMorphology(noun)
      }
  }

  test("adjective morphology generation") {
    for ((adj, result) <- adjectiveTests)
      expect(result) {
        Lexicon.getAdjectiveMorphology(adj)
      }
  }

  // XXX: Pretty pathetic test, really...
  test("can create lexicon from file") {
    val instream = this.getClass.getResourceAsStream("/lexicon")
    val lex = new Lexicon(instream)
    (lex.byindicator.isEmpty) should not be (true)
    (lex.byclass.isEmpty) should not be (true)
    (lex.lexicon.isEmpty) should not be (true)
    assert(lex.byindicator.contains("folklore") === true)
    assert(lex.byclass.contains("TRADITION_NOUN") === true)
  }
}